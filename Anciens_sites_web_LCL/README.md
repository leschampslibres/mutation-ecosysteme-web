# Projet de mutation de l'écosystème web des Champs Libres

Parmi l'écosystème, le site web occupe une place centrale. Vous trouverez ici une compilation de captures d'écran de la page d'accueil des anciens sites web depuis 2006 (source : Internet Archive) :<br /><br />
![Wayback Machine Les Champs Libres](https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/Anciens_sites_web_LCL/wayback-toolbar-logo-150.png)<br />
→ [https://web.archive.org/web/*/https://www.leschampslibres.fr](https://web.archive.org/web/*/https://www.leschampslibres.fr)

**Chronologie** du site web https://www.leschampslibres.fr :
- 2006 : naissance des Champs Libres et premier site web
- 2010 : refonte web sous Wordpress (SARL In Cité Solution)
- 2017 : migration sous Typo3 (Agence W-Seils)
- 2020 : lancement du projet de mutation de l'écosystème web

# Informations sur l'interface d'affichage (Front-End)

## — GÉNÉRAL

Il est nécessaire d'utiliser `browsersync` (sans https), via la commande suivante, pour lancer le projet :

```bash
make start-dev-env
```

## — TWIG

### Architecture

Dans le dossier `templates/bundles/SyliusShopBundle` vous retrouverez les dossiers suivants :

* `_blocks` : Dossier contenant les "blocs" de contenus. De manière générale ce sont les éléments qui représentent des sections graphiques de pages.
* `_components` : Dossier contenant les composants d'interfaces. Ce sont des partiels qui ont pour but d'être "agnostiques", c'est-à-dire de ne dépendre d'aucun modèle de données, afin de pouvoir les utilisés dans tout type de contexte. Ainsi, ils ne sont pas censés comporter autre chose que des variables qui leurs sont transmises en paramètre.
**Ils ne sont pas censé être modifiés par d'autres personnes que les Front-end.**
* `_layouts` : Dossier contenant des partiels qui sont utilisés, soit pour des éléments transversaux (header, footer…), soit des meta informations.
* `_form` : Dossier contenant les surcharges des thèmes de formulaires.
* `Prototype` : Dossier contenant des templates purement statiques.
* `…` : Un ensemble de dossiers pouvant suivre la structure de typologies de contenus.

### Surcharge de template

Pour surcharger les templates par défaut de Sylius, il est possible de recréer le fichier visé depuis `vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Resources/views/` dans le dossier du theme du projet `templates/bundles/SyliusShopBundle`.

## — ASSETS

Dans le dossier `assets/shop`, on retrouve l'intégralité des ressources statiques (scss, js, images, fonts) utilisées au sein du projet.

Pour ajouter une ressource statique à la _pipeline_ de Sylius, il faut la renseigner dans le fichier `assets/shop/entry.js`.

**Au même titre que les fichiers Twig situés dans les dossiers `_components`, ces fichiers ne sont pas censés être modifiés par d'autres personnes que les développeurs Front-end.**

### Sprite SVG

Pour ajouter des éléments au sprite SVG :

1. Ajouter le nouveau picto dans le dossier `sources`
2. Lancer la commande `npm run svgsprite`
3. Lancer la commande `npm run build`

## — AUTRES

### Filtres et fonctions Twig

Afin d'éviter un certains nombre de répétitions au sein des templates, des fonctions et des filtres Twig ont été implémentés et sont disponibles dans les fichiers présents dans le dossier `src/Twig/Ui/ShopTwigExtension.php`.

En voici une liste non exhaustive :

* `html_safe` (filtre) : Filtre permettant d'échapper du HTML en amont du rendu de la variable.
* `deep_merge` (fonction) : Utilisée en générale pour fusionner, deux tableaux associatifs multidimmensionnels, elle permet aussi de concatener les valeurs des clés ciblées (par défaut, la clé `class` est concaténée plutôt que suchargée).
* `html_attributes` (fonction) : Transforme un tableau en attribut HTML.
* `image_attributes` (fonction) : Récupère les valeurs `src`, `srcset` et `sizes` correspondantes au contexte spécifié en paramètre, nécessaire pour générer les différents format d'images (voir la partie [Gestion des images](#gestion-des-formats-dimages)).
* `ui` (fonction) : Simplifie l'appel aux composants en ajoutant le chemin complet vers les composants. Exemple : `ui('button')` équivaut à `'@SyliusShop/_components/button.html.twig'`.

### Gestion des formats d'images

La gestion des formats d'image se configure via [LiipThemeBundle](https://github.com/liip/LiipThemeBundle) dans le fichier `config/packages/liip_imagine.yaml`. Chaque format doit y être référencé.

Étant donné qu'une gestion responsive implique de spécifier plusieurs formats par image, il est devenu assez laborieux de les indiquer systématiquement dans chaque déclaration.

Ainsi, pour faciliter le développement, une fonction `image_attributes` a été mise en place dans le fichier `src/Twig/Ui/ShopTwigExtension.php`.

Via un identifiant, elle permet de récupérer les informations des différents formats d'image et comportement de l'image en responsive à transmettre à la fonction `image_attributes()`.

**Exemple :**

La déclaration :

```twig
{% set attributes = image_attributes(image, 'figure', { loading: "lazy" }) %}
```

Va retourner un tableau comportant :

* en `src` : le format d'image par défaut
* en `srcset` : la liste des formats suivi de la largeur corresponsante, séparés par des virgule
* en `sizes` : la description du comportement de l'image en fonction des différentes résolutions.
* et les attributs spécifié en dernier paramètres.

```php
[
  'src' => 'figure-regular',
  'srcset' => 'figure-small 500w, figure-regular 1000w, figure-medium 1500w, figure-large 2000w',
  'sizes' => '(max-width: 1048px) calc(100vw - 48px), 1000px'
  'loading' => 'lazy'
];
```

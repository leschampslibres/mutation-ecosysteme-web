<?php

declare(strict_types=1);

namespace App\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

final class AccessibilityFilter implements FilterInterface
{
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (empty($data)) {
            return;
        }

        // Les champs accessibilité de l'entité, renseignés dans la configuration de la grille
        $fields = $options['fields'];

        // Si le type d'accessibilité recherché est dans les champs accessibilité de l'entité,
        // on restreint la recherche à ce type d'accessibilité
        if (\in_array($data, $fields, true)) {
            $expressionBuilder = $dataSource->getExpressionBuilder();
            $dataSource->restrict($expressionBuilder->equals('event.' . $data, true));
        } else {
            return;
        }
    }
}

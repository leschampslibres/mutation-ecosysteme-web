<?php

declare(strict_types=1);

namespace App\Twig\Ui;

use App\Entity\Event\Event;
use App\Entity\Event\EventOccurrence;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ShopTwigExtension extends AbstractExtension
{
    protected $projectDir;

    public function __construct(
        KernelInterface $kernel,
        private readonly CacheManager $imagineCacheManager,
        private ChannelContextInterface $channelContext,
    ) {
        $this->projectDir = $kernel->getProjectDir();
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('svg_tag', [$this, 'svgTag'], ['is_safe' => ['html']]),
            new TwigFunction('image_attributes', [$this, 'imageAttributes'], ['is_safe' => ['html']]),
            new TwigFunction('get_faq_category_icon', [$this, 'getFaqCategoryIcon']),
            new TwigFunction('get_transverse_icon', [$this, 'getTransverseIcon']),
            new TwigFunction('get_taxon_icon', [$this, 'getTaxonIcon']),
            new TwigFunction('get_event_image', [$this, 'getEventImage']),
            new TwigFunction('get_event_occurrence_date', [$this, 'getEventOccurrenceDate']),
            new TwigFunction('get_event_date', [$this, 'getEventDate']),
            new TwigFunction('get_occurrence_date', [$this, 'getOccurrenceDate']),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('first_navigation_level', [$this, 'firstNavigationLevel'], ['is_safe' => ['html']]),
            new TwigFilter('formatted_date_time', [$this, 'displayFormattedDateTime']),
            new TwigFilter('formatted_date', [$this, 'displayFormattedDate']),
            new TwigFilter('formatted_hours', [$this, 'displayFormattedHours']),
        ];
    }

    /**
     * Generate a svg tag with use or markup from a file.
     */
    public static function svgTag(string $path, array $options = []): string
    {
        $html = [];

        if (isset($options['title']) && $options['title']) {
            $html[] = '<title>' . htmlspecialchars($options['title']) . '</title>';
            unset($options['title']);
        }

        if (isset($options['inline'])) {
            unset($options['inline']);

            $path = $_SERVER['DOCUMENT_ROOT'] . $path;

            if (!file_exists($path)) {
                trigger_error("Impossible de trouver l'asset \"$path\".", \E_USER_ERROR);
            }
            $svg = file_get_contents($path);
            $xml = simplexml_load_string($svg);

            $xml_attributes = $xml->attributes();
            foreach ($xml_attributes as $name => $value) {
                if (isset($options[$name])) {
                    continue;
                }
                $options[$name] = (string) $value;
            }

            $xml_children = $xml->children();
            foreach ($xml_children as $child) {
                $html[] = $child->asXML();
            }
        } else {
            $path = explode('#', $path);
            $anchor = \count($path) > 1 ? '#' . $path[1] : null;
            $html[] = '<use href="' . $path[0] . $anchor . '" />';
        }

        $attrs = [];
        foreach ($options as $key => $value) {
            $attrs[] = $key . '="' . $value . '"';
        }
        $attrs = implode(' ', $attrs);

        return '<svg' . ($attrs ? ' ' . $attrs : '') . '>' . implode('', $html) . '</svg>';
    }

    public function getImageData($format): array
    {
        switch ($format) {
            case 'handy':
                return [
                    'sizes' => '(max-width: 479px) calc(100vw - 48px), ((min-width: 480px) and (max-width: 719px)) calc(100vw - 120px), ((min-width: 720px) and (max-width: 848px)) calc(100vw - 144px), 704px',
                    'width' => 704,
                    'height' => null,
                    'formats' => [
                        'handy_small' => '327w',
                        'handy_regular' => '432w',
                        'handy_medium' => '600w',
                        'handy_large' => '704w',
                    ],
                ];

            case 'video':
                return [
                    'sizes' => '(max-width: 848px) calc(100vw - 48px), 800px',
                    'width' => 800,
                    'height' => 450,
                    'formats' => [
                        'video_small' => '327w',
                        'video_regular' => '432w',
                        'video_medium' => '672w',
                        'video_large' => '800w',
                    ],
                ];

            case 'banner_default':
                return [
                    'sizes' => '(max-width: 728px) calc(100vw - 48px), 680px',
                    'width' => 680,
                    'height' => 324,
                    'formats' => [
                        'banner_default_small' => '327w',
                        'banner_default_regular' => '432w',
                        'banner_default_large' => '680w',
                    ],
                ];

            case 'banner_large':
                return [
                    'sizes' => '(max-width: 488px) calc(100vw - 48px), 440px',
                    'width' => 440,
                    'height' => 214,
                    'formats' => [
                        'banner_large_small' => '327w',
                        'banner_large_regular' => '440w',
                    ],
                ];

            case 'banner_event':
                return [
                    'sizes' => '(max-width: 584px) calc(100vw - 24px), 560px',
                    'width' => 560,
                    'height' => 378,
                    'formats' => [
                        'banner_event_small' => '351w',
                        'banner_event_regular' => '428w',
                        'banner_event_large' => '560w',
                    ],
                ];

            case 'banner_post':
                return [
                    'sizes' => '(max-width: 728px) calc(100vw - 48px), 680px',
                    'width' => 680,
                    'height' => 453,
                    'formats' => [
                        'banner_post_small' => '327w',
                        'banner_post_regular' => '432w',
                        'banner_post_large' => '680w',
                    ],
                ];

            case 'banner_series':
                return [
                    'sizes' => '(max-width: 464px) calc(100vw - 24px), 440px',
                    'width' => 440,
                    'height' => 440,
                    'formats' => [
                        'banner_series_small' => '351w',
                        'banner_series_regular' => '428w',
                        'banner_series_large' => '440w',
                    ],
                ];

            case 'image_medium':
                return [
                    'sizes' => '(max-width: 848px) calc(100vw - 48px), 800px',
                    'width' => 800,
                    'height' => null,
                    'formats' => [
                        'image_medium_small' => '327w',
                        'image_medium_regular' => '432w',
                        'image_medium_medium' => '672w',
                        'image_medium_large' => '800w',
                    ],
                ];

            case 'image_large':
                return [
                    'sizes' => '(max-width: 1208px) calc(100vw - 48px), 1160px',
                    'width' => 1160,
                    'height' => null,
                    'formats' => [
                        'image_large_small' => '327w',
                        'image_large_regular' => '432w',
                        'image_large_medium' => '672w',
                        'image_large_large' => '912w',
                        'image_large_xlarge' => '1160w',
                    ],
                ];

            case 'gallery':
                return [
                    'sizes' => '(max-width: 719px) calc(100vw - 48px), ((min-width: 720px) and (max-width: 1208px)) calc((100vw - 88px) / 2), 560px',
                    'width' => 560,
                    'height' => null,
                    'formats' => [
                        'gallery_small' => '327w',
                        'gallery_regular' => '672w',
                        'gallery_medium' => '436w',
                        'gallery_large' => '560w',
                    ],
                ];

            case 'illustrated_aside':
                return [
                    'sizes' => '(max-width: 848px) calc(100vw - 48px), ((min-width: 849px) and (max-width: 1208px)) 800px, 560px',
                    'width' => 800,
                    'height' => null,
                    'formats' => [
                        'illustrated_aside_small' => '327w',
                        'illustrated_aside_regular' => '432w',
                        'illustrated_aside_medium' => '672w',
                        'illustrated_aside_large' => '800w',
                        'illustrated_aside_xlarge' => '560w',
                    ],
                ];

            case 'illustrated_full':
                return [
                    'sizes' => '(max-width: 848px) calc(100vw - 48px), 800px',
                    'width' => 800,
                    'height' => null,
                    'formats' => [
                        'illustrated_full_small' => '327w',
                        'illustrated_full_regular' => '432w',
                        'illustrated_full_medium' => '672w',
                        'illustrated_full_large' => '800w',
                    ],
                ];

            case 'logos_big':
                return [
                    'sizes' => '300px',
                    'width' => 300,
                    'height' => null,
                    'formats' => [
                        'logos_big_regular' => '300w',
                        'logos_big_large' => '600w',
                    ],
                ];

            case 'logos_small':
                return [
                    'sizes' => '163px',
                    'width' => 163,
                    'height' => null,
                    'formats' => [
                        'logos_small_regular' => '163w',
                        'logos_small_large' => '326w',
                    ],
                ];

            case 'figure':
                return [
                    'sizes' => '(max-width: 1059px) calc(100vw - 48px), 996px',
                    'width' => 996,
                    'height' => null,
                    'formats' => [
                        'figure_small' => '498w',
                        'figure_regular' => '996w',
                        'figure_medium' => '1494w',
                        'figure_large' => '1992w',
                    ],
                ];

            default:
                return [
                    'sizes' => '(max-width: 1059px) calc(100vw - 48px), 996px',
                    'width' => 996,
                    'height' => null,
                    'formats' => [
                        'figure_small' => '498w',
                        'figure_regular' => '996w',
                        'figure_medium' => '1494w',
                        'figure_large' => '1992w',
                    ],
                ];
        }
    }

    public function imageAttributes($path, $name, $additionals = [])
    {
        $data = $this->getImageData($name);
        $imageSizes = $this->imageSize($path, $data['formats']);
        $originalWidth = null;
        $originalHeight = null;
        $originalRatio = null;
        $formatWidth = $data['width'] ?: null;
        $formatHeight = $data['height'] ?: null;
        $formatRatio = $formatWidth && $formatHeight ? $formatHeight / $formatWidth : null;
        $finalWidth = null;
        $finalHeight = null;
        $attributes = $additionals;
        $srcsets = [];

        if ($imageSizes) {
            $originalWidth = $imageSizes[0];
            $originalHeight = $imageSizes[1];
            $originalRatio = $originalHeight / $originalWidth;

            if (\array_key_exists('crop', $data) && $data['crop']) {
                if ($formatWidth && $formatHeight) {
                    $finalWidth = $formatWidth;
                    $finalHeight = $formatHeight;
                }
            } else {
                if ($formatWidth && $formatHeight) {
                    if ($formatRatio < $originalRatio) {
                        $finalHeight = $formatHeight;
                        $finalWidth = floor($formatHeight / $originalRatio);
                    } elseif ($formatRatio > $originalRatio) {
                        $finalWidth = $formatWidth;
                        $finalHeight = floor($formatWidth * $originalRatio);
                    }
                } elseif ($formatWidth) {
                    $finalWidth = $formatWidth;
                    $finalHeight = floor($formatWidth * $originalRatio);
                } elseif ($formatHeight) {
                    $finalHeight = $formatHeight;
                    $finalWidth = floor($formatHeight / $originalRatio);
                }
            }
        }

        foreach ($data['formats'] as $format => $descriptor) {
            if (preg_match('/^([0-9]+)w$/', $descriptor)) {
                $maxWidthReached = false;
                $w = (int) (substr($descriptor, 0, -1));

                if (null === $originalWidth || $originalWidth > $w) {
                    $srcsets[] = $this->imagineCacheManager->getBrowserPath($path, $format) . ' ' . $descriptor;
                } else {
                    $maxWidthReached = true;
                }
            }
        }

        if ($maxWidthReached) {
            $srcsets[] = $this->imagineCacheManager->getBrowserPath($path, array_key_last($data['formats'])) . ' ' . $originalWidth . 'w';
        }

        $attributes['src'] = $this->imagineCacheManager->getBrowserPath($path, str_replace('-', '_', $name) . '_regular');
        $attributes['srcset'] = implode(', ', $srcsets);
        $attributes['sizes'] = $data['sizes'];
        $attributes['width'] = $finalWidth;
        $attributes['height'] = $finalHeight;

        return $attributes;
    }

    public function imageSize($imagePath, $formats = null)
    {
        $paths = [
            ($this->projectDir . '/public'),
            ($this->projectDir . '/public/media/gallery/'),
            ($this->projectDir . '/public/media/image/'),
            ($this->projectDir . '/public/media/rich-editor/'),
        ];

        foreach ($formats as $format => $descriptor) {
            $paths[] = ($this->projectDir . '/public/media/cache/' . $format . '/');
        }

        foreach ($paths as &$path) {
            if (file_exists($path . $imagePath)) {
                return getimagesize($path . $imagePath);
            }
        }

        return null;
    }

    public function displayFormattedDateTime(\DateTime|string $date): string
    {
        if (\is_string($date)) {
            $date = new \DateTime($date);
        }

        $minutes = $date->format('i');

        return $date->format('d/m/y à H') . 'h' . ('00' != $minutes ? $minutes : '');
    }

    public function displayFormattedDate(\DateTime|string $date): string
    {
        if (\is_string($date)) {
            $date = new \DateTime($date);
        }

        return $date->format('d/m/y');
    }

    public function getEventImage(Event $event): array|bool
    {
        return $event->getCategory()->getDisplayImages() ? ($event->getMainImage() ?: false) : false;
    }

    public function getEventOccurrenceDate(EventOccurrence $occurrence): string
    {
        if ($occurrence->getEvent()->getCategory()->getDisplayOccurrences()) {
            return $this->displayFormattedDateTime($occurrence->getStartingAt());
        }

        return 'Du ' . $this->displayFormattedDate($occurrence->getEvent()->getStartingAt()) . ' au ' . $this->displayFormattedDate($occurrence->getEvent()->getEndingAt());
    }

    public function getEventDate(Event $event): string
    {
        $startingAt = $this->displayFormattedDate($event->getStartingAt());
        $endingAt = $this->displayFormattedDate($event->getEndingAt());
        // Si l'événement n'est que sur une journée
        if ($startingAt == $endingAt) {
            // Mais qu'il a plusieurs occurrences : on affiche uniquement la date du jour
            if (\count($event->getOccurrences()) > 1) {
                return 'Le ' . $this->displayFormattedDate($event->getStartingAt());
                // Sinon, on affiche la date et l'heure
            }

            return 'Le ' . $this->displayFormattedDateTime($event->getStartingAt());
        }

        return 'Du ' . $startingAt . ' au ' . $endingAt;
    }

    public function getOccurrenceDate(\DateTime $start, \DateTime $end): string
    {
        $startingAt = $this->displayFormattedDate($start);

        $endingAt = $this->displayFormattedDate($end);
        if ($startingAt == $endingAt) {
            return 'le ' . $this->displayFormattedDateTime($start);
        }

        return 'du ' . $startingAt . ' au ' . $endingAt;
    }

    public function displayFormattedHours(string $hour): string
    {
        $hoursInArray = explode(':', $hour);
        $hours = $hoursInArray[0];
        $minutes = $hoursInArray[1];
        if ('0' == $hours) {
            $string = $minutes . ' min';
        } else {
            $string = $hours . 'h' . ('00' != $minutes ? $minutes : '');
        }

        return $string;
    }
}

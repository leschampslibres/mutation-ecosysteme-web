<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Entity\AlertMessage\AlertMessage;
use App\Repository\AlertMessage\AlertMessageRepositoryInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;
use Webmozart\Assert\Assert;

final class AlertMessageTwigExtension extends AbstractExtension implements ExtensionInterface
{
    private AlertMessageRepositoryInterface $alertMessageRepository;

    private LocaleContextInterface $localeContext;

    /**
     * MenuExtension constructor.
     */
    public function __construct(
        RepositoryInterface $alertMessageRepository,
        LocaleContextInterface $localeContext
    ) {
        Assert::isInstanceOf($alertMessageRepository, AlertMessageRepositoryInterface::class);
        $this->alertMessageRepository = $alertMessageRepository;
        $this->localeContext = $localeContext;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('alert_message', [$this, 'getAlertMessage']),
        ];
    }

    public function getAlertMessage(): ?AlertMessage
    {
        return $this->alertMessageRepository->findOnePublished($this->localeContext->getLocaleCode());
    }
}

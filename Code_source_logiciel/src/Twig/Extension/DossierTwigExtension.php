<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Dossier\DossierRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class DossierTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(protected DossierRepositoryInterface $dossierRepository, protected UnderPagesService $underPagesService, protected RouterInterface $router)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_dossier_by_id', [$this->dossierRepository, 'find']),
        ];
    }
}

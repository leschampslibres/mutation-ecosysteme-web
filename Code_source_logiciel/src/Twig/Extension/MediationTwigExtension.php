<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Mediation\MediationRepositoryInterface;
use App\Repository\Mediation\MediationSubjectRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class MediationTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(
        protected MediationRepositoryInterface $mediationRepository,
        protected MediationSubjectRepositoryInterface $mediationSubjectRepository,
        protected UnderPagesService $underPagesService,
        protected RouterInterface $router
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_mediation_by_id', [$this->mediationRepository, 'find']),
            new TwigFunction('get_subject_by_id', [$this->mediationSubjectRepository, 'find']),
        ];
    }
}

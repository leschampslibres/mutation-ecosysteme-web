<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Entity\Event\EventCategory;
use Doctrine\ORM\EntityManagerInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;

class MeetingsTwigExtension extends AbstractExtension
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected UnderPagesService $underPagesService,
        protected UrlGeneratorInterface $router
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new \Twig\TwigFunction('get_meetings_block_categories', [$this, 'getMeetingsBlockCategories']),
        ];
    }

    public function getMeetingsBlockCategories(): array
    {
        $categories = $this->em->getRepository(EventCategory::class)->findForMeetingsBlock();

        $categoriesArray = [];
        foreach ($categories as $category) {
            $categoriesArray[] = [
                'icon' => $category->getIcon(),
                'title' => $category->getLabelPlural(),
                'description' => $category->getDescription(),
                'link' => [
                    'label' => 'Accéder à la catégorie',
                    'href' => $this->router->generate('app_event_category_show', ['slug' => $category->getSlug(), 'page_slug' => $this->underPagesService->underPageSlugByCode('events')]),
                ],
            ];
        }

        return $categoriesArray;
    }
}

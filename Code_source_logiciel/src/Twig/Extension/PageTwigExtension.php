<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Page\PageRepositoryDecorator;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class PageTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(
        private PageRepositoryDecorator $pageRepositoryDecorator
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('find_one_by_code_and_professional', [$this->pageRepositoryDecorator, 'findOneByCodeAndProfessional']),
        ];
    }
}

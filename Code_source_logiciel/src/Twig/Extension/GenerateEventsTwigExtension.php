<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Entity\Event\Event;
use App\Entity\Event\EventCategory;
use App\Entity\Event\EventKeyword;
use App\Entity\Event\EventOccurrence;
use Doctrine\ORM\EntityManagerInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;
use function Symfony\Component\Clock\now;

class GenerateEventsTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected UnderPagesService $underPagesService,
        protected UrlGeneratorInterface $router
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_event_by_id', [$this, 'getEventById']),
            new TwigFunction('get_category_by_id', [$this, 'getCategoryById']),
            new TwigFunction('get_category_ids_from_block', [$this, 'getCategoryIdsFromBlock']),
            new TwigFunction('get_category_labels_by_ids', [$this, 'getCategoryLabelsByIds']),
            new TwigFunction('get_keyword_by_id', [$this, 'getKeywordById']),
            new TwigFunction('get_event_by_id_and_date', [$this, 'getEventByIdAndDate']),
            new TwigFunction('get_automatic_events', [$this, 'getAutomaticEvents']),
        ];
    }

    public function getEventById(int $id): ?Event
    {
        return $this->em->getRepository(Event::class)->find($id);
    }

    public function getCategoryById(int $id): string
    {
        $category = $this->em->getRepository(EventCategory::class)->find($id);

        return $category ? $category->getLabelPlural() : '';
    }

    public function getCategoryIdsFromBlock(array $categories): array
    {
        $ids = [];
        foreach ($categories as $key => $value) {
            if ($value) {
                $ids[] = $key;
            }
        }

        return $ids;
    }

    public function getCategoryLabelsByIds(array $ids): string
    {
        $categories = $this->em->getRepository(EventCategory::class)->findBy(['id' => $ids]);

        $categoryLabels = [];
        foreach ($categories as $category) {
            $categoryLabels[] = $category->getLabelPlural();
        }

        return implode(', ', $categoryLabels);
    }

    public function getKeywordById(int $id): string
    {
        $keyword = $this->em->getRepository(EventKeyword::class)->find($id);

        return $keyword->getTitle();
    }

    public function getEventByIdAndDate(int $id, \DateTime|string $from, \DateTime|string $to): array
    {
        if (\is_string($from)) {
            $from = new \DateTime($from);
        }
        if (\is_string($to)) {
            $to = new \DateTime($to);
        }
        $today = new \DateTime();

        $event = $this->em->getRepository(Event::class)->find($id);

        $eventOrOccurrence = [];

        if (null === $event) {
            return $eventOrOccurrence;
            // Événement terminé
        }
        if ($event->getStatus() >= 20 || $event->getEndingAt() < $today) {
            $eventOrOccurrence = [
                'picture' => $event->getMainImage(),
                'tag' => $event->getCategory() ? $event->getCategory()->getLabel() : '',
                'title' => $event->getTitle(),
                'terminated' => true,
                'color' => $event->getColor() ? $event->getColor()->value : null,
            ];
            if ($event->getStatus() <= Event::STATUS_COMPLETED) {
                $eventOrOccurrence['href'] = $this->router->generate('app_event_show', ['page_slug' => $this->underPagesService->underPageSlugByCode('events'), 'slug' => $event->getSlug()]);
            }

        // Événement en cours
        } else {
            // Si on est avant la période sélectionnée
            if ($today < $from) {
                // On affiche, dans le cas d'une occurrence, la première occurrence depuis la date de début de la mise en avant
                $eventOrOccurrence = $this->getEventOrOccurrence($event, $from);

            // Si on est pendant ou après la période sélectionnée
            } else {
                // On récupère, dans le cas d'une occurrence, la prochaine occurrence depuis la date du jour
                $eventOrOccurrence = $this->getEventOrOccurrence($event, $today);

                // Si on est après la période sélectionnée
                if ($to < $today) {
                    // On affiche les dates de début et de fin de l'événement, et pas la date de l'occurrence
                    $eventOrOccurrence['dateStarting'] = $event->getStartingAt();
                    $eventOrOccurrence['dateEnding'] = $event->getEndingAt();
                }
            }
        }

        return $eventOrOccurrence;
    }

    public function getAutomaticEvents(array $filters): array
    {
        $categories = $filters['categories'] ?? null;
        $originAgenda = $filters['originAgenda'] ?? null;
        $audience = $filters['audience'] ?? null;
        $keyword = $filters['keyword'] ?? null;
        $limit = $filters['limit'] ?? 3;
        $from = $filters['from'] ?? null;
        $to = $filters['to'] ?? null;
        $highlithedEventId = (int) $filters['event'] ?? null;

        $from = $from < now() ? date('d/m/y') : $from;
        $from = $from ? \DateTime::createFromFormat('d/m/y H:i:s', $from . ' 00:00:00') : null;
        $to = $to ? \DateTime::createFromFormat('d/m/y H:i:s', $to . ' 00:00:00') : null;

        $categories = \is_array($categories) ? $categories : [$categories];

        // Si on met une limite lors de la requête, cela pose un problème lorsque les événements ont plusieurs occurrences sur la plage de dates demandée.
        $eventsList = $this->em->getRepository(Event::class)->findEventsInAutomaticBlock($categories, $originAgenda, $audience, $keyword, $from, $to, $highlithedEventId);

        $finalEvents = [];

        if (null != $eventsList) {
            foreach ($eventsList as $event) {
                $event = $this->getEventOrOccurrence($event, $from);
                if (null != $event) {
                    $finalEvents[] = $event;
                }
            }

            usort($finalEvents, function ($a, $b) {
                return $a['dateStarting'] <=> $b['dateStarting'];
            });

            // C'est ici que l'on limite le nombre d'événements à afficher.
            $limit = null != $highlithedEventId ? $limit - 1 : $limit;
            $finalEvents = \array_slice($finalEvents, 0, $limit);
        }

        if (null != $highlithedEventId) {
            $highlithedEvent = $this->em->getRepository(Event::class)->findHighlightedInAutomaticBlock($highlithedEventId, $from, $to);
            if (null != $highlithedEvent) {
                $highlithedEvent = $this->getEventOrOccurrence($highlithedEvent, $from);
                if (null != $highlithedEvent) {
                    array_unshift($finalEvents, $highlithedEvent);
                }
            }
        }

        return $finalEvents;
    }

    private function getEventOrOccurrence(Event $event, ?\DateTime $date = null): ?array
    {
        $events = [];
        if ($event->getCategory()->getDisplayOccurrences()) {
            $occurrence = $this->em->getRepository(EventOccurrence::class)->findNext($event, $date);
            if (null != $occurrence) {
                $events = [
                    'picture' => $event->getMainImage(),
                    'tag' => $event->getCategory()->getLabel(),
                    'title' => $event->getTitle(),
                    'dateStarting' => $occurrence->getStartingAt(),
                    'location' => $event->getLocation(),
                    'pricing' => $event->getPricing(),
                    'status' => $event->getStatus(),
                    'href' => $this->router->generate('app_event_show', ['slug' => $event->getSlug(), 'occurrence_id' => $occurrence->getid(), 'page_slug' => $this->underPagesService->underPageSlugByCode('events')]),
                    'color' => $event->getColor(),
                ];
            }
        } else {
            $events = [
                'picture' => $event->getMainImage(),
                'tag' => $event->getCategory()->getLabel(),
                'title' => $event->getTitle(),
                'dateStarting' => $event->getStartingAt(),
                'dateEnding' => $event->getEndingAt(),
                'location' => $event->getLocation(),
                'pricing' => $event->getPricing(),
                'status' => $event->getStatus(),
                'href' => $this->router->generate('app_event_show', ['slug' => $event->getSlug(), 'page_slug' => $this->underPagesService->underPageSlugByCode('events')]),
                'color' => $event->getColor(),
            ];
        }

        if (!empty($events)) {
            return $events;
        }

        return null;
    }
}

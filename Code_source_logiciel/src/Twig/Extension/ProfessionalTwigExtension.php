<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Professional\ProfessionalRepositoryInterface;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class ProfessionalTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(protected ProfessionalRepositoryInterface $professionalRepository, protected UnderPagesService $underPagesService, protected RouterInterface $router)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_professional_by_id', [$this->professionalRepository, 'find']),
            new TwigFunction('get_audience_by_professional', [$this, 'getAudienceByProfessional']),
        ];
    }

    public function getAudienceByProfessional(int $professionalId, Collection $professionals): ?string
    {
        foreach ($professionals as $professional) {
            if ($professional->getProfessional()->getId() === $professionalId) {
                return $professional->getAudience();
            }
        }

        return null;
    }
}

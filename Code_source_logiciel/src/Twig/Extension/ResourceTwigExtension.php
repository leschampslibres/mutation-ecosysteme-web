<?php

namespace App\Twig\Extension;


use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class ResourceTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('generate_resources', [$this, 'generateResources'])
        ];
    }

    public function generateResources(array $resources): array
    {
        $result = [];

        foreach ($resources as $resource) {
            $result[] = [
                'title' => $resource['data']['title'],
                'description' => $resource['data']['description'],
                'link' => [
                    'label' => $resource['data']['link']['label'],
                    'href' => $resource['data']['link']['url'],
                    'target' => $resource['data']['link']['target'],
                    'title' => in_array('attribut_title', $resource['data']['link']) ? $resource['data']['link']['attribut_title'] : null,
                ]
            ];
        }

        return $result;
    }
}

<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Tool\ToolRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class ToolTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(protected ToolRepositoryInterface $toolRepository, protected UnderPagesService $underPagesService, protected RouterInterface $router)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_tool_by_id', [$this->toolRepository, 'find']),
        ];
    }
}

<?php

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class LinkTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('generate_links', [$this, 'generateLinks'])
        ];
    }

    public function generateLinks(array $links): array
    {
        $results = [];
        foreach ($links as $link) {
            $results[]['data'] = [
               'label' => $link['data']['label'],
                'href' => $link['data']['url'],
                'target' => $link['data']['target'],
                'attribut_title' => in_array('attribut_title', $link['data']) ? $link['data']['attribut_title'] : null,
            ];
        }
        return $results;
    }

}

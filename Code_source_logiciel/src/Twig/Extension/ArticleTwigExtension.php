<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Article\ArticleCategoryRepositoryInterface;
use App\Repository\Article\ArticleRepositoryInterface;
use App\Repository\Article\ArticleSubjectRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

final class ArticleTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(
        protected ArticleRepositoryInterface $articleRepository,
        protected ArticleSubjectRepositoryInterface $articleSubjectRepository,
        protected ArticleCategoryRepositoryInterface $articleCategoryRepository,
        protected LocaleContextInterface $localeContext,
        protected UnderPagesService $underPagesService,
        protected RouterInterface $router
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_article_categories', [$this, 'getArticleCategories']),
            new TwigFunction('get_latest_articles', [$this, 'getLatestArticles']),
            new TwigFunction('get_article_by_id', [$this->articleRepository, 'find']),
            new TwigFunction('get_article_subject_by_id', [$this->articleSubjectRepository, 'find']),
            new TwigFunction('get_article_subjects_by_id', [$this, 'getArticleSubjects'])
        ];
    }

    public function getArticleCategories(): mixed
    {
        return $this->articleCategoryRepository->findAll($this->localeContext->getLocaleCode());
    }

    public function getLatestArticles($limit = 5): array
    {
        return $this->articleRepository->findLatest($limit, $this->localeContext->getLocaleCode());
    }

    public function getArticleSubjects($ids): array
    {
        $topics = [];
        foreach ($ids as $id) {
            $topic = $this->articleSubjectRepository->find($id['data']['articleSubject']);

            if (null === $topic) {
                continue;
            }

            $topics[] = [
                'label' => $topic->getTitle(),
                'href' => $this->router->generate('app_article_subject_show', ['slug' => $topic->getSlug(), 'page_slug' => $this->underPagesService->underPageSlugByCode('articles')]),
            ];
        }
        return $topics;
    }
}

<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Serie\SerieRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class SerieTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(protected SerieRepositoryInterface $serieRepository, protected UnderPagesService $underPagesService, protected RouterInterface $router)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_serie_by_id', [$this->serieRepository, 'find']),
            new TwigFunction('get_series', [$this, 'getSeries']),
        ];
    }

    public function getSeries(array $ids, bool $image = true, string $formatting = 'column'): array
    {
        $series = [];

        foreach ($ids as $id) {
            $serie = $this->serieRepository->find($id['data']['serie']);

            if (null === $serie) {
                continue;
            }

            $series[] = [
                'horizontal' => 'column' == $formatting,
                'color' => $serie->getColor() ? $serie->getColor()->value : null,
                'picture' => $image ? $serie->getMainImage() : null,
                'title' => $serie->getTitle(),
                'description' => $serie->getDescription(),
                'href' => $this->router->generate('app_serie_show', ['slug' => $serie->getSlug(), 'page_slug' => $this->underPagesService->underPageSlugByCode('series')]),
            ];
        }

        return $series;
    }
}

<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Repository\Project\ProjectRepositoryInterface;
use Luna\CoreBundle\Service\UnderPagesService;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFunction;

class ProjectTwigExtension extends AbstractExtension implements ExtensionInterface
{
    public function __construct(protected ProjectRepositoryInterface $projectRepository, protected UnderPagesService $underPagesService, protected RouterInterface $router)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_project_by_id', [$this->projectRepository, 'find']),
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Entity\Menu\MenuItem;
use Twig\Extension\AbstractExtension;

class MenuTwigExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new \Twig\TwigFunction('menu_item_target', [$this, 'menuItemTarget']),
            new \Twig\TwigFunction('menu_item_rel', [$this, 'menuItemRel']),
        ];
    }

    public function menuItemTarget(MenuItem $menuItem): ?string
    {
        return $menuItem->isTargetBlank() ? '_blank' : '_self';
    }

    public function menuItemRel(MenuItem $menuItem): ?string
    {
        $rel = [];
        if ($menuItem->isNoreferrer()) {
            $rel[] = 'noreferrer';
        }
        if ($menuItem->isNoopener()) {
            $rel[] = 'noopener';
        }
        if ($menuItem->isNofollow()) {
            $rel[] = 'nofollow';
        }

        return implode(' ', $rel);
    }
}

<?php

declare(strict_types=1);

namespace App\Enum\MenuItem;

enum IconEnum: string
{
    case ArrowRight = 'arrow-right';
    case Briefcase = 'briefcase';
    case Accessibility = 'accessibility';
    case Ticket = 'ticket';
    case World = 'world';
    case Book = 'book';
    case Clock = 'clock';
    case QuestionMessage = 'question-message';
    case Email = 'email';
    case Facebook = 'facebook';
    case Instagram = 'instagram';
    case Youtube = 'youtube';
    case XTwitter = 'x-twitter';
}

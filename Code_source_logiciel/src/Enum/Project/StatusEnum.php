<?php

namespace App\Enum\Project;

enum StatusEnum: string
{
    case Past = 'past';
    case In_progress = 'in_progress';
}

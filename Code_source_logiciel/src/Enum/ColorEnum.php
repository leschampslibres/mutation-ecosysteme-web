<?php

declare(strict_types=1);

namespace App\Enum;

enum ColorEnum: string
{
    case Green = 'green';
    case Red = 'red';
    case Yellow = 'yellow';
    case Blue = 'blue';
}

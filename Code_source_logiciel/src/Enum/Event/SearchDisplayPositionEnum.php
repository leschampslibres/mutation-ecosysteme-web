<?php

declare(strict_types=1);

namespace App\Enum\Event;

enum SearchDisplayPositionEnum: int
{
    case First = 1;
    case Second = 2;
    case Third = 3;
}

<?php

namespace App\Enum\Event;

enum OriginAgendaEnum: int
{
    case Direction = 95497319;
    case Bibliothèque = 13227241;
    case Musée_de_Bretagne = 60332170;
    case Espace_des_sciences = 9767408;
    case RDV4C = 41328821;
}

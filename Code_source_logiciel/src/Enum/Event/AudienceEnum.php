<?php

namespace App\Enum\Event;

enum AudienceEnum: int
{
    case tout_public = 21;

    case famille = 22;
}

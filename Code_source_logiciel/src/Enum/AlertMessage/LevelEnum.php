<?php

declare(strict_types=1);

namespace App\Enum\AlertMessage;

enum LevelEnum: string
{
    case High = 'green';
    case VeryHigh = 'red';
}

<?php

declare(strict_types=1);

namespace App\Enum\Contact;

enum ServiceEnum: string
{
    case accueil = 'Accueil';
    case accessibilite = 'Accessibilité';
    case mediation = 'Médiation';
    case rdv_4c = 'Rendez-vous 4C';
    case culture = 'Programmation culturelle';
    case communication = 'Communication';
    case presse = 'Presse';
    case direction = 'Direction des Champs Libres';
    case bibliotheque = 'Bibliothèque';
    case musee = 'Musée de Bretagne';
    case sciences = 'Espace des Sciences';

    public function generateServiceEmail($service): string
    {
        return match ($service) {
            ServiceEnum::accueil->value => 'TODO CONFIGURE ME',
            ServiceEnum::accessibilite->value => 'TODO CONFIGURE ME',
            ServiceEnum::mediation->value => 'TODO CONFIGURE ME',
            ServiceEnum::rdv_4c->value => 'TODO CONFIGURE ME',
            ServiceEnum::culture->value => 'TODO CONFIGURE ME',
            ServiceEnum::communication->value => 'TODO CONFIGURE ME',
            ServiceEnum::presse->value => 'TODO CONFIGURE ME',
            ServiceEnum::direction->value => 'TODO CONFIGURE ME',
            ServiceEnum::bibliotheque->value => 'TODO CONFIGURE ME',
            ServiceEnum::musee->value => 'TODO CONFIGURE ME',
            ServiceEnum::sciences->value => 'TODO CONFIGURE ME',
            default => 'TODO CONFIGURE ME',
        };
    }

    public static function objects(): array
    {
        return [
            'Accueil' => [
            ],
            'Accessibilité' => [
            ],
            'Médiation' => [
            ],
            'Rendez-vous 4C' => [
            ],
            'Programmation culturelle' => [
            ],
            'Communication' => [
            ],
            'Presse' => [
            ],
            'Direction des Champs Libres' => [
            ],
            'Bibliothèque' => [
            ],
            'Musée de Bretagne' => [
                'Documentation',
                'Collections et inventaire',
                'Identification de collections',
                'Expositions itinérantes',
                'Autre objet',
            ],
            'Espace des Sciences' => [
            ],
        ];
    }
}

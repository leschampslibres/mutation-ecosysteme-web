<?php

declare(strict_types=1);

namespace App\Enum\Mediation;

enum MonthEnum: string
{
    case September = 'septembre';
    case October = 'octobre';
    case November = 'novembre';
    case December = 'décembre';
    case January = 'janvier';
    case February = 'février';
    case March = 'mars';
    case April = 'avril';
    case May = 'mai';
    case June = 'juin';
    case July = 'juillet';
    case August = 'août';

    public static function choices(): array
    {
        $choices = [];
        foreach (self::cases() as $object) {
            $choices[$object->name] = $object->value;
        }

        return $choices;
    }
}

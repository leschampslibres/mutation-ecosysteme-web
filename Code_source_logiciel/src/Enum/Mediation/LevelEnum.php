<?php

declare(strict_types=1);

namespace App\Enum\Mediation;

enum LevelEnum: string
{
    case Level1 = 'maternelle';
    case Level2 = 'elementaire';
    case Level3 = 'college';
    case Level4 = 'lycee';
    case Level5 = 'enseignement_superieur';
    case Level6 = 'enseignement_specialise';

    public static function choices(): array
    {
        $choices = [];
        foreach (self::cases() as $object) {
            $choices[$object->name] = $object->value;
        }

        return $choices;
    }
}

<?php

declare(strict_types=1);

namespace App\Enum\Mediation;

enum AccessibilityEnum: string
{
    case IntellectuallyImpaired = 'intellectually_impaired';
    case VisuallyImpaired = 'visually_impaired';
    case HearingImpaired = 'hearing_impaired';
    case PsychicImpaired = 'psychic_impaired';
    case MotorImpaired = 'motor_impaired';

    public static function choices(): array
    {
        $choices = [];
        foreach (self::cases() as $object) {
            $choices[$object->name] = $object->value;
        }

        return $choices;
    }
}

<?php

declare(strict_types=1);

namespace App\Enum\Mediation;

enum PricingEnum: string
{
    case Free = 'gratuit';
    case Paying = 'payant';
}

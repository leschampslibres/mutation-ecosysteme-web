<?php

declare(strict_types=1);

namespace App\Enum\Mediation;

enum SupervisionEnum: string
{
    case Accompanied = 'accompagnement';
    case Autonomous = 'autonomie';
}

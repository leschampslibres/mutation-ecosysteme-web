<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\OpenAgendaApiService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import-events', description: 'Import des événements depuis l\'API OpenAgenda.')]
class ImportEvents extends Command
{
    public function __construct(
        protected OpenAgendaApiService $openAgendaApiService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDefinition(
                new InputDefinition([
                    new InputOption('all', 'a', InputOption::VALUE_NONE, 'Importer tous les événements.'),
                ])
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            'Import des événements depuis l\'API OpenAgenda',
            '==============================================',
            '',
        ]);

        $events = $this->openAgendaApiService->batchEvents($input->getOption('all'));

        $output->writeln('--- Fin de l\'import des événements ---');

        return Command::SUCCESS;
    }
}

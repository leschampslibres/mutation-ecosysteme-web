<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\MediaClearingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:clear-medias', description: 'Nettoyage des medias qui ne sont plus utilisés.')]
class ClearMedias extends Command
{
    public function __construct(
        protected MediaClearingService $mediaClearingService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            'Nettoyage des médias non utilisés',
            '==============================================',
            '',
        ]);

        $this->mediaClearingService->clearMedias();

        $output->writeln('--- Fin du nettoyage des médias non utilisés ---');

        return Command::SUCCESS;
    }
}

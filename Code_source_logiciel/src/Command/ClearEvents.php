<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\EventClearingService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:clear-events', description: 'Nettoyage des événements terminés.')]
class ClearEvents extends Command
{
    public function __construct(
        protected EventClearingService $eventClearingService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            'Nettoyage des événements terminés',
            '==============================================',
            '',
        ]);

        $this->eventClearingService->clearEvents();

        $output->writeln('--- Fin du nettoyage des événements terminés ---');

        return Command::SUCCESS;
    }
}

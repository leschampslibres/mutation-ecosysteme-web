<?php

declare(strict_types=1);

namespace App\Provider\Sitemap;

use Luna\CoreBundle\Provider\Sitemap\AbstractUrlProvider;
use SitemapPlugin\Provider\UrlProviderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleUrlProvider extends AbstractUrlProvider implements UrlProviderInterface
{
    protected function generateUrl($translation): string
    {
        $locale = $translation->getLocale();
        return $this->router->generate('app_article_show', [
            'page_slug' => $this->underPagesService->underPageSlugByCode('articles', $locale),
            'category_slug' => $translation->getTranslatable()->getCategory()->getTranslation($locale)->getSlug(),
            'slug' => $translation->getSlug(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}

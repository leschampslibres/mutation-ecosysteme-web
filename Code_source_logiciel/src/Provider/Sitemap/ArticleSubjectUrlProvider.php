<?php

declare(strict_types=1);

namespace App\Provider\Sitemap;

use Luna\CoreBundle\Provider\Sitemap\AbstractUrlProvider;
use SitemapPlugin\Provider\UrlProviderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleSubjectUrlProvider extends AbstractUrlProvider implements UrlProviderInterface
{
    protected function generateUrl($translation): string
    {
        $locale = $translation->getLocale();
        return $this->router->generate('app_article_subject_show', [
            'page_slug' => $this->underPagesService->underPageSlugByCode('articles', $locale),
            'slug' => $translation->getSlug(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}

<?php

declare(strict_types=1);

namespace App\Provider\Sitemap;

use App\Repository\Page\PageRepositoryDecorator;
use App\Repository\Professional\ProfessionalRepository;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Luna\CoreBundle\Service\AirbrakeService;
use Luna\CoreBundle\Service\UnderPagesService;
use SitemapPlugin\Factory\AlternativeUrlFactoryInterface;
use SitemapPlugin\Factory\UrlFactoryInterface;
use SitemapPlugin\Provider\UrlProviderInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class MediationSubjectUrlProvider extends ProfessionalAbstractUrlProvider implements UrlProviderInterface
{
    public function __construct(
        protected RouterInterface $router,
        protected UrlFactoryInterface $urlFactory,
        protected AlternativeUrlFactoryInterface $urlAlternativeFactory,
        protected LocaleContextInterface $localeContext,
        protected UnderPagesService $underPagesService,
        protected AirbrakeService $airbrakeService,
        protected SitemapRepositoryInterface $resourceRepository,
        protected PageRepositoryDecorator $pageRepositoryDecorator,
        protected ProfessionalRepository $professionalRepository,
        protected string $underPagesCode,
        protected string $getResourceProfessionalsMethod,
        protected string $urlProviderName
    ) {
        parent::__construct($router, $urlFactory, $urlAlternativeFactory, $localeContext, $underPagesService, $airbrakeService, $resourceRepository, $pageRepositoryDecorator, $underPagesCode, $getResourceProfessionalsMethod, $urlProviderName);
    }

    protected function createResourceProfessionalUrls($resource): array
    {
        $urls = [];

        $resourceProfessionals = $this->professionalRepository->findByMediationWithSubject($resource);
        foreach ($resourceProfessionals as $resourceProfessional) {
            $resourceProfessionalUrl = $this->createResourceProfessionalUrl($resource, $resourceProfessional);

            if (!empty($resourceProfessionalUrl->getLocation())) {
                $urls[] = $resourceProfessionalUrl;
            }
        }

        return $urls;
    }

    protected function generateProfessionalUrl($translation, $page): string
    {
        return $this->router->generate('app_mediation_subject_show', [
            'page_slug' => $page->getTranslation($translation->getLocale())->getCompositeSlug(),
            'slug' => $translation->getSlug(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}

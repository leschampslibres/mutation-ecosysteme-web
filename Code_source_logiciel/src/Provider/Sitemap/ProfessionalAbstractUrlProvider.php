<?php

declare(strict_types=1);

namespace App\Provider\Sitemap;

use App\Repository\Page\PageRepositoryDecorator;
use Luna\CoreBundle\Provider\Sitemap\AbstractUrlProvider;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Luna\CoreBundle\Service\AirbrakeService;
use Luna\CoreBundle\Service\UnderPagesService;
use SitemapPlugin\Factory\AlternativeUrlFactoryInterface;
use SitemapPlugin\Factory\UrlFactoryInterface;
use SitemapPlugin\Model\UrlInterface;
use SitemapPlugin\Provider\UrlProviderInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Resource\Model\TranslationInterface;
use Symfony\Component\Routing\RouterInterface;

abstract class ProfessionalAbstractUrlProvider extends AbstractUrlProvider implements UrlProviderInterface
{
    public function __construct(
        protected RouterInterface $router,
        protected UrlFactoryInterface $urlFactory,
        protected AlternativeUrlFactoryInterface $urlAlternativeFactory,
        protected LocaleContextInterface $localeContext,
        protected UnderPagesService $underPagesService,
        protected AirbrakeService $airbrakeService,
        protected SitemapRepositoryInterface $resourceRepository,
        protected PageRepositoryDecorator $pageRepositoryDecorator,
        protected string $underPagesCode,
        protected string $getResourceProfessionalsMethod,
        protected string $urlProviderName
    ) {
        parent::__construct($router, $urlFactory, $urlAlternativeFactory, $localeContext, $underPagesService, $airbrakeService, $resourceRepository, $urlProviderName);
    }

    public function generate(ChannelInterface $channel): iterable
    {
        try {
            $this->channel = $channel;
            $urls = [];
            $this->channelLocaleCodes = [];

            foreach ($this->getResources() as $resource) {
                // Pas d'ajout au sitemap si la ressource est précisée comme non indexable
                if (method_exists($resource, 'getNoIndex') && $resource->getNoIndex()) {
                    continue;
                }

                // Modification de l'AbstractUrlProvider pour générer une URL par professionnel de la ressource
                $resourceUrls = $this->createResourceProfessionalUrls($resource);
                foreach ($resourceUrls as $resourceUrl) {
                    if (!empty($resourceUrl->getLocation())) {
                        $urls[] = $resourceUrl;
                    }
                }
            }

            return $urls;
        } catch (\Exception $e) {
            $this->airbrakeService->notifyException($e);

            throw $e;
        }
    }

    protected function createResourceProfessionalUrls($resource): array
    {
        $urls = [];

        $resourceProfessionals = \call_user_func_array([$resource, $this->getResourceProfessionalsMethod], []);
        foreach ($resourceProfessionals as $resourceProfessional) {
            $resourceProfessionalUrl = $this->createResourceProfessionalUrl($resource, $resourceProfessional);

            if (!empty($resourceProfessionalUrl->getLocation())) {
                $urls[] = $resourceProfessionalUrl;
            }
        }

        return $urls;
    }

    protected function createResourceProfessionalUrl($resource, $resourceProfessional): UrlInterface
    {
        $professionalCode = method_exists($resourceProfessional, 'getProfessional') ? $resourceProfessional->getProfessional()->getCode() : $resourceProfessional->getCode();

        $resourceUrl = $this->urlFactory->createNew('');

        $updatedAt = $resource->getUpdatedAt();
        if (null !== $updatedAt) {
            $resourceUrl->setLastModification($updatedAt);
        }

        /** @var TranslationInterface $translation */
        foreach ($this->getTranslations($resource) as $translation) {
            $locale = $translation->getLocale();
            $page = $this->pageRepositoryDecorator->findOneByCodeAndProfessional($locale, $this->underPagesCode, $professionalCode);

            if (null === $locale || null === $page) {
                continue;
            }

            if (!$this->localeInLocaleCodes($translation)) {
                continue;
            }

            $location = $this->generateProfessionalUrl($translation, $page);

            // TODO @back : décommenter  le "if" quand le "bug de session" sera corrigé.
            // Deux issues sont ouvertes au niveau du plugin et de sylius, à surveiller pour voir si une solution est trouvée :
            // - https://github.com/stefandoorn/sitemap-plugin/issues/228
            // - https://github.com/Sylius/Sylius/issues/15011
            // if ($locale === $this->localeContext->getLocaleCode()) {
            $resourceUrl->setLocation($location);

            // continue;
            // }

            $resourceUrl->addAlternative($this->urlAlternativeFactory->createNew($location, $locale));
        }

        return $resourceUrl;
    }

    protected function generateProfessionalUrl($translation, $page): string
    {
        $message = 'You must implement the generateProfessionalUrl method in a child class.';
        $this->airbrakeService->notifyMessage($message);

        throw new \RuntimeException($message);
    }
}

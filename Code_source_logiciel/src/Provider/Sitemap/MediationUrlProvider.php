<?php

declare(strict_types=1);

namespace App\Provider\Sitemap;

use SitemapPlugin\Provider\UrlProviderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MediationUrlProvider extends ProfessionalAbstractUrlProvider implements UrlProviderInterface
{
    protected function generateProfessionalUrl($translation, $page): string
    {
        return $this->router->generate('app_mediation_show', [
            'page_slug' => $page->getTranslation($translation->getLocale())->getCompositeSlug(),
            'slug' => $translation->getSlug(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}

<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\AlertMessage\AlertMessage;
use App\Entity\Article\Article;
use App\Entity\Dossier\Dossier;
use App\Entity\Event\Event;
use App\Entity\Exhibit\Exhibit;
use App\Entity\Mediation\Mediation;
use App\Entity\Project\Project;
use App\Entity\Serie\Serie;
use App\Entity\ThematicTour\ThematicTour;
use App\Entity\Tool\Tool;
use Doctrine\Common\EventArgs;
use Luna\CoreBundle\Entity\Page\Page;
use Symfony\Component\Process\Process;

class CacheClearer
{
    protected array $cachedClasses;

    public function __construct(
    ) {
        $this->cachedClasses = [
            AlertMessage::class,
            Article::class,
            Dossier::class,
            Event::class,
            Exhibit::class,
            Mediation::class,
            Page::class,
            Project::class,
            Serie::class,
            ThematicTour::class,
            Tool::class,
        ];
    }

    public function postUpdate(EventArgs $args): void
    {
        $this->clearCache($args);
    }

    public function postRemove(EventArgs $args): void
    {
        $this->clearCache($args);
    }

    private function clearCache(EventArgs $args): void
    {
        // Nettoyage de tout le cache HTTP
        $process = new Process(['rm', '-rf', 'var/cache/prod/http_cache/']);
        $process->setWorkingDirectory(getcwd() . '/../');
        $process->setTimeout(null);
        $process->run();
    }
}

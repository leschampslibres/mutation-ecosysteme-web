<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface EventTranslationInterface extends BasePageTranslationInterface
{
}

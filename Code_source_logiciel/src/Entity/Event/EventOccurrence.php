<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Repository\Event\EventOccurrenceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Trait\IdentifiableTrait;
use Luna\CoreBundle\Model\Trait\TimestampableTrait;

#[ORM\Entity(repositoryClass: EventOccurrenceRepository::class)]
#[ORM\Table(name: 'app_event_occurrence')]
#[ORM\Index(name: "idx_starting_at", columns: ["starting_at"])]
#[ORM\Index(name: "idx_ending_at", columns: ["ending_at"])]
class EventOccurrence implements EventOccurrenceInterface
{
    use IdentifiableTrait;
    use TimestampableTrait;

    #[ORM\Column(name: 'starting_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $startingAt = null;

    #[ORM\Column(name: 'ending_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $endingAt = null;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'occurrences')]
    #[ORM\JoinColumn(name: 'event_id', referencedColumnName: 'id')]
    protected Event|null $event = null;

    public function getStartingAt(): ?\DateTime
    {
        return $this->startingAt;
    }

    public function setStartingAt(?\DateTime $startingAt): void
    {
        $this->startingAt = $startingAt;
    }

    public function getEndingAt(): ?\DateTime
    {
        return $this->endingAt;
    }

    public function setEndingAt(?\DateTime $endingAt): void
    {
        $this->endingAt = $endingAt;
    }

    public function getEvent(): Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): void
    {
        $this->event = $event;
    }

    public function calculateDuration(): ?string
    {
        if ($this->startingAt === null || $this->endingAt === null) {
            return null;
        }
        $duration = $this->startingAt->diff($this->endingAt);
        return $duration->format('%h:%I');
    }
}

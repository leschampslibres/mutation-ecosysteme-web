<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Luna\CoreBundle\Model\Trait\TimestampableTraitInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface EventOccurrenceInterface extends
    ResourceInterface,
    TimestampableTraitInterface
{
    public function getStartingAt(): ?\DateTime;

    public function setStartingAt(?\DateTime $startingAt): void;

    public function getEndingAt(): ?\DateTime;

    public function setEndingAt(?\DateTime $endingAt): void;

    public function calculateDuration(): ?string;

    public function getEvent(): Event;

    public function setEvent(?Event $event): void;
}

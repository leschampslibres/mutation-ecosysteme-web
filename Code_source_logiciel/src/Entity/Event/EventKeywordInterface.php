<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Doctrine\Common\Collections\Collection;

interface EventKeywordInterface
{
    public function getTitle(): ?string;

    public function setTitle(?string $title): void;

    public function getEvents(): Collection;

    public function addEvent(Event $event): void;

    public function removeEvent(Event $event): void;
}

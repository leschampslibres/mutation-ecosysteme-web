<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_event_category_translation')]
#[ORM\Index(name: "idx_locale", columns: ["locale"])]
#[ORM\Index(name: "idx_slug", columns: ["slug"])]
class EventCategoryTranslation extends BasePageTranslation implements EventCategoryTranslationInterface
{
    #[ORM\Column(name: 'label', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $label = null;

    #[ORM\Column(name: 'label_plural', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $labelPlural = null;

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): void
    {
        $this->label = $label;
    }

    public function getLabelPlural(): ?string
    {
        return $this->labelPlural;
    }

    public function setLabelPlural(?string $labelPlural): void
    {
        $this->labelPlural = $labelPlural;
    }
}

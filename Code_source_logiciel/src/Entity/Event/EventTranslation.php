<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_event_translation')]
#[ORM\Index(name: "idx_slug", columns: ["slug"])]
#[ORM\Index(name: "idx_locale", columns: ["locale"])]
class EventTranslation extends BasePageTranslation implements EventTranslationInterface
{
    public const CLEAR_TO_END = [
        'blocks',
        'description',
        'canonicalUrl',
        'metaTitle',
        'metaDescription',
        'openGraphTitle',
        'openGraphDescription',
        'openGraphImage'
    ];

    public const CLEAR_TO_DELETE = [
        'blocks',
        'description',
        'canonicalUrl',
        'metaTitle',
        'metaDescription',
        'openGraphTitle',
        'openGraphDescription',
        'openGraphImage',
        'mainImage'
    ];
}

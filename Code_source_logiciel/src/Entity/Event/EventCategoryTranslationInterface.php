<?php

declare(strict_types=1);

namespace App\Entity\Event;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface EventCategoryTranslationInterface extends BasePageTranslationInterface
{
    public function getLabel(): ?string;

    public function setLabel(?string $label): void;

    public function getLabelPlural(): ?string;

    public function setLabelPlural(?string $labelPlural): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Enum\Event\SearchDisplayPositionEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;

interface EventCategoryInterface extends BasePageInterface
{
    public function getIdOa(): ?int;

    public function setIdOa(?int $idOa): void;

    public function getIcon(): ?string;

    public function setIcon(?string $icon): void;

    public function getPosition(): int;

    public function setPosition(int $position): void;

    public function getEventAutoEnabled(): bool;

    public function setEventAutoEnabled(bool $eventAutoEnabled): void;

    public function getEventAutoDeletable(): bool;

    public function setEventAutoDeletable(bool $eventAutoDeletable): void;

    public function getDisplayImages(): bool;

    public function setDisplayImages(bool $displayImages): void;

    public function getSearchDisplayPosition(): SearchDisplayPositionEnum;

    public function setSearchDisplayPosition(SearchDisplayPositionEnum $searchDisplayPosition): void;

    public function getEvents(): Collection;

    public function addEvent(Event $event): void;

    public function removeEvent(Event $event): void;
}

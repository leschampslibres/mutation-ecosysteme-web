<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Enum\ColorEnum;
use App\Repository\Event\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;

#[ORM\Entity(repositoryClass: EventRepository::class)]
#[ORM\Table(name: 'app_event')]
#[ORM\Index(name: 'idx_id_oa', columns: ['id_oa'])]
#[ORM\Index(name: 'idx_origin_agenda', columns: ['origin_agenda'])]
#[ORM\Index(name: 'idx_pricing', columns: ['pricing'])]
#[ORM\Index(name: 'idx_audience', columns: ['audience'])]
#[ORM\Index(name: 'idx_accessibility_ii', columns: ['accessibility_ii'])]
#[ORM\Index(name: 'idx_accessibility_hi', columns: ['accessibility_hi'])]
#[ORM\Index(name: 'idx_accessibility_vi', columns: ['accessibility_vi'])]
#[ORM\Index(name: 'idx_accessibility_pi', columns: ['accessibility_pi'])]
#[ORM\Index(name: 'idx_accessibility_mi', columns: ['accessibility_mi'])]
#[ORM\Index(name: 'idx_starting_at', columns: ['starting_at'])]
#[ORM\Index(name: 'idx_ending_at', columns: ['ending_at'])]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Event extends BasePage implements EventInterface
{
    public const CLEAR_TO_END = [
        'childrenKeyword',
        'originAgenda',
        'location',
        'pricing',
        'price',
        'ticketing',
        'audience',
        'ageMin',
        'ageMax',
        'accessibilityIi',
        'accessibilityHi',
        'accessibilityVi',
        'accessibilityPi',
        'accessibilityMi',
        'mainImageCredits',
        'startingAt',
        'endingAt',
        'color',
        'oaUpdatedAt'
    ];

    public const CLEAR_TO_DELETE = [
        'childrenKeyword',
        'originAgenda',
        'location',
        'pricing',
        'price',
        'ticketing',
        'audience',
        'ageMin',
        'ageMax',
        'accessibilityIi',
        'accessibilityHi',
        'accessibilityVi',
        'accessibilityPi',
        'accessibilityMi',
        'mainImageCredits',
        'startingAt',
        'endingAt',
        'color',
        'oaUpdatedAt',
        'category'
    ];

    // Statuts OpenAgenda : 1 à 6
    // Programmé (identifiant : 1)
    // Re-programmé (identifiant : 2)
    // Déplacé en ligne (identifiant : 3)
    // Reporté (identifiant : 4)
    // Complet (identifiant : 5)
    // Annulé (identifiant : 6)
    // On ne récupère que des événements avec les statuts 1, 4 et 6.
    // On utilise 3 autres statuts pour gérer les événements terminés et leur (non) affichage.
    public const STATUS_COMPLETED = 20; // Événement terminé qui doit être "supprimé" manuellement

    public const STATUS_ENDED = 21; // Événement terminé mais utilisé, et donc nettoyé partiellement

    public const STATUS_DELETED = 22; // Événement terminé et nettoyé totalement

    #[ORM\Column(name: 'id_oa', type: Types::INTEGER, nullable: true)]
    protected ?int $idOa = null;

    #[ORM\Column(name: 'origin_agenda', type: Types::INTEGER, nullable: true)]
    protected ?int $originAgenda = null;

    #[ORM\Column(name: 'location', type: Types::INTEGER, nullable: true)]
    protected ?int $location = null;

    #[ORM\Column(name: 'pricing', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $pricing = null;

    #[ORM\Column(name: 'price', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $price = null;

    #[ORM\Column(name: 'ticketing', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $ticketing = null;

    #[ORM\Column(name: 'audience', type: Types::INTEGER, nullable: true)]
    protected ?int $audience = null;

    #[ORM\Column(name: 'age_min', type: Types::INTEGER, nullable: true)]
    protected ?int $ageMin = null;

    #[ORM\Column(name: 'age_max', type: Types::INTEGER, nullable: true)]
    protected ?int $ageMax = null;

    #[ORM\Column(name: 'accessibility_ii', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $accessibilityIi = null;

    #[ORM\Column(name: 'accessibility_hi', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $accessibilityHi = null;

    #[ORM\Column(name: 'accessibility_vi', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $accessibilityVi = null;

    #[ORM\Column(name: 'accessibility_pi', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $accessibilityPi = null;

    #[ORM\Column(name: 'accessibility_mi', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $accessibilityMi = null;

    #[ORM\Column(name: 'status', type: Types::INTEGER, length: 255, nullable: true)]
    protected ?int $status = null;

    #[ORM\Column(name: 'main_image_credits', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $mainImageCredits = null;

    #[ORM\Column(name: 'starting_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $startingAt = null;

    #[ORM\Column(name: 'ending_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $endingAt = null;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\Column(name: 'oa_updated_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $oaUpdatedAt;

    #[ORM\ManyToMany(targetEntity: EventKeyword::class, inversedBy: 'events', fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'app_event_event_keyword')]
    protected Collection $keywords;

    #[ORM\ManyToOne(targetEntity: EventKeyword::class, fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinColumn(name: 'children_keyword_id', referencedColumnName: 'id')]
    protected ?EventKeyword $childrenKeyword = null;

    #[ORM\OneToMany(targetEntity: EventOccurrence::class, mappedBy: 'event', fetch: 'LAZY', cascade: ['persist', 'remove'], orphanRemoval: true)]
    protected Collection $occurrences;

    #[ORM\ManyToOne(targetEntity: EventCategory::class, inversedBy: 'events', fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id')]
    protected ?EventCategory $category = null;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->keywords = new ArrayCollection();
        $this->occurrences = new ArrayCollection();
        $this->oaUpdatedAt = new \DateTime();
    }

    public function getIdOa(): ?int
    {
        return $this->idOa;
    }

    public function setIdOa(?int $idOa): void
    {
        $this->idOa = $idOa;
    }

    public function getOriginAgenda(): ?int
    {
        return $this->originAgenda;
    }

    public function setOriginAgenda(?int $originAgenda): void
    {
        $this->originAgenda = $originAgenda;
    }

    public function getLocation(): ?int
    {
        return $this->location;
    }

    public function setLocation(?int $location): void
    {
        $this->location = $location;
    }

    public function getPricing(): ?string
    {
        return $this->pricing;
    }

    public function setPricing(?string $pricing): void
    {
        $this->pricing = $pricing;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }

    public function getTicketing(): ?string
    {
        return $this->ticketing;
    }

    public function setTicketing(?string $ticketing): void
    {
        $this->ticketing = $ticketing;
    }

    public function getAudience(): ?int
    {
        return $this->audience;
    }

    public function setAudience(?int $audience): void
    {
        $this->audience = $audience;
    }

    public function getAgeMin(): ?int
    {
        return $this->ageMin;
    }

    public function setAgeMin(?int $ageMin): void
    {
        $this->ageMin = $ageMin;
    }

    public function getAgeMax(): ?int
    {
        return $this->ageMax;
    }

    public function setAgeMax(?int $ageMax): void
    {
        $this->ageMax = $ageMax;
    }

    public function getAccessibilityIi(): ?bool
    {
        return $this->accessibilityIi;
    }

    public function setAccessibilityIi(?bool $accessibilityIi): void
    {
        $this->accessibilityIi = $accessibilityIi;
    }

    public function getAccessibilityHi(): ?bool
    {
        return $this->accessibilityHi;
    }

    public function setAccessibilityHi(?bool $accessibilityHi): void
    {
        $this->accessibilityHi = $accessibilityHi;
    }

    public function getAccessibilityVi(): ?bool
    {
        return $this->accessibilityVi;
    }

    public function setAccessibilityVi(?bool $accessibilityVi): void
    {
        $this->accessibilityVi = $accessibilityVi;
    }

    public function getAccessibilityPi(): ?bool
    {
        return $this->accessibilityPi;
    }

    public function setAccessibilityPi(?bool $accessibilityPi): void
    {
        $this->accessibilityPi = $accessibilityPi;
    }

    public function getAccessibilityMi(): ?bool
    {
        return $this->accessibilityMi;
    }

    public function setAccessibilityMi(?bool $accessibilityMi): void
    {
        $this->accessibilityMi = $accessibilityMi;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    public function getMainImageCredits(): ?string
    {
        return $this->mainImageCredits;
    }

    public function setMainImageCredits(?string $mainImageCredits): void
    {
        $this->mainImageCredits = $mainImageCredits;
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getStartingAt(): ?\DateTime
    {
        return $this->startingAt;
    }

    public function setStartingAt(?\DateTime $startingAt): void
    {
        $this->startingAt = $startingAt;
    }

    public function getEndingAt(): ?\DateTime
    {
        return $this->endingAt;
    }

    public function setEndingAt(?\DateTime $endingAt): void
    {
        $this->endingAt = $endingAt;
    }

    public function getOaUpdatedAt(): ?\DateTime
    {
        return $this->oaUpdatedAt;
    }

    public function setOaUpdatedAt(?\DateTime $oaUpdatedAt): void
    {
        $this->oaUpdatedAt = $oaUpdatedAt;
    }

    public function getKeywords(): Collection
    {
        return $this->keywords;
    }

    public function addKeyword(EventKeyword $keyword): void
    {
        if (!$this->keywords->contains($keyword)) {
            $this->keywords->add($keyword);
            $keyword->addEvent($this);
        }
    }

    public function removeKeyword(EventKeyword $keyword): void
    {
        if ($this->keywords->contains($keyword)) {
            $this->keywords->removeElement($keyword);
            $keyword->removeEvent($this);
        }
    }

    public function getChildrenKeyword(): ?EventKeyword
    {
        return $this->childrenKeyword;
    }

    public function setChildrenKeyword(?EventKeyword $childrenKeyword): void
    {
        $this->childrenKeyword = $childrenKeyword;
    }

    public function getOccurrences(): Collection
    {
        return $this->occurrences;
    }

    public function addOccurrence(EventOccurrence $occurrence): void
    {
        if (!$this->occurrences->contains($occurrence)) {
            $this->occurrences->add($occurrence);
            $occurrence->setEvent($this);
        }
    }

    public function removeOccurrence(EventOccurrence $occurrence): void
    {
        if ($this->occurrences->contains($occurrence)) {
            $this->occurrences->removeElement($occurrence);
            $occurrence->setEvent(null);
        }
    }

    public function getCategory(): ?EventCategory
    {
        return $this->category;
    }

    public function setCategory(?EventCategory $category): void
    {
        $this->category = $category;
    }
}

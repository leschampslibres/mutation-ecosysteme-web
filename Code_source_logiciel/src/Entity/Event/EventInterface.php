<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Enum\ColorEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;

interface EventInterface extends BasePageInterface
{
    public function getIdOa(): ?int;

    public function setIdOa(?int $idOa): void;

    public function getOriginAgenda(): ?int;

    public function setOriginAgenda(?int $originAgenda): void;

    public function getLocation(): ?int;

    public function setLocation(?int $location): void;

    public function getPricing(): ?string;

    public function setPricing(?string $pricing): void;

    public function getPrice(): ?string;

    public function setPrice(?string $price): void;

    public function getTicketing(): ?string;

    public function setTicketing(?string $ticketing): void;

    public function getAudience(): ?int;

    public function setAudience(?int $audience): void;

    public function getAgeMin(): ?int;

    public function setAgeMin(?int $ageMin): void;

    public function getAgeMax(): ?int;

    public function setAgeMax(?int $ageMax): void;

    public function getAccessibilityIi(): ?bool;

    public function setAccessibilityIi(?bool $accessibilityIi): void;

    public function getAccessibilityHi(): ?bool;

    public function setAccessibilityHi(?bool $accessibilityHi): void;

    public function getAccessibilityVi(): ?bool;

    public function setAccessibilityVi(?bool $accessibilityVi): void;

    public function getAccessibilityPi(): ?bool;

    public function setAccessibilityPi(?bool $accessibilityPi): void;

    public function getAccessibilityMi(): ?bool;

    public function setAccessibilityMi(?bool $accessibilityMi): void;

    public function getStatus(): ?int;

    public function setStatus(?int $status): void;

    public function getMainImageCredits(): ?string;

    public function setMainImageCredits(?string $mainImageCredits): void;

    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getStartingAt(): ?\DateTime;

    public function setStartingAt(?\DateTime $startingAt): void;

    public function getEndingAt(): ?\DateTime;

    public function setEndingAt(?\DateTime $endingAt): void;

    public function getOaUpdatedAt(): ?\DateTime;

    public function setOaUpdatedAt(?\DateTime $oaUpdatedAt): void;

    public function getKeywords(): Collection;

    public function addKeyword(EventKeyword $keyword): void;

    public function removeKeyword(EventKeyword $keyword): void;

    public function getChildrenKeyword(): ?EventKeyword;

    public function setChildrenKeyword(?EventKeyword $childrenKeyword): void;

    public function getOccurrences(): Collection;

    public function addOccurrence(EventOccurrence $occurrence): void;

    public function removeOccurrence(EventOccurrence $occurrence): void;

    public function getCategory(): ?EventCategory;

    public function setCategory(?EventCategory $category): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Repository\Event\EventKeywordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Trait\IdentifiableTrait;

#[ORM\Entity(repositoryClass: EventKeywordRepository::class)]
#[ORM\Table(name: 'app_event_keyword')]
class EventKeyword implements EventKeywordInterface
{
    use IdentifiableTrait;

    #[ORM\Column(name: 'title', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $title = null;

    #[ORM\ManyToMany(targetEntity: Event::class, mappedBy: 'keywords')]
    protected Collection $events;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): void
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
        }
    }

    public function removeEvent(Event $event): void
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
        }
    }
}

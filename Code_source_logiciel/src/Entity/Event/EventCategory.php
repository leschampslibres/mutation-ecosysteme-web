<?php

declare(strict_types=1);

namespace App\Entity\Event;

use App\Entity\Event\EventCategoryTranslation;
use App\Enum\Event\SearchDisplayPositionEnum;
use App\Repository\Event\EventCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Luna\CoreBundle\Model\Page\Page as BasePage;

#[ORM\Entity(repositoryClass: EventCategoryRepository::class)]
#[ORM\Table(name: 'app_event_category')]
#[ORM\Index(name: "idx_id_oa", columns: ["id_oa"])]
#[ORM\Index(name: "idx_display_occurrences", columns: ["display_occurrences"])]
#[ORM\Index(name: "idx_search_display_position", columns: ["search_display_position"])]
class EventCategory extends BasePage implements EventCategoryInterface
{
    #[ORM\Column(name: 'id_oa', type: Types::INTEGER, nullable: true)]
    protected ?int $idOa = null;

    #[ORM\Column(name: 'icon', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $icon = null;

    #[Gedmo\SortablePosition]
    #[ORM\Column(name: 'position', type: 'integer')]
    protected int $position;

    #[ORM\Column(name: 'event_auto_enabled', type: Types::BOOLEAN)]
    protected bool $eventAutoEnabled = true;

    #[ORM\Column(name: 'event_auto_deletable', type: Types::BOOLEAN)]
    protected bool $eventAutoDeletable = true;

    #[ORM\Column(name: 'display_images', type: Types::BOOLEAN)]
    protected bool $displayImages = false;

    #[ORM\Column(name: 'display_occurrences', type: Types::BOOLEAN)]
    protected bool $displayOccurrences = true;

    #[ORM\Column(name: 'search_display_position', type: Types::INTEGER, enumType: SearchDisplayPositionEnum::class)]
    protected SearchDisplayPositionEnum $searchDisplayPosition = SearchDisplayPositionEnum::Third;

    #[ORM\OneToMany(targetEntity: Event::class, mappedBy: 'category', cascade: ['persist'])]
    protected Collection $events;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->events = new ArrayCollection();
    }

    public function getIdOa(): ?int
    {
        return $this->idOa;
    }

    public function setIdOa(?int $idOa): void
    {
        $this->idOa = $idOa;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function getEventAutoEnabled(): bool
    {
        return $this->eventAutoEnabled;
    }

    public function setEventAutoEnabled(bool $eventAutoEnabled): void
    {
        $this->eventAutoEnabled = $eventAutoEnabled;
    }

    public function getEventAutoDeletable(): bool
    {
        return $this->eventAutoDeletable;
    }

    public function setEventAutoDeletable(bool $eventAutoDeletable): void
    {
        $this->eventAutoDeletable = $eventAutoDeletable;
    }

    public function getDisplayImages(): bool
    {
        return $this->displayImages;
    }

    public function setDisplayImages(bool $displayImages): void
    {
        $this->displayImages = $displayImages;
    }

    public function getDisplayOccurrences(): bool
    {
        return $this->displayOccurrences;
    }

    public function setDisplayOccurrences(bool $displayOccurrences): void
    {
        $this->displayOccurrences = $displayOccurrences;
    }

    public function getSearchDisplayPosition(): SearchDisplayPositionEnum
    {
        return $this->searchDisplayPosition;
    }

    public function setSearchDisplayPosition(SearchDisplayPositionEnum $searchDisplayPosition): void
    {
        $this->searchDisplayPosition = $searchDisplayPosition;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): void
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
            $event->setCategory($this);
        }
    }

    public function removeEvent(Event $event): void
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->setCategory(null);
        }
    }

    public function getLabel(): ?string
    {
        return $this->getTranslation()->getLabel();
    }

    public function setLabel(?string $label): void
    {
        $this->getTranslation()->setLabel($label);
    }

    public function getLabelPlural(): ?string
    {
        return $this->getTranslation()->getLabelPlural();
    }

    public function setLabelPlural(?string $labelPlural): void
    {
        $this->getTranslation()->setLabelPlural($labelPlural);
    }

    protected function createTranslation(): EventCategoryTranslation
    {
        return new EventCategoryTranslation();
    }
}

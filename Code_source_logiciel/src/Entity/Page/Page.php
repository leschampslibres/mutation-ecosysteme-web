<?php

declare(strict_types=1);

namespace App\Entity\Page;

use App\Entity\Article\Article;
use App\Entity\Professional\Professional;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Entity\Page\Page as BasePage;

#[ORM\Entity]
#[ORM\Table(name: 'luna_page')]
class Page extends BasePage
{
    // Mise en avant d'un article sur la page d'accueil du Mag
    #[ORM\ManyToOne(targetEntity: Article::class, inversedBy: 'pages')]
    #[ORM\JoinColumn(name: 'article_id', referencedColumnName: 'id')]
    protected ?Article $article = null;

    // Association avec un type de professionnel
    #[ORM\ManyToOne(targetEntity: Professional::class, inversedBy: 'pages', fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'professional_id', referencedColumnName: 'id')]
    protected ?Professional $professional = null;

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getProfessional(): ?Professional
    {
        return $this->professional;
    }

    public function setProfessional(?Professional $professional): self
    {
        $this->professional = $professional;

        return $this;
    }
}

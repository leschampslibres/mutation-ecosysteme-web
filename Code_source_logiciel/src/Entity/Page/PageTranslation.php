<?php

declare(strict_types=1);

namespace App\Entity\Page;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Entity\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'luna_page_translation')]
class PageTranslation extends BasePageTranslation
{
}

<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use App\Entity\Professional\Professional;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'app_mediations_professionals')]
class MediationProfessional
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(name: 'id', type: Types::INTEGER, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Mediation::class, inversedBy: 'mediationProfessionals')]
    #[ORM\JoinColumn(name: 'mediation_id', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    protected ?Mediation $mediation = null;

    #[ORM\ManyToOne(targetEntity: Professional::class, inversedBy: 'mediationProfessionals')]
    #[ORM\JoinColumn(name: 'professional_id', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    protected Professional $professional;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audience = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMediation(): Mediation
    {
        return $this->mediation;
    }

    public function setMediation(?Mediation $mediation): void
    {
        $this->mediation = $mediation;
    }

    public function getProfessional(): Professional
    {
        return $this->professional;
    }

    public function setProfessional(Professional $professional): void
    {
        $this->professional = $professional;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): void
    {
        $this->audience = $audience;
    }
}

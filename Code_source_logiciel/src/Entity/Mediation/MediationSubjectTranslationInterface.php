<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use Luna\CoreBundle\Model\Taxon\RichTaxonTranslationInterface;

interface MediationSubjectTranslationInterface extends RichTaxonTranslationInterface
{
}

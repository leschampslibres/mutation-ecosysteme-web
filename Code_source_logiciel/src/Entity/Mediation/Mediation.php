<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use App\Enum\ColorEnum;
use App\Enum\Mediation\PricingEnum;
use App\Enum\Mediation\SupervisionEnum;
use App\Repository\Mediation\MediationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: MediationRepository::class)]
#[ORM\Table(name: 'app_mediation')]
#[ORM\Index(name: 'idx_level', columns: ['level'])]
#[ORM\Index(name: 'idx_supervision', columns: ['supervision'])]
#[ORM\Index(name: 'idx_month', columns: ['month'])]
#[ORM\Index(name: 'idx_pricing', columns: ['pricing'])]
class Mediation extends BasePage implements MediationInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\Column(name: 'accessibility', type: Types::JSON, nullable: true)]
    protected ?array $accessibility = null;

    #[ORM\Column(name: 'pricing', type: Types::STRING, enumType: PricingEnum::class, nullable: true)]
    protected ?PricingEnum $pricing = null;

    // Accompagné ou en autonomie
    #[ORM\Column(name: 'supervision', type: Types::STRING, enumType: SupervisionEnum::class, nullable: true)]
    protected ?SupervisionEnum $supervision = null;

    #[ORM\Column(name: 'level', type: Types::JSON, nullable: true)]
    protected ?array $level = null;

    #[ORM\Column(name: 'month', type: Types::JSON, nullable: true)]
    protected ?array $month = null;

    #[ORM\OneToMany(targetEntity: MediationProfessional::class, mappedBy: 'mediation', fetch: 'EAGER', cascade: ['persist'], orphanRemoval: true)]
    protected Collection $mediationProfessionals;

    #[ORM\ManyToMany(targetEntity: MediationSubject::class, inversedBy: 'mediations', fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'app_mediation_mediation_subject')]
    protected Collection $subjects;

    #[ORM\ManyToMany(targetEntity: self::class, fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'app_mediation_related_mediation')]
    protected Collection $relatedMediations;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->mediationProfessionals = new ArrayCollection();
        $this->subjects = new ArrayCollection();
        $this->relatedMediations = new ArrayCollection();
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getAccessibility(): ?array
    {
        return $this->accessibility;
    }

    public function setAccessibility(?array $accessibility): void
    {
        $this->accessibility = $accessibility;
    }

    public function getPricing(): ?PricingEnum
    {
        return $this->pricing;
    }

    public function setPricing(?PricingEnum $pricing): void
    {
        $this->pricing = $pricing;
    }

    public function getSupervision(): ?SupervisionEnum
    {
        return $this->supervision;
    }

    public function setSupervision(?SupervisionEnum $supervision): void
    {
        $this->supervision = $supervision;
    }

    public function getLevel(): ?array
    {
        return $this->level;
    }

    public function setLevel(?array $level): void
    {
        $this->level = $level;
    }

    public function getMonth(): ?array
    {
        return $this->month;
    }

    public function setMonth(?array $month): void
    {
        $this->month = $month;
    }

    public function getMediationProfessionals(): Collection
    {
        return $this->mediationProfessionals;
    }

    public function addMediationProfessional(MediationProfessional $mediationProfessional): void
    {
        if (!$this->mediationProfessionals->contains($mediationProfessional)) {
            $this->mediationProfessionals->add($mediationProfessional);
            $mediationProfessional->setMediation($this);
        }
    }

    public function removeMediationProfessional(MediationProfessional $mediationProfessional): void
    {
        if ($this->mediationProfessionals->contains($mediationProfessional)) {
            $this->mediationProfessionals->removeElement($mediationProfessional);
            $mediationProfessional->setMediation(null);
        }
    }

    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(MediationSubject $subject): void
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects->add($subject);
            $subject->addMediation($this);
        }
    }

    public function removeSubject(MediationSubject $subject): void
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
            $subject->removeMediation($this);
        }
    }

    public function getRelatedMediations(): Collection
    {
        return $this->relatedMediations;
    }

    public function addRelatedMediation(MediationInterface $mediation): void
    {
        if (!$this->relatedMediations->contains($mediation)) {
            $this->relatedMediations->add($mediation);
            $mediation->addRelatedMediation($this);
        }
    }

    public function removeRelatedMediation(MediationInterface $mediation): void
    {
        if ($this->relatedMediations->contains($mediation)) {
            $this->relatedMediations->removeElement($mediation);
            $mediation->removeRelatedMediation($this);
        }
    }

    public function setPrice(?string $price): void
    {
        $this->getTranslation()->setPrice($price);
    }

    public function getPrice(): ?string
    {
        return $this->getTranslation()->getPrice();
    }

    public function setAudience(?string $audience): void
    {
        $this->getTranslation()->setAudience($audience);
    }

    public function getAudience(): ?string
    {
        return $this->getTranslation()->getAudience();
    }

    public function setPeriod(?string $period): void
    {
        $this->getTranslation()->setPeriod($period);
    }

    public function getPeriod(): ?string
    {
        return $this->getTranslation()->getPeriod();
    }

    public function setSchedule(?string $schedule): void
    {
        $this->getTranslation()->setSchedule($schedule);
    }

    public function getSchedule(): ?string
    {
        return $this->getTranslation()->getSchedule();
    }

    public function setDuration(?string $duration): void
    {
        $this->getTranslation()->setDuration($duration);
    }

    public function getDuration(): ?string
    {
        return $this->getTranslation()->getDuration();
    }

    public function setLocation(?string $location): void
    {
        $this->getTranslation()->setLocation($location);
    }

    public function getLocation(): ?string
    {
        return $this->getTranslation()->getLocation();
    }

    public function setPhone(?string $phone): void
    {
        $this->getTranslation()->setPhone($phone);
    }

    public function getPhone(): ?string
    {
        return $this->getTranslation()->getPhone();
    }
}

<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_mediation_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class MediationTranslation extends BasePageTranslation implements MediationTranslationInterface
{
    #[ORM\Column(name: 'price', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $price = null;

    #[ORM\Column(name: 'audience', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audience = null;

    #[ORM\Column(name: 'period', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $period = null;

    #[ORM\Column(name: 'schedule', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $schedule = null;

    #[ORM\Column(name: 'duration', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $duration = null;

    #[ORM\Column(name: 'location', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $location = null;

    #[ORM\Column(name: 'phone', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $phone = null;

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): self
    {
        $this->audience = $audience;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(?string $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}

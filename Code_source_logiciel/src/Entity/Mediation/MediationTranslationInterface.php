<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface MediationTranslationInterface extends BasePageTranslationInterface
{
    public function getPrice(): ?string;

    public function setPrice(?string $price): self;

    public function getAudience(): ?string;

    public function setAudience(?string $audience): self;

    public function getPeriod(): ?string;

    public function setPeriod(?string $period): self;

    public function getSchedule(): ?string;

    public function setSchedule(?string $schedule): self;

    public function getDuration(): ?string;

    public function setDuration(?string $duration): self;

    public function getLocation(): ?string;

    public function setLocation(?string $location): self;

    public function getPhone(): ?string;

    public function setPhone(?string $phone): self;
}

<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use App\Enum\ColorEnum;
use App\Enum\Mediation\PricingEnum;
use App\Enum\Mediation\SupervisionEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface MediationInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getAccessibility(): ?array;

    public function setAccessibility(?array $accessibility): void;

    public function getPricing(): ?PricingEnum;

    public function setPricing(?PricingEnum $pricing): void;

    public function getSupervision(): ?SupervisionEnum;

    public function setSupervision(?SupervisionEnum $supervision): void;

    public function getLevel(): ?array;

    public function setLevel(?array $level): void;

    public function getMonth(): ?array;

    public function setMonth(?array $month): void;

    public function getMediationProfessionals(): Collection;

    public function addMediationProfessional(MediationProfessional $mediationProfessional): void;

    public function removeMediationProfessional(MediationProfessional $mediationProfessional): void;

    public function getSubjects(): Collection;

    public function addSubject(MediationSubject $subject): void;

    public function removeSubject(MediationSubject $subject): void;

    public function getRelatedMediations(): Collection;

    public function addRelatedMediation(self $mediation): void;

    public function removeRelatedMediation(self $mediation): void;

    public function setPrice(?string $price): void;

    public function getPrice(): ?string;

    public function setAudience(?string $audience): void;

    public function getAudience(): ?string;

    public function setPeriod(?string $period): void;

    public function getPeriod(): ?string;

    public function setSchedule(?string $schedule): void;

    public function getSchedule(): ?string;

    public function setDuration(?string $duration): void;

    public function getDuration(): ?string;

    public function setLocation(?string $location): void;

    public function getLocation(): ?string;

    public function setPhone(?string $phone): void;

    public function getPhone(): ?string;
}

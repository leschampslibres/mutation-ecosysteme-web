<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use App\Repository\Mediation\MediationSubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Taxon\RichTaxon;

#[ORM\Entity(repositoryClass: MediationSubjectRepository::class)]
#[ORM\Table(name: 'app_mediation_subject')]
class MediationSubject extends RichTaxon implements MediationSubjectInterface
{
    #[ORM\ManyToMany(targetEntity: Mediation::class, mappedBy: 'subjects')]
    protected Collection $mediations;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->mediations = new ArrayCollection();
    }

    public function addMediation(Mediation $mediation): void
    {
        if (!$this->mediations->contains($mediation)) {
            $this->mediations->add($mediation);
        }
    }

    public function removeMediation(Mediation $mediation): void
    {
        if ($this->mediations->contains($mediation)) {
            $this->mediations->removeElement($mediation);
        }
    }
}

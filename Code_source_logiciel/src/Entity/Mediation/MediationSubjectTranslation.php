<?php

declare(strict_types=1);

namespace App\Entity\Mediation;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Taxon\RichTaxonTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_mediation_subject_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class MediationSubjectTranslation extends RichTaxonTranslation implements MediationSubjectTranslationInterface
{
}

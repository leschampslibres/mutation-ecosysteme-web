<?php

declare(strict_types=1);

namespace App\Entity\Channel;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Channel as BaseChannel;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_channel')]
#[ORM\AssociationOverrides([
  new ORM\AssociationOverride(
    name: 'baseCurrency',
    joinColumns: new ORM\JoinColumn(
      name: 'base_currency_id',
      referencedColumnName: 'id',
      nullable: true
    ),
  ),
])]
class Channel extends BaseChannel
{
}

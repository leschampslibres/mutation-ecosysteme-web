<?php

declare(strict_types=1);

namespace App\Entity\Article;

use App\Entity\Page\Page;
use App\Enum\ColorEnum;
use App\Repository\Article\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
#[ORM\Table(name: 'app_article')]
#[ORM\Index(name: 'idx_published_at', columns: ['published_at'])]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Article extends BasePage implements ArticleInterface
{
    #[ORM\Column(name: 'published_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTimeInterface $publishedAt = null;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\ManyToOne(targetEntity: ArticleCategory::class)]
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id', nullable: true)]
    protected ?ArticleCategory $category = null;

    #[ORM\ManyToMany(targetEntity: ArticleSubject::class, inversedBy: 'articles', fetch: 'LAZY', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'app_article_article_subject')]
    protected Collection $subjects;

    // Mise en avant d'un article sur la page d'accueil du Mag
    #[ORM\OneToMany(targetEntity: Page::class, mappedBy: 'article')]
    private ?Collection $pages;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->subjects = new ArrayCollection();
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getCategory(): ?ArticleCategory
    {
        return $this->category;
    }

    public function setCategory(?ArticleCategory $category): void
    {
        $this->category = $category;
    }

    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(ArticleSubject $subject): void
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects->add($subject);
            $subject->addArticle($this);
        }
    }

    public function removeSubject(ArticleSubject $subject): void
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
            $subject->removeArticle($this);
        }
    }

    public function getPages(): ?Collection
    {
        return $this->pages;
    }

    public function getAuthor(): ?string
    {
        return $this->getTranslation()->getAuthor();
    }

    public function setAuthor(?string $author): void
    {
        $this->getTranslation()->setAuthor($author);
    }

    public function getLicense(): ?string
    {
        return $this->getTranslation()->getLicense();
    }

    public function setLicense(?string $license): void
    {
        $this->getTranslation()->setLicense($license);
    }
}

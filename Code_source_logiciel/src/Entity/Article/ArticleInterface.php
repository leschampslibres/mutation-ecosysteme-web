<?php

declare(strict_types=1);

namespace App\Entity\Article;

use App\Enum\ColorEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;

interface ArticleInterface extends BasePageInterface
{
    public function getPublishedAt(): ?\DateTimeInterface;

    public function setPublishedAt(?\DateTimeInterface $publishedAt): void;

    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getCategory(): ?ArticleCategory;

    public function setCategory(?ArticleCategory $category): void;

    public function getPages(): ?Collection;

    public function getAuthor(): ?string;

    public function setAuthor(?string $author): void;

    public function getLicense(): ?string;

    public function setLicense(?string $license): void;
}

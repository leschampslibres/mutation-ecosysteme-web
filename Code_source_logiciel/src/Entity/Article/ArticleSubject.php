<?php

declare(strict_types=1);

namespace App\Entity\Article;

use App\Repository\Article\ArticleSubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Taxon\RichTaxon;

#[ORM\Entity(repositoryClass: ArticleSubjectRepository::class)]
#[ORM\Table(name: 'app_article_subject')]
class ArticleSubject extends RichTaxon implements ArticleSubjectInterface
{
    #[ORM\ManyToMany(targetEntity: Article::class, mappedBy: 'subjects')]
    protected Collection $articles;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->articles = new ArrayCollection();
    }

    public function addArticle(Article $article): void
    {
        if (!$this->articles->contains($article)) {
            $this->articles->add($article);
        }
    }

    public function removeArticle(Article $article): void
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
        }
    }
}

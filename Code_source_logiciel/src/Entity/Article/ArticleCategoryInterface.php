<?php

declare(strict_types=1);

namespace App\Entity\Article;

use Luna\CoreBundle\Model\Taxon\RichTaxonInterface;

interface ArticleCategoryInterface extends RichTaxonInterface
{
    public function getIcon(): ?string;

    public function setIcon(?string $icon): void;
}

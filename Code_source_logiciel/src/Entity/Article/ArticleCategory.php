<?php

declare(strict_types=1);

namespace App\Entity\Article;

use App\Repository\Article\ArticleCategoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Taxon\RichTaxon;

#[ORM\Entity(repositoryClass: ArticleCategoryRepository::class)]
#[ORM\Table(name: 'app_article_category')]
class ArticleCategory extends RichTaxon implements ArticleCategoryInterface
{
    #[ORM\Column(name: 'icon', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $icon = null;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }
}

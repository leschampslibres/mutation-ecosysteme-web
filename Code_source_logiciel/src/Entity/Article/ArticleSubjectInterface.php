<?php

declare(strict_types=1);

namespace App\Entity\Article;

use Luna\CoreBundle\Model\Taxon\RichTaxonInterface;

interface ArticleSubjectInterface extends RichTaxonInterface
{
}

<?php

declare(strict_types=1);

namespace App\Entity\Article;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Taxon\RichTaxonTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_article_subject_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class ArticleSubjectTranslation extends RichTaxonTranslation implements ArticleSubjectTranslationInterface
{
}

<?php

declare(strict_types=1);

namespace App\Entity\Article;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ArticleTranslationInterface extends BasePageTranslationInterface
{
    public function getAuthor(): ?string;

    public function setAuthor(?string $author): void;

    public function getLicense(): ?string;

    public function setLicense(?string $license): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\Article;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_article_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class ArticleTranslation extends BasePageTranslation implements ArticleTranslationInterface
{
    #[ORM\Column(name: 'author', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $author = null;

    #[ORM\Column(name: 'license', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $license = null;

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): void
    {
        $this->author = $author;
    }

    public function getLicense(): ?string
    {
        return $this->license;
    }

    public function setLicense(?string $license): void
    {
        $this->license = $license;
    }
}

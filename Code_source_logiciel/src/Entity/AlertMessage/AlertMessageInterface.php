<?php

declare(strict_types=1);

namespace App\Entity\AlertMessage;

use App\Enum\AlertMessage\LevelEnum;
use Luna\CoreBundle\Model\Trait\TimestampableTraitInterface;
use Luna\CoreBundle\Model\Trait\ToggleableTraitInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

interface AlertMessageInterface extends
    ResourceInterface,
    TimestampableTraitInterface,
    ToggleableTraitInterface,
    TranslatableInterface
{
    public function getLevel(): LevelEnum;

    public function setLevel(LevelEnum $level): void;

    public function getStartingAt(): ?\DateTime;

    public function setStartingAt(?\DateTime $startingAt): void;

    public function getContent(): ?string;

    public function setContent(?string $content): void;
}

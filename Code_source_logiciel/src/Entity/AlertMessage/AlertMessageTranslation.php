<?php

declare(strict_types=1);

namespace App\Entity\AlertMessage;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Trait\IdentifiableTrait;
use Luna\CoreBundle\Model\Trait\TimestampableTrait;
use Sylius\Component\Resource\Model\AbstractTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_alert_message_translation')]
class AlertMessageTranslation extends AbstractTranslation implements AlertMessageTranslationInterface
{
    use IdentifiableTrait;
    use TimestampableTrait;

    #[ORM\Column(name: 'content', type: Types::TEXT, nullable: true)]
    protected ?string $content = null;

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }
}

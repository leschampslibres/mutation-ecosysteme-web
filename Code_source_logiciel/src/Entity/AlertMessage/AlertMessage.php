<?php

declare(strict_types=1);

namespace App\Entity\AlertMessage;

use App\Enum\AlertMessage\LevelEnum;
use App\Repository\AlertMessage\AlertMessageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Trait\IdentifiableTrait;
use Luna\CoreBundle\Model\Trait\TimestampableTrait;
use Luna\CoreBundle\Model\Trait\ToggleableTrait;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

#[ORM\Entity(repositoryClass: AlertMessageRepository::class)]
#[ORM\Table(name: 'app_alert_message')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
#[ORM\Index(name: 'idx_starting_at', columns: ['starting_at'])]
#[ORM\Index(name: 'idx_ending_at', columns: ['ending_at'])]
class AlertMessage implements AlertMessageInterface
{
    use IdentifiableTrait;
    use TimestampableTrait;
    use ToggleableTrait;
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
    }

    #[ORM\Column(name: 'level', type: Types::STRING, enumType: LevelEnum::class)]
    protected LevelEnum $level = LevelEnum::High;

    #[ORM\Column(name: 'starting_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $startingAt = null;

    #[ORM\Column(name: 'ending_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    protected ?\DateTime $endingAt = null;

    public function __construct()
    {
        $this->initializeTranslationsCollection();
    }

    public function getLevel(): LevelEnum
    {
        return $this->level;
    }

    public function setLevel(LevelEnum $level): void
    {
        $this->level = $level;
    }

    public function getStartingAt(): ?\DateTime
    {
        return $this->startingAt;
    }

    public function setStartingAt(?\DateTime $startingAt): void
    {
        $this->startingAt = $startingAt;
    }

    public function getEndingAt(): ?\DateTime
    {
        return $this->endingAt;
    }

    public function setEndingAt(?\DateTime $endingAt): void
    {
        $this->endingAt = $endingAt;
    }

    public function getContent(): ?string
    {
        return $this->getTranslation()->getContent();
    }

    public function setContent(?string $content): void
    {
        $this->getTranslation()->setContent($content);
    }

    protected function createTranslation(): TranslationInterface
    {
        return new AlertMessageTranslation();
    }
}

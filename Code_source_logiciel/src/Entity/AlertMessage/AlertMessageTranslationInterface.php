<?php

declare(strict_types=1);

namespace App\Entity\AlertMessage;

use Luna\CoreBundle\Model\Trait\TimestampableTraitInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

interface AlertMessageTranslationInterface extends
    ResourceInterface,
    TimestampableTraitInterface
{
    public function getContent(): ?string;

    public function setContent(?string $content): void;
}

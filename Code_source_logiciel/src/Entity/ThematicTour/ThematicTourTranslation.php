<?php

declare(strict_types=1);

namespace App\Entity\ThematicTour;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;


#[ORM\Entity]
#[ORM\Table(name: 'app_thematic_tour_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class ThematicTourTranslation extends BasePageTranslation implements ThematicTourTranslationInterface
{
    #[ORM\Column(name: 'audience', type: Types::STRING, length: 255, nullable: true)]
    private ?string $audience = null;

    #[ORM\Column(name: 'phone', type: Types::STRING, length: 255, nullable: true)]
    private ?string $phone = null;

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): self
    {
        $this->audience = $audience;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

}

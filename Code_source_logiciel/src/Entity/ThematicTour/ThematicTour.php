<?php

declare(strict_types=1);

namespace App\Entity\ThematicTour;

use App\Enum\ColorEnum;
use App\Enum\ThematicTour\SupervisionEnum;
use App\Repository\ThematicTour\ThematicTourRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: ThematicTourRepository::class)]
#[ORM\Table(name: 'app_thematic_tour')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
#[ORM\Index(name: 'idx_supervision', columns: ['supervision'])]
class ThematicTour extends BasePage implements ThematicTourInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    private ?ColorEnum $color = null;

    #[ORM\Column(name: 'supervision', type: Types::STRING, enumType: SupervisionEnum::class, nullable: true)]
    private ?SupervisionEnum $supervision = null;

    #[ORM\OneToMany(targetEntity: ThematicTourProfessional::class, mappedBy: 'thematicTour', fetch: 'EAGER', cascade: ['persist'], orphanRemoval: true)]
    protected Collection $thematicTourProfessionals;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->thematicTourProfessionals = new ArrayCollection();
    }

    public function getAudience(): ?string
    {
        return $this->getTranslation()->getAudience();
    }

    public function setAudience(?string $audience): void
    {
        $this->getTranslation()->setAudience($audience);
    }

    public function getPhone(): ?string
    {
        return $this->getTranslation()->getPhone();
    }

    public function setPhone(?string $phone): void
    {
        $this->getTranslation()->setPhone($phone);
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getSupervision(): ?SupervisionEnum
    {
        return $this->supervision;
    }

    public function setSupervision(?SupervisionEnum $supervision): void
    {
        $this->supervision = $supervision;
    }

    public function getThematicTourProfessionals(): Collection
    {
        return $this->thematicTourProfessionals;
    }

    public function addThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void
    {
        if (!$this->thematicTourProfessionals->contains($thematicTourProfessional)) {
            $this->thematicTourProfessionals->add($thematicTourProfessional);
            $thematicTourProfessional->setThematicTour($this);
        }
    }

    public function removeThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void
    {
        if ($this->thematicTourProfessionals->contains($thematicTourProfessional)) {
            $this->thematicTourProfessionals->removeElement($thematicTourProfessional);
            $thematicTourProfessional->setThematicTour(null);
        }
    }
}

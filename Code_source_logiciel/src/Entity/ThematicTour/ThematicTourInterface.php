<?php

declare(strict_types=1);

namespace App\Entity\ThematicTour;

use App\Enum\ColorEnum;
use App\Enum\ThematicTour\SupervisionEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface ThematicTourInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getAudience(): ?string;

    public function setAudience(?string $audience): void;

    public function getPhone(): ?string;

    public function setPhone(?string $phone): void;

    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getSupervision(): ?SupervisionEnum;

    public function setSupervision(?SupervisionEnum $supervision): void;

    public function getThematicTourProfessionals(): Collection;

    public function addThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void;

    public function removeThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\ThematicTour;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ThematicTourTranslationInterface extends BasePageTranslationInterface
{
    public function getPhone(): ?string;

    public function setPhone(?string $phone): self;

    public function getAudience(): ?string;

    public function setAudience(?string $audience): self;
}

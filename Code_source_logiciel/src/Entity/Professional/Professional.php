<?php

declare(strict_types=1);

namespace App\Entity\Professional;

use App\Entity\Dossier\DossierProfessional;
use App\Entity\Exhibit\ExhibitProfessional;
use App\Entity\Mediation\MediationProfessional;
use App\Entity\Project\Project;
use App\Entity\ThematicTour\ThematicTourProfessional;
use App\Entity\Tool\ToolProfessional;
use App\Repository\Professional\ProfessionalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Entity\Page\PageInterface;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: ProfessionalRepository::class)]
#[ORM\Table(name: 'app_professional')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Professional extends BasePage implements ProfessionalInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'audience_icon', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audienceIcon = null;

    #[ORM\OneToMany(targetEntity: DossierProfessional::class, mappedBy: 'professional')]
    protected Collection $dossierProfessionals;

    #[ORM\OneToMany(targetEntity: ExhibitProfessional::class, mappedBy: 'professional')]
    protected Collection $exhibitProfessionals;

    #[ORM\OneToMany(targetEntity: MediationProfessional::class, mappedBy: 'professional')]
    protected Collection $mediationProfessionals;

    #[ORM\OneToMany(targetEntity: ThematicTourProfessional::class, mappedBy: 'professional')]
    protected Collection $thematicTourProfessionals;

    #[ORM\ManyToMany(targetEntity: Project::class, mappedBy: 'professionals')]
    protected Collection $projects;

    #[ORM\OneToMany(targetEntity: ToolProfessional::class, mappedBy: 'professional')]
    protected Collection $toolProfessionals;

    // relation inverse de Page::professional
    #[ORM\OneToMany(targetEntity: PageInterface::class, mappedBy: 'professional')]
    protected Collection $pages;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->dossierProfessionals = new ArrayCollection();
        $this->exhibitProfessionals = new ArrayCollection();
        $this->mediationProfessionals = new ArrayCollection();
        $this->thematicTourProfessionals = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->toolProfessionals = new ArrayCollection();
    }

    public function getAudienceIcon(): ?string
    {
        return $this->audienceIcon;
    }

    public function setAudienceIcon(?string $audienceIcon): void
    {
        $this->audienceIcon = $audienceIcon;
    }

    public function getDossierProfessionals(): Collection
    {
        return $this->dossierProfessionals;
    }

    public function addDossierProfessional(DossierProfessional $dossierProfessional): void
    {
        if (!$this->dossierProfessionals->contains($dossierProfessional)) {
            $this->dossierProfessionals->add($dossierProfessional);
        }
    }

    public function removeDossierProfessional(DossierProfessional $dossierProfessional): void
    {
        if ($this->dossierProfessionals->contains($dossierProfessional)) {
            $this->dossierProfessionals->removeElement($dossierProfessional);
        }
    }

    public function getExhibitProfessionals(): Collection
    {
        return $this->exhibitProfessionals;
    }

    public function addExhibitProfessional(ExhibitProfessional $exhibitProfessional): void
    {
        if (!$this->exhibitProfessionals->contains($exhibitProfessional)) {
            $this->exhibitProfessionals->add($exhibitProfessional);
        }
    }

    public function removeExhibitProfessional(ExhibitProfessional $exhibitProfessional): void
    {
        if ($this->exhibitProfessionals->contains($exhibitProfessional)) {
            $this->exhibitProfessionals->removeElement($exhibitProfessional);
        }
    }

    public function getMediationProfessionals(): Collection
    {
        return $this->mediationProfessionals;
    }

    public function addMediationProfessional(MediationProfessional $mediationProfessional): void
    {
        if (!$this->mediationProfessionals->contains($mediationProfessional)) {
            $this->mediationProfessionals->add($mediationProfessional);
        }
    }

    public function removeMediationProfessional(MediationProfessional $mediationProfessional): void
    {
        if ($this->mediationProfessionals->contains($mediationProfessional)) {
            $this->mediationProfessionals->removeElement($mediationProfessional);
        }
    }

    public function getThematicTourProfessionals(): Collection
    {
        return $this->thematicTourProfessionals;
    }

    public function addThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void
    {
        if (!$this->thematicTourProfessionals->contains($thematicTourProfessional)) {
            $this->thematicTourProfessionals->add($thematicTourProfessional);
        }
    }

    public function removeThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void
    {
        if ($this->thematicTourProfessionals->contains($thematicTourProfessional)) {
            $this->thematicTourProfessionals->removeElement($thematicTourProfessional);
        }
    }

    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): void
    {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
        }
    }

    public function removeProject(Project $project): void
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
        }
    }

    public function getToolProfessionals(): Collection
    {
        return $this->toolProfessionals;
    }

    public function addToolProfessional(ToolProfessional $toolProfessional): void
    {
        if (!$this->toolProfessionals->contains($toolProfessional)) {
            $this->toolProfessionals->add($toolProfessional);
        }
    }

    public function removeToolProfessional(ToolProfessional $toolProfessional): void
    {
        if ($this->toolProfessionals->contains($toolProfessional)) {
            $this->toolProfessionals->removeElement($toolProfessional);
        }
    }

    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function setAudienceLabel(?string $audienceLabel): void
    {
        $this->getTranslation()->setAudienceLabel($audienceLabel);
    }

    public function getAudienceLabel(): ?string
    {
        return $this->getTranslation()->getAudienceLabel();
    }
}

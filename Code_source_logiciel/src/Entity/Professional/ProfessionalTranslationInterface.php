<?php

declare(strict_types=1);

namespace App\Entity\Professional;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ProfessionalTranslationInterface extends BasePageTranslationInterface
{
    public function getAudienceLabel(): ?string;

    public function setAudienceLabel(?string $audienceLabel): void;
}

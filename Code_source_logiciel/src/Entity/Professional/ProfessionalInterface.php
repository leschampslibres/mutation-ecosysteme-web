<?php

declare(strict_types=1);

namespace App\Entity\Professional;

use App\Entity\Dossier\DossierProfessional;
use App\Entity\Exhibit\ExhibitProfessional;
use App\Entity\Mediation\MediationProfessional;
use App\Entity\Project\Project;
use App\Entity\ThematicTour\ThematicTourProfessional;
use App\Entity\Tool\ToolProfessional;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface ProfessionalInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getAudienceIcon(): ?string;

    public function setAudienceIcon(?string $audienceIcon): void;

    public function getDossierProfessionals(): Collection;

    public function addDossierProfessional(DossierProfessional $dossierProfessional): void;

    public function removeDossierProfessional(DossierProfessional $dossierProfessional): void;

    public function getExhibitProfessionals(): Collection;

    public function addExhibitProfessional(ExhibitProfessional $exhibitProfessional): void;

    public function removeExhibitProfessional(ExhibitProfessional $exhibitProfessional): void;

    public function getMediationProfessionals(): Collection;

    public function addMediationProfessional(MediationProfessional $mediationProfessional): void;

    public function removeMediationProfessional(MediationProfessional $mediationProfessional): void;

    public function getProjects(): Collection;

    public function addProject(Project $project): void;

    public function removeProject(Project $project): void;

    public function getThematicTourProfessionals(): Collection;

    public function addThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void;

    public function removeThematicTourProfessional(ThematicTourProfessional $thematicTourProfessional): void;

    public function getToolProfessionals(): Collection;

    public function addToolProfessional(ToolProfessional $toolProfessional): void;

    public function removeToolProfessional(ToolProfessional $toolProfessional): void;

    public function setAudienceLabel(?string $audienceLabel): void;

    public function getAudienceLabel(): ?string;
}

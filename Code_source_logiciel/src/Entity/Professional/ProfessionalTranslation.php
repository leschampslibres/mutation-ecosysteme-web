<?php

declare(strict_types=1);

namespace App\Entity\Professional;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_professional_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class ProfessionalTranslation extends BasePageTranslation implements ProfessionalTranslationInterface
{
    #[ORM\Column(name: 'audience_icon', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audienceLabel = null;

    public function getAudienceLabel(): ?string
    {
        return $this->audienceLabel;
    }

    public function setAudienceLabel(?string $audienceLabel): void
    {
        $this->audienceLabel = $audienceLabel;
    }
}

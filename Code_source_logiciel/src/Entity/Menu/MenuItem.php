<?php

declare(strict_types=1);

namespace App\Entity\Menu;

use App\Enum\MenuItem\IconEnum;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MonsieurBiz\SyliusMenuPlugin\Entity\MenuItem as BaseMenuItem;

#[ORM\Entity]
#[ORM\Table(name: 'monsieurbiz_menu_item')]
class MenuItem extends BaseMenuItem
{
    #[ORM\Column(name: 'icon', type: Types::STRING, enumType: IconEnum::class, nullable: true)]
    protected ?IconEnum $icon = null;

    #[ORM\Column(name: 'hide_label', type: Types::BOOLEAN, nullable: true)]
    protected ?bool $hideLabel = null;

    public function getIcon(): ?IconEnum
    {
        return $this->icon;
    }

    public function setIcon(?IconEnum $icon): void
    {
        $this->icon = $icon;
    }

    public function getHideLabel(): ?bool
    {
        return $this->hideLabel;
    }

    public function setHideLabel(?bool $hideLabel): void
    {
        $this->hideLabel = $hideLabel;
    }

    public function getDescription(): ?string
    {
        return $this->getTranslation()->getDescription();
    }
}

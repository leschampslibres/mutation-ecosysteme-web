<?php

declare(strict_types=1);

namespace App\Entity\Menu;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MonsieurBiz\SyliusMenuPlugin\Entity\Menu as BaseMenu;

#[ORM\Entity]
#[ORM\Table(name: 'monsieurbiz_menu')]
class Menu extends BaseMenu
{
    #[ORM\Column(name: 'title', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $title = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
}

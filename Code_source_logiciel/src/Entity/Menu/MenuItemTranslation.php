<?php

declare(strict_types=1);

namespace App\Entity\Menu;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use MonsieurBiz\SyliusMenuPlugin\Entity\MenuItemTranslation as BaseMenuItemTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'monsieurbiz_menu_item_translation')]
class MenuItemTranslation extends BaseMenuItemTranslation
{
    #[ORM\Column(name: 'description', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(name: 'language', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $language = null;

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description ? trim($description) : null;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language ? trim($language) : null;
    }
}

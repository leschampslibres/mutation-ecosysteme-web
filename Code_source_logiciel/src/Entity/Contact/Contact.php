<?php

declare(strict_types=1);

namespace App\Entity\Contact;

use App\Enum\Contact\ServiceEnum;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Contact\Contact as BaseContact;
use Luna\CoreBundle\Repository\Contact\ContactRepository;

#[ORM\Entity]
#[ORM\Table(name: 'app_contact')]
class Contact extends BaseContact
{
    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $company = null;

    #[ORM\Column(type: Types::STRING, length: 255, enumType: ServiceEnum::class)]
    private ?ServiceEnum $serviceName = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $serviceEmail = null;

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): static
    {
        $this->company = $company;

        return $this;
    }

    public function getServiceName(): ?ServiceEnum
    {
        return $this->serviceName;
    }

    public function setServiceName(ServiceEnum $serviceName): static
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    public function getServiceEmail(): ?string
    {
        return $this->serviceEmail;
    }

    public function setServiceEmail(string $serviceEmail): static
    {
        $this->serviceEmail = $serviceEmail;

        return $this;
    }
}

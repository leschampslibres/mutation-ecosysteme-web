<?php

declare(strict_types=1);

namespace App\Entity\Contact;

use App\Enum\Contact\ServiceEnum;
use Luna\CoreBundle\Model\Contact\ContactInterface as BaseContactInterface;

interface ContactInterface extends BaseContactInterface
{
    public function getCompany(): ?string;

    public function setCompany(?string $company): static;

    public function getServiceName(): ?ServiceEnum;

    public function setServiceName(ServiceEnum $serviceName): static;

    public function getServiceEmail(): ?string;

    public function setServiceEmail(string $serviceEmail): static;
}

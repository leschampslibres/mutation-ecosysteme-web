<?php

declare(strict_types=1);

namespace App\Entity\Dossier;

use App\Enum\ColorEnum;
use App\Repository\Dossier\DossierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: DossierRepository::class)]
#[ORM\Table(name: 'app_dossier')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Dossier extends BasePage implements DossierInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\OneToMany(targetEntity: DossierProfessional::class, mappedBy: 'dossier', fetch: 'EAGER', cascade: ['persist'], orphanRemoval: true)]
    protected Collection $dossierProfessionals;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->dossierProfessionals = new ArrayCollection();
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function setAudience(?string $audience): void
    {
        $this->getTranslation()->setAudience($audience);
    }

    public function getAudience(): ?string
    {
        return $this->getTranslation()->getAudience();
    }

    public function getDossierProfessionals(): Collection
    {
        return $this->dossierProfessionals;
    }

    public function addDossierProfessional(DossierProfessional $dossierProfessional): void
    {
        if (!$this->dossierProfessionals->contains($dossierProfessional)) {
            $this->dossierProfessionals->add($dossierProfessional);
            $dossierProfessional->setDossier($this);
        }
    }

    public function removeDossierProfessional(DossierProfessional $dossierProfessional): void
    {
        if ($this->dossierProfessionals->contains($dossierProfessional)) {
            $this->dossierProfessionals->removeElement($dossierProfessional);
            $dossierProfessional->setDossier(null);
        }
    }
}

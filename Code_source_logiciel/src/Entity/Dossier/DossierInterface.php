<?php

declare(strict_types=1);

namespace App\Entity\Dossier;

use App\Enum\ColorEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface DossierInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function setAudience(?string $audience): void;

    public function getAudience(): ?string;

    public function getDossierProfessionals(): Collection;

    public function addDossierProfessional(DossierProfessional $dossierProfessional): void;

    public function removeDossierProfessional(DossierProfessional $dossierProfessional): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\Dossier;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_dossier_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class DossierTranslation extends BasePageTranslation implements DossierTranslationInterface
{
    #[ORM\Column(name: 'audience', type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audience = null;

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): void
    {
        $this->audience = $audience;
    }
}

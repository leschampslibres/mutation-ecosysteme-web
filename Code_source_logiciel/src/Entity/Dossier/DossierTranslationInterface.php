<?php

declare(strict_types=1);

namespace App\Entity\Dossier;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface DossierTranslationInterface extends BasePageTranslationInterface
{
    public function getAudience(): ?string;

    public function setAudience(?string $audience): void;
}

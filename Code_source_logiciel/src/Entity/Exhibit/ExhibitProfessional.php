<?php

declare(strict_types=1);

namespace App\Entity\Exhibit;

use App\Entity\Professional\Professional;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'app_exhibits_professionals')]
class ExhibitProfessional
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(name: 'id', type: Types::INTEGER, options: ['unsigned' => true])]
    protected ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Exhibit::class, inversedBy: 'exhibitProfessionals')]
    #[ORM\JoinColumn(name: 'exhibit_id', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    protected ?Exhibit $exhibit = null;

    #[ORM\ManyToOne(targetEntity: Professional::class, inversedBy: 'exhibitProfessionals')]
    #[ORM\JoinColumn(name: 'professional_id', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    protected Professional $professional;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    protected ?string $audience = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExhibit(): Exhibit
    {
        return $this->exhibit;
    }

    public function setExhibit(?Exhibit $exhibit): void
    {
        $this->exhibit = $exhibit;
    }

    public function getProfessional(): Professional
    {
        return $this->professional;
    }

    public function setProfessional(Professional $professional): void
    {
        $this->professional = $professional;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setAudience(?string $audience): void
    {
        $this->audience = $audience;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity\Exhibit;

use App\Enum\ColorEnum;
use App\Enum\Mediation\PricingEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface ExhibitInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getPricing(): ?PricingEnum;

    public function setPricing(?PricingEnum $pricing): void;

    public function setAudience(?string $audience): void;

    public function getAudience(): ?string;

    public function getExhibitProfessionals(): Collection;

    public function addExhibitProfessional(ExhibitProfessional $exhibitProfessional): void;

    public function removeExhibitProfessional(ExhibitProfessional $exhibitProfessional): void;
}

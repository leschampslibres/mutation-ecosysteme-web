<?php

declare(strict_types=1);

namespace App\Entity\Exhibit;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ExhibitTranslationInterface extends BasePageTranslationInterface
{
    public function getAudience(): ?string;

    public function setAudience(?string $audience): void;
}

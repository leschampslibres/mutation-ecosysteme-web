<?php

declare(strict_types=1);

namespace App\Entity\Exhibit;

use App\Enum\ColorEnum;
use App\Enum\Mediation\PricingEnum;
use App\Repository\Exhibit\ExhibitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: ExhibitRepository::class)]
#[ORM\Table(name: 'app_exhibit')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Exhibit extends BasePage implements ExhibitInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\Column(name: 'pricing', type: Types::STRING, enumType: PricingEnum::class, nullable: true)]
    protected ?PricingEnum $pricing = null;

    #[ORM\OneToMany(targetEntity: ExhibitProfessional::class, mappedBy: 'exhibit', fetch: 'EAGER', cascade: ['persist'], orphanRemoval: true)]
    protected Collection $exhibitProfessionals;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->exhibitProfessionals = new ArrayCollection();
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getPricing(): ?PricingEnum
    {
        return $this->pricing;
    }

    public function setPricing(?PricingEnum $pricing): void
    {
        $this->pricing = $pricing;
    }

    public function setAudience(?string $audience): void
    {
        $this->getTranslation()->setAudience($audience);
    }

    public function getAudience(): ?string
    {
        return $this->getTranslation()->getAudience();
    }

    public function getExhibitProfessionals(): Collection
    {
        return $this->exhibitProfessionals;
    }

    public function addExhibitProfessional(ExhibitProfessional $exhibitProfessional): void
    {
        if (!$this->exhibitProfessionals->contains($exhibitProfessional)) {
            $this->exhibitProfessionals->add($exhibitProfessional);
            $exhibitProfessional->setExhibit($this);
        }
    }

    public function removeExhibitProfessional(ExhibitProfessional $exhibitProfessional): void
    {
        if ($this->exhibitProfessionals->contains($exhibitProfessional)) {
            $this->exhibitProfessionals->removeElement($exhibitProfessional);
            $exhibitProfessional->setExhibit(null);
        }
    }
}

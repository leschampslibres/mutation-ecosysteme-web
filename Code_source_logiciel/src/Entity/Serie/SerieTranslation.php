<?php

declare(strict_types=1);

namespace App\Entity\Serie;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_serie_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class SerieTranslation extends BasePageTranslation implements SerieTranslationInterface
{
}

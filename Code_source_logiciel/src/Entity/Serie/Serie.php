<?php

declare(strict_types=1);

namespace App\Entity\Serie;

use App\Enum\ColorEnum;
use App\Repository\Serie\SerieRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;

#[ORM\Entity(repositoryClass: SerieRepository::class)]
#[ORM\Table(name: 'app_serie')]
#[ORM\Index(name: 'idx_published_at', columns: ['published_at'])]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Serie extends BasePage implements SerieInterface
{
    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    private ?ColorEnum $color = null;

    #[ORM\Column(name: 'published_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $publishedAt = null;

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }
}

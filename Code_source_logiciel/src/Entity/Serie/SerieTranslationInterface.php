<?php

declare(strict_types=1);

namespace App\Entity\Serie;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface SerieTranslationInterface extends BasePageTranslationInterface
{
}

<?php

declare(strict_types=1);

namespace App\Entity\Serie;

use App\Enum\ColorEnum;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;

interface SerieInterface extends BasePageInterface
{
    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getPublishedAt(): ?\DateTimeInterface;

    public function setPublishedAt(\DateTimeInterface $publishedAt): void;
}

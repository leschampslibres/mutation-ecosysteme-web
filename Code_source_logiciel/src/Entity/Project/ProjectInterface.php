<?php

declare(strict_types=1);

namespace App\Entity\Project;

use App\Entity\Professional\Professional;
use App\Enum\ColorEnum;
use App\Enum\Project\StatusEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface ProjectInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getSchool(): ?string;

    public function setSchool(string $school): void;

    public function getStatus(): ?StatusEnum;

    public function setStatus(StatusEnum $status): void;

    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function getDate(): ?string;

    public function setDate(?string $date): void;

    public function getProfessionals(): Collection;

    public function addProfessional(Professional $professional): void;

    public function removeProfessional(Professional $professional): void;
}

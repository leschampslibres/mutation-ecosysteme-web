<?php

declare(strict_types=1);

namespace App\Entity\Project;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ProjectTranslationInterface extends BasePageTranslationInterface
{
}

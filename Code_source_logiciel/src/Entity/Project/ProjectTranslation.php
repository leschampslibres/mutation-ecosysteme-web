<?php

declare(strict_types=1);

namespace App\Entity\Project;

use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\PageTranslation as BasePageTranslation;

#[ORM\Entity]
#[ORM\Table(name: 'app_project_translation')]
#[ORM\Index(name: 'idx_slug', columns: ['slug'])]
#[ORM\Index(name: 'idx_locale', columns: ['locale'])]
class ProjectTranslation extends BasePageTranslation implements ProjectTranslationInterface
{
}

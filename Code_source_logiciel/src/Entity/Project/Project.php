<?php

declare(strict_types=1);

namespace App\Entity\Project;

use App\Entity\Professional\Professional;
use App\Enum\ColorEnum;
use App\Enum\Project\StatusEnum;
use App\Repository\Project\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
#[ORM\Table(name: 'app_project')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Project extends BasePage implements ProjectInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'school', type: Types::STRING, nullable: true)]
    private ?string $school = null;

    #[ORM\Column(name: 'status', type: Types::STRING, enumType: StatusEnum::class, nullable: true)]
    private ?StatusEnum $status = null;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    private ?ColorEnum $color = null;

    #[ORM\Column(name: 'date', type: Types::STRING, nullable: true)]
    private ?string $date = null;

    #[ORM\ManyToMany(targetEntity: Professional::class, inversedBy: 'project')]
    #[ORM\JoinTable(name: 'app_projects_professionals')]
    protected Collection $professionals;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->professionals = new ArrayCollection();
    }

    public function getSchool(): ?string
    {
        return $this->school;
    }

    public function setSchool(string $school): void
    {
        $this->school = $school;
    }

    public function getStatus(): ?StatusEnum
    {
        return $this->status;
    }

    public function setStatus(StatusEnum $status): void
    {
        $this->status = $status;
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): void
    {
        $this->date = $date;
    }

    public function getProfessionals(): Collection
    {
        return $this->professionals;
    }

    public function addProfessional(Professional $professional): void
    {
        if (!$this->professionals->contains($professional)) {
            $this->professionals->add($professional);
            $professional->addProject($this);
        }
    }

    public function removeProfessional(Professional $professional): void
    {
        if ($this->professionals->contains($professional)) {
            $this->professionals->removeElement($professional);
            $professional->removeProject($this);
        }
    }
}

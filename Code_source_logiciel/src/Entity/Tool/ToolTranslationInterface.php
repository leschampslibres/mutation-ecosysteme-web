<?php

declare(strict_types=1);

namespace App\Entity\Tool;

use Luna\CoreBundle\Model\Page\PageTranslationInterface as BasePageTranslationInterface;

interface ToolTranslationInterface extends BasePageTranslationInterface
{
    public function getAudience(): ?string;

    public function setAudience(?string $audience): void;
}

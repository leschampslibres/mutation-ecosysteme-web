<?php

declare(strict_types=1);

namespace App\Entity\Tool;

use App\Enum\ColorEnum;
use Doctrine\Common\Collections\Collection;
use Luna\CoreBundle\Model\Page\PageInterface as BasePageInterface;
use Luna\CoreBundle\Model\Trait\PositionnableTraitInterface;

interface ToolInterface extends BasePageInterface, PositionnableTraitInterface
{
    public function getColor(): ?ColorEnum;

    public function setColor(?ColorEnum $color): void;

    public function setAudience(?string $audience): void;

    public function getAudience(): ?string;

    public function getToolProfessionals(): Collection;

    public function addToolProfessional(ToolProfessional $toolProfessional): void;

    public function removeToolProfessional(ToolProfessional $toolProfessional): void;
}

<?php

declare(strict_types=1);

namespace App\Entity\Tool;

use App\Enum\ColorEnum;
use App\Repository\Tool\ToolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Luna\CoreBundle\Model\Page\Page as BasePage;
use Luna\CoreBundle\Model\Trait\PositionnableTrait;

#[ORM\Entity(repositoryClass: ToolRepository::class)]
#[ORM\Table(name: 'app_tool')]
#[ORM\Index(name: 'idx_enabled', columns: ['enabled'])]
class Tool extends BasePage implements ToolInterface
{
    use PositionnableTrait;

    #[ORM\Column(name: 'color', type: Types::STRING, enumType: ColorEnum::class, nullable: true)]
    protected ?ColorEnum $color = null;

    #[ORM\OneToMany(targetEntity: ToolProfessional::class, mappedBy: 'tool', fetch: 'EAGER', cascade: ['persist'], orphanRemoval: true)]
    protected Collection $toolProfessionals;

    public function __construct()
    {
        // Ne pas oublier pour les traductions (erreur "Call to a member function containsKey() on null")
        parent::__construct();

        $this->toolProfessionals = new ArrayCollection();
    }

    public function getColor(): ?ColorEnum
    {
        return $this->color;
    }

    public function setColor(?ColorEnum $color): void
    {
        $this->color = $color;
    }

    public function setAudience(?string $audience): void
    {
        $this->getTranslation()->setAudience($audience);
    }

    public function getAudience(): ?string
    {
        return $this->getTranslation()->getAudience();
    }

    public function getToolProfessionals(): Collection
    {
        return $this->toolProfessionals;
    }

    public function addToolProfessional(ToolProfessional $toolProfessional): void
    {
        if (!$this->toolProfessionals->contains($toolProfessional)) {
            $this->toolProfessionals->add($toolProfessional);
            $toolProfessional->setTool($this);
        }
    }

    public function removeToolProfessional(ToolProfessional $toolProfessional): void
    {
        if ($this->toolProfessionals->contains($toolProfessional)) {
            $this->toolProfessionals->removeElement($toolProfessional);
            $toolProfessional->setTool(null);
        }
    }
}

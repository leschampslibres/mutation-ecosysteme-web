<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Event\Event;
use App\Entity\Event\EventCategory;
use App\Entity\Event\EventCategoryTranslation;
use App\Entity\Event\EventKeyword;
use App\Entity\Event\EventOccurrence;
use App\Entity\Event\EventTranslation;
use App\Enum\Event\SearchDisplayPositionEnum;
use App\Repository\Event\EventCategoryRepository;
use App\Repository\Event\EventKeywordRepository;
use App\Repository\Event\EventOccurrenceRepository;
use App\Repository\Event\EventRepository;
use Doctrine\ORM\EntityManager;
use Luna\CoreBundle\Service\AirbrakeService;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenAgendaApiService
{
    protected EventRepository $eventRepository;

    protected EventKeywordRepository $eventKeywordRepository;

    protected EventOccurrenceRepository $eventOccurrenceRepository;

    protected EventCategoryRepository $eventCategoryRepository;

    protected string $imageFullDirectory;

    protected string $imageGalleryDirectory;

    public function __construct(
        protected string $agendaId,
        protected string $apiKey,
        protected string $publicDirectory,
        protected string $mediaDirectory,
        protected EntityManager $entityManager,
        protected HttpClientInterface $client,
        protected SluggerInterface $slugger,
        protected AirbrakeService $airbrakeService
    ) {
        $this->eventRepository = $this->entityManager->getRepository(Event::class);
        $this->eventKeywordRepository = $this->entityManager->getRepository(EventKeyword::class);
        $this->eventOccurrenceRepository = $this->entityManager->getRepository(EventOccurrence::class);
        $this->eventCategoryRepository = $this->entityManager->getRepository(EventCategory::class);
        $this->imageGalleryDirectory = 'gallery/images/open-agenda';
        $this->imageFullDirectory = rtrim($publicDirectory, '/') . '/' . rtrim($mediaDirectory, '/') . '/' . $this->imageGalleryDirectory;
    }

    public function batchEvents(bool $all, ?array $after = null): void
    {
        $more = $this->saveEvents($all, $after);

        if (null !== $more) {
            $this->batchEvents($all, $more);
        }
    }

    protected function saveEvents(bool $all, ?array $after = null): mixed
    {
        $events = $this->getEventsFromApi($all, $after);

        foreach ($events->events as $eventData) {
            $this->saveEvent($eventData);
        }

        return $events->after;
    }

    protected function saveEvent(\stdClass $eventData): void
    {
        $event = $this->eventRepository->findOneBy(['idOa' => $eventData->uid]);
        // Cas d'un nouvel événement
        if (null === $event) {
            $event = new Event();
            $eventTranslation = new EventTranslation();
            // Il faut définir le statut de publication en fonction de divers critères.
            $setEnabled = true;

        // Cas d'un événement existant nettoyé, qu'on ne doit pas reremplir
        // Attention, certains événements peuvent être réouverts
        // (ex : "Incroyable cerveau" a de nouvelles occurrences chaque année, ajoutées après que l'événement ait été considéré comme terminé)
        } elseif ($event->getStatus() >= Event::STATUS_ENDED && new \DateTime($eventData->lastTiming->begin) <= new \DateTime()) {
            return;
        // Cas des autres événements existants, à mettre à jour
        } else {
            $eventTranslation = $event->getTranslation('fr_FR');
            // Le statut de publication a déjà été défini et ne doit pas être modifié.
            $setEnabled = false;
        }

        $eventTranslation->setLocale('fr_FR');

        // Éléments non traduits

        $event->setIdOa($eventData->uid);
        // Modification du statut uniquement si l'événement n'est pas terminé/nettoyé
        // Attention, certains événements peuvent être réouverts
        // (ex : "Incroyable cerveau" a de nouvelles occurrences chaque année, ajoutées après que l'événement ait été considéré comme terminé)
        if ($event->getStatus() < Event::STATUS_COMPLETED || new \DateTime($eventData->lastTiming->begin) > new \DateTime()) {
            $event->setStatus($eventData->status);
        }
        $event->setOaUpdatedAt(new \DateTime());
        $event->setAudience($this->checkProperty($eventData, 'publics', 'integer') ? $eventData->publics : null);

        // Propriétés pour lesquelles il vaut mieux vérifier ce qui est transmis par Open Agenda
        $event->setOriginAgenda($this->checkProperty($eventData, 'originAgenda', 'object') ? $eventData->originAgenda->uid : null);
        $event->setLocation($this->checkProperty($eventData, 'location', 'object') ? $eventData->location->uid : null);
        $event->setMainImageCredits($this->checkProperty($eventData, 'imageCredits', 'string') ? $eventData->imageCredits : null);
        if ($this->checkProperty($eventData, 'conditions', 'string')) {
            $conditions = explode(' ', $eventData->conditions);
            $event->setPricing(strtolower(trim($conditions[0])));
            $event->setPrice(\array_key_exists(1, $conditions) ? (string) (trim(str_replace('€', '', $conditions[1]))) : null);
        } else {
            $event->setPricing(null);
            $event->setPrice(null);
        }

        if ($this->checkProperty($eventData, 'registration', 'array') && false !== reset($eventData->registration)) {
            $event->setTicketing(reset($eventData->registration)->value);
        } else {
            $event->setTicketing(null);
        }
        if ($this->checkProperty($eventData, 'age', 'object')) {
            $event->setAgeMin($eventData->age->min);
            // Si l'âge maximum est supérieur à 80, on le considère comme non défini.
            $event->setAgeMax((80 > $eventData->age->max) ? $eventData->age->max : null);
        } else {
            $event->setAgeMin(null);
            $event->setAgeMax(null);
        }
        if ($this->checkProperty($eventData, 'accessibility', 'object')) {
            $event->setAccessibilityIi($eventData->accessibility->ii);
            $event->setAccessibilityHi($eventData->accessibility->hi);
            $event->setAccessibilityVi($eventData->accessibility->vi);
            $event->setAccessibilityPi($eventData->accessibility->pi);
            $event->setAccessibilityMi($eventData->accessibility->mi);
        } else {
            $event->setAccessibilityIi(false);
            $event->setAccessibilityHi(false);
            $event->setAccessibilityVi(false);
            $event->setAccessibilityPi(false);
            $event->setAccessibilityMi(false);
        }

        // Éléments traduits

        if ($this->checkProperty($eventData, 'title', 'string')) {
            $eventTranslation->setTitle($eventData->title);
        } else {
            $eventTranslation->setTitle(null);
            $error = 'Titre manquant';
            $this->airbrakeService->notifyMessage($this->notificationMessage($error, $event));
        }
        $eventTranslation->setDescription($eventData->longDescription);

        $event->addTranslation($eventTranslation);

        // Image principale

        $this->saveMainImage($event, $eventTranslation, $eventData);

        // Catégorie

        $this->saveCategory($event, $eventData);

        // Mots-clés

        if ($this->checkProperty($eventData, 'keywords', 'array')) {
            foreach ($eventData->keywords as $keyword) {
                if (!empty($keyword)) {
                    $eventKeyword = $this->eventKeywordRepository->findOneBy(['title' => $keyword]);
                    if (null === $eventKeyword) {
                        $eventKeyword = new EventKeyword();
                        $eventKeyword->setTitle($keyword);
                    }
                    $event->addKeyword($eventKeyword);
                }
            }

            // Suppression des mots-clés qui ne sont plus dans OpenAgenda
            $databaseEventKeywords = $event->getKeywords();
            foreach ($databaseEventKeywords as $databaseEventKeyword) {
                if (!\in_array($databaseEventKeyword->getTitle(), $eventData->keywords, true)) {
                    $event->removeKeyword($databaseEventKeyword);
                }
            }
        }

        // Occurrences

        if ($this->checkProperty($eventData, 'timings', 'array')) {
            $eventData->timings;
            $first = reset($eventData->timings);
            $last = end($eventData->timings);
            $openAgendaEventOccurrences = [];
            $format = 'Y-m-d H:i:s';

            // Traitement des occurrences d'OpenAgenda
            foreach ($eventData->timings as $timing) {
                // Récupération des occurrences existantes
                $eventOccurrence = $this->eventOccurrenceRepository->findOneBy(['event' => $event, 'startingAt' => new \DateTime($timing->begin), 'endingAt' => new \DateTime($timing->end)]);

                // Ajout des nouvelles occurrences
                if (null === $eventOccurrence) {
                    $eventOccurrence = new EventOccurrence();
                    $eventOccurrence->setStartingAt(new \DateTime($timing->begin));
                    $eventOccurrence->setEndingAt(new \DateTime($timing->end));
                    $event->addOccurrence($eventOccurrence);
                }

                // Date de début et date de fin globales de l'événement
                if ($timing === $first) {
                    $event->setStartingAt($eventOccurrence->getStartingAt());
                }
                if ($timing === $last) {
                    $event->setEndingAt($eventOccurrence->getEndingAt());
                }

                // Préparation pour suppression des occurrences qui ne sont plus dans OpenAgenda
                $openAgendaEventOccurrences[date_format(new \DateTime($timing->begin), $format)] = date_format(new \DateTime($timing->end), $format);
            }

            // Suppression des occurrences qui ne sont plus dans OpenAgenda
            $databaseEventOccurrences = $event->getOccurrences();
            foreach ($databaseEventOccurrences as $databaseEventOccurrence) {
                $dbStartingAt = date_format($databaseEventOccurrence->getStartingAt(), $format);
                $dbEndingAt = date_format($databaseEventOccurrence->getEndingAt(), $format);
                $correspondingOaStartingAt = \array_key_exists($dbStartingAt, $openAgendaEventOccurrences);
                $correspondingOaEndingAt = $correspondingOaStartingAt ? ($openAgendaEventOccurrences[$dbStartingAt] == $dbEndingAt) : false;
                if (!$correspondingOaStartingAt || !$correspondingOaEndingAt) {
                    $event->removeOccurrence($databaseEventOccurrence);
                }
            }
        }

        // Vérification pour le statut de publication de l'événement, uniquement
        // si c'est un nouvel événement (pour ne pas surcharger une modification
        // manuelle du statut de publication).
        // L'événement n'est pas publié si la catégorie a eventAutoEnabled à false.
        if ($setEnabled) {
            $autoEnabled = $event->getCategory()->getEventAutoEnabled();
            $hasTitle = !empty($eventTranslation->getTitle());
            $enabled = $autoEnabled && $hasTitle;
            $event->setEnabled($enabled);
        }

        $event->setEnabled(true);

        $this->entityManager->persist($event);
        $this->entityManager->persist($eventTranslation);
        $this->entityManager->flush();
    }

    protected function checkProperty(\stdClass $eventData, string $property, ?string $type = null): bool
    {
        if (!property_exists($eventData, $property)) {
            return false;
        }
        if (null !== $type && \gettype($eventData->{$property}) !== $type) {
            return false;
        }

        return true;
    }

    protected function saveMainImage(Event $event, EventTranslation $eventTranslation, \stdClass $eventData): void
    {
        // S'il n'y a pas d'image principale, on nullifie l'image principale de l'événement
        if (null === $eventData->image) {
            $eventTranslation->setMainImage(null);
        } else {
            // On remplace l'image à chaque fois, au cas où elle ait changé ou si le titre
            // de l'event a changé (la nouvelle image a le bon nom de fichier dans ce cas).
            $url = $eventData->image->base . $eventData->image->filename;
            $title = $eventData->title;

            // Vérification du répertoire de stockage des images provenant d'OpenAgenda
            if (!is_dir($this->imageFullDirectory)) {
                @mkdir($this->imageFullDirectory, 0777, true);
            }

            // Nommage
            $filename = basename($url);
            $fileExtension = pathinfo($filename, \PATHINFO_EXTENSION);
            $slugifiedTitle = $event->getIdOa() . '-' . $this->slugger->slug($title)->lower()->toString();
            $timestamp = (new \DateTime())->getTimestamp();
            $newFilename = $slugifiedTitle . '-' . $timestamp . '.' . $fileExtension;
            $localPath = $this->imageFullDirectory . '/' . $newFilename;
            $galleryPath = $this->imageGalleryDirectory . '/' . $newFilename;

            // Récupération de l'image et vérification du contenu récupéré
            $response = $this->client->request('GET', $url);
            if (200 == $response->getStatusCode()) {
                $contentType = $response->getHeaders()['content-type'][0];
                if (!str_contains($contentType, 'image')) {
                    $error = 'Mauvais type de fichier (' . $contentType . ') lors de la récupération de l\'image ' . $url;
                    $this->airbrakeService->notifyMessage($this->notificationMessage($error, $event));
                }

                // Entregistrement de l'image
                file_put_contents($localPath, $response->getContent());

                // Association de l'image à l'événement
                $image['image'] = $galleryPath;
                $eventTranslation->setMainImage($image);
            } else {
                $error = 'Code ' . $response->getStatusCode() . ' lors de la récupération de l\'image ' . $url;
                $this->airbrakeService->notifyMessage($this->notificationMessage($error, $event));
            }
        }
    }

    protected function saveCategory(Event $event, \stdClass $eventData): void
    {
        if ($this->checkProperty($eventData, 'categorie', 'integer')) {
            $eventCategory = $this->eventCategoryRepository->findOneBy(['idOa' => $eventData->categorie]);
            if (null === $eventCategory) {
                $eventCategory = new EventCategory();
                $eventCategory->setEnabled(true);
                $eventCategory->setIdOa($eventData->categorie);
                // Critères par défaut de certaines catégories, à ne modifier que lors de la création des catégories.
                if (\in_array($eventCategory->getIdOa(), [9, 10], true)) {
                    $eventCategory->setEventAutoEnabled(false);
                }
                if (\in_array($eventCategory->getIdOa(), [9, 10], true)) {
                    $eventCategory->setEventAutoDeletable(false);
                }
                if (\in_array($eventCategory->getIdOa(), [6, 7, 8, 9, 10], true)) {
                    $eventCategory->setDisplayImages(true);
                }
                if (\in_array($eventCategory->getIdOa(), [9, 10], true)) {
                    $eventCategory->setDisplayOccurrences(false);
                }
                if (\in_array($eventCategory->getIdOa(), [9], true)) {
                    $eventCategory->setSearchDisplayPosition(SearchDisplayPositionEnum::First);
                }
                if (\in_array($eventCategory->getIdOa(), [6, 7, 8, 10], true)) {
                    $eventCategory->setSearchDisplayPosition(SearchDisplayPositionEnum::Second);
                }
                $categoryTexts = [
                    2 => [
                        'title' => 'Visites aux Champs Libres',
                        'label' => 'Visite',
                        'plural' => 'Visites',
                    ],
                    3 => [
                        'title' => 'Animations aux Champs Libres',
                        'label' => 'Animation',
                        'plural' => 'Animations',
                    ],
                    4 => [
                        'title' => 'Ateliers aux Champs Libres',
                        'label' => 'Atelier',
                        'plural' => 'Ateliers',
                    ],
                    5 => [
                        'title' => 'Ateliers 4C aux Champs Libres',
                        'label' => 'Atelier 4C',
                        'plural' => 'Ateliers 4C',
                    ],
                    6 => [
                        'title' => 'Projections aux Champs Libres',
                        'label' => 'Projection',
                        'plural' => 'Projections',
                    ],
                    7 => [
                        'title' => 'Concerts aux Champs Libres',
                        'label' => 'Concert',
                        'plural' => 'Concerts',
                    ],
                    8 => [
                        'title' => 'Rencontres aux Champs Libres',
                        'label' => 'Rencontre',
                        'plural' => 'Rencontres',
                    ],
                    9 => [
                        'title' => 'Expositions aux Champs Libres',
                        'label' => 'Exposition',
                        'plural' => 'Expositions',
                    ],
                    10 => [
                        'title' => 'Événements aux Champs Libres',
                        'label' => 'Événement',
                        'plural' => 'Événements',
                    ],
                    27 => [
                        'title' => 'Spectacles aux Champs Libres',
                        'label' => 'Spectacle',
                        'plural' => 'Spectacles',
                    ],
                ];
                if (\array_key_exists($eventCategory->getIdOa(), $categoryTexts)) {
                    $eventCategoryTranslation = new EventCategoryTranslation();
                    $eventCategoryTranslation->setLocale('fr_FR');
                    $eventCategoryTranslation->setTitle($categoryTexts[$eventCategory->getIdOa()]['title']);
                    $eventCategoryTranslation->setLabel($categoryTexts[$eventCategory->getIdOa()]['label']);
                    $eventCategoryTranslation->setLabelPlural($categoryTexts[$eventCategory->getIdOa()]['plural']);
                    $eventCategory->addTranslation($eventCategoryTranslation);
                } else {
                    $error = 'Nouvelle catégorie inconnue à renseigner en BO (identifiant OA : ' . $eventData->categorie . ')';
                    $this->airbrakeService->notifyMessage($this->notificationMessage($error, $event));
                }
            }
            $event->setCategory($eventCategory);
        } else {
            $error = 'OA ne retourne pas un identifiant de catégorie correct (donnée OA : ' . $eventData->categorie . ')';
            $this->airbrakeService->notifyMessage($this->notificationMessage($error, $event));
        }
    }

    protected function getEventsFromApi(bool $all, ?array $after): \stdClass
    {
        $url = 'https://api.openagenda.com/v2/agendas/[AGENDA_ID]/events?key=[API_KEY]&detailed=1';

        // Récupération uniquement des champs utilisés
        $fields = ['uid', 'title', 'image', 'imageCredits', 'timings', 'lastTiming', 'location', 'categorie', 'longDescription', 'conditions', 'registration', 'publics', 'age', 'accessibility', 'status', 'keywords', 'originAgenda'];
        $fieldSeparator = '&includeFields[]=';
        $url .= $fieldSeparator . implode($fieldSeparator, $fields);

        // Récupération uniquement des contenus en français
        $url .= '&monolingual=fr';

        // Récupération des états et status demandés par les CL
        $url .= '&state=2';
        $url .= '&status[]=1&status[]=4&status[]=6';

        // Récupération de la description longue au format HTML
        $url .= '&longDescriptionFormat=HTML';

        if (!$all) {
            // Récupération de la date de mise à jour OA la plus récente
            // - Si aucun événement n'est présent en base de données, on récupère tous les événements d'OpenAgenda.
            // - Si des événements sont présents en base de données, on récupère les événements d'OpenAgenda qui ont
            //   été mis à jour depuis la date de mise à jour la plus récente provenant de OpenAgenda.
            $lastUpdatedEvent = $this->eventRepository->findOneBy([], ['oaUpdatedAt' => 'DESC']);
            if ($lastUpdatedEvent instanceof Event) {
                $lastUpdatedAt = $lastUpdatedEvent->getOaUpdatedAt()->format('Y-m-d\TH:i:s') . '.000Z';
                // Marge de sécurité pour ne pas rater des événements
                $lastUpdatedAt = date('Y-m-d\TH:i:s', strtotime($lastUpdatedAt . ' -3 day')) . '.000Z';
                $url .= '&updatedAt[gte]=' . $lastUpdatedAt;
            }
        }

        // Récupération des pages de résultats
        // Documentation : https://developers.openagenda.com/10-lecture/#navigation
        $size = 20;
        $url .= '&size=' . $size;
        if (null !== $after) {
            $afterSeparator = '&after[]=';
            $after = array_map(fn ($value) => $value ?? 'null', $after);
            $url .= $afterSeparator . implode($afterSeparator, $after);
        }

        return $this->getApiContent($url);
    }

    protected function getApiContent($url): \stdClass
    {
        $url = str_replace(['[AGENDA_ID]', '[API_KEY]'], [$this->agendaId, $this->apiKey], $url);

        $response = $this->client->request('GET', $url);

        $statusCode = $response->getStatusCode();
        if (200 !== $statusCode) {
            $error = 'Erreur de connexion à l\'API, code HTTP ' . $statusCode;

            throw new \Exception($this->notificationMessage($error), 500);
        }

        $contentType = $response->getHeaders()['content-type'][0];
        if (!str_contains($contentType, 'application/json')) {
            $error = 'Mauvais type de contenu retourné par l\'API (' . $contentType . ')';

            throw new \Exception($this->notificationMessage($error), 500);
        }

        return json_decode($response->getContent(), false);
    }

    protected function notificationMessage(string $error, ?Event $event = null): string
    {
        if (null === $event) {
            return 'OpenAgenda : ' . $error;
        }

        return 'OpenAgenda, événement ' . $event->getIdOa() . ' : ' . $error;
    }
}

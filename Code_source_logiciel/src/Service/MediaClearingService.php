<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Article\Article;
use App\Entity\Article\ArticleCategory;
use App\Entity\Article\ArticleSubject;
use App\Entity\Dossier\Dossier;
use App\Entity\Event\Event;
use App\Entity\Event\EventCategory;
use App\Entity\Exhibit\Exhibit;
use App\Entity\Mediation\Mediation;
use App\Entity\Mediation\MediationSubject;
use App\Entity\Project\Project;
use App\Entity\Serie\Serie;
use App\Entity\ThematicTour\ThematicTour;
use App\Entity\Tool\Tool;
use App\Repository\Event\EventCategoryRepository;
use App\Repository\Event\EventKeywordRepository;
use App\Repository\Event\EventOccurrenceRepository;
use App\Repository\Event\EventRepository;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Luna\CoreBundle\Entity\Page\Page;
use Luna\CoreBundle\Service\AirbrakeService;
use Sylius\Component\Locale\Model\Locale;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MediaClearingService
{
    protected EventRepository $eventRepository;

    protected EventKeywordRepository $eventKeywordRepository;

    protected EventOccurrenceRepository $eventOccurrenceRepository;

    protected EventCategoryRepository $eventCategoryRepository;

    protected string $galleryFullDirectory;

    protected int $timeThreshold;

    protected array $resources;

    protected array $locales;

    public function __construct(
        protected string $publicDirectory,
        protected string $mediaDirectory,
        protected string $galleryDirectory,
        protected string $maxAge,
        protected EntityManager $entityManager,
        protected HttpClientInterface $client,
        protected SluggerInterface $slugger,
        protected AirbrakeService $airbrakeService,
        protected CacheManager $liipCacheManager,
    ) {
        $this->galleryFullDirectory = rtrim($publicDirectory, '/') . '/' . rtrim($mediaDirectory, '/') . '/' . rtrim($galleryDirectory, '/');
        $this->timeThreshold = strtotime($this->maxAge);

        // Liste des types de ressources pouvant référencer une image.
        // Le repository de ces ressources doit implémenter une méthode hasMedia,
        // à faire en utilisant le trait MediaInUseTrait.
        $this->resources = [
            Article::class,
            ArticleCategory::class,
            ArticleSubject::class,
            Dossier::class,
            Event::class,
            EventCategory::class,
            Exhibit::class,
            Mediation::class,
            MediationSubject::class,
            Page::class,
            Project::class,
            Serie::class,
            ThematicTour::class,
            Tool::class,
        ];

        // Il faut faire la vérification et le nettoyage pour toutes les langues
        $this->locales = $this->entityManager->getRepository(Locale::class)->findAll();
    }

    public function clearMedias(): void
    {
        $iterator = new \RecursiveDirectoryIterator($this->galleryFullDirectory);

        foreach (new \RecursiveIteratorIterator($iterator) as $file) {
            if ($file->isFile() && $file->getMTime() <= $this->timeThreshold) {
                $fileGalleryPath = str_replace($this->galleryFullDirectory, $this->galleryDirectory, $file->getPathname());

                if (!$this->mediaInUse($fileGalleryPath)) {
                    if (unlink($file->getPathname())) {
                        // Suppression du cache LiipImagine
                        // Exemple Liip path : "gallery/images/un-dossier/une-image.jpg"
                        $this->liipCacheManager->remove($fileGalleryPath);
                    } else {
                        $error = 'Erreur lors de la suppression du fichier "' . $file . '".';
                        $this->airbrakeService->notifyMessage($this->notificationMessage($error));
                    }
                }
            }
        }
    }

    protected function mediaInUse(string $filePath): bool
    {
        foreach ($this->resources as $resource) {
            foreach ($this->locales as $locale) {
                if ($this->entityManager->getRepository($resource)->hasMedia($locale->getCode(), $filePath)) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function notificationMessage(string $error): string
    {
        return 'Nettoyage des médias - ' . $error;
    }
}

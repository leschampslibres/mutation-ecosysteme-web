<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Article\Article;
use App\Entity\Dossier\Dossier;
use App\Entity\Event\Event;
use App\Entity\Event\EventCategory;
use App\Entity\Event\EventKeyword;
use App\Entity\Event\EventOccurrence;
use App\Entity\Event\EventTranslation;
use App\Entity\Exhibit\Exhibit;
use App\Entity\Mediation\Mediation;
use App\Entity\Project\Project;
use App\Entity\Serie\Serie;
use App\Entity\ThematicTour\ThematicTour;
use App\Entity\Tool\Tool;
use App\Repository\Event\EventCategoryRepository;
use App\Repository\Event\EventKeywordRepository;
use App\Repository\Event\EventOccurrenceRepository;
use App\Repository\Event\EventRepository;
use Doctrine\ORM\EntityManager;
use Luna\CoreBundle\Entity\Page\Page;
use Luna\CoreBundle\Service\AirbrakeService;
use Sylius\Component\Locale\Model\Locale;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EventClearingService
{
    protected EventRepository $eventRepository;

    protected EventKeywordRepository $eventKeywordRepository;

    protected EventOccurrenceRepository $eventOccurrenceRepository;

    protected EventCategoryRepository $eventCategoryRepository;

    protected string $imageFullDirectory;

    protected string $imageGalleryDirectory;

    protected array $resources;

    protected array $locales;

    public function __construct(
        protected string $publicDirectory,
        protected string $mediaDirectory,
        protected EntityManager $entityManager,
        protected HttpClientInterface $client,
        protected SluggerInterface $slugger,
        protected AirbrakeService $airbrakeService
    ) {
        $this->eventRepository = $this->entityManager->getRepository(Event::class);
        $this->eventKeywordRepository = $this->entityManager->getRepository(EventKeyword::class);
        $this->eventOccurrenceRepository = $this->entityManager->getRepository(EventOccurrence::class);
        $this->eventCategoryRepository = $this->entityManager->getRepository(EventCategory::class);
        $this->imageGalleryDirectory = 'gallery/images/open-agenda';
        $this->imageFullDirectory = rtrim($publicDirectory, '/') . '/' . rtrim($mediaDirectory, '/') . '/' . $this->imageGalleryDirectory;

        // Liste des types de ressources pouvant référencer un événement
        // Ce sont toutes les ressources ayant un champ "blocks"
        // Le bloc 12 ("événements manuel" est concerné
        // Le champ "blocks" peut contenir : {\"event\":\"4\"}
        // Le repository de ces ressources doit implémenter une méthode hasEventInBlock,
        // à faire en utilisant le trait EventsInUseTrait.
        $this->resources = [
            Article::class,
            Dossier::class,
            Event::class,
            EventCategory::class,
            Exhibit::class,
            Mediation::class,
            Page::class,
            Project::class,
            Serie::class,
            ThematicTour::class,
            Tool::class,
        ];

        // Il faut faire la vérification et le nettoyage pour toutes les langues
        $this->locales = $this->entityManager->getRepository(Locale::class)->findAll();
    }

    // Vérifier si l'event est terminé aujourd'hui

    // S'il n'est pas terminé, on ne fait rien

    // S'il est terminé :
    // - on vérifie s'il est utilisé dans un bloc de contenu (bloc 12 versions manuelle et automatique)
    //   - s'il n'est pas utilisé, on supprime les données et les images, en laissant titre et slug -> + statut "terminé et supprimé" ? -> code 410
    //   - s'il est utilisé, on supprime tout sauf titre, slug, image principale et catégorie -> status "terminé mais utilisé" ?

    // Vérification des statuts "terminé mais utilisé"
    // - on vérifie s'il est utilisé dans un bloc de contenu
    //   - s'il est toujours utilisé, on ne fait rien
    //   - s'il n'est plus utilisé, on le passe en "terminé et supprimé"

    // ! suppression des images

    // Vider le cache

    public function clearEvents(): void
    {
        // Récupération des événements terminés
        $events = $this->eventRepository->findDeletableEvents();

        // Vérification de l'utilisation de l'événement et traitements appropriés
        foreach ($events as $event) {
            $this->clearEvent($event, false);
        }

        $this->entityManager->flush();
    }

    // $forceClear permet de forcer le nettoyage des données même si l'événement fait partie d'une catégorie qui n'autorise pas la suppression automatique
    public function clearEvent(Event $event, bool $flush, bool $forceClear = false): void
    {
        // Si on ne forceClear pas et que la catégorie n'autorise pas la suppression automatique
        if (!$forceClear && !$event->getCategory()->getEventAutoDeletable()) {
            // On ne fait que changer le statut
            $event->setStatus(Event::STATUS_COMPLETED);

        // Sinon...
        } else {
            // Vérification de l'utilisation de l'événement
            $inUse = false;
            // Pour chacune des ressources pouvant utiliser l'événement
            foreach ($this->resources as $resource) {
                // Et pour chacune des langues
                foreach ($this->locales as $locale) {
                    // Si l'événement est utilisé dans un bloc de contenu de la ressource pour la locale en cours
                    if ($this->entityManager->getRepository($resource)->hasEventInBlock($locale->getCode(), $event)) {
                        $inUse = true;

                        break;
                    }
                }
            }
            // Nettoyage partiel ou total des données
            if ($inUse) {
                // Si l'événement est utilisé, on nettoie partiellement les données
                $this->endEvent($event, $this->locales);
            } else {
                // Si l'événement n'est pas utilisé, on nettoie totalement les données
                $this->deleteEvent($event, $this->locales);
            }
        }

        $this->entityManager->persist($event);

        if ($flush) {
            $this->entityManager->flush();
        }
    }

    protected function endEvent(Event $event, array $locales): void
    {
        // Nullification des propriétés non traduites
        $this->clearProperties($event, Event::CLEAR_TO_END);

        // Nullification des propriétés traduites
        foreach ($locales as $locale) {
            $translation = $event->getTranslation($locale->getCode());
            $this->clearProperties($translation, EventTranslation::CLEAR_TO_END);
        }

        // Suppression des associations
        $this->clearAssociations($event);

        // On ne modifie pas "enabled" puisqu'il faut pouvoir continuer à afficher la vignette de l'événement concerné.

        // Mise à jour du statut de l'événément ("terminé mais utilisé, et donc nettoyé partiellement")
        $event->setStatus(Event::STATUS_ENDED);
    }

    protected function deleteEvent(Event $event, array $locales): void
    {
        // Nullification des propriétés non traduites
        $this->clearProperties($event, Event::CLEAR_TO_DELETE);

        // Nullification des propriétés traduites
        foreach ($locales as $locale) {
            $translation = $event->getTranslation($locale->getCode());
            $this->clearProperties($translation, EventTranslation::CLEAR_TO_DELETE);
        }

        // Suppression des associations
        $this->clearAssociations($event);

        // "enabled" passé à "false"
        // Ou pas : la route pour afficher un événement vérifie si le slug est celui d'un événement
        // publié ou non. Si l'événement n'est pas publié, une exception 404 est déclenchée, avant même d'arriver
        // à la vérification permettant de déclencher une exception 410, qui est celle que l'on veut.
        // Il est toutefois pertinent de conserver cette vérification de la publication, par exemple
        // dans le cas d'un événement dépublié dont on ne voudrait pas qu'il génère une exception 410.
        // $event->setEnabled(false);

        // Mise à jour du statut de l'événément ("terminé et nettoyé totalement")
        $event->setStatus(Event::STATUS_DELETED);
    }

    protected function clearProperties(Event|EventTranslation $entity, array $clearing = []): void
    {
        foreach ($clearing as $field) {
            $setter = 'set' . ucfirst($field);
            $entity->{$setter}(null);
        }
    }

    protected function clearAssociations(Event $event): void
    {
        // Suppression des occurrences
        foreach ($event->getOccurrences() as $occurrence) {
            $event->removeOccurrence($occurrence);
        }

        // Suppression des associations de mots-clés
        foreach ($event->getKeywords() as $keyword) {
            $event->removeKeyword($keyword);
        }
    }

    protected function notificationMessage(string $error, ?Event $event = null): string
    {
        return 'OpenAgenda, événement ' . $event->getIdOa() . ' : ' . $error;
    }
}

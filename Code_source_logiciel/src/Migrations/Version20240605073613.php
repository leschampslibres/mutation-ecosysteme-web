<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240605073613 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_mediation_mediation_subject (mediation_id INT UNSIGNED NOT NULL, mediationsubject_id INT UNSIGNED NOT NULL, INDEX IDX_E4BD9F0FB6617407 (mediation_id), INDEX IDX_E4BD9F0F59F91116 (mediationsubject_id), PRIMARY KEY(mediation_id, mediationsubject_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_mediation_mediation_subject ADD CONSTRAINT FK_E4BD9F0FB6617407 FOREIGN KEY (mediation_id) REFERENCES app_mediation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediation_mediation_subject ADD CONSTRAINT FK_E4BD9F0F59F91116 FOREIGN KEY (mediationsubject_id) REFERENCES app_mediation_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediation CHANGE accessibility accessibility JSON DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_mediation_mediation_subject DROP FOREIGN KEY FK_E4BD9F0FB6617407');
        $this->addSql('ALTER TABLE app_mediation_mediation_subject DROP FOREIGN KEY FK_E4BD9F0F59F91116');
        $this->addSql('DROP TABLE app_mediation_mediation_subject');
        $this->addSql('ALTER TABLE app_mediation CHANGE accessibility accessibility VARCHAR(255) DEFAULT NULL');
    }
}

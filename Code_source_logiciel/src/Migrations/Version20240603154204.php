<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240603154204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des médiations et des thématiques des médiations';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_mediation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, color VARCHAR(255) DEFAULT NULL, accessibility VARCHAR(255) DEFAULT NULL, pricing VARCHAR(255) DEFAULT NULL, supervision VARCHAR(255) DEFAULT NULL, level VARCHAR(255) DEFAULT NULL, month VARCHAR(255) DEFAULT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_6425177D77153098 (code), INDEX idx_level (level), INDEX idx_supervision (supervision), INDEX idx_month (month), INDEX idx_pricing (pricing), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_mediation_related_mediation (mediation_source INT UNSIGNED NOT NULL, mediation_target INT UNSIGNED NOT NULL, INDEX IDX_A9ACD1C8E3AB9 (mediation_source), INDEX IDX_A9ACD56B6A36 (mediation_target), PRIMARY KEY(mediation_source, mediation_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_mediation_subject (id INT UNSIGNED AUTO_INCREMENT NOT NULL, position INT NOT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2179BC0E77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_mediation_subject_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_C9A2F0792C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_mediation_subject_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_mediation_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, price VARCHAR(255) DEFAULT NULL, audience VARCHAR(255) DEFAULT NULL, period VARCHAR(255) DEFAULT NULL, schedule VARCHAR(255) DEFAULT NULL, duration VARCHAR(255) DEFAULT NULL, location VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_7F07BEFF2C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_mediation_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_mediation_related_mediation ADD CONSTRAINT FK_A9ACD1C8E3AB9 FOREIGN KEY (mediation_source) REFERENCES app_mediation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediation_related_mediation ADD CONSTRAINT FK_A9ACD56B6A36 FOREIGN KEY (mediation_target) REFERENCES app_mediation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediation_subject_translation ADD CONSTRAINT FK_C9A2F0792C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_mediation_subject (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediation_translation ADD CONSTRAINT FK_7F07BEFF2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_mediation (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_mediation_related_mediation DROP FOREIGN KEY FK_A9ACD1C8E3AB9');
        $this->addSql('ALTER TABLE app_mediation_related_mediation DROP FOREIGN KEY FK_A9ACD56B6A36');
        $this->addSql('ALTER TABLE app_mediation_subject_translation DROP FOREIGN KEY FK_C9A2F0792C2AC5D3');
        $this->addSql('ALTER TABLE app_mediation_translation DROP FOREIGN KEY FK_7F07BEFF2C2AC5D3');
        $this->addSql('DROP TABLE app_mediation');
        $this->addSql('DROP TABLE app_mediation_related_mediation');
        $this->addSql('DROP TABLE app_mediation_subject');
        $this->addSql('DROP TABLE app_mediation_subject_translation');
        $this->addSql('DROP TABLE app_mediation_translation');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240201092552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout du bandeau d\'alerte';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_alert_message (id INT UNSIGNED AUTO_INCREMENT NOT NULL, level VARCHAR(255) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_alert_message_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, content LONGTEXT DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_22B56F3F2C2AC5D3 (translatable_id), UNIQUE INDEX app_alert_message_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_alert_message_translation ADD CONSTRAINT FK_22B56F3F2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_alert_message (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_alert_message_translation DROP FOREIGN KEY FK_22B56F3F2C2AC5D3');
        $this->addSql('DROP TABLE app_alert_message');
        $this->addSql('DROP TABLE app_alert_message_translation');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240520140502 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Articles et séries mises en avant sur les pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pages_series (page_id INT UNSIGNED NOT NULL, serie_id INT UNSIGNED NOT NULL, INDEX IDX_979A7A22C4663E4 (page_id), INDEX IDX_979A7A22D94388BD (serie_id), PRIMARY KEY(page_id, serie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pages_series ADD CONSTRAINT FK_979A7A22C4663E4 FOREIGN KEY (page_id) REFERENCES luna_page (id)');
        $this->addSql('ALTER TABLE pages_series ADD CONSTRAINT FK_979A7A22D94388BD FOREIGN KEY (serie_id) REFERENCES app_serie (id)');
        $this->addSql('ALTER TABLE luna_page ADD article_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE luna_page ADD CONSTRAINT FK_2FC2238B7294869C FOREIGN KEY (article_id) REFERENCES app_article (id)');
        $this->addSql('CREATE INDEX IDX_2FC2238B7294869C ON luna_page (article_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pages_series DROP FOREIGN KEY FK_979A7A22C4663E4');
        $this->addSql('ALTER TABLE pages_series DROP FOREIGN KEY FK_979A7A22D94388BD');
        $this->addSql('DROP TABLE pages_series');
        $this->addSql('ALTER TABLE luna_page DROP FOREIGN KEY FK_2FC2238B7294869C');
        $this->addSql('DROP INDEX IDX_2FC2238B7294869C ON luna_page');
        $this->addSql('ALTER TABLE luna_page DROP article_id');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240830080556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la méthode de cache pour les pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE luna_page ADD cache_method VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE luna_page SET cache_method = "none"');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE luna_page DROP cache_method');
    }
}

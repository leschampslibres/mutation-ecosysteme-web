<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240612094342 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajouts des ressources';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_tool (id INT UNSIGNED AUTO_INCREMENT NOT NULL, category_id INT UNSIGNED DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, pricing VARCHAR(255) DEFAULT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_25DD1B7177153098 (code), INDEX IDX_25DD1B7112469DE2 (category_id), INDEX idx_enabled (enabled), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_tool_category (id INT UNSIGNED AUTO_INCREMENT NOT NULL, position INT NOT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_73F0504A77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_tool_category_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_915C06AD2C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_tool_category_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_tool_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, audience VARCHAR(255) DEFAULT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_BA65BA902C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_tool_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_tool ADD CONSTRAINT FK_25DD1B7112469DE2 FOREIGN KEY (category_id) REFERENCES app_tool_category (id)');
        $this->addSql('ALTER TABLE app_tool_category_translation ADD CONSTRAINT FK_915C06AD2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_tool_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_tool_translation ADD CONSTRAINT FK_BA65BA902C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_tool (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_tool DROP FOREIGN KEY FK_25DD1B7112469DE2');
        $this->addSql('ALTER TABLE app_tool_category_translation DROP FOREIGN KEY FK_915C06AD2C2AC5D3');
        $this->addSql('ALTER TABLE app_tool_translation DROP FOREIGN KEY FK_BA65BA902C2AC5D3');
        $this->addSql('DROP TABLE app_tool');
        $this->addSql('DROP TABLE app_tool_category');
        $this->addSql('DROP TABLE app_tool_category_translation');
        $this->addSql('DROP TABLE app_tool_translation');
    }
}

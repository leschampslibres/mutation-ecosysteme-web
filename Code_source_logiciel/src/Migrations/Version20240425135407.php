<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240425135407 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_article_article_subject (article_id INT UNSIGNED NOT NULL, articlesubject_id INT UNSIGNED NOT NULL, INDEX IDX_E52E2ABA7294869C (article_id), INDEX IDX_E52E2ABA95972DBE (articlesubject_id), PRIMARY KEY(article_id, articlesubject_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_article_article_subject ADD CONSTRAINT FK_E52E2ABA7294869C FOREIGN KEY (article_id) REFERENCES app_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_article_article_subject ADD CONSTRAINT FK_E52E2ABA95972DBE FOREIGN KEY (articlesubject_id) REFERENCES app_article_subject (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_article_article_subject DROP FOREIGN KEY FK_E52E2ABA7294869C');
        $this->addSql('ALTER TABLE app_article_article_subject DROP FOREIGN KEY FK_E52E2ABA95972DBE');
        $this->addSql('DROP TABLE app_article_article_subject');
    }
}

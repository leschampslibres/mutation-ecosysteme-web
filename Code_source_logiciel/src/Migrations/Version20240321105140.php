<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240321105140 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Modification des pages pour under-pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE luna_page CHANGE indexed_resource under_pages_code VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_UNDER_PAGES_CODE ON luna_page (under_pages_code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_UNDER_PAGES_CODE ON luna_page');
        $this->addSql('ALTER TABLE luna_page CHANGE under_pages_code indexed_resource VARCHAR(255) DEFAULT NULL');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240326161241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Index';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX idx_slug ON luna_page_translation (slug)');
        $this->addSql('CREATE INDEX idx_composite_slug ON luna_page_translation (composite_slug)');
        $this->addSql('CREATE INDEX idx_locale ON luna_page_translation (locale)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_slug ON luna_page_translation');
        $this->addSql('DROP INDEX idx_composite_slug ON luna_page_translation');
        $this->addSql('DROP INDEX idx_locale ON luna_page_translation');
    }
}

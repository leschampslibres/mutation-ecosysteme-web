<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240202100409 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Renommage des tables page et page_translation en luna_page et luna_page_translation';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('RENAME TABLE page TO luna_page');
        $this->addSql('RENAME TABLE page_translation TO luna_page_translation');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('RENAME TABLE luna_page TO page');
        $this->addSql('RENAME TABLE luna_page_translation TO page_translation');
    }
}

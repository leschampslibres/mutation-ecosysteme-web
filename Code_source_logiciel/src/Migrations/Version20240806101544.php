<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240806101544 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Public en fonction du type de professionnel pour plusieurs ressources';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_dossiers_professionals ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL, ADD audience VARCHAR(255) DEFAULT NULL, CHANGE dossier_id dossier_id INT UNSIGNED DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE app_exhibits_professionals ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL, ADD audience VARCHAR(255) DEFAULT NULL, CHANGE exhibit_id exhibit_id INT UNSIGNED DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE app_mediations_professionals ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL, ADD audience VARCHAR(255) DEFAULT NULL, CHANGE mediation_id mediation_id INT UNSIGNED DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL, ADD audience VARCHAR(255) DEFAULT NULL, CHANGE thematictour_id thematicTour_id INT UNSIGNED DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE app_tools_professionals ADD id INT UNSIGNED AUTO_INCREMENT NOT NULL, ADD audience VARCHAR(255) DEFAULT NULL, CHANGE tool_id tool_id INT UNSIGNED DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_dossiers_professionals MODIFY id INT UNSIGNED NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON app_dossiers_professionals');
        $this->addSql('ALTER TABLE app_dossiers_professionals DROP id, DROP audience, CHANGE dossier_id dossier_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE app_dossiers_professionals ADD PRIMARY KEY (dossier_id, professional_id)');
        $this->addSql('ALTER TABLE app_exhibits_professionals MODIFY id INT UNSIGNED NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON app_exhibits_professionals');
        $this->addSql('ALTER TABLE app_exhibits_professionals DROP id, DROP audience, CHANGE exhibit_id exhibit_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE app_exhibits_professionals ADD PRIMARY KEY (exhibit_id, professional_id)');
        $this->addSql('ALTER TABLE app_mediations_professionals MODIFY id INT UNSIGNED NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON app_mediations_professionals');
        $this->addSql('ALTER TABLE app_mediations_professionals DROP id, DROP audience, CHANGE mediation_id mediation_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE app_mediations_professionals ADD PRIMARY KEY (mediation_id, professional_id)');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals MODIFY id INT UNSIGNED NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON app_thematic_tours_professionals');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals DROP id, DROP audience, CHANGE thematicTour_id thematictour_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals ADD PRIMARY KEY (thematictour_id, professional_id)');
        $this->addSql('ALTER TABLE app_tools_professionals MODIFY id INT UNSIGNED NOT NULL');
        $this->addSql('DROP INDEX `PRIMARY` ON app_tools_professionals');
        $this->addSql('ALTER TABLE app_tools_professionals DROP id, DROP audience, CHANGE tool_id tool_id INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE app_tools_professionals ADD PRIMARY KEY (tool_id, professional_id)');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240327160406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event CHANGE accessibility_ii accessibility_ii TINYINT(1) DEFAULT NULL, CHANGE accessibility_hi accessibility_hi TINYINT(1) DEFAULT NULL, CHANGE accessibility_vi accessibility_vi TINYINT(1) DEFAULT NULL, CHANGE accessibility_pi accessibility_pi TINYINT(1) DEFAULT NULL, CHANGE accessibility_mi accessibility_mi TINYINT(1) DEFAULT NULL, CHANGE oa_updated_at oa_updated_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event CHANGE accessibility_ii accessibility_ii TINYINT(1) NOT NULL, CHANGE accessibility_hi accessibility_hi TINYINT(1) NOT NULL, CHANGE accessibility_vi accessibility_vi TINYINT(1) NOT NULL, CHANGE accessibility_pi accessibility_pi TINYINT(1) NOT NULL, CHANGE accessibility_mi accessibility_mi TINYINT(1) NOT NULL, CHANGE oa_updated_at oa_updated_at DATETIME NOT NULL');
    }
}

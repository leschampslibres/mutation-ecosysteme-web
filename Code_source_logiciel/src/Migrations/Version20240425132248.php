<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240425132248 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Association articles et catégories';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_article ADD category_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE app_article ADD CONSTRAINT FK_EF678E2B12469DE2 FOREIGN KEY (category_id) REFERENCES app_article_category (id)');
        $this->addSql('CREATE INDEX IDX_EF678E2B12469DE2 ON app_article (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_article DROP FOREIGN KEY FK_EF678E2B12469DE2');
        $this->addSql('DROP INDEX IDX_EF678E2B12469DE2 ON app_article');
        $this->addSql('ALTER TABLE app_article DROP category_id');
    }
}

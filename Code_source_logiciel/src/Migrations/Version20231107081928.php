<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231107081928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout du code et du slug des pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page ADD code VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_140AB62077153098 ON page (code)');
        $this->addSql('ALTER TABLE page_translation ADD slug VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_140AB62077153098 ON page');
        $this->addSql('ALTER TABLE page DROP code');
        $this->addSql('ALTER TABLE page_translation DROP slug');
    }
}

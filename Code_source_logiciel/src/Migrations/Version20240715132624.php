<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240715132624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Suppression des colonnes pricing dans app_dossier et app_tool';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_dossier DROP pricing');
        $this->addSql('ALTER TABLE app_tool DROP pricing');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_dossier ADD pricing VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE app_tool ADD pricing VARCHAR(255) DEFAULT NULL');
    }
}

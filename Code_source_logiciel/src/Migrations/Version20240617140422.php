<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240617140422 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des relations entre les professionnels et les types de contenus pour professionnels';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_mediations_professionals (mediation_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_EF8F2BC8B6617407 (mediation_id), INDEX IDX_EF8F2BC8DB77003 (professional_id), PRIMARY KEY(mediation_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_projects_professionals (project_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_8B7360E2166D1F9C (project_id), INDEX IDX_8B7360E2DB77003 (professional_id), PRIMARY KEY(project_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_thematic_tours_professionals (thematictour_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_F5B3FD598B871C (thematictour_id), INDEX IDX_F5B3FD5DB77003 (professional_id), PRIMARY KEY(thematictour_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_tools_professionals (tool_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_6BD462608F7B22CC (tool_id), INDEX IDX_6BD46260DB77003 (professional_id), PRIMARY KEY(tool_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_mediations_professionals ADD CONSTRAINT FK_EF8F2BC8B6617407 FOREIGN KEY (mediation_id) REFERENCES app_mediation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_mediations_professionals ADD CONSTRAINT FK_EF8F2BC8DB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_projects_professionals ADD CONSTRAINT FK_8B7360E2166D1F9C FOREIGN KEY (project_id) REFERENCES app_project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_projects_professionals ADD CONSTRAINT FK_8B7360E2DB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals ADD CONSTRAINT FK_F5B3FD598B871C FOREIGN KEY (thematictour_id) REFERENCES app_thematic_tour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals ADD CONSTRAINT FK_F5B3FD5DB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_tools_professionals ADD CONSTRAINT FK_6BD462608F7B22CC FOREIGN KEY (tool_id) REFERENCES app_tool (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_tools_professionals ADD CONSTRAINT FK_6BD46260DB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_mediations_professionals DROP FOREIGN KEY FK_EF8F2BC8B6617407');
        $this->addSql('ALTER TABLE app_mediations_professionals DROP FOREIGN KEY FK_EF8F2BC8DB77003');
        $this->addSql('ALTER TABLE app_projects_professionals DROP FOREIGN KEY FK_8B7360E2166D1F9C');
        $this->addSql('ALTER TABLE app_projects_professionals DROP FOREIGN KEY FK_8B7360E2DB77003');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals DROP FOREIGN KEY FK_F5B3FD598B871C');
        $this->addSql('ALTER TABLE app_thematic_tours_professionals DROP FOREIGN KEY FK_F5B3FD5DB77003');
        $this->addSql('ALTER TABLE app_tools_professionals DROP FOREIGN KEY FK_6BD462608F7B22CC');
        $this->addSql('ALTER TABLE app_tools_professionals DROP FOREIGN KEY FK_6BD46260DB77003');
        $this->addSql('DROP TABLE app_mediations_professionals');
        $this->addSql('DROP TABLE app_projects_professionals');
        $this->addSql('DROP TABLE app_thematic_tours_professionals');
        $this->addSql('DROP TABLE app_tools_professionals');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240226165126 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Événements';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_event (id INT UNSIGNED AUTO_INCREMENT NOT NULL, children_keyword_id INT UNSIGNED DEFAULT NULL, category_id INT UNSIGNED DEFAULT NULL, id_oa INT DEFAULT NULL, origin_agenda INT DEFAULT NULL, location INT DEFAULT NULL, pricing VARCHAR(255) DEFAULT NULL, price INT DEFAULT NULL, ticketing VARCHAR(255) DEFAULT NULL, audience VARCHAR(255) DEFAULT NULL, age_min INT DEFAULT NULL, age_max INT DEFAULT NULL, accessibility_ii TINYINT(1) NOT NULL, accessibility_hi TINYINT(1) NOT NULL, accessibility_vi TINYINT(1) NOT NULL, accessibility_pi TINYINT(1) NOT NULL, accessibility_mi TINYINT(1) NOT NULL, status INT DEFAULT NULL, main_image_credits VARCHAR(255) DEFAULT NULL, starting_at DATETIME DEFAULT NULL, ending_at DATETIME DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, oa_updated_at DATETIME NOT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, UNIQUE INDEX UNIQ_ED7D876A77153098 (code), INDEX IDX_ED7D876A6615BBCF (children_keyword_id), INDEX IDX_ED7D876A12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_event_keyword (event_id INT UNSIGNED NOT NULL, eventkeyword_id INT UNSIGNED NOT NULL, INDEX IDX_A52380FD71F7E88B (event_id), INDEX IDX_A52380FDDA54DA72 (eventkeyword_id), PRIMARY KEY(event_id, eventkeyword_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_category (id INT UNSIGNED AUTO_INCREMENT NOT NULL, id_oa INT DEFAULT NULL, event_auto_enabled TINYINT(1) NOT NULL, event_auto_deletable TINYINT(1) NOT NULL, display_images TINYINT(1) NOT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, UNIQUE INDEX UNIQ_9F5A62EF77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_category_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_6377DF9F2C2AC5D3 (translatable_id), UNIQUE INDEX app_event_category_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_keyword (id INT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_occurrence (id INT UNSIGNED AUTO_INCREMENT NOT NULL, event_id INT UNSIGNED DEFAULT NULL, starting_at DATETIME DEFAULT NULL, ending_at DATETIME DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, INDEX IDX_722DB97D71F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_event_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_BEA53B702C2AC5D3 (translatable_id), UNIQUE INDEX app_event_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876A6615BBCF FOREIGN KEY (children_keyword_id) REFERENCES app_event_keyword (id)');
        $this->addSql('ALTER TABLE app_event ADD CONSTRAINT FK_ED7D876A12469DE2 FOREIGN KEY (category_id) REFERENCES app_event_category (id)');
        $this->addSql('ALTER TABLE app_event_event_keyword ADD CONSTRAINT FK_A52380FD71F7E88B FOREIGN KEY (event_id) REFERENCES app_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_event_event_keyword ADD CONSTRAINT FK_A52380FDDA54DA72 FOREIGN KEY (eventkeyword_id) REFERENCES app_event_keyword (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_event_category_translation ADD CONSTRAINT FK_6377DF9F2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_event_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_event_occurrence ADD CONSTRAINT FK_722DB97D71F7E88B FOREIGN KEY (event_id) REFERENCES app_event (id)');
        $this->addSql('ALTER TABLE app_event_translation ADD CONSTRAINT FK_BEA53B702C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_event (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876A6615BBCF');
        $this->addSql('ALTER TABLE app_event DROP FOREIGN KEY FK_ED7D876A12469DE2');
        $this->addSql('ALTER TABLE app_event_event_keyword DROP FOREIGN KEY FK_A52380FD71F7E88B');
        $this->addSql('ALTER TABLE app_event_event_keyword DROP FOREIGN KEY FK_A52380FDDA54DA72');
        $this->addSql('ALTER TABLE app_event_category_translation DROP FOREIGN KEY FK_6377DF9F2C2AC5D3');
        $this->addSql('ALTER TABLE app_event_occurrence DROP FOREIGN KEY FK_722DB97D71F7E88B');
        $this->addSql('ALTER TABLE app_event_translation DROP FOREIGN KEY FK_BEA53B702C2AC5D3');
        $this->addSql('DROP TABLE app_event');
        $this->addSql('DROP TABLE app_event_event_keyword');
        $this->addSql('DROP TABLE app_event_category');
        $this->addSql('DROP TABLE app_event_category_translation');
        $this->addSql('DROP TABLE app_event_keyword');
        $this->addSql('DROP TABLE app_event_occurrence');
        $this->addSql('DROP TABLE app_event_translation');
    }
}

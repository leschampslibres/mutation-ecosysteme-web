<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240611102559 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Suppression des séries associées aux pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pages_series DROP FOREIGN KEY FK_979A7A22C4663E4');
        $this->addSql('ALTER TABLE pages_series DROP FOREIGN KEY FK_979A7A22D94388BD');
        $this->addSql('DROP TABLE pages_series');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pages_series (page_id INT UNSIGNED NOT NULL, serie_id INT UNSIGNED NOT NULL, INDEX IDX_979A7A22D94388BD (serie_id), INDEX IDX_979A7A22C4663E4 (page_id), PRIMARY KEY(page_id, serie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE pages_series ADD CONSTRAINT FK_979A7A22C4663E4 FOREIGN KEY (page_id) REFERENCES luna_page (id)');
        $this->addSql('ALTER TABLE pages_series ADD CONSTRAINT FK_979A7A22D94388BD FOREIGN KEY (serie_id) REFERENCES app_serie (id)');
    }
}

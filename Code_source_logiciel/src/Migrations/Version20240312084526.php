<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240312084526 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Index pour les événements';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX IDX_ID_OA ON app_event (id_oa);');
        $this->addSql('CREATE INDEX IDX_ORIGIN_AGENDA ON app_event (origin_agenda);');
        $this->addSql('CREATE INDEX IDX_PRICING ON app_event (pricing);');
        $this->addSql('CREATE INDEX IDX_AUDIENCE ON app_event (audience);');
        $this->addSql('CREATE INDEX IDX_ACCESSIBILITY_II ON app_event (accessibility_ii);');
        $this->addSql('CREATE INDEX IDX_ACCESSIBILITY_HI ON app_event (accessibility_hi);');
        $this->addSql('CREATE INDEX IDX_ACCESSIBILITY_VI ON app_event (accessibility_vi);');
        $this->addSql('CREATE INDEX IDX_ACCESSIBILITY_PI ON app_event (accessibility_pi);');
        $this->addSql('CREATE INDEX IDX_ACCESSIBILITY_MI ON app_event (accessibility_mi);');
        $this->addSql('CREATE INDEX IDX_STARTING_AT ON app_event (starting_at);');
        $this->addSql('CREATE INDEX IDX_ENDING_AT ON app_event (ending_at);');
        $this->addSql('CREATE INDEX IDX_ENABLED ON app_event (enabled);');

        $this->addSql('CREATE INDEX IDX_ID_OA ON app_event_category (id_oa);');
        $this->addSql('CREATE INDEX IDX_DISPLAY_OCCURRENCES ON app_event_category (display_occurrences);');
        $this->addSql('CREATE INDEX IDX_SEARCH_DISPLAY_POSITION ON app_event_category (search_display_position);');

        $this->addSql('CREATE INDEX IDX_LOCALE ON app_event_category_translation (locale);');
        $this->addSql('CREATE INDEX IDX_SLUG ON app_event_category_translation (slug);');

        $this->addSql('CREATE INDEX IDX_STARTING_AT ON app_event_occurrence (starting_at);');
        $this->addSql('CREATE INDEX IDX_ENDING_AT ON app_event_occurrence (ending_at);');

        $this->addSql('CREATE INDEX IDX_SLUG ON app_event_translation (slug);');
        $this->addSql('CREATE INDEX IDX_LOCALE ON app_event_translation (locale);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ID_OA;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ORIGIN_AGENDA;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_PRICING;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_AUDIENCE;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ACCESSIBILITY_II;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ACCESSIBILITY_HI;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ACCESSIBILITY_VI;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ACCESSIBILITY_PI;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ACCESSIBILITY_MI;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_STARTING_AT;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ENDING_AT;');
        $this->addSql('ALTER TABLE app_event DROP INDEX IDX_ENABLED;');

        $this->addSql('ALTER TABLE app_event_category DROP INDEX IDX_ID_OA;');
        $this->addSql('ALTER TABLE app_event_category DROP INDEX IDX_DISPLAY_OCCURRENCES;');
        $this->addSql('ALTER TABLE app_event_category DROP INDEX IDX_SEARCH_DISPLAY_POSITION;');

        $this->addSql('ALTER TABLE app_event_category_translation DROP INDEX IDX_LOCALE;');
        $this->addSql('ALTER TABLE app_event_category_translation DROP INDEX IDX_SLUG;');

        $this->addSql('ALTER TABLE app_event_occurrence DROP INDEX IDX_STARTING_AT;');
        $this->addSql('ALTER TABLE app_event_occurrence DROP INDEX IDX_ENDING_AT;');

        $this->addSql('ALTER TABLE app_event_translation DROP INDEX IDX_SLUG;');
        $this->addSql('ALTER TABLE app_event_translation DROP INDEX IDX_LOCALE;');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240619093911 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la relation entre pages et professionnels';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE luna_page ADD professional_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE luna_page ADD CONSTRAINT FK_2FC2238BDB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id)');
        $this->addSql('CREATE INDEX IDX_2FC2238BDB77003 ON luna_page (professional_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE luna_page DROP FOREIGN KEY FK_2FC2238BDB77003');
        $this->addSql('DROP INDEX IDX_2FC2238BDB77003 ON luna_page');
        $this->addSql('ALTER TABLE luna_page DROP professional_id');
    }
}

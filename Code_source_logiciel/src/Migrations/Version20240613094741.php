<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240613094741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des positions pour les projets et les parcours';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_project ADD position INT NOT NULL');
        $this->addSql('ALTER TABLE app_thematic_tour ADD position INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_project DROP position');
        $this->addSql('ALTER TABLE app_thematic_tour DROP position');
    }
}

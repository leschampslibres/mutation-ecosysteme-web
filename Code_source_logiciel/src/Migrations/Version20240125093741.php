<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240125093741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout du titre pour les menus et des icones pour les items de menu';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE monsieurbiz_menu ADD title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE monsieurbiz_menu_item ADD icon VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE monsieurbiz_menu DROP title');
        $this->addSql('ALTER TABLE monsieurbiz_menu_item DROP icon');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240930144518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Dates de début et de fin pour les messages d\'alerte';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_alert_message ADD starting_at DATETIME DEFAULT NULL, ADD ending_at DATETIME DEFAULT NULL');
        $this->addSql('CREATE INDEX idx_enabled ON app_alert_message (enabled)');
        $this->addSql('CREATE INDEX idx_starting_at ON app_alert_message (starting_at)');
        $this->addSql('CREATE INDEX idx_ending_at ON app_alert_message (ending_at)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX idx_enabled ON app_alert_message');
        $this->addSql('DROP INDEX idx_starting_at ON app_alert_message');
        $this->addSql('DROP INDEX idx_ending_at ON app_alert_message');
        $this->addSql('ALTER TABLE app_alert_message DROP starting_at, DROP ending_at');
    }
}

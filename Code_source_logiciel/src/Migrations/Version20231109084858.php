<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231109084858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de l\'image principale des pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE page_main_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT UNSIGNED DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_6D8186877E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE page_main_image ADD CONSTRAINT FK_6D8186877E3C61F9 FOREIGN KEY (owner_id) REFERENCES page (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page_main_image DROP FOREIGN KEY FK_6D8186877E3C61F9');
        $this->addSql('DROP TABLE page_main_image');
    }
}

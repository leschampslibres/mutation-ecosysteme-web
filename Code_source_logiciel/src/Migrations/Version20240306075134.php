<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240306075134 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event_category ADD search_display_position INT NOT NULL');
        $this->addSql('ALTER TABLE app_event_category_translation ADD label VARCHAR(255) DEFAULT NULL, ADD label_plural VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_event_category DROP search_display_position');
        $this->addSql('ALTER TABLE app_event_category_translation DROP label, DROP label_plural');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240701154055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '3 types de contenus pour les dossiers, boîtes à outils et expositions';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_tool DROP FOREIGN KEY FK_25DD1B7112469DE2');
        $this->addSql('CREATE TABLE app_dossier (id INT UNSIGNED AUTO_INCREMENT NOT NULL, color VARCHAR(255) DEFAULT NULL, pricing VARCHAR(255) DEFAULT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_D015607A77153098 (code), INDEX idx_enabled (enabled), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_dossiers_professionals (dossier_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_8BBDB2E3611C0C56 (dossier_id), INDEX IDX_8BBDB2E3DB77003 (professional_id), PRIMARY KEY(dossier_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_dossier_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, audience VARCHAR(255) DEFAULT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A1FD03A92C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_dossier_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_exhibit (id INT UNSIGNED AUTO_INCREMENT NOT NULL, color VARCHAR(255) DEFAULT NULL, pricing VARCHAR(255) DEFAULT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) NOT NULL, enabled TINYINT(1) DEFAULT 1 NOT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_9A64D5D77153098 (code), INDEX idx_enabled (enabled), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_exhibits_professionals (exhibit_id INT UNSIGNED NOT NULL, professional_id INT UNSIGNED NOT NULL, INDEX IDX_392B206F2E5CE433 (exhibit_id), INDEX IDX_392B206FDB77003 (professional_id), PRIMARY KEY(exhibit_id, professional_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_exhibit_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, audience VARCHAR(255) DEFAULT NULL, blocks LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, main_image JSON DEFAULT NULL, canonical_url VARCHAR(255) DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, open_graph_title VARCHAR(255) DEFAULT NULL, open_graph_description LONGTEXT DEFAULT NULL, open_graph_image JSON DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_5D3934562C2AC5D3 (translatable_id), INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_exhibit_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_dossiers_professionals ADD CONSTRAINT FK_8BBDB2E3611C0C56 FOREIGN KEY (dossier_id) REFERENCES app_dossier (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_dossiers_professionals ADD CONSTRAINT FK_8BBDB2E3DB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_dossier_translation ADD CONSTRAINT FK_A1FD03A92C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_dossier (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_exhibits_professionals ADD CONSTRAINT FK_392B206F2E5CE433 FOREIGN KEY (exhibit_id) REFERENCES app_exhibit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_exhibits_professionals ADD CONSTRAINT FK_392B206FDB77003 FOREIGN KEY (professional_id) REFERENCES app_professional (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_exhibit_translation ADD CONSTRAINT FK_5D3934562C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_exhibit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_tool_category_translation DROP FOREIGN KEY FK_915C06AD2C2AC5D3');
        $this->addSql('DROP TABLE app_tool_category');
        $this->addSql('DROP TABLE app_tool_category_translation');
        $this->addSql('DROP INDEX IDX_25DD1B7112469DE2 ON app_tool');
        $this->addSql('ALTER TABLE app_tool DROP category_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_tool_category (id INT UNSIGNED AUTO_INCREMENT NOT NULL, position INT NOT NULL, no_index TINYINT(1) DEFAULT 0 NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, code VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, UNIQUE INDEX UNIQ_73F0504A77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE app_tool_category_translation (id INT UNSIGNED AUTO_INCREMENT NOT NULL, translatable_id INT UNSIGNED NOT NULL, blocks LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, main_image LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, canonical_url VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, meta_title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, meta_description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, open_graph_title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, open_graph_description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, open_graph_image LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, locale VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX idx_slug (slug), INDEX idx_locale (locale), UNIQUE INDEX app_tool_category_translation_uniq_trans (translatable_id, locale), INDEX IDX_915C06AD2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE app_tool_category_translation ADD CONSTRAINT FK_915C06AD2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_tool_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_dossiers_professionals DROP FOREIGN KEY FK_8BBDB2E3611C0C56');
        $this->addSql('ALTER TABLE app_dossiers_professionals DROP FOREIGN KEY FK_8BBDB2E3DB77003');
        $this->addSql('ALTER TABLE app_dossier_translation DROP FOREIGN KEY FK_A1FD03A92C2AC5D3');
        $this->addSql('ALTER TABLE app_exhibits_professionals DROP FOREIGN KEY FK_392B206F2E5CE433');
        $this->addSql('ALTER TABLE app_exhibits_professionals DROP FOREIGN KEY FK_392B206FDB77003');
        $this->addSql('ALTER TABLE app_exhibit_translation DROP FOREIGN KEY FK_5D3934562C2AC5D3');
        $this->addSql('DROP TABLE app_dossier');
        $this->addSql('DROP TABLE app_dossiers_professionals');
        $this->addSql('DROP TABLE app_dossier_translation');
        $this->addSql('DROP TABLE app_exhibit');
        $this->addSql('DROP TABLE app_exhibits_professionals');
        $this->addSql('DROP TABLE app_exhibit_translation');
        $this->addSql('ALTER TABLE app_tool ADD category_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE app_tool ADD CONSTRAINT FK_25DD1B7112469DE2 FOREIGN KEY (category_id) REFERENCES app_tool_category (id)');
        $this->addSql('CREATE INDEX IDX_25DD1B7112469DE2 ON app_tool (category_id)');
    }
}

<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231030144533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des éléments SEO pour les pages';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page ADD no_index TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE page_translation ADD meta_title VARCHAR(255) DEFAULT NULL, ADD meta_description LONGTEXT DEFAULT NULL, ADD canonical_url VARCHAR(255) DEFAULT NULL, ADD open_graph_title VARCHAR(255) DEFAULT NULL, ADD open_graph_description LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP no_index');
        $this->addSql('ALTER TABLE page_translation DROP meta_title, DROP meta_description, DROP canonical_url, DROP open_graph_title, DROP open_graph_description');
    }
}

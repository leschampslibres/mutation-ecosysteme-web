<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrototypeController extends AbstractController
{
    public function indexAction(Request $request): Response
    {
        return $this->render('@SyliusShop/Prototype/index.html.twig');
    }

    public function showAction(Request $request, string $type, string $slug): Response
    {
        return $this->render('@SyliusShop/Prototype/' . $type . '/' . $slug . '.html.twig');
    }
}

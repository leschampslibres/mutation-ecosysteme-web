<?php

declare(strict_types=1);

namespace App\Controller\Event;

use App\Entity\Event\Event;
use App\Entity\Event\EventOccurrence;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EventController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Event $resource */
        $resource = $this->findOr404($configuration);

        // Déclenchement d'une exception 410 si l'événement est terminé et nettoyé
        $this->isPublishedOr410($resource);

        $event = $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        $underPagesService = $this->container->get('luna.under_pages.service');
        $underPage = $underPagesService->underPageByCode('events');

        // Calcul des éléments du breadcrumb
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode('events');
        $label = $resource->getTitle();
        $href = $this->generateUrl('app_event_show', ['slug' => $resource->getSlug(), 'page_slug' => $underPage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];

        $em = $this->getDoctrine()->getManager();

        $relatedProgrammesByDays = [];
        if (null != $resource->getChildrenKeyword()) {
            // récupération des occurrences pour le bloc 'programmation'
            $relatedProgrammes = $em->getRepository(Event::class)->findByKeywordAndDate($resource->getChildrenKeyword(), $resource->getStartingAt(), $resource->getEndingAt());
            foreach ($relatedProgrammes as $relatedProgramme) {
                if ($relatedProgramme->getCategory()->getDisplayOccurrences()) {
                    $occurrences = $em->getRepository(EventOccurrence::class)->findByDate($relatedProgramme, $resource->getStartingAt(), $resource->getEndingAt());
                    foreach ($occurrences as $occurrence) {
                        $day = $occurrence->getStartingAt()->format('Y-m-d');
                        if (!isset($relatedProgrammesByDays[$day])) {
                            $relatedProgrammesByDays[$day] = [];
                        }
                        if ($occurrence->getStartingAt() > new \DateTime()) {
                            $date = $occurrence->getStartingAt()->format('H:i');
                        } else {
                            $date = 'Terminé';
                        }
                        $relatedProgrammesByDays[$day][] = [
                            'title' => $relatedProgramme->getTitle(),
                            'href' => $this->generateUrl('app_event_show', ['slug' => $relatedProgramme->getSlug(), 'occurrence_id' => $occurrence->getid(), 'page_slug' => $underPage->getCompositeSlug()]),
                            'description' => $relatedProgramme->getCategory()->getLabel(),
                            'state' => $date,
                        ];
                    }
                }
            }
            ksort($relatedProgrammesByDays);
        }

        $occurrencesByMonths = [];
        $currentEventOccurrence = null;

        // toutes les occurrences de l'événement
        if ($resource->getCategory()->getDisplayOccurrences()) {
            $occurrences = $em->getRepository(EventOccurrence::class)->findByDate($resource, new \DateTime());
            $currentOccurrence = $request->attributes->get('occurrence_id');
            if (null != $currentOccurrence) {
                $currentEventOccurrence = $em->getRepository(EventOccurrence::class)->find($currentOccurrence);
                if (null == $currentEventOccurrence) {
                    return $this->redirectToRoute('app_event_show', ['slug' => $resource->getSlug(), 'page_slug' => $underPage->getCompositeSlug()]);
                }
                if (null == $resource->getCanonicalUrl()) {
                    $resource->setCanonicalUrl($this->generateUrl('app_event_show', ['slug' => $resource->getSlug(), 'page_slug' => $underPage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
                }
            }

            foreach ($occurrences as $occurrence) {
                $month = $occurrence->getStartingAt()->format('Y-m');
                if (!isset($occurrencesByMonths[$month])) {
                    $occurrencesByMonths[$month] = [];
                }

                if ($occurrence->getId() == $currentOccurrence) {
                    $url = '';
                } else {
                    $url = $this->generateUrl('app_event_show', ['slug' => $resource->getSlug(), 'occurrence_id' => $occurrence->getid(), 'page_slug' => $underPage->getCompositeSlug()]);
                }
                $occurrencesByMonths[$month][] = [
                    'title' => $occurrence->getStartingAt(),
                    'href' => $url,
                    'description' => '',
                    'state' => $occurrence->getStartingAt()->format('H:i'),
                    'duration' => $occurrence->CalculateDuration(),
                ];
            }
        }

        // les événements de la même catégorie
        $eventsSameCategory = $em->getRepository(Event::class)->findByCategoryAndNotId($resource->getCategory(), $resource);
        $eventsNext = [];
        if ($resource->getCategory()->getDisplayOccurrences()) {
            foreach ($eventsSameCategory as $event) {
                $nextOccurrence = $em->getRepository(EventOccurrence::class)->findNext($event);
                if (null != $nextOccurrence) {
                    $eventsNext[] = [
                        'picture' => $event->getMainImage(),
                        'tag' => $event->getCategory()->getLabel(),
                        'title' => $event->getTitle(),
                        'dateStarting' => $nextOccurrence->getStartingAt(),
                        'location' => $event->getLocation(),
                        'pricing' => $event->getPricing(),
                        'status' => $event->getStatus(),
                        'color' => $event->getColor(),
                        'href' => $this->generateUrl('app_event_show', ['slug' => $event->getSlug(), 'occurrence_id' => $nextOccurrence->getid(), 'page_slug' => $underPage->getCompositeSlug()]),
                    ];
                }
            }

            usort($eventsNext, function ($a, $b) {
                return $a['dateStarting'] <=> $b['dateStarting'];
            });
        } else {
            foreach ($eventsSameCategory as $event) {
                $eventsNext[] = [
                    'picture' => $event->getMainImage(),
                    'tag' => $event->getCategory()->getLabel(),
                    'title' => $event->getTitle(),
                    'dateStarting' => $event->getStartingAt(),
                    'dateEnding' => $event->getEndingAt(),
                    'location' => $event->getLocation(),
                    'pricing' => $event->getPricing(),
                    'status' => $event->getStatus(),
                    'href' => $this->generateUrl('app_event_show', ['slug' => $event->getSlug(), 'page_slug' => $underPage->getCompositeSlug()]),
                ];
            }
        }

        if (\count($eventsNext) < 3) {
            $eventsNext = \array_slice($eventsNext, 0, \count($eventsNext));
        }

        $template = '@SyliusShop/Event/show.html.twig';
        if ($request->attributes->get('preview')) {
            $showTemplate = $template;
            $template = '@LunaCore/Admin/Crud/preview.html.twig';
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($template, [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                'breadcrumb' => $breadcrumb,
                'relatedProgrammesByDays' => $relatedProgrammesByDays,
                'occurrencesByMonths' => $occurrencesByMonths,
                'eventsSameCategory' => $eventsNext,
                'isOccurrence' => (null !== $currentEventOccurrence),
                'eventOccurrence' => $currentEventOccurrence,
                $this->metadata->getName() => $resource,
                'show_template' => $showTemplate ?? null,
                'page' => $underPage,
            ]);
        }

        return $this->createRestView($configuration, $resource);
    }

    public function previewAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Event $resource */
        $resource = $this->findOr404($configuration);

        return $this->forward('App\Controller\Event\EventController::showAction', [
            'id' => $resource->getId(),
            '_route' => $request->attributes->get('_route'),
            'preview' => true,
        ]);
    }

    public function clearAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::DELETE);
        /** @var Event $resource */
        $resource = $this->findOr404($configuration);

        if ($configuration->isCsrfProtectionEnabled() && !$this->isCsrfTokenValid((string) $resource->getId(), (string) $request->request->get('_csrf_token'))) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid csrf token.');
        }

        $eventClearingService = $this->container->get('luna.service.event_clearing_service');
        $eventClearingService->clearEvent($resource, true, true);

        return $this->redirectHandler->redirectToIndex($configuration, $resource);
    }

    /**
     * @throws NotFoundHttpException
     */
    protected function isPublishedOr410(Event $event): void
    {
        if ($event->getStatus() >= Event::STATUS_ENDED) {
            throw new GoneHttpException($event->getTitle());
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Event;

use App\Entity\Event\EventCategory;
use App\Entity\Event\EventOccurrence;
use Luna\CoreBundle\Entity\Page\PageInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventOccurrenceController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function indexAction(Request $request): Response
    {
        // Traitement automatique de la grille Sylius

        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $this->isGrantedOr403($configuration, ResourceActions::INDEX);

        // Traitement des plages de dates prédéfinies ("aujourd'hui", "ce week-end", "cette semaine")

        $dateRange = null;
        $dateRangeOnly = true;
        $searchingWithDates = false;

        // Le paramètre "date" peut être passé de différentes manières au controller.
        if (\array_key_exists('date', $request->query->all())) {
            $dateRange = ('date-personnalisee' != $request->query->get('date')) ? $request->query->get('date') : null;
        } elseif (\array_key_exists('date', $request->attributes->all())) {
            $dateRange = ('date-personnalisee' != $request->attributes->get('date')) ? $request->attributes->get('date') : null;
            $request->query->set('date', $dateRange);
        }

        if (\array_key_exists('criteria', $request->query->all())) {
            $criteria = $request->query->all()['criteria'];
        } else {
            $criteria = [];
        }

        // Conversion des plages de dates prédéfinies ("aujourd'hui", "ce week-end", "cette semaine") en plages de dates réelles
        if ($dateRange) {
            $searchingWithDates = true;
            $from = null;
            $to = null;
            if ('aujourdhui' == $dateRange) {
                $from = new \DateTime();
                $to = new \DateTime();
            } elseif ('ce-week-end' == $dateRange) {
                $today = new \DateTime();
                $saturday = new \DateTime('saturday this week');
                $from = ($today <= $saturday) ? $saturday : $today;
                $to = new \DateTime('sunday this week');
            } elseif ('cette-semaine' == $dateRange) {
                $from = new \DateTime();
                $to = new \DateTime('sunday this week');
            }
            if (\array_key_exists('criteria', $request->query->all())) {
                $criteria = $request->query->all()['criteria'];
            } else {
                $criteria = [];
            }
        } else {
            // Préparation pour l'initialisation des heures de début et de fin pour une sélection de dates avec les datepickers
            $hasCriteriaDates = (\array_key_exists('criteria', $request->query->all()) && \array_key_exists('dates', $request->query->all()['criteria']));
            $hasCriteriaDatesFromOrTo = ($hasCriteriaDates && ($request->query->all()['criteria']['dates']['from'] || $request->query->all()['criteria']['dates']['to']));
            if ($hasCriteriaDatesFromOrTo) {
                $searchingWithDates = true;
                $from = ($request->query->all()['criteria']['dates']['from'] && strtotime($request->query->all()['criteria']['dates']['from'])) ? new \DateTime($request->query->all()['criteria']['dates']['from']) : '';
                $to = ($request->query->all()['criteria']['dates']['to'] && strtotime($request->query->all()['criteria']['dates']['to'])) ? new \DateTime($request->query->all()['criteria']['dates']['to']) : '';
            }
        }

        // Initialisation des heures de début et de fin pour les dates de début et de fin y compris pour une sélection de date avec les datepickers
        // (pour avoir du jour de début à 0h au jour de fin à 23h59)

        if ($searchingWithDates) {
            $criteria['dates'] = [
                'from' => $from ? $from->format('Y-m-d 0:0:0') : '',
                'to' => $to ? $to->format('Y-m-d 23:59:59') : '',
            ];
            $request->query->set('criteria', $criteria);
        }

        // Une recherche par catégorie entraîne des comportements spécifiques

        $displaySections = true;

        // Rétrocompatibilité avec les anciennes URLs (avant, on ne pouvait chercher que par une seule catégorie)
        $isCategorySearch = (\array_key_exists('criteria', $request->query->all()) && \array_key_exists('category', $request->query->all()['criteria']) && \is_string($request->query->all()['criteria']['category']));
        $isCategoriesSearch = (\array_key_exists('criteria', $request->query->all()) && \array_key_exists('category', $request->query->all()['criteria']) && \is_array($request->query->all()['criteria']['category']));

        // Pour une recherche multi-catégories, il faut trier les catégories en fonction de leur position d'affichage

        $categoryIds = $isCategoriesSearch ? $request->query->all()['criteria']['category'] : ($isCategorySearch ? [$request->query->all()['criteria']['category']] : null);
        $sectionCategoryIds = ['first' => [], 'second' => [], 'third' => []];
        if ($categoryIds) {
            $displaySections = \count($categoryIds) > 1 ? true : false;

            $categories = $this->getDoctrine()->getManager()->getRepository(EventCategory::class)->findBy(['id' => $categoryIds]);
            foreach ($categories as $category) {
                $sectionCategoryIds[strtolower($category->getSearchDisplayPosition()->name)][] = $category->getId();
            }
        }

        // Pour une recherche avec une seule catégorie

        $categoryOnly = false;

        // Catégorie recherchée par slug ou id
        // Récupération de la catégorie
        $categoryId = (\is_array($categoryIds) && 1 == \count($categoryIds)) ? $categoryIds[0] : null;
        $categorySlug = \array_key_exists('slug', $request->attributes->all()) ? $request->attributes->all()['slug'] : null;

        $category = null;

        if ($categoryId || $categorySlug) {
            $displaySections = false;

            $categoryOnly = true;

            if ($categoryId) {
                $category = $this->getDoctrine()->getManager()->getRepository(EventCategory::class)->find($categoryId);
            } elseif ($categorySlug) {
                /** @var EventCategoryRepository $categoryRepository */
                $categoryRepository = $this->getDoctrine()->getManager()->getRepository(EventCategory::class);
                $category = $categoryRepository->findOneBySlug($request->getLocale(), $categorySlug);
                if ($category) {
                    $request->query->set('criteria', ['category' => $category->getId()]);
                }
            }

            // Utilisation d'une grille spécifique pour récupérer les occurrences ou les événements de la catégorie, en fonction de displayOccurrences
            if ($category) {
                $_sylius = $request->attributes->get('_sylius');
                if ($category->getDisplayOccurrences()) {
                    $_sylius['grid'] = 'app_shop_event_occurrence_category_search';
                } else {
                    $_sylius['grid'] = 'app_shop_event_category_search';
                }
                $request->attributes->set('_sylius', $_sylius);
                $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
            }
        }

        if ($dateRange || $categoryId) {
            // Pour une recherche uniquement par plage prédéfinie ou par catégorie, on a un comportement particulier
            $criteria = $request->query->all()['criteria'];
            foreach ($criteria as $key => $value) {
                // Cas des plages de dates personnalisées
                if ('dates' == $key) {
                    foreach ($value as $subValue) {
                        if ($subValue) {
                            $categoryOnly = false;

                            break;
                        }
                    }
                } elseif ($value && 'category' != $key && 'date' != $key) {
                    $categoryOnly = false;

                    break;
                }
            }
            if ($dateRange) {
                $categoryOnly = false;
            }
        }

        // Traitement automatique de la grille Sylius
        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);
        $event = $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $resources);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        // Comportement spécifique pour gestion des sections de mise en avant d'événements, dans certaines conditions :
        // - uniquement sur la 1ère page des résultats
        // - s'il n'y a pas de catégorie sélectionnée dans le moteur de recherche OU s'il y a plusieurs catégories sélectionnées

        $sectionResources = [];

        if ($displaySections) {
            if (1 == $resources->getData()->getCurrentPage()) {
                $sections = ['first', 'second'];
                foreach ($sections as $section) {
                    // Dans le cas d'une sélection de catégories, il ne faut récupérer les occurrences et événements que lorsque qu'une ou plusieurs catégories de la section ont été sélectionnées.
                    if (null !== $categoryIds && \count($categoryIds) > 1 && \is_array($sectionCategoryIds[$section]) && empty($sectionCategoryIds[$section])) {
                        continue;
                    }
                    // On se branche sur le système de grille de Sylius pour utiliser des grilles supplémentaires.
                    $sectionRequest = $request;
                    $criteria['category'] = $sectionCategoryIds[$section];
                    $sectionRequest->query->set('criteria', $criteria);
                    $_sylius = $sectionRequest->attributes->get('_sylius');

                    // Utilisation de la grille pour les occurrences
                    $_sylius['grid'] = 'app_shop_event_occurrence_' . $section . '_search';
                    $sectionRequest->attributes->set('_sylius', $_sylius);
                    $sectionConfiguration = $this->requestConfigurationFactory->create($this->metadata, $sectionRequest);

                    // Récupération des occurrences
                    $sectionOccurrences = $this->resourcesCollectionProvider->get($sectionConfiguration, $this->repository);

                    // Utilisation de la grille pour les événements
                    $_sylius['grid'] = 'app_shop_event_' . $section . '_search';
                    $sectionRequest->attributes->set('_sylius', $_sylius);
                    $sectionConfiguration = $this->requestConfigurationFactory->create($this->metadata, $sectionRequest);

                    // Récupération des événements
                    $sectionEvents = $this->resourcesCollectionProvider->get($sectionConfiguration, $this->repository);

                    // On a récupéré des occurrences et des événements, que l'on doit afficher
                    // par ordre chronologique dans une même liste.

                    // Initialisation d'un tableau listant les occurrences et événements de la section en cours
                    // avec la date de début/date de fin/id de l'occurrence/événement en clé
                    // et l'occurrence/événement en valeur
                    $currentSectionResources = [];

                    // Ajout des occurrences dans le tableau
                    foreach ($sectionOccurrences->getData() as $occurrence) {
                        $index = $occurrence->getStartingAt()->format('Y-m-d H:i:s') . ' - ' . $occurrence->getEndingAt()->format('Y-m-d H:i:s') . ' - ' . $occurrence->getId();
                        $currentSectionResources[$index] = $occurrence;
                    }

                    // Ajout des événements dans le tableau
                    foreach ($sectionEvents->getData() as $event) {
                        $index = $event->getStartingAt()->format('Y-m-d H:i:s') . ' - ' . $event->getEndingAt()->format('Y-m-d H:i:s') . ' - ' . $event->getId();
                        $currentSectionResources[$index] = $event;
                    }

                    // Tri du tableau par clé, c'est-à-dire par date de début/date de fin/id
                    ksort($currentSectionResources);

                    // Récupération uniquement des valeurs du tableau, c'est-à-dire de la liste des occurrences et événements
                    $currentSectionResources = array_values($currentSectionResources);

                    // Ajout des occurrences et événements de cette section au tableau global des ressources par section
                    $sectionResources[$section] = $currentSectionResources;
                }
            }
        }

        // Pour que les liens de pagination, lorsque l'on est sur une page de catégorie, ne contiennent pas de "criteria" en paramètre GET
        // Exemple à éviter : https://localhost:3000/au-programme/categorie/visites-aux-champs-libres?criteria%5Bcategory%5D=5&page=2
        if ($categorySlug) {
            $request->query->remove('criteria');
        }

        // Récupération de la page parente si elle existe

        if ($dateRange && $dateRangeOnly) {
            if ('aujourdhui' == $dateRange) {
                $underPagesCode = 'events_today';
                $route = 'app_event_today';
            } elseif ('ce-week-end' == $dateRange) {
                $underPagesCode = 'events_weekend';
                $route = 'app_event_weekend';
            } elseif ('cette-semaine' == $dateRange) {
                $underPagesCode = 'events_week';
                $route = 'app_event_week';
            }
        } elseif ($category && $categoryOnly) {
            $underPagesCode = 'events';
            $route = 'app_event_category_show';
        } else {
            $underPagesCode = 'event_search';
            $route = 'app_event_occurrence_index';
        }

        // Récupération du service under-pages
        $underPagesService = $this->container->get('luna.under_pages.service');

        $pageRepository = $this->getDoctrine()->getManager()->getRepository(PageInterface::class);
        // À partir de l'URL en cours sur la page de recherche,
        // ou à partir du code under-pages de la page de recherche sur la page d'accueil programme
        if ($underPagesCode) {
            $page = $underPagesService->underPageByCode($underPagesCode);
        } else {
            $page = $pageRepository->findOneByCompositeSlug($request->getLocale(), $request->attributes->get('page_slug'));
        }

        // En fonction de la recherche, on peut être sur la page de recherche, sur une page "Dates" ou sur une catégorie.
        // Les informations à afficher sont différentes.
        if ($dateRange && $dateRangeOnly) {
            // Breadcrumb
            $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);

            // URL canonique lorsqu'on est sur l'URL de la recherche et que la recherche porte uniquement sur une plage de dates prédéfinie
            // Attention à ne pas surcharger une URL canonique déjà définie en BO pour la page concernée
            if (!$page->getCanonicalUrl()) {
                $href = $this->generateUrl($route, ['page_slug' => $page->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
                $page->setCanonicalUrl($href);
            }
        } elseif ($category && $categoryOnly) {
            // Breadcrumb
            $breadcrumb = $underPagesService->underPageBreadcrumbByCode('events');
            $label = $category->getTitle();
            $href = $this->generateUrl('app_event_category_show', ['slug' => $category->getSlug(), 'page_slug' => $underPagesService->underPageSlugByCode('events')], UrlGeneratorInterface::ABSOLUTE_URL);
            $breadcrumb[] = ['label' => $label, 'href' => $href];

            // URL canonique lorsqu'on est sur l'URL de la recherche et que la recherche porte uniquement sur la catégorie
            // Attention à ne pas surcharger une URL canonique déjà définie en BO pour la catégorie concernée
            if (!$category->getCanonicalUrl()) {
                $category->setCanonicalUrl($href);
            }
        } else {
            $breadcrumb = $underPagesService->underPageBreadcrumbByCode($page->getUnderPagesCode());
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($configuration->getTemplate(ResourceActions::INDEX . '.html'), [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resources' => $resources,
                $this->metadata->getPluralName() => $resources,
                'breadcrumb' => $breadcrumb,
                'section_resources' => $sectionResources,
                'category_page' => !empty($categorySlug),
                'category' => $categoryOnly ? $category : null,
                'page' => $page,
            ]);
        }

        return $this->createRestView($configuration, $resources);
    }

    public function iCalendarAction(Request $request, TranslatorInterface $translator): Response
    {
        $underPagesService = $this->container->get('luna.under_pages.service');
        $pageSlug = $underPagesService->underPageSlugByCode($request->attributes->get('_under_pages_code'));

        $iCalendar = $this->iCalendarLine('BEGIN:VCALENDAR');
        $iCalendar .= $this->iCalendarLine('VERSION:2.0');
        $iCalendar .= $this->iCalendarLine('PRODID:-//Les Champs Libres//Agenda//FR');
        $iCalendar .= $this->iCalendarLine('CALSCALE:GREGORIAN');
        $iCalendar .= $this->iCalendarLine('METHOD:PUBLISH');

        $occurrences = $this->getDoctrine()->getManager()->getRepository(EventOccurrence::class)->findAllUpcoming();

        foreach ($occurrences as $occurrence) {
            $iCalendar .= $this->iCalendarLine('BEGIN:VEVENT');
            $iCalendar .= $this->iCalendarLine('UID:' . $occurrence->getEvent()->getIdOa() . $occurrence->getId());
            $iCalendar .= $this->iCalendarLine('DTSTAMP:' . $occurrence->getStartingAt()->format('Ymd\THis\Z'));
            $iCalendar .= $this->iCalendarLine('DTSTART:' . $occurrence->getStartingAt()->format('Ymd\THis\Z'));
            $iCalendar .= $this->iCalendarLine('DTEND:' . $occurrence->getEndingAt()->format('Ymd\THis\Z'));
            $iCalendar .= $this->iCalendarLine('SUMMARY:' . $occurrence->getEvent()->getTitle());
            $iCalendar .= $this->iCalendarLine('LOCATION:' . $translator->trans('app.ui.resource.event.location.' . (string) ($occurrence->getEvent()->getLocation())));
            $iCalendar .= $this->iCalendarLine('CATEGORIES:' . $occurrence->getEvent()->getCategory()->getLabel());
            $iCalendar .= $this->iCalendarLine('STATUS:' . $translator->trans('app.ui.resource.event.status.ical.' . (string) ($occurrence->getEvent()->getStatus())));
            $iCalendar .= $this->iCalendarLine('DESCRIPTION:' . $occurrence->getEvent()->getDescription());
            $iCalendar .= $this->iCalendarLine('URL:' . $this->generateUrl('app_event_show', ['slug' => $occurrence->getEvent()->getSlug(), 'occurrence_id' => $occurrence->getId(), 'page_slug' => $pageSlug], UrlGeneratorInterface::ABSOLUTE_URL));
            $iCalendar .= $this->iCalendarLine('END:VEVENT');
        }

        $iCalendar .= $this->iCalendarLine('END:VCALENDAR');

        $headers = [
            'Content-Type' => 'text/calendar; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="agenda-des-champs-libres.ics"',
        ];

        return new Response($iCalendar, 200, $headers);
    }

    protected function iCalendarLine($content): string
    {
        $lineSeparator = "\r\n";
        $lineLength = 75;

        $content = str_replace(["\r", "\n"], '', $content);

        while (\strlen($content) > $lineLength) {
            $lines[] = mb_strcut($content, 0, $lineLength, 'utf-8');
            $content = ' ' . mb_strcut($content, $lineLength, \strlen($content), 'utf-8');
        }
        $lines[] = $content;

        return implode($lineSeparator, $lines) . $lineSeparator;
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Newsletter;

use App\Form\Type\Newsletter\NewsletterType;
use CaptchaEU\Service as CaptchaService;
use Luna\CoreBundle\Service\UnderPagesService;
use Mailjet\Client;
use Mailjet\Resources;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsletterController extends AbstractController
{
    public function __construct(protected readonly UnderPagesService $underPagesService)
    {
    }

    public function new(Request $request): Response
    {
        $underPagesCode = $request->attributes->get('_under_pages_code');

        // Calcul des éléments du breadcrumb
        $breadcrumb = $this->underPagesService->underPageBreadcrumbByCode($underPagesCode);

        // Page parente
        $page = $this->underPagesService->underPageByCode($underPagesCode);

        $form = $this->createForm(NewsletterType::class);

        $captcha = new CaptchaService($request->server->get('CAPTCHA_EU_PUBLIC_KEY'), $request->server->get('CAPTCHA_EU_REST_KEY'), $request->server->get('CAPTCHA_EU_ENDPOINT'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $captcha->validate($_POST['captcha_at_solution'])) {
            $newsletter = $form->getData();

            $mj = new Client($this->getParameter('mailjet.api_key'), $this->getParameter('mailjet.api_secret'), $this->getParameter('mailjet.enabled'));

            // Ajout du contact à tous les contacts
            $bodyContact = [
                'IsExcludedFromCampaigns' => false,
                'Email' => $newsletter['email'],
            ];
            $responseContact = $mj->post(Resources::$Contact, ['body' => $bodyContact]);

            // Si le contact est déjà présent dans la liste générale
            if (str_contains($responseContact->getReasonPhrase(), 'for Email already exists')) {
                $responseEmailExists = $mj->get(Resources::$Contact, ['id' => $newsletter['email']]);
                $responseContact = $responseEmailExists;
            }

            if ($responseContact->success()) {
                // Récupération de l'id du contact
                $contactId = $responseContact->getData()[0]['ID'];

                // Ajout du contact à la liste de contacts
                $body = [
                    'ContactsLists' => [
                        [
                            'Action' => 'addnoforce',
                            'ListID' => $this->getParameter('mailjet.list_contacts'),
                        ],
                    ],
                ];

                $response = $mj->post(Resources::$ContactManagecontactslists, ['id' => $contactId, 'body' => $body]);

                return $this->redirectToRoute('app_newsletter_confirm', ['page_slug' => $page->getCompositeSlug()]);
            }
        }

        return $this->render('@SyliusShop/Newsletter/new.html.twig', [
            'script_captcha' => $captcha->script(),
            'validator' => $captcha->protectForm('newsletter'),
            'form' => $form,
            'breadcrumb' => $breadcrumb,
            'page' => $page,
        ]);
    }

    public function confirm(Request $request): Response
    {
        $underPagesCode = $request->attributes->get('_under_pages_code');

        // Calcul des éléments du breadcrumb
        $breadcrumb = $this->underPagesService->underPageBreadcrumbByCode($underPagesCode);

        // Page parente
        $page = $this->underPagesService->underPageByCode($underPagesCode);

        return $this->render('@SyliusShop/Newsletter/confirm.html.twig', [
            'breadcrumb' => $breadcrumb,
            'page' => $page,
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Article;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleCategoryController extends ResourceController
{
    /**
     * @throws HttpException
     */
    public function moveupAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var ArticleCategory $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() - 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_article_category_index');
    }

    /**
     * @throws HttpException
     */
    public function movedownAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var ArticleCategory $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() + 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_article_category_index');
    }
}

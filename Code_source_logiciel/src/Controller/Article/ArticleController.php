<?php

declare(strict_types=1);

namespace App\Controller\Article;

use App\Entity\Article\Article;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleController extends ResourceController
{
    #[Cache(public: true, maxage: 8640000, mustRevalidate: true)]
    public function searchAction(Request $request): Response
    {
        // Traitement automatique de la grille Sylius

        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        $this->isGrantedOr403($configuration, ResourceActions::INDEX);

        // Recherche par rubrique ou sujet si les infos sont présentes dans la route

        $taxonType = $request->attributes->get('_taxon_type');
        if ($taxonType) {
            // Récupération de la rubrique ou du sujet
            $taxonClass = $request->attributes->get('_entity_class');
            $repository = $this->getDoctrine()->getManager()->getRepository($taxonClass);
            $taxon = $repository->findOneBySlug($request->getLocale(), $request->attributes->get('slug'));

            // Ajout de la rubrique ou du sujet en tant que critère de recherche
            $request->query->set('criteria', [$request->attributes->get('_criteria') => $taxon->getId()]);
            $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);
        }

        // Traitement automatique de la grille Sylius

        $resources = $this->resourcesCollectionProvider->get($configuration, $this->repository);
        $event = $this->eventDispatcher->dispatchMultiple(ResourceActions::INDEX, $configuration, $resources);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        // Under-pages

        $underPagesService = $this->container->get('luna.under_pages.service');
        $articlePage = $underPagesService->underPageByCode('articles');
        $indexPage = $underPagesService->underPageByCode('articles_index');

        // Flux RSS

        $feedTitle = $taxonType ? $taxon->getTitle() . ' dans le Mag des Champs Libres' : 'Le Mag des Champs Libres';
        $feedUrl = $taxonType ?
            $this->generateUrl('app_article_' . $taxonType . '_feed', ['slug' => $taxon->getSlug(), 'page_slug' => $articlePage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL)
            : $this->generateUrl('app_article_index_feed', ['page_slug' => $indexPage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);

        if ('rss' == $request->attributes->get('format')) {
            $filename = $taxonType ? $taxon->getSlug() . '-dans-le-mag-des-champs-libres' : 'le-mag-des-champs-libres';
            $description = $taxonType ? $taxon->getDescription() : 'Les derniers articles du Mag des Champs Libres';

            $headers = [
                'Content-Type' => 'application/rss+xml',
                'Content-Disposition' => 'attachment; filename="' . $filename . '.rss"',
            ];

            $feedData = [
                'title' => $feedTitle,
                'link' => $this->generateUrl('sylius_shop_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'feed_url' => $feedUrl,
                'description' => $description,
                'locale' => $request->getLocale(),
                'page_slug' => $articlePage->getCompositeSlug(),
                'items' => $resources,
            ];

            return new Response($this->createRssFeed($feedData), 200, $headers);
        }

        // Breadcrumb

        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($request->attributes->get('_under_pages_code'));
        if ($taxonType) {
            $label = $taxon->getTitle();
            $href = $this->generateUrl('app_article_' . $taxonType . '_show', ['slug' => $taxon->getSlug(), 'page_slug' => $articlePage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
            $breadcrumb[] = ['label' => $label, 'href' => $href];
        }

        return $this->render($configuration->getParameters()->get('template'), [
            'configuration' => $configuration,
            'metadata' => $this->metadata,
            'resources' => $resources,
            $this->metadata->getPluralName() => $resources,
            'breadcrumb' => $breadcrumb,
            'page' => $articlePage ?? $indexPage,
            'taxon' => $taxon ?? null,
            'taxon_type' => $taxonType,
            'page_slug' => $articlePage->getCompositeSlug(),
            'feed_title' => $feedTitle,
            'feed_url' => $feedUrl,
        ]);
    }

    #[Cache(public: true, maxage: 8640000, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Article $resource */
        $resource = $this->findOr404($configuration);

        $event = $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        $underPagesService = $this->container->get('luna.under_pages.service');
        $underPagesCode = $request->attributes->get('_under_pages_code');

        // Breadcrumb

        // Page parente de la ressource
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);
        $pageSlug = $underPagesService->underPageSlugByCode($underPagesCode);
        // Rubrique
        $label = $resource->getCategory()->getTitle();
        $href = $this->generateUrl('app_article_category_show', ['slug' => $resource->getCategory()->getSlug(), 'page_slug' => $pageSlug], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];
        // Article en cours
        $label = $resource->getTitle();
        $href = $this->generateUrl('app_article_show', ['slug' => $resource->getSlug(), 'page_slug' => $pageSlug, 'category_slug' => $resource->getCategory()->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];

        // Utilisation du template show ou preview

        $template = '@SyliusShop/Article/show.html.twig';
        if ($request->attributes->get('preview')) {
            $showTemplate = $template;
            $template = '@LunaCore/Admin/Crud/preview.html.twig';
        }

        // Informations pour le flux RSS

        $feedTitle = $resource->getCategory()->getTitle() . ' dans le Mag des Champs Libres';
        $feedUrl = $this->generateUrl('app_article_category_feed', ['slug' => $resource->getCategory()->getSlug(), 'page_slug' => $pageSlug], UrlGeneratorInterface::ABSOLUTE_URL);

        if ($configuration->isHtmlRequest()) {
            return $this->render($template, [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                'breadcrumb' => $breadcrumb,
                $this->metadata->getName() => $resource,
                'show_template' => $showTemplate ?? null,
                'feed_title' => $feedTitle,
                'feed_url' => $feedUrl,
            ]);
        }

        return $this->createRestView($configuration, $resource);
    }

    public function previewAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Article $resource */
        $resource = $this->findOr404($configuration);

        return $this->forward('App\Controller\Article\ArticleController::showAction', [
            'id' => $resource->getId(),
            '_route' => $request->attributes->get('_route'),
            '_under_pages_code' => $request->attributes->get('_under_pages_code'),
            'preview' => true,
        ]);
    }

    private function createRssFeed(array $feedData): string
    {
        $rss = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $rss .= '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">' . "\n";
        $rss .= '<channel>' . "\n";
        $rss .= '<title>' . $feedData['title'] . '</title>' . "\n";
        $rss .= '<link>' . $feedData['link'] . '</link>' . "\n";
        $rss .= '<description>' . $feedData['description'] . '</description>' . "\n";
        $rss .= '<language>' . str_replace('_', '-', $feedData['locale']) . '</language>' . "\n";
        $rss .= '<atom:link href="' . $feedData['feed_url'] . '" rel="self" type="application/rss+xml" />' . "\n";

        foreach ($feedData['items']->getData()->getCurrentPageResults() as $item) {
            $link = $this->generateUrl('app_article_show', [
                'page_slug' => $feedData['page_slug'],
                'slug' => $item->getSlug(),
                'category_slug' => $item->getCategory()->getSlug(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            $rss .= '<item>' . "\n";
            $rss .= '<title>' . $item->getTitle() . '</title>' . "\n";
            $rss .= '<description>Par ' . $item->getAuthor() . ' - Les Champs Libres</description>' . "\n";
            $rss .= '<pubDate>' . $item->getCreatedAt()->format('D, d M Y H:i:s O') . '</pubDate>' . "\n";
            $rss .= '<link>' . $link . '</link>' . "\n";
            $rss .= '<guid>' . $link . '</guid>' . "\n";
            $rss .= '<source url="' . $feedData['feed_url'] . '">' . $feedData['title'] . '</source>' . "\n";

            $rss .= '</item>' . "\n";
        }

        $rss .= '</channel>' . "\n";
        $rss .= '</rss>';

        return $rss;
    }
}

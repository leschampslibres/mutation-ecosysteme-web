<?php

declare(strict_types=1);

namespace App\Controller\Project;

use App\Entity\Project\Project;
use Luna\CoreBundle\Entity\Page\PageInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProjectController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function indexAction(Request $request): Response
    {
        return parent::indexAction($request);
    }

    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Project $resource */
        $resource = $this->findOr404($configuration);

        $event = $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        $underPagesService = $this->container->get('luna.under_pages.service');
        $underPagesCode = $request->attributes->get('_under_pages_code');
        $underPage = $underPagesService->underPageByCode($underPagesCode);

        // Calcul des éléments du breadcrumb

        // Page parente de la ressource
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);
        // Projet en cours
        $label = $resource->getTitle();
        $href = $this->generateUrl('app_project_show', ['slug' => $resource->getSlug(), 'page_slug' => $underPage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];

        $template = '@SyliusShop/Project/show.html.twig';
        if ($request->attributes->get('preview')) {
            $showTemplate = $template;
            $template = '@LunaCore/Admin/Crud/preview.html.twig';
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($template, [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                'breadcrumb' => $breadcrumb,
                $this->metadata->getName() => $resource,
                'show_template' => $showTemplate ?? null,
                'page' => $underPage,
            ]);
        }

        return $this->createRestView($configuration, $resource);
    }

    /**
     * @throws HttpException
     */
    public function moveupAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() - 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_project_index');
    }

    /**
     * @throws HttpException
     */
    public function movedownAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() + 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_project_index');
    }

    public function previewAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Project $resource */
        $resource = $this->findOr404($configuration);

        return $this->forward('App\Controller\Project\ProjectController::showAction', [
            'id' => $resource->getId(),
            'page_slug' => $request->attributes->get('page_slug'),
            '_route' => $request->attributes->get('_route'),
            'preview' => true,
            '_under_pages_code' => $request->attributes->get('_under_pages_code'),
        ]);
    }
}

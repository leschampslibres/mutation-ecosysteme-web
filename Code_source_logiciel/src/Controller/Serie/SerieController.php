<?php

declare(strict_types=1);

namespace App\Controller\Serie;

use App\Entity\Serie\Serie;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SerieController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function indexAction(Request $request): Response
    {
        return parent::indexAction($request);
    }

    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Serie $resource */
        $resource = $this->findOr404($configuration);

        $event = $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        $underPagesService = $this->container->get('luna.under_pages.service');
        $underPagesCode = $request->attributes->get('_under_pages_code');
        $underPage = $underPagesService->underPageByCode($underPagesCode);

        // Calcul des éléments du breadcrumb
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);
        $label = $resource->getTitle();
        $href = $this->generateUrl('app_serie_show', ['slug' => $resource->getSlug(), 'page_slug' => $underPage->getCompositeSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];

        $template = '@SyliusShop/Serie/show.html.twig';
        if ($request->attributes->get('preview')) {
            $showTemplate = $template;
            $template = '@LunaCore/Admin/Crud/preview.html.twig';
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($template, [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                'breadcrumb' => $breadcrumb,
                $this->metadata->getName() => $resource,
                'show_template' => $showTemplate ?? null,
                'page' => $underPage,
            ]);
        }

        return $this->createRestView($configuration, $resource);
    }

    public function previewAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Serie $resource */
        $resource = $this->findOr404($configuration);

        return $this->forward('App\Controller\Serie\SerieController::showAction', [
            'id' => $resource->getId(),
            '_route' => $request->attributes->get('_route'),
            'preview' => true,
            '_under_pages_code' => $request->attributes->get('_under_pages_code'),
        ]);
    }
}

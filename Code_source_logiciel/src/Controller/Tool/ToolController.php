<?php

declare(strict_types=1);

namespace App\Controller\Tool;

use App\Entity\Tool\Tool;
use Luna\CoreBundle\Entity\Page\PageInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ToolController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function indexAction(Request $request): Response
    {
        return parent::indexAction($request);
    }

    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        $event = $this->eventDispatcher->dispatch(ResourceActions::SHOW, $configuration, $resource);
        $eventResponse = $event->getResponse();
        if (null !== $eventResponse) {
            return $eventResponse;
        }

        $underPagesService = $this->container->get('luna.under_pages.service');

        // Breadcrumb

        // Page parente de la ressource
        $page = $this->getDoctrine()->getManager()->getRepository(PageInterface::class)->findOneByCompositeSlug($request->getLocale(), $request->attributes->get('page_slug'));
        $pageSlug = $page->getCompositeSlug();
        $breadcrumb = $underPagesService->underPageBreadcrumbByPage($page);
        // Tool en cours
        $label = $resource->getTitle();
        $href = $this->generateUrl('app_mediation_show', ['slug' => $resource->getSlug(), 'page_slug' => $pageSlug], UrlGeneratorInterface::ABSOLUTE_URL);
        $breadcrumb[] = ['label' => $label, 'href' => $href];

        // Utilisation du template show ou preview

        $template = '@SyliusShop/Tool/show.html.twig';
        if ($request->attributes->get('preview')) {
            $showTemplate = $template;
            $template = '@LunaCore/Admin/Crud/preview.html.twig';
        }

        if ($configuration->isHtmlRequest()) {
            return $this->render($template, [
                'configuration' => $configuration,
                'metadata' => $this->metadata,
                'resource' => $resource,
                $this->metadata->getName() => $resource,
                'breadcrumb' => $breadcrumb,
                'page' => $page,
                'page_slug' => $pageSlug,
                'show_template' => $showTemplate ?? null,
            ]);
        }

        return $this->createRestView($configuration, $resource);
    }

    /**
     * @throws HttpException
     */
    public function moveupAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() - 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_tool_index');
    }

    /**
     * @throws HttpException
     */
    public function movedownAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() + 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_tool_index');
    }

    public function previewAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        return $this->forward('App\Controller\Tool\ToolController::showAction', [
            'id' => $resource->getId(),
            'page_slug' => $request->attributes->get('page_slug'),
            '_route' => $request->attributes->get('_route'),
            '_under_pages_code' => $request->attributes->get('_under_pages_code'),
            'preview' => true,
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Mediation;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;

class MediationSubjectController extends ResourceController
{
    #[Cache(public: true, maxage: 86400, mustRevalidate: true)]
    public function showAction(Request $request): Response
    {
        return parent::showAction($request);
    }

    /**
     * @throws HttpException
     */
    public function moveupAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var MediationSubject $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() - 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_mediation_subject_index');
    }

    /**
     * @throws HttpException
     */
    public function movedownAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var MediationSubject $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() + 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_mediation_subject_index');
    }
}

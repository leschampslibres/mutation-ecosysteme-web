<?php

declare(strict_types=1);

namespace App\Controller\Contact;

use App\Entity\Contact\Contact;
use App\Enum\Contact\ServiceEnum;
use App\Form\Type\Contact\ContactType;
use CaptchaEU\Service as CaptchaService;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Mailer\Sender\SenderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends ResourceController
{
    public function new(Request $request, EntityManagerInterface $entityManager, SenderInterface $sender): Response
    {
        $underPagesService = $this->container->get('luna.under_pages.service');

        $underPagesCode = 'contact';

        // Calcul des éléments du breadcrumb
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);

        // Page parente
        $page = $underPagesService->underPageByCode($underPagesCode);

        $contact = new Contact();

        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        $captcha = new CaptchaService($request->server->get('CAPTCHA_EU_PUBLIC_KEY'), $request->server->get('CAPTCHA_EU_REST_KEY'), $request->server->get('CAPTCHA_EU_ENDPOINT'));

        if ($form->isSubmitted() && $form->isValid() && $captcha->validate($_POST['captcha_at_solution'])) {
            $contact = $form->getData();

            $serviceName = $contact->getServiceName()->value;
            $contact->setServiceEmail(ServiceEnum::accueil->generateServiceEmail($serviceName));

            $entityManager->persist($contact);
            $entityManager->flush();

            $recipientEmail = $contact->getServiceEmail();
            $sender->send('contact', [$recipientEmail], ['contact' => $contact]);

            return $this->redirectToRoute('app_contact_confirm', ['page_slug' => $page->getCompositeSlug()]);
        }

        return $this->render('@SyliusShop/Contact/new.html.twig', [
            'script_captcha' => $captcha->script(),
            'validator' => $captcha->protectForm('contact'),
            'form' => $form->createView(),
            'breadcrumb' => $breadcrumb,
            'page' => $page,
            'objects' => ServiceEnum::objects(),
        ]);
    }

    public function confirm(): Response
    {
        $underPagesService = $this->container->get('luna.under_pages.service');

        $underPagesCode = 'contact';

        // Calcul des éléments du breadcrumb
        $breadcrumb = $underPagesService->underPageBreadcrumbByCode($underPagesCode);

        // Page parente
        $page = $underPagesService->underPageByCode($underPagesCode);

        return $this->render('@SyliusShop/Contact/confirm.html.twig', [
            'breadcrumb' => $breadcrumb,
            'page' => $page,
        ]);
    }
}

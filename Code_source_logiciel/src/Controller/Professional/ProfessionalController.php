<?php

declare(strict_types=1);

namespace App\Controller\Professional;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfessionalController extends ResourceController
{
    /**
     * @throws HttpException
     */
    public function moveupAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() - 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_professional_index');
    }

    /**
     * @throws HttpException
     */
    public function movedownAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::UPDATE);
        /** @var Tool $resource */
        $resource = $this->findOr404($configuration);

        // Récupération de l'entity manager
        $em = $this->getDoctrine()->getManager();

        $resource->setPosition($resource->getPosition() + 1);
        $em->persist($resource);
        $em->flush();

        return $this->redirectToRoute('app_admin_professional_index');
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends AbstractController
{
    public function alertMessage(): Response
    {
        return $this->render('@SyliusShop/_layouts/front/alert_message.html.twig')
            ->setPublic()
            ->setMaxAge(86400)
        ;
    }

    public function header(string $currentUrl, array $breadcrumb = []): Response
    {
        return $this->render('@SyliusShop/_layouts/front/header.html.twig', [
            'current_url' => $currentUrl,
            'breadcrumb' => $breadcrumb,
        ])
            ->setPublic()
            ->setMaxAge(86400)
        ;
    }

    public function footer(): Response
    {
        return $this->render('@SyliusShop/_layouts/front/footer.html.twig')
            ->setPublic()
            ->setMaxAge(86400)
        ;
    }

    public function opacAction(RequestStack $requestStack, HtmlSanitizerInterface $htmlSanitizer)
    {
        $query = $requestStack->getCurrentRequest()->getPayload()->get('search');
        if (is_string($query)) {
            $query = $htmlSanitizer->sanitize($query);
            $url = 'https://opac.si.leschampslibres.fr/iii/encore/search/C__S' . $query . '__Orightresult__U__X0?lang=frf&suite=pearl';
        } else {
            $url = 'https://opac.si.leschampslibres.fr/iii/encore/';
        }
        return $this->redirect($url);
    }
}

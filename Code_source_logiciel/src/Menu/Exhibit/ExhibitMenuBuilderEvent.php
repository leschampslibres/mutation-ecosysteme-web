<?php

declare(strict_types=1);

namespace App\Menu\Exhibit;

use App\Entity\Exhibit\Exhibit;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ExhibitMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Exhibit $exhibit)
    {
        parent::__construct($factory, $menu);
    }

    public function getExhibit(): Exhibit
    {
        return $this->exhibit;
    }
}

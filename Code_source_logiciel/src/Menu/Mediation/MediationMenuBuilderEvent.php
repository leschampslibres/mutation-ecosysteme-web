<?php

declare(strict_types=1);

namespace App\Menu\Mediation;

use App\Entity\Mediation\Mediation;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class MediationMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Mediation $mediation)
    {
        parent::__construct($factory, $menu);
    }

    public function getMediation(): Mediation
    {
        return $this->mediation;
    }
}

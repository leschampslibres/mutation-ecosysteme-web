<?php

declare(strict_types=1);

namespace App\Menu\Mediation;

use App\Entity\Mediation\MediationSubject;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class MediationSubjectMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly MediationSubject $mediationSubject)
    {
        parent::__construct($factory, $menu);
    }

    public function getMediationSubject(): MediationSubject
    {
        return $this->mediationSubject;
    }
}

<?php

declare(strict_types=1);

namespace App\Menu\Article;

use App\Entity\Article\Article;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ArticleMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Article $article)
    {
        parent::__construct($factory, $menu);
    }

    public function getArticle(): Article
    {
        return $this->article;
    }
}

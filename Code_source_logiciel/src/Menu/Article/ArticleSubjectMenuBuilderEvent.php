<?php

declare(strict_types=1);

namespace App\Menu\Article;

use App\Entity\Article\ArticleSubject;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ArticleSubjectMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly ArticleSubject $articleSubject)
    {
        parent::__construct($factory, $menu);
    }

    public function getArticleSubject(): ArticleSubject
    {
        return $this->articleSubject;
    }
}

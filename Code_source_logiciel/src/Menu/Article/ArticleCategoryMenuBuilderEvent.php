<?php

declare(strict_types=1);

namespace App\Menu\Article;

use App\Entity\Article\ArticleCategory;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ArticleCategoryMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly ArticleCategory $articleCategory)
    {
        parent::__construct($factory, $menu);
    }

    public function getArticleCategory(): ArticleCategory
    {
        return $this->articleCategory;
    }
}

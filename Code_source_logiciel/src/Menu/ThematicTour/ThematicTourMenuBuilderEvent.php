<?php

namespace App\Menu\ThematicTour;

use App\Entity\ThematicTour\ThematicTour;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ThematicTourMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly ThematicTour $thematicTour)
    {
        parent::__construct($factory, $menu);
    }

    public function getThematicTour(): ThematicTour
    {
        return $this->thematicTour;
    }
}

<?php

declare(strict_types=1);

namespace App\Menu\Tool;

use App\Entity\Tool\Tool;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ToolMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Tool $tool)
    {
        parent::__construct($factory, $menu);
    }

    public function getTool(): Tool
    {
        return $this->tool;
    }
}

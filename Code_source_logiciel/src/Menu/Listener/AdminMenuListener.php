<?php

declare(strict_types=1);

namespace App\Menu\Listener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Symfony\Bundle\SecurityBundle\Security;

final class AdminMenuListener
{
    public function __construct(private Security $security)
    {
    }

    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        // Suppression des éléments par défaut de Sylius qui ne sont pas utilisés dans le projet.

        $menu->removeChild('catalog');
        $menu->removeChild('sales');
        $menu->removeChild('customers');
        $menu->removeChild('marketing');

        // Modification de la section "Contenu"

        if (!$this->security->isGranted('ROLE_CONTENT_ACCESS')) {
            $menu->removeChild('content');
        } else {
            if (!$content = $menu->getChild('content')) {
                $content = $menu
                        ->addChild('content')
                        ->setLabel('luna.ui.content')
                ;
            }

            if ($this->security->isGranted('ROLE_EVENT_ACCESS')) {
                $content->addChild('admin_event', ['route' => 'app_admin_event_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_event_create'],
                    ['route' => 'app_admin_event_update'],
                ]]])
                    ->setLabel('app.ui.resource.event.admin.header')
                    ->setLabelAttribute('icon', 'calendar alternate')
                ;
            }

            if ($this->security->isGranted('ROLE_EVENT_CATEGORY_ACCESS')) {
                $content->addChild('admin_event_category', ['route' => 'app_admin_event_category_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_event_category_create'],
                    ['route' => 'app_admin_event_category_update'],
                ]]])
                    ->setLabel('app.ui.resource.event_category.admin.header')
                    ->setLabelAttribute('icon', 'calendar alternate')
                ;
            }

            if ($this->security->isGranted('ROLE_ARTICLE_ACCESS')) {
                $content->addChild('admin_article', ['route' => 'app_admin_article_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_article_create'],
                    ['route' => 'app_admin_article_update'],
                ]]])
                    ->setLabel('app.ui.resource.article.admin.header')
                    ->setLabelAttribute('icon', 'copy')
                ;
            }

            if ($this->security->isGranted('ROLE_ARTICLE_CATEGORY_ACCESS')) {
                $content->addChild('admin_article_category', ['route' => 'app_admin_article_category_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_article_category_create'],
                    ['route' => 'app_admin_article_category_update'],
                ]]])
                    ->setLabel('app.ui.resource.article_category.admin.header')
                    ->setLabelAttribute('icon', 'copy')
                ;
            }

            if ($this->security->isGranted('ROLE_ARTICLE_SUBJECT_ACCESS')) {
                $content->addChild('admin_article_subject', ['route' => 'app_admin_article_subject_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_article_subject_create'],
                    ['route' => 'app_admin_article_subject_update'],
                ]]])
                    ->setLabel('app.ui.resource.article_subject.admin.header')
                    ->setLabelAttribute('icon', 'copy')
                ;
            }

            if ($this->security->isGranted('ROLE_SERIE_ACCESS')) {
                $content->addChild('admin_serie', ['route' => 'app_admin_serie_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_serie_create'],
                    ['route' => 'app_admin_serie_update'],
                ]]])
                    ->setLabel('app.ui.resource.serie.admin.header')
                    ->setLabelAttribute('icon', 'folder')
                ;
            }
        }

        // Modification de la section "Formulaires"

        if (!$this->security->isGranted('ROLE_FORM_ACCESS')) {
            $menu->removeChild('form');
        } else {
            if (!$form = $menu->getChild('form')) {
                $form = $menu
                    ->addChild('form')
                    ->setLabel('app.ui.menu.forms')
                ;
            }

            if ($this->security->isGranted('ROLE_CONTACT_ACCESS')) {
                $form->addChild('admin_page', ['route' => 'app_admin_contact_index'])
                    ->setLabel('luna.ui.resource.contact.admin.header')
                    ->setLabelAttribute('icon', 'envelope')
                ;
            }
        }

        // Modification de la section "Contenu professionnel"

        if (!$this->security->isGranted('ROLE_PROFESSIONAL_CONTENT_ACCESS')) {
            $menu->removeChild('professional_content');
        } else {
            if (!$professionalContent = $menu->getChild('professional_content')) {
                $professionalContent = $menu
                        ->addChild('professional_content')
                        ->setLabel('app.ui.menu.professional_content')
                ;
            }

            if ($this->security->isGranted('ROLE_PROFESSIONAL_ACCESS')) {
                $professionalContent->addChild('admin_professional', ['route' => 'app_admin_professional_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_professional_create'],
                    ['route' => 'app_admin_professional_update'],
                ]]])
                    ->setLabel('app.ui.resource.professional.admin.header')
                    ->setLabelAttribute('icon', 'address book')
                ;
            }

            if ($this->security->isGranted('ROLE_MEDIATION_ACCESS')) {
                $professionalContent->addChild('admin_mediation', ['route' => 'app_admin_mediation_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_mediation_create'],
                    ['route' => 'app_admin_mediation_update'],
                ]]])
                    ->setLabel('app.ui.resource.mediation.admin.header')
                    ->setLabelAttribute('icon', 'street view')
                ;
            }

            if ($this->security->isGranted('ROLE_MEDIATION_SUBJECT_ACCESS')) {
                $professionalContent->addChild('admin_mediation_subject', ['route' => 'app_admin_mediation_subject_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_mediation_subject_create'],
                    ['route' => 'app_admin_mediation_subject_update'],
                ]]])
                    ->setLabel('app.ui.resource.mediation_subject.admin.header')
                    ->setLabelAttribute('icon', 'street view')
                ;
            }

            if ($this->security->isGranted('ROLE_THEMATIC_TOUR_ACCESS')) {
                $professionalContent->addChild('admin_thematic_tour', ['route' => 'app_admin_thematic_tour_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_thematic_tour_create'],
                    ['route' => 'app_admin_thematic_tour_update'],
                ]]])
                    ->setLabel('app.ui.resource.thematic_tour.admin.header')
                    ->setLabelAttribute('icon', 'random')
                ;
            }

            if ($this->security->isGranted('ROLE_PROJECT_ACCESS')) {
                $professionalContent->addChild('admin_project', ['route' => 'app_admin_project_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_project_create'],
                    ['route' => 'app_admin_project_update'],
                ]]])
                    ->setLabel('app.ui.resource.project.admin.header')
                    ->setLabelAttribute('icon', 'flask')
                ;
            }

            if ($this->security->isGranted('ROLE_DOSSIER_ACCESS')) {
                $professionalContent->addChild('admin_dossier', ['route' => 'app_admin_dossier_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_dossier_create'],
                    ['route' => 'app_admin_dossier_update'],
                ]]])
                    ->setLabel('app.ui.resource.dossier.admin.header')
                    ->setLabelAttribute('icon', 'folder open')
                ;
            }

            if ($this->security->isGranted('ROLE_TOOL_ACCESS')) {
                $professionalContent->addChild('admin_tool', ['route' => 'app_admin_tool_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_tool_create'],
                    ['route' => 'app_admin_tool_update'],
                ]]])
                    ->setLabel('app.ui.resource.tool.admin.header')
                    ->setLabelAttribute('icon', 'boxes')
                ;
            }

            if ($this->security->isGranted('ROLE_EXHIBIT_ACCESS')) {
                $professionalContent->addChild('admin_exhibit', ['route' => 'app_admin_exhibit_index', 'extras' => ['routes' => [
                    ['route' => 'app_admin_exhibit_create'],
                    ['route' => 'app_admin_exhibit_update'],
                ]]])
                    ->setLabel('app.ui.resource.exhibit.admin.header')
                    ->setLabelAttribute('icon', 'images')
                ;
            }
        }

        // Modification de la section "Éléments transversaux"

        if (!$this->security->isGranted('ROLE_TRANSVERSE_ACCESS')) {
            $menu->removeChild('transverse');
        } else {
            if (!$transverse = $menu->getChild('transverse')) {
                $transverse = $menu
                    ->addChild('transverse')
                    ->setLabel('app.ui.menu.transverse')
                ;
            }

            if ($this->security->isGranted('ROLE_ALERT_MESSAGE_ACCESS')) {
                $transverse
                    ->addChild('admin_page', ['route' => 'app_admin_alert_message_index', 'extras' => ['routes' => [
                        ['route' => 'app_admin_alert_message_create'],
                        ['route' => 'app_admin_alert_message_update'],
                    ]]])
                    ->setLabel('app.ui.resource.alert_message.admin.header')
                    ->setLabelAttribute('icon', 'bullhorn')
                ;
            }

            if ($this->security->isGranted('ROLE_MENU_ACCESS')) {
                $transverse
                    ->addChild('app-menu', ['route' => 'monsieurbiz_menu_admin_menu_index', 'extras' => ['routes' => [
                        ['route' => 'monsieurbiz_menu_admin_menu_create'],
                        ['route' => 'monsieurbiz_menu_admin_menu_update'],
                        ['route' => 'monsieurbiz_menu_admin_menu_item_create_for_menu'],
                        ['route' => 'monsieurbiz_menu_admin_menu_item_create_for_parent'],
                        ['route' => 'monsieurbiz_menu_admin_menu_item_update'],
                    ]]])
                    ->setLabel('monsieurbiz_menu.ui.menus')
                    ->setLabelAttribute('icon', 'sitemap')
                ;
            }
        }

        // Modification de la section "Configuration"

        if (!$this->security->isGranted('ROLE_CONFIGURATION_ACCESS')) {
            $menu->removeChild('configuration');
        } else {
            if (null !== $configuration = $menu->getChild('configuration')) {
                $configuration->removeChild('channels');
                $configuration->removeChild('monsieurbiz_settings');
                $configuration->removeChild('locales');
                $configuration->removeChild('app-menu');

                if (!$this->security->isGranted('ROLE_ADMIN_USER_ACCESS')) {
                    $configuration->removeChild('admin_users');
                }
            }
        }

        // Modification de la section "Commandes planifiées", chargée automatiquement par le bundle "SyliusSchedulerCommandPlugin"

        if (!$this->security->isGranted('ROLE_SCHEDULER_ACCESS')) {
            $menu->removeChild('scheduler');
        }
    }

    public function reorderMenu(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();

        // Code ci-dessous pour éviter l'erreur "Cannot reorder children,
        // order does not contain all children." lorsque certains éléments
        // ne sont pas affichés en fonction des droits de l'utilisateur.

        if ($this->security->isGranted('ROLE_CONTENT_ACCESS')) {
            $order[] = 'content';
        }
        if ($this->security->isGranted('ROLE_PROFESSIONAL_CONTENT_ACCESS')) {
            $order[] = 'professional_content';
        }
        if ($this->security->isGranted('ROLE_FORM_ACCESS')) {
            $order[] = 'form';
        }
        if ($this->security->isGranted('ROLE_TRANSVERSE_ACCESS')) {
            $order[] = 'transverse';
        }
        if ($this->security->isGranted('ROLE_CONFIGURATION_ACCESS')) {
            $order[] = 'configuration';
        }
        if ($this->security->isGranted('ROLE_SCHEDULER_ACCESS')) {
            $order[] = 'scheduler';
        }

        $children = array_keys($menu->getChildren());

        $newOrder = array_unique(array_merge($order, $children));

        $menu->reorderChildren($newOrder);
    }
}

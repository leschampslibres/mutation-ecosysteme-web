<?php

declare(strict_types=1);

namespace App\Menu\Serie;

use App\Entity\Serie\Serie;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class SerieMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Serie $serie)
    {
        parent::__construct($factory, $menu);
    }

    public function getSerie(): Serie
    {
        return $this->serie;
    }
}

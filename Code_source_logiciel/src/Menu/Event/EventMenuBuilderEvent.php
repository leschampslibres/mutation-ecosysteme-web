<?php

declare(strict_types=1);

namespace App\Menu\Event;

use App\Entity\Event\Event;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class EventMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Event $event)
    {
        parent::__construct($factory, $menu);
    }

    public function getEvent(): Event
    {
        return $this->event;
    }
}

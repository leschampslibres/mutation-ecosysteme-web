<?php

declare(strict_types=1);

namespace App\Menu\Event;

use App\Entity\Event\EventCategory;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class EventCategoryMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly EventCategory $eventCategory)
    {
        parent::__construct($factory, $menu);
    }

    public function getEventCategory(): EventCategory
    {
        return $this->eventCategory;
    }
}

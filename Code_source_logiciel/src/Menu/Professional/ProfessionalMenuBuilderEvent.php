<?php

declare(strict_types=1);

namespace App\Menu\Professional;

use App\Entity\Professional\Professional;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ProfessionalMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Professional $professional)
    {
        parent::__construct($factory, $menu);
    }

    public function getProfessional(): Professional
    {
        return $this->professional;
    }
}

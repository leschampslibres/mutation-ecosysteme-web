<?php

declare(strict_types=1);

namespace App\Menu\Dossier;

use App\Entity\Dossier\Dossier;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class DossierMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Dossier $dossier)
    {
        parent::__construct($factory, $menu);
    }

    public function getDossier(): Dossier
    {
        return $this->dossier;
    }
}

<?php

declare(strict_types=1);

namespace App\Menu\Project;

use App\Entity\Project\Project;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ProjectMenuBuilderEvent extends MenuBuilderEvent
{
    public function __construct(FactoryInterface $factory, ItemInterface $menu, private readonly Project $project)
    {
        parent::__construct($factory, $menu);
    }

    public function getProject(): Project
    {
        return $this->project;
    }
}

<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Article\ArticleSubject;
use App\Menu\Article\ArticleSubjectMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ArticleSubjectFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.article_subject_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('article_subject', $options) || !$options['article_subject'] instanceof ArticleSubject) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/ArticleSubject/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/ArticleSubject/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new ArticleSubjectMenuBuilderEvent($this->factory, $menu, $options['article_subject']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

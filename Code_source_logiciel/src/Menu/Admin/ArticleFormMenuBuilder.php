<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Article\Article;
use App\Menu\Article\ArticleMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ArticleFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.article_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('article', $options) || !$options['article'] instanceof Article) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Article/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Article/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Article/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new ArticleMenuBuilderEvent($this->factory, $menu, $options['article']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

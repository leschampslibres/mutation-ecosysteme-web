<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Exhibit\Exhibit;
use App\Menu\Exhibit\ExhibitMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ExhibitFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.exhibit_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('exhibit', $options) || !$options['exhibit'] instanceof Exhibit) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Exhibit/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Exhibit/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Exhibit/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new ExhibitMenuBuilderEvent($this->factory, $menu, $options['exhibit']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

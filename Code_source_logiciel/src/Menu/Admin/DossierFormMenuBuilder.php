<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Dossier\Dossier;
use App\Menu\Dossier\DossierMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class DossierFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.dossier_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('dossier', $options) || !$options['dossier'] instanceof Dossier) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Dossier/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Dossier/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Dossier/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new DossierMenuBuilderEvent($this->factory, $menu, $options['dossier']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

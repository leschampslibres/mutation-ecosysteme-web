<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Project\Project;
use App\Menu\Project\ProjectMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProjectFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.project_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly Security $security
    )
    {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('project', $options) || !$options['project'] instanceof Project) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Project/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true);

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Project/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content');

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Project/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo');

        $this->eventDispatcher->dispatch(
            new ProjectMenuBuilderEvent($this->factory, $menu, $options['project']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

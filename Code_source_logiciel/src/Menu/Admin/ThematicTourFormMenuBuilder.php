<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\ThematicTour\ThematicTour;
use App\Menu\ThematicTour\ThematicTourMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ThematicTourFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.thematic_tour_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('thematic_tour', $options) || !$options['thematic_tour'] instanceof ThematicTour) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/ThematicTour/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/ThematicTour/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/ThematicTour/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new ThematicTourMenuBuilderEvent($this->factory, $menu, $options['thematic_tour']),
            self::EVENT_NAME,
        );

        return $menu;
    }

}

<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Article\ArticleCategory;
use App\Menu\Article\ArticleCategoryMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ArticleCategoryFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.article_category_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('article_category', $options) || !$options['article_category'] instanceof ArticleCategory) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/ArticleCategory/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/ArticleCategory/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new ArticleCategoryMenuBuilderEvent($this->factory, $menu, $options['article_category']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

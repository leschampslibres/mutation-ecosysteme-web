<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Serie\Serie;
use App\Menu\Serie\SerieMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SerieFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.serie_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('serie', $options) || !$options['serie'] instanceof Serie) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Serie/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Serie/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Serie/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new SerieMenuBuilderEvent($this->factory, $menu, $options['serie']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

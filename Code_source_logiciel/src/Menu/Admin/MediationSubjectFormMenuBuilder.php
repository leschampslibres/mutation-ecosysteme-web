<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Mediation\MediationSubject;
use App\Menu\Mediation\MediationSubjectMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class MediationSubjectFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.mediation_subject_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('mediation_subject', $options) || !$options['mediation_subject'] instanceof MediationSubject) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/MediationSubject/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/MediationSubject/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new MediationSubjectMenuBuilderEvent($this->factory, $menu, $options['mediation_subject']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

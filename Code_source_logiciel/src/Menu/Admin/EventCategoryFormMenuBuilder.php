<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Event\EventCategory;
use App\Menu\Event\EventCategoryMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EventCategoryFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.event_category_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('event_category', $options) || !$options['event_category'] instanceof EventCategory) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/EventCategory/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/EventCategory/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/EventCategory/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new EventCategoryMenuBuilderEvent($this->factory, $menu, $options['event_category']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

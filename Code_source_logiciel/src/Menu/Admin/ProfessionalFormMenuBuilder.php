<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Professional\Professional;
use App\Menu\Professional\ProfessionalMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProfessionalFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.professional_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('professional', $options) || !$options['professional'] instanceof Professional) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Professional/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $this->eventDispatcher->dispatch(
            new ProfessionalMenuBuilderEvent($this->factory, $menu, $options['professional']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

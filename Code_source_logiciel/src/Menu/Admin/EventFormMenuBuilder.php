<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use App\Entity\Event\Event;
use App\Menu\Event\EventMenuBuilderEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EventFormMenuBuilder
{
    public const EVENT_NAME = 'app.admin.event_form';

    public function __construct(
        private readonly FactoryInterface $factory,
        private readonly EventDispatcherInterface $eventDispatcher,
        private Security $security
    ) {
    }

    public function createMenu(array $options = []): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        if (!\array_key_exists('event', $options) || !$options['event'] instanceof Event) {
            return $menu;
        }

        $menu
            ->addChild('details')
            ->setAttribute('template', 'app/Admin/Event/Tab/_details.html.twig')
            ->setLabel('luna.ui.menu.details')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('openagenda')
            ->setAttribute('template', 'app/Admin/Event/Tab/_openagenda.html.twig')
            ->setLabel('app.ui.resource.event.tab.openagenda')
            ->setCurrent(true)
        ;

        $menu
            ->addChild('content')
            ->setAttribute('template', 'app/Admin/Event/Tab/_content.html.twig')
            ->setLabel('luna.ui.menu.content')
        ;

        $menu
            ->addChild('seo')
            ->setAttribute('template', 'app/Admin/Event/Tab/_seo.html.twig')
            ->setLabel('luna.ui.menu.seo')
        ;

        $this->eventDispatcher->dispatch(
            new EventMenuBuilderEvent($this->factory, $menu, $options['event']),
            self::EVENT_NAME,
        );

        return $menu;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Dossier;

use App\Entity\Dossier\Dossier;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface DossierRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function findByTitle(string $title, int $limit): array;

    public function findById(array $id): array;

    public function createShopListQueryBuilder(string $locale, string $pageSlug): QueryBuilder;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?Dossier;
}

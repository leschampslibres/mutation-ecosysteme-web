<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\ArticleCategory;
use App\Repository\Trait\MediasInUseTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ArticleCategoryRepository extends EntityRepository implements ArticleCategoryRepositoryInterface, SitemapRepositoryInterface
{
    use MediasInUseTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('category')
        ->addSelect('translation')
        ->innerJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
        ->setParameter('locale', $locale)
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('category')
            ->select('COUNT(category.id)')
            ->innerJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?ArticleCategory
    {
        return $this->createQueryBuilder('category')
            ->leftJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation')
            ->getQuery()
            ->getResult()
        ;
    }
}

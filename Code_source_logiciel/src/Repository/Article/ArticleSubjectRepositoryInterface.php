<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\ArticleSubject;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ArticleSubjectRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createAdminListArticleSubjectByTitleQueryBuilder(string $title, int $limit): array;

    public function createAdminListArticleSubjectByIdQueryBuilder(array $id): array;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?ArticleSubject;
}

<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\ArticleCategory;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ArticleCategoryRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?ArticleCategory;
}

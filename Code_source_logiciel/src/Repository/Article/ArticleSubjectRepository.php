<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\ArticleSubject;
use App\Repository\Trait\MediasInUseTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ArticleSubjectRepository extends EntityRepository implements ArticleSubjectRepositoryInterface, SitemapRepositoryInterface
{
    use MediasInUseTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('subject')
        ->addSelect('translation')
        ->innerJoin('subject.translations', 'translation', 'WITH', 'translation.locale = :locale')
        ->setParameter('locale', $locale)
        ;
    }

    public function createAdminListArticleSubjectByTitleQueryBuilder(string $title, int $limit): array
    {
        return $this->createQueryBuilder('subject')
            ->select('subject.id', 'translation.title')
            ->innerJoin('subject.translations', 'translation')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('title', '%'.$title.'%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createAdminListArticleSubjectByIdQueryBuilder(array $id): array
    {
        $id = $id[0];

        return $this->createQueryBuilder('subject')
            ->select('subject.id', 'translation.title')
            ->innerJoin('subject.translations', 'translation')
            ->andWhere('subject.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('subject')
            ->select('COUNT(subject.id)')
            ->innerJoin('subject.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?ArticleSubject
    {
        return $this->createQueryBuilder('subject')
            ->leftJoin('subject.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('subject')
            ->addSelect('translation')
            ->innerJoin('subject.translations', 'translation')
            ->getQuery()
            ->getResult()
        ;
    }
}

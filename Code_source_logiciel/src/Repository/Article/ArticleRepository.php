<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\Article;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use App\Repository\Trait\MediasInUseTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ArticleRepository extends BaseEntityRepository implements ArticleRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use RelatedResourcesTrait;
    use MediasInUseTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('article')
            ->addSelect('translation')
            ->innerJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('article')
            ->select('article.id', 'translation.title')
            ->innerJoin('article.translations', 'translation')
            ->andWhere('article.enabled = :enabled')
            ->andWhere('article.publishedAt <= :now')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('article')
            ->select('article.id', 'translation.title')
            ->innerJoin('article.translations', 'translation')
            ->andWhere('article.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createShopSearchQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('article')
            ->addSelect('translation')
            ->innerJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('article.enabled = :enabled')
            ->andWhere('article.publishedAt <= :now')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
        ;
    }

    public function createAdminListArticleByTitleQueryBuilder(string $title, int $limit): array
    {
        return $this->createQueryBuilder('article')
            ->select('article.id', 'translation.title')
            ->innerJoin('article.translations', 'translation')
            ->andWhere('translation.title LIKE :title')
            ->andWhere('article.enabled = true')
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createAdminListArticleByIdQueryBuilder(array $id): array
    {
        $id = $id[0];

        return $this->createQueryBuilder('article')
            ->select('article.id', 'translation.title')
            ->innerJoin('article.translations', 'translation')
            ->andWhere('article.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int)$this
            ->createQueryBuilder('article')
            ->select('COUNT(article.id)')
            ->innerJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('article.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsByCategoryAndSlug(string $locale, array $arguments): bool
    {
        $count = (int)$this
            ->createQueryBuilder('article')
            ->select('COUNT(article.id)')
            ->innerJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('article.category', 'category')
            ->innerJoin('category.translations', 'categoryTranslation', 'WITH', 'categoryTranslation.locale = :locale')
            ->andWhere('categoryTranslation.slug = :categorySlug')
            ->andWhere('translation.slug = :slug')
            ->andWhere('article.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('categorySlug', $arguments[0])
            ->setParameter('slug', $arguments[1])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Article
    {
        return $this->createQueryBuilder('article')
            ->leftJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('article.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findLatest(int $limit, string $locale): array
    {
        return $this->createQueryBuilder('article')
            ->addSelect('translation')
            ->innerJoin('article.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->leftJoin('article.pages', 'page')
            ->andWhere('page.article IS NULL')
            ->andWhere('article.enabled = :enabled')
            ->andWhere('article.publishedAt <= :now')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
            ->orderBy('article.publishedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('article')
            ->addSelect('translation')
            ->addSelect('category')
            ->addSelect('categoryTranslation')
            ->innerJoin('article.translations', 'translation')
            ->innerJoin('article.category', 'category')
            ->innerJoin('category.translations', 'categoryTranslation')
            ->andWhere('article.enabled = :enabled')
            ->andWhere('article.publishedAt <= :now')
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }
}

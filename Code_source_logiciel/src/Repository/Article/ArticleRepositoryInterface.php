<?php

declare(strict_types=1);

namespace App\Repository\Article;

use App\Entity\Article\Article;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ArticleRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createAdminListArticleByTitleQueryBuilder(string $title, int $limit): array;

    public function createAdminListArticleByIdQueryBuilder(array $id): array;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?Article;

    public function findLatest(int $limit, string $locale): array;
}

<?php

declare(strict_types=1);

namespace App\Repository\ThematicTour;

use App\Entity\ThematicTour\ThematicTour;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ThematicTourRepository extends BaseEntityRepository implements ThematicTourRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('thematicTour')
            ->addSelect('translation')
            ->innerJoin('thematicTour.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function createShopIndexQueryBuilder(string $locale, string $pageSlug): QueryBuilder
    {
        return $this->createQueryBuilder('thematicTour')
            ->addSelect('translation')
            ->innerJoin('thematicTour.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('thematicTour.thematicTourProfessionals', 'thematicTourProfessional')
            ->innerJoin('thematicTourProfessional.professional', 'professional')
            ->innerJoin('professional.pages', 'page')
            ->innerJoin('page.translations', 'pageTranslation', 'WITH', 'pageTranslation.locale = :locale')
            ->andWhere('thematicTour.enabled = :enabled')
            ->andWhere('pageTranslation.compositeSlug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setParameter('slug', $pageSlug)
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('thematicTour')
            ->select('COUNT(thematicTour.id)')
            ->innerJoin('thematicTour.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('thematicTour.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?ThematicTour
    {
        return $this->createQueryBuilder('thematicTour')
            ->leftJoin('thematicTour.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('thematicTour.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('thematicTour')
            ->addSelect('translation')
            ->innerJoin('thematicTour.translations', 'translation')
            ->andWhere('thematicTour.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}

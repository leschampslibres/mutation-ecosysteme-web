<?php

namespace App\Repository\ThematicTour;

use Doctrine\ORM\QueryBuilder;

interface ThematicTourRepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createShopIndexQueryBuilder(string $locale, string $pageSlug): QueryBuilder;

    public function existsOneBySlug(?string $locale, string $slug): bool;
}

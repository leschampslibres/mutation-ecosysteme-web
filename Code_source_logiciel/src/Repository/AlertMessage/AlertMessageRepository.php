<?php

declare(strict_types=1);

namespace App\Repository\AlertMessage;

use App\Entity\AlertMessage\AlertMessageInterface;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class AlertMessageRepository extends BaseEntityRepository implements AlertMessageRepositoryInterface
{
    use ResourceRepositoryTrait;

    public function createListQueryBuilder(string $locale): QueryBuilder
    {
        return $this->createQueryBuilder('am')
            ->addSelect('translation')
            ->leftJoin('am.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOnePublished(?string $locale): ?AlertMessageInterface
    {
        return $this->createQueryBuilder('am')
            ->leftJoin('am.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.locale = :locale')
            ->andWhere('am.enabled = :enabled')
            ->andWhere('(am.startingAt <= :now AND am.endingAt IS NULL) OR (am.startingAt <= :now AND am.endingAt >= :now) OR (am.startingAt IS NULL AND am.endingAt >= :now) OR (am.startingAt IS NULL AND am.endingAt IS NULL)')
            ->orderBy('am.updatedAt', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime('today'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}

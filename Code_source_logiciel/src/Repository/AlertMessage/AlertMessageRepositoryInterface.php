<?php

declare(strict_types=1);

namespace App\Repository\AlertMessage;

use App\Entity\AlertMessage\AlertMessageInterface;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface AlertMessageRepositoryInterface extends RepositoryInterface
{
    public function createListQueryBuilder(string $locale): QueryBuilder;

    public function findOnePublished(?string $locale): ?AlertMessageInterface;
}

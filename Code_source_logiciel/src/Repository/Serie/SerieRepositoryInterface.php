<?php

declare(strict_types=1);

namespace App\Repository\Serie;

use App\Entity\Serie\Serie;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface SerieRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createShopIndexQueryBuilder(?string $locale): QueryBuilder;

    public function createAdminListSerieByTitleQueryBuilder(string $title, int $limit): array;

    public function createAdminListSerieByIdQueryBuilder(array $id): array;

    public function findOneBySlug(?string $locale, string $slug): ?Serie;
}

<?php

declare(strict_types=1);

namespace App\Repository\Serie;

use App\Entity\Serie\Serie;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class SerieRepository extends BaseEntityRepository implements SerieRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('serie')
            ->addSelect('translation')
            ->innerJoin('serie.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function createShopIndexQueryBuilder(?string $locale): QueryBuilder
    {
        return $this->createQueryBuilder('serie')
            ->andWhere('serie.enabled = :enabled')
            ->andWhere('serie.publishedAt <= :now')
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
        ;
    }

    public function createAdminListSerieByTitleQueryBuilder(string $title, int $limit): array
    {
        return $this->createQueryBuilder('serie')
            ->select('serie.id', 'translation.title')
            ->innerJoin('serie.translations', 'translation')
            ->andWhere('translation.title LIKE :title')
            ->andWhere('serie.enabled = true')
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createAdminListSerieByIdQueryBuilder(array $id): array
    {
        $id = $id[0];

        return $this->createQueryBuilder('serie')
            ->select('serie.id', 'translation.title')
            ->innerJoin('serie.translations', 'translation')
            ->andWhere('serie.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('serie')
            ->select('serie.id', 'translation.title')
            ->innerJoin('serie.translations', 'translation')
            ->andWhere('serie.enabled = :enabled')
            ->andWhere('serie.publishedAt <= :now')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('serie')
            ->select('serie.id', 'translation.title')
            ->innerJoin('serie.translations', 'translation')
            ->andWhere('serie.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Serie
    {
        return $this->createQueryBuilder('serie')
            ->leftJoin('serie.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('serie.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('serie')
            ->addSelect('translation')
            ->innerJoin('serie.translations', 'translation')
            ->andWhere('serie.enabled = :enabled')
            ->andWhere('serie.publishedAt <= :now')
            ->setParameter('enabled', true)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Trait;

use App\Entity\Article\Article;
use App\Entity\Dossier\Dossier;
use App\Entity\Event\Event;
use App\Entity\Exhibit\Exhibit;
use App\Entity\Mediation\Mediation;
use App\Entity\Project\Project;
use App\Entity\Serie\Serie;
use App\Entity\Tool\Tool;

trait RelatedResourcesTrait
{
    public function findByArticleInBlock(string $locale, Article $article): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"article\\\":' . $article->getId() . '\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByDossierInBlock(string $locale, Dossier $dossier): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"dossier\\\":\\\"' . $dossier->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByEventInBlock(string $locale, Event $event): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"event\\\":\\\"' . $event->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByAutomaticEventsInBlock(string $locale): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"code\\\":\\\"lunablock.automaticEvents\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByExhibitInBlock(string $locale, Exhibit $exhibit): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"exhibit\\\":\\\"' . $exhibit->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByMediationInBlock(string $locale, Mediation $mediation): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"mediation\\\":\\\"' . $mediation->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByProjectInBlock(string $locale, Project $project): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"project\\\":\\\"' . $project->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findBySerieInBlock(string $locale, Serie $serie): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"serie\\\":\\\"' . $serie->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByToolInBlock(string $locale, Tool $tool): array
    {
        return $this->createQueryBuilder('entity')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :resource')
            ->setParameter('locale', $locale)
            ->setParameter('resource', '%\{\\\"tool\\\":\\\"' . $tool->getId() . '\\\"\}%')
            ->getQuery()
            ->getResult()
        ;
    }
}

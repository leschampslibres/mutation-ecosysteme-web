<?php

declare(strict_types=1);

namespace App\Repository\Trait;

trait MediasInUseTrait
{
    public function hasMedia(string $locale, string $mediaPath): mixed
    {
        $queryPath = '%"' . $mediaPath . '"%';
        $queryPathWithEscapedSlashes = str_replace('/', '\\\/', $queryPath);
        $queryPathWithEscapedQuotes = '%"' . $mediaPath . '\\\"%';

        $count = (int) $this->createQueryBuilder('entity')
            ->select('COUNT(entity.id)')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->where('translation.blocks LIKE :queryPath') // Médias dans des blocs
            ->orWhere('translation.blocks LIKE :queryPathWithEscapedQuotes') // Médias dans des blocs imbriqués dans d'autres blocs
            ->orWhere('translation.mainImage LIKE :queryPathWithEscapedSlashes')
            ->orWhere('translation.openGraphImage LIKE :queryPathWithEscapedSlashes')
            ->setParameter('locale', $locale)
            ->setParameter('queryPath', $queryPath)
            ->setParameter('queryPathWithEscapedQuotes', $queryPathWithEscapedQuotes)
            ->setParameter('queryPathWithEscapedSlashes', $queryPathWithEscapedSlashes)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }
}

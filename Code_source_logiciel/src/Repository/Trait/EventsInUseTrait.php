<?php

declare(strict_types=1);

namespace App\Repository\Trait;

use App\Entity\Event\Event;

trait EventsInUseTrait
{
    public function hasEventInBlock(string $locale, Event $event): mixed
    {
        $count = (int) $this->createQueryBuilder('entity')
            ->select('COUNT(entity.id)')
            ->leftJoin('entity.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.blocks LIKE :event')
            ->setParameter('locale', $locale)
            ->setParameter('event', '%\{\\\"event\\\":\\\"' . $event->getId() . '\\\"\}%')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Page;

use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Luna\CoreBundle\Entity\Page\PageInterface;
use Luna\CoreBundle\Repository\Page\PageRepository;

class PageRepositoryDecorator extends PageRepository
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;

    public function __construct(
        private readonly PageRepository $decorated
    ) {
        parent::__construct(
            $decorated->getEntityManager(),
            $decorated->getClassMetadata()
        );
    }

    public function findOneByCodeAndProfessional(string $locale, string $underPagesCode, string $professional): ?PageInterface
    {
        return $this->createQueryBuilder('page')
            ->addSelect('translation')
            ->innerJoin('page.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('page.professional', 'professional', 'WITH', 'professional.code = :professional')
            ->andWhere('page.enabled = true')
            ->andWhere('page.underPagesCode = :underPagesCode')
            ->setParameter('professional', $professional)
            ->setParameter('locale', $locale)
            ->setParameter('underPagesCode', $underPagesCode)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}

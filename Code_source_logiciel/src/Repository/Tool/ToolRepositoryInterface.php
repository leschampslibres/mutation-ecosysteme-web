<?php

declare(strict_types=1);

namespace App\Repository\Tool;

use App\Entity\Tool\Tool;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ToolRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findByTitle(string $title, int $limit): array;

    public function findById(array $id): array;

    public function findOneBySlug(?string $locale, string $slug): ?Tool;
}

<?php

declare(strict_types=1);

namespace App\Repository\Exhibit;

use App\Entity\Exhibit\Exhibit;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ExhibitRepository extends BaseEntityRepository implements ExhibitRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('exhibit')
            ->addSelect('translation')
            ->innerJoin('exhibit.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('exhibit')
            ->select('exhibit.id', 'translation.title')
            ->innerJoin('exhibit.translations', 'translation')
            ->andWhere('exhibit.enabled = :enabled')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('exhibit')
            ->select('exhibit.id', 'translation.title')
            ->innerJoin('exhibit.translations', 'translation')
            ->andWhere('exhibit.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createShopListQueryBuilder(string $locale, string $pageSlug): QueryBuilder
    {
        return $this->createQueryBuilder('exhibit')
            ->addSelect('translation')
            ->innerJoin('exhibit.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('exhibit.exhibitProfessionals', 'exhibitProfessional')
            ->innerJoin('exhibitProfessional.professional', 'professional')
            ->innerJoin('professional.pages', 'page')
            ->innerJoin('page.translations', 'pageTranslation', 'WITH', 'pageTranslation.locale = :locale')
            ->andWhere('exhibit.enabled = :enabled')
            ->andWhere('pageTranslation.compositeSlug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $pageSlug)
            ->setParameter('enabled', true)
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('exhibit')
            ->select('COUNT(exhibit.id)')
            ->innerJoin('exhibit.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('exhibit.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Exhibit
    {
        return $this->createQueryBuilder('exhibit')
            ->leftJoin('exhibit.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('exhibit.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('exhibit')
            ->addSelect('translation')
            ->innerJoin('exhibit.translations', 'translation')
            ->andWhere('exhibit.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}

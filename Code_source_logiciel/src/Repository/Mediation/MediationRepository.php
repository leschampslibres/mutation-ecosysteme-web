<?php

declare(strict_types=1);

namespace App\Repository\Mediation;

use App\Entity\Mediation\Mediation;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class MediationRepository extends BaseEntityRepository implements MediationRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('mediation')
            ->addSelect('translation')
            ->innerJoin('mediation.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('mediation')
            ->select('mediation.id', 'translation.title')
            ->innerJoin('mediation.translations', 'translation')
            ->andWhere('mediation.enabled = :enabled')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('mediation')
            ->select('mediation.id', 'translation.title')
            ->innerJoin('mediation.translations', 'translation')
            ->andWhere('mediation.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createShopListQueryBuilder(string $locale, string $pageSlug): QueryBuilder
    {
        return $this->createQueryBuilder('mediation')
            ->addSelect('translation')
            ->innerJoin('mediation.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('mediation.mediationProfessionals', 'mediationProfessional')
            ->innerJoin('mediationProfessional.professional', 'professional')
            ->innerJoin('professional.pages', 'page')
            ->innerJoin('page.translations', 'pageTranslation', 'WITH', 'pageTranslation.locale = :locale')
            ->andWhere('mediation.enabled = :enabled')
            ->andWhere('pageTranslation.compositeSlug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $pageSlug)
            ->setParameter('enabled', true)
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('mediation')
            ->select('COUNT(mediation.id)')
            ->innerJoin('mediation.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('mediation.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Mediation
    {
        return $this->createQueryBuilder('mediation')
            ->leftJoin('mediation.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('mediation.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findLatest(int $limit, string $locale): array
    {
        return $this->createQueryBuilder('mediation')
            ->addSelect('translation')
            ->innerJoin('mediation.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('mediation.enabled = :enabled')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->orderBy('mediation.position', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('mediation')
            ->addSelect('translation')
            ->innerJoin('mediation.translations', 'translation')
            ->andWhere('mediation.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}

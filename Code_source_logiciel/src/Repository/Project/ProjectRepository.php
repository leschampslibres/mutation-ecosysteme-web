<?php

declare(strict_types=1);

namespace App\Repository\Project;

use App\Entity\Project\Project;
use App\Enum\Project\StatusEnum;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ProjectRepository extends BaseEntityRepository implements ProjectRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        return $this->createQueryBuilder('project')
            ->addSelect('translation')
            ->innerJoin('project.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function createShopIndexQueryBuilder(string $locale, string $pageSlug): QueryBuilder
    {
        return $this->createQueryBuilder('project')
            ->addSelect('translation')
            ->innerJoin('project.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->innerJoin('project.professionals', 'professional')
            ->innerJoin('professional.pages', 'page')
            ->innerJoin('page.translations', 'pageTranslation', 'WITH', 'pageTranslation.locale = :locale')
            ->andWhere('project.status = :status')
            ->andWhere('project.enabled = :enabled')
            ->andWhere('pageTranslation.compositeSlug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('status', StatusEnum::Past)
            ->setParameter('enabled', true)
            ->setParameter('slug', $pageSlug)
        ;
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('project')
            ->select('project.id', 'translation.title')
            ->innerJoin('project.translations', 'translation')
            ->andWhere('project.enabled = :enabled')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('project')
            ->select('project.id', 'translation.title')
            ->innerJoin('project.translations', 'translation')
            ->andWhere('project.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Project
    {
        return $this->createQueryBuilder('project')
            ->leftJoin('project.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('project.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('project')
            ->addSelect('translation')
            ->innerJoin('project.translations', 'translation')
            ->andWhere('project.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Project;

use App\Entity\Project\Project;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ProjectRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createShopIndexQueryBuilder(string $locale, string $pageSlug): QueryBuilder;

    public function findByTitle(string $title, int $limit): array;

    public function findById(array $id): array;

    public function findOneBySlug(?string $locale, string $slug): ?Project;
}

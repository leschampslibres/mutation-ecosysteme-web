<?php

declare(strict_types=1);

namespace App\Repository\Event;

use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface EventOccurrenceRepositoryInterface extends RepositoryInterface
{
    public function createSearchQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createSectionSearchQueryBuilder(string $locale, array $filters, int $position): QueryBuilder;
}

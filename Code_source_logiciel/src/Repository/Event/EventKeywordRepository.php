<?php

declare(strict_types=1);

namespace App\Repository\Event;

use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class EventKeywordRepository extends BaseEntityRepository implements EventKeywordRepositoryInterface
{
    use ResourceRepositoryTrait;

    public function createListQueryBuilder(string $locale): QueryBuilder
    {
        return $this->createQueryBuilder('ek')
            ->orderBy('ek.title', 'ASC')
        ;
    }
}

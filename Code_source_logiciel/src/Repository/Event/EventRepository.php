<?php

declare(strict_types=1);

namespace App\Repository\Event;

use App\Entity\Event\Event;
use App\Entity\Event\EventCategory;
use App\Entity\Event\EventKeyword;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use App\Repository\Trait\RelatedResourcesTrait;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class EventRepository extends BaseEntityRepository implements EventRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use RelatedResourcesTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('event')
        ->addSelect('translation')
        ->innerJoin('event.translations', 'translation', 'WITH', 'translation.locale = :locale')
        ->leftJoin('event.keywords', 'keywords')
        ->setParameter('locale', $locale)
        ;

        // Par défaut ne sont affichés que les événements dont les données ne sont pas nettoyables/nettoyées
        $noStatusFilter = empty($filters) || empty($filters['status']);
        if ($noStatusFilter) {
            $queryBuilder
                ->andWhere('event.status < :status')
                ->setParameter('status', Event::STATUS_COMPLETED)
            ;
        }

        return $queryBuilder;
    }

    public function createAdminListEventByTitleQueryBuilder(string $title, int $limit): array
    {
        return $this->createQueryBuilder('event')
            ->select('event.id', 'translation.title')
            ->innerJoin('event.translations', 'translation')
            ->andWhere('translation.title LIKE :title')
            ->andWhere('event.enabled = true')
            ->andWhere('event.status < :status')
            ->setParameter('title', '%' . $title . '%')
            ->setParameter('status', Event::STATUS_ENDED)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function createAdminListEventByIdQueryBuilder(array $id): array
    {
        $id = $id[0];

        return $this->createQueryBuilder('event')
            ->select('event.id', 'translation.title')
            ->innerJoin('event.translations', 'translation')
            ->andWhere('event.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('event')
            ->select('COUNT(event.id)')
            ->innerJoin('event.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('event.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?Event
    {
        return $this->createQueryBuilder('event')
            ->leftJoin('event.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->andWhere('event.enabled = true')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function createSectionSearchQueryBuilder(string $locale, array $filters, int $position): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('event')
            ->addSelect('category')
            ->innerJoin('event.category', 'category', 'WITH', 'category = event.category')
            ->where('event.enabled = true')
            ->innerJoin('event.occurrences', 'occurrence', 'WITH', 'event = occurrence.event')
        ;

        // Récupération des événements de la section/position souhaitée
        // pour les catégories dont on n'affiche pas les occurrences
        $queryBuilder
            ->andWhere('category.searchDisplayPosition = :searchDisplayPosition')
            ->setParameter('searchDisplayPosition', $position)
            ->andWhere('category.displayOccurrences = :displayOccurrences')
            ->setParameter('displayOccurrences', 0)
        ;

        // Si aucune date n'est sélectionnée dans le moteur de recherche, on récupère les données à partir d'aujourd'hui
        $noDateFilter = empty($filters) || (empty($filters['dates']['from']) && empty($filters['dates']['to']));
        if ($noDateFilter) {
            $queryBuilder
                ->andWhere('event.startingAt >= :now OR event.endingAt >= :now')
                ->setParameter('now', new \DateTime())
            ;
        } else {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :from')
                ->andWhere('occurrence.startingAt <= :to')
                ->setParameter('from', $filters['dates']['from'])
                ->setParameter('to', $filters['dates']['to'])
            ;
        }

        $queryBuilder
            ->orderBy('event.startingAt', 'ASC')
            ->addOrderBy('event.endingAt', 'ASC')
        ;

        return $queryBuilder;
    }

    public function createCategorySearchQueryBuilder(array $filters): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('event')
            ->addSelect('category')
            ->innerJoin('event.category', 'category', 'WITH', 'category = event.category')
            ->where('event.enabled = true')
            ->innerJoin('event.occurrences', 'occurrence', 'WITH', 'event = occurrence.event')
        ;

        // Si aucune date n'est sélectionnée dans le moteur de recherche, on récupère les données à partir d'aujourd'hui
        $noDateFilter = empty($filters) || (empty($filters['dates']['from']) && empty($filters['dates']['to']));
        if ($noDateFilter) {
            $queryBuilder
                ->andWhere('event.startingAt >= :now OR event.endingAt >= :now')
                ->andWhere('occurrence.startingAt >= :now OR occurrence.endingAt >= :now')
                ->setParameter('now', new \DateTime('today'))
            ;
        } else {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :from')
                ->andWhere('occurrence.startingAt <= :to')
                ->setParameter('from', $filters['dates']['from'])
                ->setParameter('to', $filters['dates']['to'])
            ;
        }

        $queryBuilder
            ->orderBy('event.startingAt', 'ASC')
            ->addOrderBy('event.endingAt', 'ASC')
        ;

        return $queryBuilder;
    }

    public function findByKeywordAndDate(EventKeyword $keyword, \DateTime $dateStart, \DateTime $dateEnd): ?array
    {
        return $this->createQueryBuilder('event')
            ->select('event')
            ->innerJoin('event.keywords', 'keyword')
            ->andWhere('keyword = :keyword')
            ->andWhere('event.endingAt >= :dateStart')
            ->andWhere('event.startingAt <= :dateEnd')
            ->setParameter('keyword', $keyword)
            ->setParameter('dateStart', $dateStart)
            ->setParameter('dateEnd', $dateEnd)
            ->orderBy('event.startingAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findEventsInAutomaticBlock(?array $categories, ?int $originAgenda, ?int $audience, ?int $keyword, ?\DateTime $from, ?\DateTime $to, ?int $highlightedEvent): array
    {
        $queryBuilder = $this->createQueryBuilder('event')
            ->select('event')
            ->innerJoin('event.occurrences', 'occurrence')
            ->andWhere('event.enabled = true')
        ;

        if ($highlightedEvent) {
            $queryBuilder
                ->andWhere('event.id != :event')
                ->setParameter('event', $highlightedEvent)
            ;
        }

        if ($categories) {
            $queryBuilder
                ->innerJoin('event.category', 'category', 'WITH', 'category.id IN (:categories)')
                ->setParameter('categories', $categories)
            ;
        }

        if ($originAgenda) {
            $queryBuilder
                ->andWhere('event.originAgenda = :originAgenda')
                ->setParameter('originAgenda', $originAgenda)
            ;
        }

        if ($audience) {
            $queryBuilder
                ->andWhere('event.audience = :audience')
                ->setParameter('audience', $audience)
            ;
        }

        if ($keyword) {
            $queryBuilder
                ->innerJoin('event.keywords', 'keyword', 'WITH', 'keyword.id = :keyword')
                ->setParameter('keyword', $keyword)
            ;
        }

        $queryBuilder
            ->andWhere('event.startingAt >= :from OR event.endingAt >= :from')
            ->andWhere('occurrence.startingAt >= :from OR occurrence.endingAt >= :from')
        ;
        if ($from) {
            $queryBuilder->setParameter('from', $from);
        } else {
            $queryBuilder->setParameter('from', new \DateTime());
        }

        if ($to) {
            $queryBuilder
                ->andWhere('event.startingAt <= :to OR event.endingAt <= :to')
                ->andWhere('occurrence.startingAt <= :to OR occurrence.endingAt <= :to')
                ->setParameter('to', $to)
            ;
        }

        $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findHighlightedInAutomaticBlock(?int $event, ?\DateTime $from = null, ?\DateTime $to = null): ?Event
    {
        $queryBuilder = $this->createQueryBuilder('event')
            ->select('event')
            ->innerJoin('event.occurrences', 'occurrence')
            ->andWhere('event.enabled = true')
            ->andWhere('event.id = :event')
            ->setParameter('event', $event)
        ;

        $queryBuilder
            ->andWhere('event.startingAt >= :from OR event.endingAt >= :from')
            ->andWhere('occurrence.startingAt >= :from OR occurrence.endingAt >= :from')
        ;
        if ($from) {
            $queryBuilder->setParameter('from', $from);
        } else {
            $queryBuilder->setParameter('from', new \DateTime());
        }

        if ($to) {
            $queryBuilder
                ->andWhere('event.startingAt <= :to OR event.endingAt <= :to')
                ->andWhere('occurrence.startingAt <= :to OR occurrence.endingAt <= :to')
                ->setParameter('to', $to)
            ;
        }

        $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findByCategoryAndNotId(EventCategory $category, Event $event, \DateTime $date = new \DateTime()): ?array
    {
        return $this->createQueryBuilder('event')
            ->select('event')
            ->andWhere('event.enabled = true')
            ->andWhere('event.category = :category')
            ->andWhere('event != :event')
            ->andWhere('event.endingAt >= :date')
            ->setParameter('category', $category)
            ->setParameter('event', $event)
            ->setParameter('date', $date)
            ->orderBy('event.startingAt', 'ASC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDeletableEvents(): array
    {
        return $this->createQueryBuilder('event')
            ->select('event')
            ->innerJoin('event.category', 'category')
            ->andWhere('event.endingAt < :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('event')
            ->addSelect('translation')
            ->innerJoin('event.translations', 'translation')
            ->andWhere('event.enabled = :enabled')
            ->andWhere('event.status < :status')
            ->setParameter('enabled', true)
            ->setParameter('status', Event::STATUS_COMPLETED)
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Event;

use App\Entity\Event\EventCategory;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface EventCategoryRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale): QueryBuilder;

    public function createGridEntityFilterListQueryBuilder(): QueryBuilder;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?EventCategory;
}

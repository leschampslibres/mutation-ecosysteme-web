<?php

declare(strict_types=1);

namespace App\Repository\Event;

use App\Entity\Event\EventCategory;
use App\Repository\Trait\EventsInUseTrait;
use App\Repository\Trait\MediasInUseTrait;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class EventCategoryRepository extends BaseEntityRepository implements EventCategoryRepositoryInterface, SitemapRepositoryInterface
{
    use EventsInUseTrait;
    use MediasInUseTrait;
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(string $locale): QueryBuilder
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->setParameter('locale', $locale)
        ;
    }

    public function createGridEntityFilterListQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation')
            ->where('translation.title IS NOT NULL')
            ->orderBy('category.position', 'ASC')
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function existsOneBySlug(?string $locale, string $slug): bool
    {
        $count = (int) $this
            ->createQueryBuilder('category')
            ->select('COUNT(category.id)')
            ->innerJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $count > 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(?string $locale, string $slug): ?EventCategory
    {
        return $this->createQueryBuilder('category')
            ->innerJoin('category.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('translation.locale = :locale')
            ->andWhere('translation.slug = :slug')
            ->setParameter('locale', $locale)
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllOrderByPosition(): array
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation')
            ->orderBy('category.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForMeetingsBlock(): array
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation')
            ->andWhere('category.code NOT IN (:codes)')
            ->orderBy('category.position', 'ASC')
            ->setParameter('codes', ['21633-evenements-aux-champs-libres', '76912-spectacles-aux-champs-libres'])
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('category')
            ->addSelect('translation')
            ->innerJoin('category.translations', 'translation')
            ->andWhere('category.enabled = :enabled')
            ->andWhere('translation.title IS NOT NULL')
            ->setParameter('enabled', true)
            ->orderBy('category.position', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}

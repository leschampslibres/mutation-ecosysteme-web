<?php

declare(strict_types=1);

namespace App\Repository\Event;

use App\Entity\Event\Event;
use App\Entity\Event\EventOccurrence;
use App\Enum\Event\SearchDisplayPositionEnum;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class EventOccurrenceRepository extends BaseEntityRepository implements EventOccurrenceRepositoryInterface
{
    use ResourceRepositoryTrait;

    public function createSearchQueryBuilder(string $locale, array $filters): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('occurrence')
            ->addSelect('event')
            ->addSelect('category')
            ->innerJoin('occurrence.event', 'event', 'WITH', 'event = occurrence.event')
            ->innerJoin('event.category', 'category', 'WITH', 'category = event.category')
            ->where('event.enabled = true')
            ->orderBy('occurrence.startingAt', 'ASC')
        ;

        // Récupération uniquement des événements de la liste paginée
        $queryBuilder
            ->andWhere('category.searchDisplayPosition = :searchDisplayPosition')
            ->setParameter('searchDisplayPosition', SearchDisplayPositionEnum::Third->value)
        ;

        // Si aucune date n'est sélectionnée dans le moteur de recherche,
        // ou si seule une date de fin est sélectionnée,
        // on récupère les données à partir d'aujourd'hui
        $noDateFilter = empty($filters) || empty($filters['dates']['from']);
        if ($noDateFilter) {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :now OR occurrence.endingAt >= :now')
                ->setParameter('now', new \DateTime())
            ;
        }

        $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
        ;

        return $queryBuilder;
    }

    public function createSectionSearchQueryBuilder(string $locale, array $filters, int $position): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('occurrence')
            ->addSelect('event')
            ->addSelect('category')
            ->innerJoin('occurrence.event', 'event', 'WITH', 'event = occurrence.event')
            ->innerJoin('event.category', 'category', 'WITH', 'category = event.category')
            ->where('event.enabled = true')
        ;

        // Récupération des occurrences de la section/position souhaitée
        // pour les catégories dont on affiche les occurrences
        $queryBuilder
            ->andWhere('category.searchDisplayPosition = :searchDisplayPosition')
            ->setParameter('searchDisplayPosition', $position)
            ->andWhere('category.displayOccurrences = :displayOccurrences')
            ->setParameter('displayOccurrences', true)
        ;

        // Si aucune date n'est sélectionnée dans le moteur de recherche,
        // ou si seule une date de fin est sélectionnée,
        // on récupère les données à partir d'aujourd'hui
        $noDateFilter = empty($filters) || empty($filters['dates']['from']);
        if ($noDateFilter) {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :now OR occurrence.endingAt >= :now')
                ->setParameter('now', new \DateTime())
            ;
        }

        $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
        ;

        return $queryBuilder;
    }

    public function createCategorySearchQueryBuilder(array $filters): QueryBuilder
    {
        $queryBuilder = $this->createQueryBuilder('occurrence')
            ->addSelect('event')
            ->addSelect('category')
            ->innerJoin('occurrence.event', 'event', 'WITH', 'event = occurrence.event')
            ->innerJoin('event.category', 'category', 'WITH', 'category = event.category')
            ->where('event.enabled = true')
        ;

        // Si aucune date n'est sélectionnée dans le moteur de recherche,
        // ou si seule une date de fin est sélectionnée,
        // on récupère les données à partir d'aujourd'hui
        $noDateFilter = empty($filters) || empty($filters['dates']['from']);
        if ($noDateFilter) {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :now OR occurrence.endingAt >= :now')
                ->setParameter('now', new \DateTime())
            ;
        }

        $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
        ;

        return $queryBuilder;
    }

    public function findNext(Event $event, ?\DateTime $date = null): ?EventOccurrence
    {
        $queryBuilder = $this->createQueryBuilder('occurrence')
            ->andWhere('occurrence.event = :event')
            ->setParameter('event', $event)
        ;

        if ($date) {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :date')
                ->setParameter('date', $date->format('Y-m-d'))
            ;
        } else {
            $queryBuilder
                ->andWhere('occurrence.startingAt >= :now')
                ->setParameter('now', new \DateTime())
            ;
        }

        return $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByDate(Event $event, \DateTime $startDate, ?\DateTime $endDate = null): array
    {
        $queryBuilder = $this->createQueryBuilder('occurrence')
            ->andWhere('occurrence.event = :event')
            ->andWhere('occurrence.startingAt >= :startDate')
            ->setParameter('event', $event)
            ->setParameter('startDate', $startDate->format('Y-m-d 00:00:00'))
        ;

        if ($endDate) {
            $queryBuilder
                ->andWhere('occurrence.endingAt <= :endDate')
                ->setParameter('endDate', $endDate->format('Y-m-d 23:59:59'))
            ;
        }

        return $queryBuilder
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllUpcoming(): array
    {
        return $this->createQueryBuilder('occurrence')
            ->addSelect('event')
            ->innerJoin('occurrence.event', 'event', 'WITH', 'event = occurrence.event')
            ->where('event.enabled = true')
            ->andWhere('occurrence.startingAt >= :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('occurrence.startingAt', 'ASC')
            ->addOrderBy('occurrence.endingAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Event;

use App\Entity\Event\Event;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface EventRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(string $locale, array $filters): QueryBuilder;

    public function createAdminListEventByTitleQueryBuilder(string $title, int $limit): array;

    public function createAdminListEventByIdQueryBuilder(array $id): array;

    public function existsOneBySlug(?string $locale, string $slug): bool;

    public function findOneBySlug(?string $locale, string $slug): ?Event;

    public function createSectionSearchQueryBuilder(string $locale, array $filters, int $position): QueryBuilder;

    public function findDeletableEvents(): array;
}

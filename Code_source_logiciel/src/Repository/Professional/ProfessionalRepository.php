<?php

declare(strict_types=1);

namespace App\Repository\Professional;

use App\Entity\Mediation\MediationSubject;
use Doctrine\ORM\QueryBuilder;
use Luna\CoreBundle\Repository\SitemapRepositoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\ResourceRepositoryTrait;

class ProfessionalRepository extends BaseEntityRepository implements ProfessionalRepositoryInterface, SitemapRepositoryInterface
{
    use ResourceRepositoryTrait;

    public function createAdminListQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('professional');
    }

    public function findByTitle(string $title, int $limit): array
    {
        return $this->createQueryBuilder('professional')
            ->select('professional.id', 'translation.title')
            ->innerJoin('professional.translations', 'translation')
            ->andWhere('professional.enabled = :enabled')
            ->andWhere('translation.title LIKE :title')
            ->setParameter('enabled', true)
            ->setParameter('title', '%' . $title . '%')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByMediationWithSubject(MediationSubject $subject): array
    {
        return $this->createQueryBuilder('professional')
            ->innerJoin('professional.mediationProfessionals', 'mediationProfessional')
            ->innerJoin('mediationProfessional.mediation', 'mediation')
            ->innerJoin('mediation.subjects', 'subject')
            ->andWhere('subject = :subject')
            ->setParameter('subject', $subject)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findById(array $id): array
    {
        return $this->createQueryBuilder('professional')
            ->select('professional.id', 'translation.title')
            ->innerJoin('professional.translations', 'translation')
            ->andWhere('professional.id IN (:id)')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForSitemap(): array
    {
        return $this->createQueryBuilder('professional')
            ->addSelect('translation')
            ->innerJoin('professional.translations', 'translation')
            ->andWhere('professional.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Repository\Professional;

use App\Entity\Mediation\MediationSubject;
use Doctrine\ORM\QueryBuilder;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ProfessionalRepositoryInterface extends RepositoryInterface
{
    public function createAdminListQueryBuilder(): QueryBuilder;

    public function findByTitle(string $title, int $limit): array;

    public function findById(array $id): array;

    public function findByMediationWithSubject(MediationSubject $subject): array;
}

<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Tool\Tool;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class ToolTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $toolRepository)
    {
    }

    public function transform(mixed $value): ?Tool
    {
        if (empty($value)) {
            return null;
        }

        return $this->toolRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Article\Article;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class ArticleTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $articleRepository)
    {
    }

    public function transform(mixed $value): ?Article
    {
        if (empty($value)) {
            return null;
        }

        return $this->articleRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

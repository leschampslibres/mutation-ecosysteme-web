<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Mediation\Mediation;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class MediationTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $mediationRepository)
    {
    }

    public function transform(mixed $value): ?Mediation
    {
        if (empty($value)) {
            return null;
        }

        return $this->mediationRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Article\ArticleSubject;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class ArticleSubjectTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $articleSubjectRepository)
    {
    }

    public function transform(mixed $value): ?ArticleSubject
    {
        if (empty($value)) {
            return null;
        }

        return $this->articleSubjectRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Dossier\Dossier;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class DossierTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $dossierRepository)
    {
    }

    public function transform(mixed $value): ?Dossier
    {
        if (empty($value)) {
            return null;
        }

        return $this->dossierRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

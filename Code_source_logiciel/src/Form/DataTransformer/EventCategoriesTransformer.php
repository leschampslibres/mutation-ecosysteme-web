<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class EventCategoriesTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $eventCategoryRepository)
    {
    }

    public function transform(mixed $values): ?array
    {
        if (empty($values)) {
            return null;
        }

        $ids = [];
        foreach ($values as $key => $value) {
            if ($value) {
                $ids[] = $key;
            }
        }

        return $this->eventCategoryRepository->findBy(['id' => $ids]);
    }

    public function reverseTransform(mixed $values): array
    {
        if (empty($values)) {
            return [];
        }

        $ids = [];
        foreach ($values as $value) {
            $ids[$value->getId()] = true;
        }

        return $ids;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Exhibit\Exhibit;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class ExhibitTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $exhibitRepository)
    {
    }

    public function transform(mixed $value): ?Exhibit
    {
        if (empty($value)) {
            return null;
        }

        return $this->exhibitRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

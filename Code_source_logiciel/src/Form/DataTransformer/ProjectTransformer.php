<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Project\Project;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class ProjectTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $projectRepository)
    {
    }

    public function transform(mixed $value): ?Project
    {
        if (empty($value)) {
            return null;
        }

        return $this->projectRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

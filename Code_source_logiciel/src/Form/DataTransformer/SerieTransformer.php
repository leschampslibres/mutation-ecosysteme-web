<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Serie\Serie;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class SerieTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $serieRepository)
    {
    }

    public function transform(mixed $value): ?Serie
    {
        if (empty($value)) {
            return null;
        }

        return $this->serieRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

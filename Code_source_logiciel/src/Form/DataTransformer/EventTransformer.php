<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Event\Event;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\DataTransformerInterface;

class EventTransformer implements DataTransformerInterface
{
    public function __construct(private readonly RepositoryInterface $eventRepository)
    {
    }

    public function transform(mixed $value): ?Event
    {
        if (empty($value)) {
            return null;
        }

        return $this->eventRepository->findOneBy(['id' => $value]);
    }

    public function reverseTransform(mixed $value): int
    {
        if (empty($value)) {
            return 0;
        }

        return $value->getId();
    }
}

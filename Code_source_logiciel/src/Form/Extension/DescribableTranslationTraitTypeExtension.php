<?php

declare(strict_types=1);

namespace App\Form\Extension;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Luna\CoreBundle\Enum\PageBehavior;
use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DescribableTranslationTraitTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData'])
        ;
    }

    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = $event->getData();

        // Il faut pouvoir ajouter des liens dans la description de la page d'accueil
        if (!empty($data) && !empty($data->getTranslatable()) && method_exists($data->getTranslatable(), 'getBehavior') && PageBehavior::Homepage == $data->getTranslatable()->getBehavior()) {
            $form
                ->remove('description')
                ->add('description', CKEditorType::class, [
                    'required' => false,
                    'label' => 'luna.ui.trait.describable.description.label',
                    'help' => 'luna.ui.trait.describable.description.help',
                    'config_name' => 'links_only',
                ])
            ;
        }
    }

    public static function getExtendedTypes(): iterable
    {
        return [DescribableTranslationTraitType::class];
    }
}

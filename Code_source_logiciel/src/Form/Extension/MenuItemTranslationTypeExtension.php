<?php

declare(strict_types=1);

namespace App\Form\Extension;

use MonsieurBiz\SyliusMenuPlugin\Form\Type\MenuItemTranslationType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class MenuItemTranslationTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label', TextType::class, [
                'priority' => 3,
            ])
            ->add('description', TextType::class, [
                'required' => false,
                'label' => 'app.ui.menu_item.form.description.label',
                'help' => 'app.ui.menu_item.form.description.help',
                'priority' => 2,
            ])
            ->add('url', TextType::class, [
                'priority' => 1,
            ])
            ->add('language', TextType::class, [
                'label' => 'app.ui.menu_item.form.language.label',
                'help' => 'app.ui.menu_item.form.language.help',
                'priority' => 0,
            ])
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [MenuItemTranslationType::class];
    }
}

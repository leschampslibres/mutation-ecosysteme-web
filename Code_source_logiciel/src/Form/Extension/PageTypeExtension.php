<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Form\Type\Article\ArticleAutocompleteChoiceType;
use Luna\CoreBundle\Form\Type\Page\PageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PageTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('professional', EntityType::class, [
                'class' => 'App\Entity\Professional\Professional',
                'required' => false,
                'multiple' => false,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.page.form.professional.label',
                'help' => 'app.ui.resource.page.form.professional.help',
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData'])
        ;
    }

    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = $event->getData();

        // Article mis en avant uniquement sur la page d'accueil du Mag
        if (!empty($data) && 'articles' == $data->getTemplate()) {
            $form->add('article', ArticleAutocompleteChoiceType::class, [
                'required' => false,
                'multiple' => false,
                'resource' => 'app.article',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.resource.page.form.article.label',
                'help' => 'app.ui.resource.page.form.article.help',
            ]);
        }
    }

    public static function getExtendedTypes(): iterable
    {
        return [PageType::class];
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\MenuItem\IconEnum;
use MonsieurBiz\SyliusMenuPlugin\Form\Type\MenuItemType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\EnumType as FormEnumType;
use Symfony\Component\Form\FormBuilderInterface;

final class MenuItemTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('icon', FormEnumType::class, [
                'class' => IconEnum::class,
                'required' => false,
                'label' => 'app.ui.menu_item.form.icon.label',
                'help' => 'app.ui.menu_item.form.icon.help',
                'placeholder' => 'app.ui.menu_item.form.icon.placeholder',
            ])
            ->add('hideLabel', null, [
                'required' => false,
                'label' => 'app.ui.menu_item.form.hide_label.label',
                'help' => 'app.ui.menu_item.form.hide_label.help',
            ])
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [MenuItemType::class];
    }
}

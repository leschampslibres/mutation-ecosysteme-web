<?php

declare(strict_types=1);

namespace App\Form\Extension;

use MonsieurBiz\SyliusMenuPlugin\Form\Type\MenuType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;

final class MenuTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => 'sylius.ui.title',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData'])
        ;
    }

    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = $event->getData();

        // Modification possible du code uniquement quand il n'est pas rempli
        if (!empty($data->getCode())) {
            $form->add('code', TextType::class, [
                'disabled' => true,
            ]);
        }
    }

    public static function getExtendedTypes(): iterable
    {
        return [MenuType::class];
    }
}

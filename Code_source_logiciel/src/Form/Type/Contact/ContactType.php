<?php

declare(strict_types=1);

namespace App\Form\Type\Contact;

use App\Entity\Contact\Contact;
use App\Enum\Contact\ServiceEnum;
use Luna\CoreBundle\Form\Type\Contact\ContactType as BaseContactType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends BaseContactType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('company', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.contact.form.company.label',
                'help' => 'app.ui.resource.contact.form.company.help',
            ])
            ->add('serviceName', EnumType::class, [
                'class' => ServiceEnum::class,
                'choice_label' => static function (ServiceEnum $choice): string {
                    return $choice->value;
                },
                'label' => 'app.ui.resource.contact.form.service.label',
                'placeholder' => 'app.ui.resource.contact.form.service.placeholder',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('object', TextType::class, [
                'label' => 'luna.ui.resource.contact.form.object.label',
                'help' => 'luna.ui.resource.contact.form.object.help',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'app.ui.resource.contact.form.valid.label',
            ])
            ->getForm()
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}

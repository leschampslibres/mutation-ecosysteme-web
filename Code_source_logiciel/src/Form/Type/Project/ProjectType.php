<?php

declare(strict_types=1);

namespace App\Form\Type\Project;

use App\Entity\Professional\Professional;
use App\Form\Type\Element\ResourceColorType;
use App\Form\Type\Element\ResourceStatusType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProjectType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('color', ResourceColorType::class)
            ->add('status', ResourceStatusType::class, [
                'required' => true,
                'label' => 'app.ui.resource.project.form.status.label',
                'help' => 'app.ui.resource.project.form.status.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('date', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.resource.project.form.date.label',
                'help' => 'app.ui.resource.project.form.date.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('school', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.resource.project.form.school.label',
                'help' => 'app.ui.resource.project.form.school.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('professionals', EntityType::class, [
                'required' => true,
                'class' => Professional::class,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.mediation.form.professionals.label',
                'help' => 'app.ui.resource.mediation.form.professionals.help',
                'multiple' => true,
                'expanded' => true,
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ProjectTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

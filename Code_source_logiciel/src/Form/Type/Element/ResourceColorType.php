<?php

declare(strict_types=1);

namespace App\Form\Type\Element;

use App\Enum\ColorEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResourceColorType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => ColorEnum::class,
            'required' => false,
            'label' => 'app.ui.element.color.form.color.label',
            'help' => 'app.ui.element.color.form.color.help',
            'placeholder' => 'app.ui.element.color.form.color.placeholder',
        ]);
    }

    public function getParent(): string
    {
        return EnumType::class;
    }
}

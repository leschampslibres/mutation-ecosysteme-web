<?php

namespace App\Form\Type\Element;

use App\Enum\Project\StatusEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResourceStatusType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => StatusEnum::class,
            'required' => false,
            'placeholder' => 'app.ui.element.status.form.placeholder',
        ]);
    }

    public function getParent(): string
    {
        return EnumType::class;
    }
}

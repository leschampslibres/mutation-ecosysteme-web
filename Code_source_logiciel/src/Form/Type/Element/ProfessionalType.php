<?php

declare(strict_types=1);

namespace App\Form\Type\Element;

use App\Entity\Professional\Professional;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProfessionalType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_element_professional_type';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('professional', EntityType::class, [
                'required' => true,
                'class' => Professional::class,
                'choice_label' => 'title',
                'label' => 'app.ui.element.professional.form.professional.label',
                'help' => 'app.ui.element.professional.form.professional.help',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('audience', TextType::class, [
                'required' => true,
                'label' => 'app.ui.element.professional.form.audience.label',
                'help' => 'app.ui.element.professional.form.audience.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Element;

use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class LineType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_element_line';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('column1', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.element.line.form.column1.label',
                'help' => 'app.ui.element.line.form.column1.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('column2', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.element.line.form.column2.label',
                'help' => 'app.ui.element.line.form.column2.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

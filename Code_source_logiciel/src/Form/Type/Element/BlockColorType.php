<?php

declare(strict_types=1);

namespace App\Form\Type\Element;

use App\Enum\ColorEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

// On ne peut pas utiliser directement ColorEnum car les données sont stockées sous forme de string via le RichEditorPlugin
class BlockColorType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $choices = [];
        foreach (ColorEnum::cases() as $color) {
            $choices[$color->name] = $color->value;
        }
        $resolver->setDefaults([
            'required' => true,
            'choices' => $choices,
            'expanded' => false,
            'multiple' => false,
            'label' => 'app.ui.element.color.form.color.label',
            'help' => 'app.ui.element.color.form.color.help',
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}

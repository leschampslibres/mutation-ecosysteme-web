<?php

declare(strict_types=1);

namespace App\Form\Type\Dossier;

use App\Entity\Dossier\DossierProfessional;
use App\Form\Type\Element\ProfessionalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DossierProfessionalType extends ProfessionalType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DossierProfessional::class,
            'remove_alpha_field' => false,
        ]);
    }
}

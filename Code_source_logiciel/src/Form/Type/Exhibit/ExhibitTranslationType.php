<?php

declare(strict_types=1);

namespace App\Form\Type\Exhibit;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use Luna\CoreBundle\Form\Type\Trait\BlockableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ExhibitTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_exhibit_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('describable', DescribableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('blockable', BlockableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Autres champs
            ->add('mainImage', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'has_caption' => true,
                'required' => true,
                'label' => 'luna.ui.trait.main_imageable.main_image.label',
                'help' => 'luna.ui.trait.main_imageable.main_image.help',
            ])
            ->add('audience', TextType::class, [
                'required' => true,
                'label' => 'app.ui.resource.exhibit.form.audience.label',
                'help' => 'app.ui.resource.exhibit.form.audience.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Exhibit;

use App\Entity\Exhibit\ExhibitProfessional;
use App\Form\Type\Element\ProfessionalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExhibitProfessionalType extends ProfessionalType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ExhibitProfessional::class,
            'remove_alpha_field' => false,
        ]);
    }
}

<?php

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;

class EventsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('overtitle', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.events.form.overtitle.label',
                'help' => 'app.ui.block.events.form.overtitle.help',
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.events.form.title.label',
                'help' => 'app.ui.block.events.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.events.form.button.label',
                'help' => 'app.ui.block.events.form.button.help',
            ])
            ->add('images', FormCheckBoxType::class, [
                'required' => false,
                'label' => 'app.ui.block.events.form.images.label',
                'help' => 'app.ui.block.events.form.images.help',
            ])
            ->add('formatting', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.events.form.formatting.label',
                'help' => 'app.ui.block.events.form.formatting.help',
                'choices' => [
                    'app.ui.block.events.form.formatting.choices.column' => 'column',
                    'app.ui.block.events.form.formatting.choices.line' => 'line',
                ]
            ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;

class ExploreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('overtitle', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore.form.overtitle.label',
                'help' => 'app.ui.block.explore.form.overtitle.help',
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.explore.form.title.label',
                'help' => 'app.ui.block.explore.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore.form.description.label',
                'help' => 'app.ui.block.explore.form.description.help',
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore.form.button.label',
                'help' => 'app.ui.block.explore.form.button.help',
            ])
            ->add('images', FormCheckBoxType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore.form.images.label',
                'help' => 'app.ui.block.explore.form.images.help',
            ])
            ->add('formatting', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.explore.form.formatting.label',
                'help' => 'app.ui.block.explore.form.formatting.help',
                'choices' => [
                    'app.ui.block.explore.form.formatting.choices.column' => 'column',
                    'app.ui.block.explore.form.formatting.choices.line' => 'line',
                ]
            ])
            ->add('series', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore.form.series.label',
                'help' => 'app.ui.block.explore.form.series.help',
                'tags' => ['-default', 'serie'],
            ])
        ;
    }
}

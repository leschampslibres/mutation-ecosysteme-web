<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType as FormTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DownloadsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_downloads';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.downloads.form.title.label',
                'help' => 'app.ui.block.downloads.form.title.help',
            ])
            ->add('description', FormTextareaType::class, [
                'required' => false,
                'label' => 'app.ui.block.downloads.form.description.label',
                'help' => 'app.ui.block.downloads.form.description.help',
            ])
            ->add('downloads', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.downloads.form.downloads.label',
                'help' => 'app.ui.block.downloads.form.downloads.help',
                'tags' => ['-default', 'download'],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

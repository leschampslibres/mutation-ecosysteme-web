<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ColorCardsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_color_cards';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.color_cards.form.title.label',
                'help' => 'app.ui.block.color_cards.form.title.help',
            ])
            ->add('color_cards', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.color_cards.form.color_cards.label',
                'help' => 'app.ui.block.color_cards.form.color_cards.help',
                'tags' => ['-default', 'color_card'],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

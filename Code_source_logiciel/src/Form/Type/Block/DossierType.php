<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\DossierTransformer;
use App\Form\Type\Dossier\DossierAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DossierType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $dossierRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_dossier';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dossier', DossierAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.dossier',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.dossier.form.dossier.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('dossier')->addModelTransformer(new DossierTransformer($this->dossierRepository));
    }
}

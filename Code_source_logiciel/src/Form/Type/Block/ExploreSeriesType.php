<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ExploreSeriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('series', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.explore_series.form.series.label',
                'help' => 'app.ui.block.explore_series.form.series.help',
                'tags' => ['-default', 'serie'],
            ])
        ;
    }
}

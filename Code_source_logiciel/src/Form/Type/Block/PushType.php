<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use Luna\CoreBundle\Form\Type\Block\PushType as BasePushType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class PushType extends BasePushType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_push';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('color', BlockColorType::class)
        ;
        parent::buildForm($builder, $options);
        $builder
            ->add('style', ChoiceType::class, [
                'choices' => [
                    'app.ui.block.push.form.style.choices.margins' => 'margins',
                    'app.ui.block.push.form.style.choices.full' => 'full',
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => 'app.ui.block.push.form.style.label',
                'help' => 'app.ui.block.push.form.style.help',
            ])
        ;
    }
}

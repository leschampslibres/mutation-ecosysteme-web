<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OpacType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_opac';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use Luna\CoreBundle\Form\Type\Block\ButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType as FormTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ColorCardType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_color_card';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('color', BlockColorType::class)
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.color_card.form.title.label',
                'help' => 'app.ui.block.color_card.form.title.help',
            ])
            ->add('content', FormTextareaType::class, [
                'required' => true,
                'label' => 'app.ui.block.color_card.form.content.label',
                'help' => 'app.ui.block.color_card.form.content.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.color_card.form.button.label',
                'help' => 'app.ui.block.color_card.form.button.help',
            ])
        ;
    }
}

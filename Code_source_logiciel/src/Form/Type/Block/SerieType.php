<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\SerieTransformer;
use App\Form\Type\Serie\SerieAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SerieType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $serieRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_serie';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('serie', SerieAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.serie',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.explore.form.serie.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('serie')->addModelTransformer(new SerieTransformer($this->serieRepository));
    }
}

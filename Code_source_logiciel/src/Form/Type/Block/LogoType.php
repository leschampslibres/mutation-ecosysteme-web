<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class LogoType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_logo';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('url', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.logo.form.url.label',
                'help' => 'app.ui.block.logo.form.url.help',
            ])
            ->add('image', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'has_caption' => true,
                'required' => true,
                'label' => 'app.ui.block.logo.form.image.label',
                'help' => 'app.ui.block.logo.form.image.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\LineType;
use Luna\CoreBundle\Form\Type\Element\ImageType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as FormCollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class InformationsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_informations';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('style', FormChoiceType::class, [
                'choices' => [
                    'app.ui.block.informations.form.style.choices.grey' => 'grey',
                    'app.ui.block.informations.form.style.choices.white' => 'white',
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => true,
                'label' => 'app.ui.block.informations.form.style.label',
                'help' => 'app.ui.block.informations.form.style.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.informations.form.title.label',
                'help' => 'app.ui.block.informations.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('image', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'has_caption' => true,
                'required' => false,
                'label' => 'app.ui.block.informations.form.image.label',
                'help' => 'app.ui.block.informations.form.image.help',
            ])
            ->add('image_style', FormChoiceType::class, [
                'choices' => [
                    'app.ui.block.informations.form.image_style.choices.image' => 'image',
                    'app.ui.block.informations.form.image_style.choices.logo' => 'logo',
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => 'app.ui.block.informations.form.image_style.label',
                'help' => 'app.ui.block.informations.form.image_style.help',
            ])
            ->add('table', FormCollectionType::class, [
                'block_prefix' => 'app_block_informations_table',
                'entry_type' => LineType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'delete_empty' => true,
                'required' => false,
                'label' => 'app.ui.block.informations.form.table.label',
                'help' => 'app.ui.block.informations.form.table.help',
                'button_add_label' => 'app.ui.block.informations.form.table.add_line',
            ])
            ->add('content', WysiwygType::class, [
                'config_name' => 'rich_editor',
                'required' => false,
                'label' => 'app.ui.block.informations.form.content.label',
                'help' => 'app.ui.block.informations.form.content.help',
            ])
        ;
    }
}

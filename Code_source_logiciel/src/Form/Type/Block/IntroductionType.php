<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class IntroductionType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_introduction';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', WysiwygType::class, [
                'config_name' => 'minimal_inline',
                'required' => true,
                'label' => 'app.ui.block.introduction.form.content.label',
                'help' => 'app.ui.block.introduction.form.content.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

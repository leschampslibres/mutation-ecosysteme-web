<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType as FormTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MeetingsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_categories';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.meetings.form.title.label',
                'help' => 'app.ui.block.meetings.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', FormTextareaType::class, [
                'required' => false,
                'label' => 'app.ui.block.meetings.form.description.label',
                'help' => 'app.ui.block.meetings.form.description.help',
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use Luna\CoreBundle\Form\Type\Element\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Validator\Constraints as Assert;

class ListResourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('getMainImage', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'required' => false,
                'label' => 'app.ui.block.list_resource.form.image.label',
                'help' => 'app.ui.block.list_resource.form.image.help',
            ])
            ->add('type', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.list_resource.form.type.label',
                'help' => 'app.ui.block.list_resource.form.type.help',
                'choices' => [
                    'app.ui.block.list_resource.form.type.choices.article' => 'article',
                    'app.ui.block.list_resource.form.type.choices.podcast' => 'podcast',
                    'app.ui.block.list_resource.form.type.choices.video' => 'video',
                    'app.ui.block.list_resource.form.type.choices.document' => 'document',
                    'app.ui.block.list_resource.form.type.choices.image' => 'image',
                ],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.list_resource.form.title.label',
                'help' => 'app.ui.block.list_resource.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'app.ui.block.list_resource.form.description.label',
                'help' => 'app.ui.block.list_resource.form.description.help',
            ])
            ->add('link', ButtonType::class, [
                'required' => true,
                'label' => 'app.ui.block.list_resource.form.link.label',
                'help' => 'app.ui.block.list_resource.form.link.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }

}

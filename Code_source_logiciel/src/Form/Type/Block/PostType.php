<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ArticleTransformer;
use App\Form\Type\Article\ArticleAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PostType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $articleRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('article', ArticleAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.article',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.post.form.article.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('article')->addModelTransformer(new ArticleTransformer($this->articleRepository));
    }
}

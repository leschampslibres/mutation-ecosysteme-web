<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ToolTransformer;
use App\Form\Type\Tool\ToolAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ToolType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $toolRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_tool';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('tool', ToolAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.tool',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.tool.form.tool.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('tool')->addModelTransformer(new ToolTransformer($this->toolRepository));
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_questions';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('questions', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.questions.form.questions.label',
                'help' => 'app.ui.block.questions.form.questions.help',
                'tags' => ['-default', 'question'],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\TextType as BaseTextType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\FormBuilderInterface;

class TextType extends BaseTextType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_text';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('buttons', RichEditorType::class, [
                'required' => false,
                'label' => 'luna.ui.block.text.form.buttons.label',
                'help' => 'luna.ui.block.text.form.buttons.help',
                'tags' => ['-default', 'button_with_style'],
            ])
        ;
    }
}

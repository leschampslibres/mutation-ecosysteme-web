<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Entity\Event\EventCategory;
use App\Entity\Event\EventKeyword;
use App\Enum\Event\AudienceEnum;
use App\Enum\Event\OriginAgendaEnum;
use App\Form\DataTransformer\EventCategoriesTransformer;
use App\Form\DataTransformer\EventTransformer;
use App\Form\Type\Event\EventAutocompleteChoiceType;
use App\Repository\Event\EventCategoryRepository;
use Doctrine\ORM\EntityManager;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType as FormIntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints as Assert;

class AutomaticEventsType extends AbstractType
{
    protected EntityManager $em;

    protected EventCategoryRepository $eventCategoryRepository;

    protected array $choicesCategory = [];

    private array $choicesOriginAgenda = [];

    private array $choicesAudience = [];

    protected array $choicesKeyword = [];

    public function __construct(?EntityManager $em, private readonly RepositoryInterface $eventRepository)
    {
        $this->em = $em;

        $this->eventCategoryRepository = $this->em->getRepository(EventCategory::class);

        foreach ($this->eventCategoryRepository->findAll() as $category) {
            $this->choicesCategory[$category->getLabel()] = $category->getId();
        }

        foreach (OriginAgendaEnum::cases() as $originAgenda) {
            $this->choicesOriginAgenda['app.ui.resource.event.originAgenda.' . $originAgenda->value] = $originAgenda->value;
        }

        foreach (AudienceEnum::cases() as $audience) {
            $this->choicesAudience['app.ui.resource.event.audience.' . $audience->value] = $audience->value;
        }

        foreach ($this->em->getRepository(EventKeyword::class)->findAll() as $keyword) {
            $this->choicesKeyword[$keyword->getTitle()] = $keyword->getId();
        }
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_automatic_events';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('generalForm', EventsType::class, [
                'label' => false,
            ])
            ->add('from', DateType::class, [
                'label' => 'luna.ui.grid.filter.date_range.from.label',
                'help' => 'luna.ui.grid.filter.date_range.from.help',
                'placeholder' => 'luna.ui.grid.filter.date_range.from.placeholder',
                'widget' => 'single_text',
                'input' => 'array',
                'required' => false,
            ])
            ->add('to', DateType::class, [
                'label' => 'luna.ui.grid.filter.date_range.to.label',
                'help' => 'luna.ui.grid.filter.date_range.to.help',
                'placeholder' => 'luna.ui.grid.filter.date_range.to.placeholder',
                'widget' => 'single_text',
                'input' => 'array',
                'required' => false,
            ])
            ->add('limit', FormIntegerType::class, [
                'required' => true,
                'label' => 'app.ui.block.events.form.choice.automatic.limit.label',
                'help' => 'app.ui.block.events.form.choice.automatic.limit.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
                'attr' => [
                    'min' => 1,
                ],
            ])
            ->add('event', EventAutocompleteChoiceType::class, [
                'required' => false,
                'multiple' => false,
                'resource' => 'app.event',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.events.form.choice.automatic.event.label',
                'help' => 'app.ui.block.events.form.choice.automatic.event.help',
            ])
            ->add('category', FormChoiceType::class, [
                'choices' => $this->choicesCategory,
                'required' => false,
                'disabled' => true,
                'label' => 'app.ui.block.events.form.choice.automatic.category.label',
                'help' => 'app.ui.block.events.form.choice.automatic.category.help',
            ])
            ->add('categories', EntityType::class, [
                'class' => EventCategory::class,
                'choice_label' => 'title',
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'app.ui.block.events.form.choice.automatic.categories.label',
                'help' => 'app.ui.block.events.form.choice.automatic.categories.help',
            ])
            ->add('originAgenda', FormChoiceType::class, [
                'choices' => $this->choicesOriginAgenda,
                'required' => false,
                'label' => 'app.ui.block.events.form.choice.automatic.originAgenda.label',
                'help' => 'app.ui.block.events.form.choice.automatic.originAgenda.help',
                'placeholder' => 'app.ui.block.events.form.choice.automatic.originAgenda.placeholder',
            ])
            ->add('audience', FormChoiceType::class, [
                'choices' => $this->choicesAudience,
                'required' => false,
                'label' => 'app.ui.block.events.form.choice.automatic.audience.label',
                'help' => 'app.ui.block.events.form.choice.automatic.audience.help',
                'placeholder' => 'app.ui.block.events.form.choice.automatic.audience.placeholder',
            ])
            ->add('keyword', FormChoiceType::class, [
                'choices' => $this->choicesKeyword,
                'required' => false,
                'label' => 'app.ui.block.events.form.choice.automatic.keyword.label',
                'help' => 'app.ui.block.events.form.choice.automatic.keyword.help',
                'placeholder' => 'app.ui.block.events.form.choice.automatic.keyword.placeholder',
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData'])
        ;

        $builder->get('categories')->addModelTransformer(new EventCategoriesTransformer($this->eventCategoryRepository));
        $builder->get('event')->addModelTransformer(new EventTransformer($this->eventRepository));
    }

    public function onPreSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = $event->getData();

        // Affichage de l'ancien champ "category" uniquement s'il est rempli et que "categories" est vide.
        // Cela permet d'avoir une rétrocompatibilité avec les anciennes données, avant que l'on puisse choisir plusieurs catégories.
        $newBlock = empty($data);
        $categoryIsNotSet = \is_array($data) && (!\array_key_exists('category', $data) || null === $data['category']);
        $categoriesIsSet = \is_array($data) && \array_key_exists('categories', $data);
        if ($newBlock || $categoryIsNotSet || $categoriesIsSet) {
            $form->remove('category');
        }
    }
}

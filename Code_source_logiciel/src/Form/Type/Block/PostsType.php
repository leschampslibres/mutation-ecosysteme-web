<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;

class PostsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.posts.form.title.label',
                'help' => 'app.ui.block.posts.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.posts.form.description.label',
                'help' => 'app.ui.block.posts.form.description.help',
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.posts.form.button.label',
                'help' => 'app.ui.block.posts.form.button.help',
            ])
            ->add('images', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.block.posts.form.images.label',
                'help' => 'app.ui.block.posts.form.images.help',
            ])
            ->add('articles', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.posts.form.articles.label',
                'help' => 'app.ui.block.posts.form.articles.help',
                'tags' => ['-default', 'post'],
            ])
        ;
    }
}

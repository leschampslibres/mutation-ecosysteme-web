<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Validator\Constraints as Assert;
class ResourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.resource.form.title.label',
                'help' => 'app.ui.block.resource.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.resource.form.description.label',
                'help' => 'app.ui.block.resource.form.description.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('link', ButtonType::class, [
                'required' => true,
                'label'=> 'app.ui.block.resource.form.link.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Element\DocumentType as MediaManagerDocumentType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DownloadType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_downloads';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.download.form.title.label',
                'help' => 'app.ui.block.download.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('file', MediaManagerDocumentType::class, [
                'required' => true,
                'label' => 'app.ui.block.download.form.file.label',
                'help' => 'app.ui.block.download.form.file.help',
                'attr' => ['data-image' => 'true'],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

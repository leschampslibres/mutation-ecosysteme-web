<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ExhibitTransformer;
use App\Form\Type\Exhibit\ExhibitAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ExhibitType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $exhibitRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_exhibit';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('exhibit', ExhibitAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.exhibit',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.exhibit.form.exhibit.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('exhibit')->addModelTransformer(new ExhibitTransformer($this->exhibitRepository));
    }
}

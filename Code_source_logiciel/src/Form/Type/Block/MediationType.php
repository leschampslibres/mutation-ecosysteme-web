<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\MediationTransformer;
use App\Form\Type\Mediation\MediationAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MediationType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $mediationRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_mediation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mediation', MediationAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.mediation',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.mediation.form.mediation.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('mediation')->addModelTransformer(new MediationTransformer($this->mediationRepository));
    }
}

<?php

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ManualEventsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_manual_events';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('generalForm', EventsType::class, [
                'label' => false,
            ])
            ->add('from', DateType::class, [
                'label' => 'luna.ui.grid.filter.date_range.from.label',
                'help' => 'luna.ui.grid.filter.date_range.from.help',
                'placeholder' => 'luna.ui.grid.filter.date_range.from.placeholder',
                'widget' => 'single_text',
                'input' => 'array',
                'empty_data' => null,
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('to', DateType::class, [
                'label' => 'luna.ui.grid.filter.date_range.to.label',
                'help' => 'luna.ui.grid.filter.date_range.to.help',
                'placeholder' => 'luna.ui.grid.filter.date_range.to.placeholder',
                'widget' => 'single_text',
                'input' => 'array',
                'empty_data' => null,
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('events', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.events.form.choice.manual.events.label',
                'help' => 'app.ui.block.events.form.choice.manual.events.help',
                'tags' => ['-default', 'event'],
            ]);
    }
}

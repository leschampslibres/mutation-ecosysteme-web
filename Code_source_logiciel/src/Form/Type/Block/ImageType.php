<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ImageType as BaseImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageType extends BaseImageType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_image';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('style', FormChoiceType::class, [
                'choices' => [
                    'app.ui.block.image.form.style.choices.medium' => 'medium',
                    'app.ui.block.image.form.style.choices.large' => 'large',
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => 'app.ui.block.image.form.style.label',
                'help' => 'app.ui.block.image.form.style.help',
            ])
        ;
        parent::buildForm($builder, $options);
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.video.form.title.label',
                'help' => 'app.ui.block.video.form.title.help',
            ])
            ->add('getMainImage', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'required' => true,
                'label' => 'app.ui.block.video.form.image.label',
                'help' => 'app.ui.block.video.form.image.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('player', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.video.form.player.label',
                'help' => 'app.ui.block.video.form.player.help',
                'disabled' => true,
                'choices' => [
                    'app.ui.block.video.form.player.choices.youtube' => 'youtube',
                ],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('href_extern', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.video.form.href_extern.label',
                'help' => 'app.ui.block.video.form.href_extern.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('href_intern', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.video.form.href_intern.label',
                'help' => 'app.ui.block.video.form.href_intern.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('transcript', WysiwygType::class, [
                'required' => false,
                'label' => 'app.ui.block.video.form.transcript.label',
                'help' => 'app.ui.block.video.form.transcript.help',
            ])
        ;

    }

}

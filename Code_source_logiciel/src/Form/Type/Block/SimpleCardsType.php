<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SimpleCardsType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_simple_cards';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.simple_cards.form.title.label',
                'help' => 'app.ui.block.simple_cards.form.title.help',
            ])
            ->add('simple_cards', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.simple_cards.form.simple_cards.label',
                'help' => 'app.ui.block.simple_cards.form.simple_cards.help',
                'tags' => ['-default', 'simple-card'],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use Luna\CoreBundle\Form\Type\Block\ButtonType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.media.form.type.label',
                'help' => 'app.ui.block.media.form.type.help',
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.media.form.title.label',
                'help' => 'app.ui.block.media.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => true,
                'label' => 'app.ui.block.media.form.button.label',
                'help' => 'app.ui.block.media.form.button.help',
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
            ->add('color', BlockColorType::class, [
                'required' => true,
                'label' => 'app.ui.block.media.form.color.label',
                'help' => 'app.ui.block.media.form.color.help',
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
        ;
    }

}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ArticleSubjectTransformer;
use App\Form\Type\Article\ArticleSubjectAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TopicType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $articleSubjectRepository)
    {
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('articleSubject', ArticleSubjectAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.article_subject',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.topic.form.article_subject.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('articleSubject')->addModelTransformer(new ArticleSubjectTransformer($this->articleSubjectRepository));
    }

}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PodcastType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.podcast.form.title.label',
                'help' => 'app.ui.block.podcast.form.title.help',
            ])
            ->add('getMainImage', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'required' => true,
                'label' => 'app.ui.block.podcast.form.image.label',
                'help' => 'app.ui.block.podcast.form.image.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('player', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.podcast.form.player.label',
                'help' => 'app.ui.block.podcast.form.player.help',
                'choices' => [
                    'app.ui.block.podcast.form.player.choices.soundcloud' => 'soundcloud',
                    'app.ui.block.podcast.form.player.choices.ausha' => 'ausha',
                ],
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('href_extern', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.podcast.form.href_extern.label',
                'help' => 'app.ui.block.podcast.form.href_extern.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('href_intern', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.podcast.form.href_intern.label',
                'help' => 'app.ui.block.podcast.form.href_intern.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('transcript', WysiwygType::class, [
                'required' => false,
                'label' => 'app.ui.block.podcast.form.transcript.label',
                'help' => 'app.ui.block.podcast.form.transcript.help',
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TopicsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.topics.form.title.label',
                'help' => 'app.ui.block.topics.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('color', BlockColorType::class, [
                'required' => true,
                'label' => 'app.ui.block.topics.form.color.label',
                'help' => 'app.ui.block.topics.form.color.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('articleSubjects', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.topics.form.article_subject.label',
                'help' => 'app.ui.block.topics.form.article_subject.help',
                'tags' => ['-default', 'topic'],
            ])
        ;
    }
}

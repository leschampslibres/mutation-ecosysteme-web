<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Validator\Constraints as Assert;

class ResourcesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.resources.form.title.label',
                'help' => 'app.ui.block.resources.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('description', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.resources.form.description.label',
                'help' => 'app.ui.block.resources.form.description.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('color', BlockColorType::class, [
                'required' => false,
                'label' => 'app.ui.block.resources.form.color.label',
                'help' => 'app.ui.block.resources.form.color.help',
            ])
            ->add('resources', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.resources.form.resources.label',
                'help' => 'app.ui.block.resources.form.resources.help',
                'tags' => ['-default', 'resource'],
            ])
        ;
    }
}

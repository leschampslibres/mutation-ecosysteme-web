<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ProfessionalTransformer;
use Luna\CoreBundle\Form\Type\Block\ButtonType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MediationsType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $professionalRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('professional', EntityType::class, [
                'class' => 'App\Entity\Professional\Professional',
                'required' => true,
                'multiple' => false,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.page.form.professional.label',
                'help' => 'app.ui.resource.page.form.professional.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('surtitle', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.mediations.form.surtitle.label',
                'help' => 'app.ui.block.mediations.form.surtitle.help',
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.mediations.form.title.label',
                'help' => 'app.ui.block.mediations.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.mediations.form.button.label',
                'help' => 'app.ui.block.mediations.form.button.help',
            ])
            ->add('formatting', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.mediations.form.formatting.label',
                'help' => 'app.ui.block.mediations.form.formatting.help',
                'choices' => [
                    'app.ui.block.mediations.form.formatting.choices.column' => 'column',
                    'app.ui.block.mediations.form.formatting.choices.line' => 'line',
                ],
            ])
            ->add('mediations', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.mediations.form.mediations.label',
                'help' => 'app.ui.block.mediations.form.mediations.help',
                'tags' => ['-default', 'mediation'],
            ])
        ;

        $builder->get('professional')->addModelTransformer(new ProfessionalTransformer($this->professionalRepository));
    }
}

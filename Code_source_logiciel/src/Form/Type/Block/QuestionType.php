<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_question';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('question', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.question.form.question.label',
                'help' => 'app.ui.block.question.form.question.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('content', WysiwygType::class, [
                'config_name' => 'minimal_inline',
                'required' => true,
                'label' => 'app.ui.block.question.form.content.label',
                'help' => 'app.ui.block.question.form.content.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;
    }
}

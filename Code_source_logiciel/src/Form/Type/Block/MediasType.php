<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;

class MediasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('overtitle', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.medias.form.overtitle.label',
                'help' => 'app.ui.block.medias.form.overtitle.help',
            ])
            ->add('title', FormTextType::class, [
                'required' => true,
                'label' => 'app.ui.block.medias.form.title.label',
                'help' => 'app.ui.block.medias.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.medias.form.button.label',
                'help' => 'app.ui.block.medias.form.button.help',
            ])
            ->add('medias', RichEditorType::class, [
                'required' => true,
                'label' => 'app.ui.block.medias.form.medias.label',
                'help' => 'app.ui.block.medias.form.medias.help',
                'tags' => ['-default', 'media'],
                'constraints' => [
                    new Assert\NotBlank([])
                ],
            ])
        ;
    }
}

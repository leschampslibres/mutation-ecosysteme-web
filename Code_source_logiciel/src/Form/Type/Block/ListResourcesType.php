<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;

class ListResourcesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.list_resources.form.title.label',
                'help' => 'app.ui.block.list_resources.form.title.help',
            ])
            ->add('images', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.block.list_resources.form.images.label',
                'help' => 'app.ui.block.list_resources.form.images.help',
            ])
            ->add('resources', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.list_resources.form.resources.label',
                'help' => 'app.ui.block.list_resources.form.resources.help',
                'tags' => ['-default', 'list_resource'],
            ])
        ;
    }
}

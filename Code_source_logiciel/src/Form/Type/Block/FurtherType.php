<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\Type\Element\BlockColorType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class FurtherType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_further';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => 'app.ui.block.further.form.title.label',
                'help' => 'app.ui.block.further.form.title.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('formatting', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.block.further.form.formatting.label',
                'help' => 'app.ui.block.further.form.formatting.help',
                'choices' => [
                    'app.ui.block.further.form.formatting.choices.oneColumn' => 'oneColumn',
                    'app.ui.block.further.form.formatting.choices.twoColumns' => 'twoColumns',
                ], 'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('color', BlockColorType::class, [
                'required' => false,
                'placeholder' => 'app.ui.block.further.form.color.placeholder',
            ])
            ->add('links', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.further.form.links.label',
                'help' => 'app.ui.block.further.form.links.help',
                'tags' => ['-default', 'button'],
            ])
        ;
    }
}

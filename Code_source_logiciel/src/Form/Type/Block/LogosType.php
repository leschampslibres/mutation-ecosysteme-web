<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\RichEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;

class LogosType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_logos';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.logos.form.title.label',
                'help' => 'app.ui.block.logos.form.title.help',
            ])
            ->add('big_logos', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.logos.form.big_logos.label',
                'help' => 'app.ui.block.logos.form.big_logos.help',
                'tags' => ['-default', 'logo'],
            ])
            ->add('small_logos', RichEditorType::class, [
                'required' => false,
                'label' => 'app.ui.block.logos.form.small_logos.label',
                'help' => 'app.ui.block.logos.form.small_logos.help',
                'tags' => ['-default', 'logo'],
            ])
        ;
    }
}

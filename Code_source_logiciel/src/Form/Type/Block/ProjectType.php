<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\ProjectTransformer;
use App\Form\Type\Project\ProjectAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProjectType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $projectRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_project';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('project', ProjectAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.project',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.project.form.project.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('project')->addModelTransformer(new ProjectTransformer($this->projectRepository));
    }
}

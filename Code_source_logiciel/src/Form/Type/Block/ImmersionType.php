<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use Luna\CoreBundle\Form\Type\Block\ButtonType;
use MonsieurBiz\SyliusRichEditorPlugin\Form\Type\WysiwygType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ImmersionType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'app_block_immersion';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('surtitle', FormTextType::class, [
                'required' => false,
                'label' => 'app.ui.block.immersion.form.surtitle.label',
                'help' => 'app.ui.block.immersion.form.surtitle.help',
            ])
            ->add('content', WysiwygType::class, [
                'config_name' => 'links_only',
                'required' => true,
                'label' => 'app.ui.block.immersion.form.content.label',
                'help' => 'app.ui.block.immersion.form.content.help',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
            ->add('button', ButtonType::class, [
                'required' => false,
                'label' => 'app.ui.block.immersion.form.button.label',
                'help' => 'app.ui.block.immersion.form.button.help',
            ])
        ;
    }
}

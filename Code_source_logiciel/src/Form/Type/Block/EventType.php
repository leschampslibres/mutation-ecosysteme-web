<?php

declare(strict_types=1);

namespace App\Form\Type\Block;

use App\Form\DataTransformer\EventTransformer;
use App\Form\Type\Event\EventAutocompleteChoiceType;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EventType extends AbstractType
{
    public function __construct(private readonly RepositoryInterface $eventRepository)
    {
    }

    public function getBlockPrefix(): string
    {
        return 'app_block_event';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('event', EventAutocompleteChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'resource' => 'app.event',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.block.event.form.event.label',
                'constraints' => [
                    new Assert\NotBlank([]),
                ],
            ])
        ;

        $builder->get('event')->addModelTransformer(new EventTransformer($this->eventRepository));
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\AlertMessage;

use App\Enum\AlertMessage\LevelEnum;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AlertMessageType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_alert_message';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('level', EnumType::class, [
                'class' => LevelEnum::class,
                'required' => true,
                'label' => 'app.ui.resource.alert_message.form.level.label',
                'help' => 'app.ui.resource.alert_message.form.level.help',
                'constraints' => [
                    new Assert\NotNull([]),
                ],
            ])
            ->add('startingAt', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'label' => 'app.ui.resource.alert_message.form.starting_at.label',
                'help' => 'app.ui.resource.alert_message.form.starting_at.help',
            ])
            ->add('endingAt', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'label' => 'app.ui.resource.alert_message.form.ending_at.label',
                'help' => 'app.ui.resource.alert_message.form.ending_at.help',
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => AlertMessageTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\AlertMessage;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AlertMessageTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_alert_message_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('content', CKEditorType::class, [
                'required' => false,
                'label' => 'app.ui.resource.alert_message.form.content.label',
                'help' => 'app.ui.resource.alert_message.form.content.help',
                'config_name' => 'minimal_inline',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

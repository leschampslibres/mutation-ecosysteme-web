<?php

declare(strict_types=1);

namespace App\Form\Type\Mediation;

use App\Entity\Mediation\MediationSubject;
use App\Enum\Mediation\AccessibilityEnum;
use App\Enum\Mediation\LevelEnum;
use App\Enum\Mediation\MonthEnum;
use App\Enum\Mediation\PricingEnum;
use App\Enum\Mediation\SupervisionEnum;
use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MediationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_mediation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('color', ResourceColorType::class)
            ->add('accessibility', ChoiceType::class, [
                'choices' => AccessibilityEnum::choices(),
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'app.ui.resource.mediation.form.accessibility.label',
                'help' => 'app.ui.resource.mediation.form.accessibility.help',
            ])
            ->add('level', ChoiceType::class, [
                'choices' => LevelEnum::choices(),
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'app.ui.resource.mediation.form.level.label',
                'help' => 'app.ui.resource.mediation.form.level.help',
            ])
            ->add('month', ChoiceType::class, [
                'choices' => MonthEnum::choices(),
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'label' => 'app.ui.resource.mediation.form.month.label',
                'help' => 'app.ui.resource.mediation.form.month.help',
            ])
            ->add('pricing', EnumType::class, [
                'class' => PricingEnum::class,
                'required' => true,
                'label' => 'app.ui.resource.mediation.form.pricing.label',
                'help' => 'app.ui.resource.mediation.form.pricing.help',
                'constraints' => [
                    new Assert\NotNull([]),
                ],
            ])
            ->add('supervision', EnumType::class, [
                'class' => SupervisionEnum::class,
                'required' => true,
                'label' => 'app.ui.resource.mediation.form.supervision.label',
                'help' => 'app.ui.resource.mediation.form.supervision.help',
                'constraints' => [
                    new Assert\NotNull([]),
                ],
            ])
            ->add('mediationProfessionals', CollectionType::class, [
                'block_prefix' => 'app_block_informations_table',
                'entry_type' => MediationProfessionalType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'delete_empty' => true,
                'required' => true,
                'label' => 'app.ui.resource.mediation.form.professionals.label',
                'help' => 'app.ui.resource.mediation.form.professionals.help',
                'button_add_label' => 'app.ui.element.professional.form.add',
            ])
            ->add('subjects', EntityType::class, [
                'required' => false,
                'class' => MediationSubject::class,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.mediation.form.subjects.label',
                'help' => 'app.ui.resource.mediation.form.subjects.help',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('relatedMediations', MediationAutocompleteChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'resource' => 'app.mediation',
                'choice_name' => 'title',
                'choice_value' => 'id',
                'label' => 'app.ui.resource.mediation.form.relatedMediations.label',
                'help' => 'app.ui.resource.mediation.form.relatedMediations.help',
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => MediationTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

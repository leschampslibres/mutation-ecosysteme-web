<?php

declare(strict_types=1);

namespace App\Form\Type\Mediation;

use App\Entity\Mediation\MediationProfessional;
use App\Form\Type\Element\ProfessionalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediationProfessionalType extends ProfessionalType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MediationProfessional::class,
            'remove_alpha_field' => false,
        ]);
    }
}

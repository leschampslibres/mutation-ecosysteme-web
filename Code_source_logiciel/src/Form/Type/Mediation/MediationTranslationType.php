<?php

declare(strict_types=1);

namespace App\Form\Type\Mediation;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use Luna\CoreBundle\Form\Type\Trait\BlockableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MediationTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_mediation_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('describable', DescribableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('blockable', BlockableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Autres champs
            ->add('mainImage', ImageType::class, [
                'has_title' => true,
                'has_alt' => true,
                'has_caption' => true,
                'required' => true,
                'label' => 'luna.ui.trait.main_imageable.main_image.label',
                'help' => 'luna.ui.trait.main_imageable.main_image.help',
            ])
            ->add('price', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.price.label',
                'help' => 'app.ui.resource.mediation.form.price.help',
            ])
            ->add('audience', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.audience.label',
                'help' => 'app.ui.resource.mediation.form.audience.help',
            ])
            ->add('period', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.period.label',
                'help' => 'app.ui.resource.mediation.form.period.help',
            ])
            ->add('schedule', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.schedule.label',
                'help' => 'app.ui.resource.mediation.form.schedule.help',
            ])
            ->add('duration', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.duration.label',
                'help' => 'app.ui.resource.mediation.form.duration.help',
            ])
            ->add('location', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.location.label',
                'help' => 'app.ui.resource.mediation.form.location.help',
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.mediation.form.phone.label',
                'help' => 'app.ui.resource.mediation.form.phone.help',
            ])
        ;
    }
}

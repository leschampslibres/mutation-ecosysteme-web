<?php

declare(strict_types=1);

namespace App\Form\Type\Mediation;

use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class MediationSubjectTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_mediation_subject_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

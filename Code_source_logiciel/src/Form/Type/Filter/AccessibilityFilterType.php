<?php

declare(strict_types=1);

namespace App\Form\Type\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessibilityFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $accessibilities = [
            'accessibilityMi',
            'accessibilityPi',
            'accessibilityIi',
            'accessibilityHi',
            'accessibilityVi',
        ];

        foreach ($accessibilities as $accessibility) {
            $accessibilityChoices['app.ui.grid.filter.accessibility.choices.' . $accessibility] = $accessibility;
        }

        $resolver
            ->setDefaults([
                'label' => 'app.ui.grid.filter.accessibility.label',
                'help' => 'app.ui.grid.filter.accessibility.help',
                'placeholder' => 'app.ui.grid.filter.accessibility.placeholder',
                'choices' => $accessibilityChoices,
                'required' => false,
            ])
            ->setAllowedTypes('choices', ['array'])
        ;
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'app_grid_filter_accessibility';
    }
}

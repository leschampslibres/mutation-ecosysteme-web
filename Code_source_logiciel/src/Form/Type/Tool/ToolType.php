<?php

declare(strict_types=1);

namespace App\Form\Type\Tool;

use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ToolType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_tool';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('color', ResourceColorType::class)
            ->add('toolProfessionals', CollectionType::class, [
                'block_prefix' => 'app_block_informations_table',
                'entry_type' => ToolProfessionalType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'delete_empty' => true,
                'required' => true,
                'label' => 'app.ui.resource.tool.form.professionals.label',
                'help' => 'app.ui.resource.tool.form.professionals.help',
                'button_add_label' => 'app.ui.element.professional.form.add',
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ToolTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

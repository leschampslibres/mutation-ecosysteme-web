<?php

declare(strict_types=1);

namespace App\Form\Type\Tool;

use App\Entity\Tool\ToolProfessional;
use App\Form\Type\Element\ProfessionalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ToolProfessionalType extends ProfessionalType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ToolProfessional::class,
            'remove_alpha_field' => false,
        ]);
    }
}

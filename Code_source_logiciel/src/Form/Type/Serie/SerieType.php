<?php

declare(strict_types=1);

namespace App\Form\Type\Serie;

use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SerieType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('publishedAt', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'label' => 'app.ui.resource.serie.form.publishedAt.label',
                'help' => 'app.ui.resource.serie.form.publishedAt.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('color', ResourceColorType::class)
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => SerieTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

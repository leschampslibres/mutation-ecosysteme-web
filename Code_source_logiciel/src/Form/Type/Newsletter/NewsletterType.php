<?php

declare(strict_types=1);

namespace App\Form\Type\Newsletter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'app.ui.resource.newsletter.form.email.label',
                'help' => 'app.ui.resource.newsletter.form.email.help',
                'constraints' => [
                    new NotBlank(),
                    new Email(null, 'luna.email.invalid'),
                ],
            ])
            ->add('gdpr', CheckboxType::class, [
                'required' => true,
                'label' => 'app.ui.resource.newsletter.form.gdpr.label',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'app.ui.resource.newsletter.form.valid.label',
            ])
            ->getForm()
        ;
    }
}

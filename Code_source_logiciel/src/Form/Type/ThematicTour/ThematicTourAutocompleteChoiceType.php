<?php

declare(strict_types=1);

namespace App\Form\Type\ThematicTour;

use Sylius\Bundle\ResourceBundle\Form\Type\ResourceAutocompleteChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class ThematicTourAutocompleteChoiceType extends AbstractType
{
    public function __construct(private readonly RouterInterface $router)
    {
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'resource' => 'app.thematic_tour',
            'choice_name' => 'title',
            'choice_value' => 'id',
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars['remote_criteria_type'] = 'contains';
        $view->vars['remote_criteria_name'] = 'title';
        $view->vars['remote_url'] = $this->router->generate('app_admin_ajax_thematic_tour_search_title');
        $view->vars['load_edit_url'] = $this->router->generate('app_admin_ajax_thematic_tour_search_id');
    }

    public function getBlockPrefix(): string
    {
        return 'app_thematic_tour_autocomplete_choice';
    }

    public function getParent(): string
    {
        return ResourceAutocompleteChoiceType::class;
    }
}

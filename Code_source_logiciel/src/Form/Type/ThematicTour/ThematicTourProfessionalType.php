<?php

declare(strict_types=1);

namespace App\Form\Type\ThematicTour;

use App\Entity\ThematicTour\ThematicTourProfessional;
use App\Form\Type\Element\ProfessionalType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ThematicTourProfessionalType extends ProfessionalType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ThematicTourProfessional::class,
            'remove_alpha_field' => false,
        ]);
    }
}

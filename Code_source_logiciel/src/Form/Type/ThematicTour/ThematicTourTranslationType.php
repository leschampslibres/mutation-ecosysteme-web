<?php

declare(strict_types=1);

namespace App\Form\Type\ThematicTour;

use Luna\CoreBundle\Form\Type\Trait\BlockableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ThematicTourTranslationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('describable', DescribableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('blockable', BlockableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Autres champs
            ->add('audience', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.thematic_tour.form.audience.label',
                'help' => 'app.ui.resource.thematic_tour.form.audience.help',

            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => 'app.ui.resource.thematic_tour.form.phone.label',
                'help' => 'app.ui.resource.thematic_tour.form.phone.help',
            ])
        ;
    }
}

<?php

declare(strict_types=1);

namespace App\Form\Type\ThematicTour;

use App\Enum\ThematicTour\SupervisionEnum;
use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ThematicTourType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('color', ResourceColorType::class)
            ->add('supervision', EnumType::class, [
                'class' => SupervisionEnum::class,
                'required' => true,
                'label' => 'app.ui.resource.thematic_tour.form.supervision.label',
                'help' => 'app.ui.resource.thematic_tour.form.supervision.help',
                'constraints' => [
                    new Assert\NotNull([]),
                ],
            ])
            ->add('thematicTourProfessionals', CollectionType::class, [
                'block_prefix' => 'app_block_informations_table',
                'entry_type' => ThematicTourProfessionalType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'delete_empty' => true,
                'required' => true,
                'label' => 'app.ui.resource.thematic_tour.form.professionals.label',
                'help' => 'app.ui.resource.thematic_tour.form.professionals.help',
                'button_add_label' => 'app.ui.element.professional.form.add',
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ThematicTourTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

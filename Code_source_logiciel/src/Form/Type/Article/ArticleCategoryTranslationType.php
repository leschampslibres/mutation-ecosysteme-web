<?php

declare(strict_types=1);

namespace App\Form\Type\Article;

use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleCategoryTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_article_category_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('describable', DescribableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

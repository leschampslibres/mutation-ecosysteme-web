<?php

declare(strict_types=1);

namespace App\Form\Type\Article;

use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleSubjectType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_article_subject';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ArticleSubjectTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

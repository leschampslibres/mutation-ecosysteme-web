<?php

declare(strict_types=1);

namespace App\Form\Type\Article;

use App\Entity\Article\ArticleCategory;
use App\Entity\Article\ArticleSubject;
use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_article';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('publishedAt', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'label' => 'app.ui.resource.article.form.publishedAt.label',
                'help' => 'app.ui.resource.article.form.publishedAt.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('color', ResourceColorType::class)
            ->add('category', EntityType::class, [
                'required' => true,
                'class' => ArticleCategory::class,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.article.form.category.label',
                'help' => 'app.ui.resource.article.form.category.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('subjects', EntityType::class, [
                'required' => false,
                'class' => ArticleSubject::class,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.article.form.subjects.label',
                'help' => 'app.ui.resource.article.form.subjects.help',
                'multiple' => true,
                'expanded' => true,
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ArticleTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

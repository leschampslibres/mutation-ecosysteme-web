<?php

declare(strict_types=1);

namespace App\Form\Type\Article;

use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleCategoryType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_article_category';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Autres champs
            ->add('icon', FormChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.resource.article_category.form.icon.label',
                'help' => 'app.ui.resource.article_category.form.icon.help',
                'choices' => [
                    'À voir' => 'eye',
                    'À lire' => 'book',
                    'À entendre' => 'sound',
                ],
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ArticleCategoryTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

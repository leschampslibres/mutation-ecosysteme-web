<?php

declare(strict_types=1);

namespace App\Form\Type\Professional;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProfessionalType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('audienceIcon', ChoiceType::class, [
                'required' => true,
                'label' => 'app.ui.resource.professional.form.audienceIcon.label',
                'help' => 'app.ui.resource.professional.form.audienceIcon.help',
                'choices' => [
                    'Chapeau académique' => 'studies',
                    'Parent et enfant' => 'duo-different-heights',
                    'Groupe' => 'group',
                    'Maison' => 'house',
                ],
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => ProfessionalTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

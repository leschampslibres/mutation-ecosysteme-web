<?php

declare(strict_types=1);

namespace App\Form\Type\Event;

use Luna\CoreBundle\Form\Type\Element\ImageType;
use Luna\CoreBundle\Form\Type\Trait\BlockableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\DescribableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\MainImageableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType as FormHiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EventTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_event_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'luna.ui.trait.titleable.title.label',
                'help' => 'luna.ui.trait.titleable.title.help',
            ])
            ->add('slug', TextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'luna.ui.trait.titleable.slug.label',
            ])
            ->add('description', TextareaType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'luna.ui.trait.describable.description.label',
            ])
            ->add('mainImage', ImageType::class, [
                'has_title' => false,
                'has_alt' => false,
                'has_caption' => false,
                'disabled' => true,
                'required' => false,
                'label' => 'luna.ui.trait.main_imageable.main_image.label',
                'help' => 'luna.ui.trait.main_imageable.main_image.help',
            ])
            ->add('blockable', BlockableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

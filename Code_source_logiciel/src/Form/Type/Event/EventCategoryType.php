<?php

declare(strict_types=1);

namespace App\Form\Type\Event;

use App\Enum\Event\SearchDisplayPositionEnum;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType as FormChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType as FormEnumType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EventCategoryType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_event_category';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Autres champs
            ->add('eventAutoEnabled', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.resource.event_category.form.eventAutoEnabled.label',
                'help' => 'app.ui.resource.event_category.form.eventAutoEnabled.help',
            ])
            ->add('eventAutoDeletable', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.resource.event_category.form.eventAutoDeletable.label',
                'help' => 'app.ui.resource.event_category.form.eventAutoDeletable.help',
            ])
            ->add('displayImages', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.resource.event_category.form.displayImages.label',
                'help' => 'app.ui.resource.event_category.form.displayImages.help',
            ])
            ->add('displayOccurrences', FormCheckboxType::class, [
                'required' => false,
                'label' => 'app.ui.resource.event_category.form.displayOccurrences.label',
                'help' => 'app.ui.resource.event_category.form.displayOccurrences.help',
            ])
            ->add('icon', FormChoiceType::class, [
                'required' => false,
                'label' => 'app.ui.resource.event_category.form.icon.label',
                'help' => 'app.ui.resource.event_category.form.icon.help',
                'choices'  => [
                    'Baguette magique (Animations)' => 'magic-wand',
                    'Crayon et règle (Ateliers)' => 'workshop',
                    'Bulles de dialogue (Ateliers 4C)' => 'talk',
                    'Note de musique (Concerts)' => 'music',
                    'Calendrier (Événements)' => 'calendar',
                    'Écran (Expositions)' => 'exposition',
                    'Pellicule vidéo (Projections)' => 'video',
                    'Groupe (Rencontres)' => 'group',
                    'Étoiles (Spectacles)' => 'stars',
                    'Drapeau (Visites commentées)' => 'flag',
                ],
            ])
            ->add('searchDisplayPosition', FormEnumType::class, [
                'class' => SearchDisplayPositionEnum::class,
                'required' => true,
                'label' => 'app.ui.resource.event_category.form.searchDisplayPosition.label',
                'help' => 'app.ui.resource.event_category.form.searchDisplayPosition.help',
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => EventCategoryTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

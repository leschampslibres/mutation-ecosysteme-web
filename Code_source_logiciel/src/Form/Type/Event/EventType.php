<?php

declare(strict_types=1);

namespace App\Form\Type\Event;

use App\Entity\Event\EventKeyword;
use App\Form\Type\Element\ResourceColorType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTraitType;
use Luna\CoreBundle\Form\Type\Trait\ToggleableTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as FormEntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType as FormCheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType as FormTextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EventType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_event';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('toggleable', ToggleableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs administrables
            ->add('color', ResourceColorType::class)
            ->add('childrenKeyword', FormEntityType::class, [
                'class' => EventKeyword::class,
                'required' => false,
                'multiple' => false,
                'expanded' => false,
                'choice_label' => 'title',
                'label' => 'app.ui.resource.event.form.childrenKeyword.label',
                'help' => 'app.ui.resource.event.form.childrenKeyword.help',
                'placeholder' => 'app.ui.resource.event.form.childrenKeyword.placeholder',
            ])
            // Champs OpenAgenda
            ->add('originAgenda', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.originAgenda.label',
                'help' => 'app.ui.resource.event.form.originAgenda.help',
            ])
            ->add('location', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.location.label',
                'help' => 'app.ui.resource.event.form.location.help',
            ])
            ->add('pricing', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.pricing.label',
                'help' => 'app.ui.resource.event.form.pricing.help',
            ])
            ->add('price', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.price.label',
                'help' => 'app.ui.resource.event.form.price.help',
            ])
            ->add('ticketing', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.ticketing.label',
                'help' => 'app.ui.resource.event.form.ticketing.help',
            ])
            ->add('audience', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.audience.label',
                'help' => 'app.ui.resource.event.form.audience.help',
            ])
            ->add('ageMin', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.ageMin.label',
                'help' => 'app.ui.resource.event.form.ageMin.help',
            ])
            ->add('ageMax', FormTextType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.ageMax.label',
                'help' => 'app.ui.resource.event.form.ageMax.help',
            ])
            ->add('accessibilityIi', FormCheckboxType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.accessibilityIi.label',
                'help' => 'app.ui.resource.event.form.accessibilityIi.help',
            ])
            ->add('accessibilityHi', FormCheckboxType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.accessibilityHi.label',
                'help' => 'app.ui.resource.event.form.accessibilityHi.help',
            ])
            ->add('accessibilityVi', FormCheckboxType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.accessibilityVi.label',
                'help' => 'app.ui.resource.event.form.accessibilityVi.help',
            ])
            ->add('accessibilityPi', FormCheckboxType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.accessibilityPi.label',
                'help' => 'app.ui.resource.event.form.accessibilityPi.help',
            ])
            ->add('accessibilityMi', FormCheckboxType::class, [
                'disabled' => true,
                'required' => false,
                'label' => 'app.ui.resource.event.form.accessibilityMi.label',
                'help' => 'app.ui.resource.event.form.accessibilityMi.help',
            ])
            // Traductions
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => EventTranslationType::class,
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

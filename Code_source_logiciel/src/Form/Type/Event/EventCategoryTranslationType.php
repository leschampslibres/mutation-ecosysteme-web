<?php

declare(strict_types=1);

namespace App\Form\Type\Event;

use Luna\CoreBundle\Form\Type\Trait\BlockableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\MainImageableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\SeoableTranslationTraitType;
use Luna\CoreBundle\Form\Type\Trait\TitleableTranslationTraitType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class EventCategoryTranslationType extends AbstractResourceType
{
    public function getBlockPrefix()
    {
        return 'app_event_category_translation';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Traits
            ->add('titleable', TitleableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'luna.ui.trait.describable.description.label',
            ])
            ->add('main_imageable', MainImageableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('blockable', BlockableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('seoable', SeoableTranslationTraitType::class, [
                'constraints' => [
                    new Assert\Valid([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            // Champs
            ->add('label', TextType::class, [
                'required' => true,
                'label' => 'app.ui.resource.event_category.form.label.label',
                'help' => 'app.ui.resource.event_category.form.label.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
            ->add('labelPlural', TextType::class, [
                'required' => true,
                'label' => 'app.ui.resource.event_category.form.labelPlural.label',
                'help' => 'app.ui.resource.event_category.form.labelPlural.help',
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => 'sylius',
                    ]),
                ],
            ])
        ;
    }
}

# Le logiciel en libre accès

Le code source du site web des Champs Libres est disponible ici de manière ouverte et sous licence libre [EUPL](https://eupl.eu/1.2/fr/). Vous pouvez télécharger l'archive et suivre les instructions d'installation, modifier le code et ajouter vos propres fonctionnaliter, analyser son fonctionnement en toute transparence ou encore recréer votre propre site web sur cette base.

- Le code a été nettoyé par les équipes de développement afin de maintenir un niveau de sécurité optimal.
- Les évolutions seront intégrées au fur & à mesure de leurs déploiements
- Les bases de données ne sont pas incluses

N'hésitez pas à [nous contacter](https://www.leschampslibres.fr/nous-contacter) pour nous faire part de vos usages & remix ou pour toute question.

## — ENVIRONNEMENTS
* LOCAL : https://127.0.0.1:3000/

## — PRÉ-REQUIS
Les consignes ci-dessous sont livrées à partir de MacOS. Il est nécessaire de les adapter pour un usage sous GNU-Linux ou Windows.

### Version de PHP
* PHP v.`^8.2`

### Extensions PHP
* gd
* exif
* fileinfo
* intl

_Vous pouvez vérifier les extensions PHP installées sur votre poste via la commande en ligne suivante :_

```console
php -m | grep -E 'gd|exif|fileinfo|intl'
```

### Configuration de PHP
* `memory_limit >= 1024M`

_Vous pouvez trouver l'emplacement de vos fichiers de configuration PHP via la commande en ligne suivante :_

```console
php --ini
```

### Version de Pyhton
* Python v.`3.11.2`

#### Installation

```console
brew install pyenv
pyenv install 3.11.2
```

### Base de données
* MySQL v.`5.7+, 8.0+`

### NPM Package Manager
La version minimale de _Node.js_ actuellement supportée est :

* Node.js v.`14.x`

Certaines tâches du `Makefile` s'appuie sur un fichier `nvm.sh` qui doit être présent dans le dossier `~/.nvm/`.

Si il n'existe pas déjà, lancez la commande suivante :

```console
ln -s /usr/local/opt/nvm/nvm.sh ~/.nvm/nvm.sh
```

## — INSTALLATION

### Code source

Clôner le dépôt de code.

### Configuration

Créer un fichier `.env.local` à partir du fichier `.env.local.example`.

Créer la base de données.

Initialiser le site :

```console
make init
```

Générer les assets :

```console
make dev
make assets
```

Initialiser Sylius :

```console
symfony console sylius:install:setup
```

Avec notamment les données suivantes :

```
Currency (press enter to use USD): EUR
Language (press enter to use en_US): fr_FR
```

Configurer les `TODO CONFIGURE ME`.

### Pour un déploiement avec Deployer

Configurer les informations de déploiement dans le fichier `deploy.php` selon les besoins.

## — UTILISATION

### Après récupération de commits

La commande ci-dessous réalise un composer install + npm ci + migrations + nettoyage du cache symfo. Comme son nom l'indique, vous pouvez la lancer après avoir pullé, elle s'occupera de tout pour mettre à jour le projet en fonction des modifications _pullées_.

```
make afterpull
```

### Serveur local

Il est nécessaire que `symfony-cli` soit installé sur votre poste :

```console
brew install symfony-cli
```

Naviguer localement dans la version sécurisée de l'application permet de détecter les problèmes de contenu mixte et d'exécuter les bibliothèques qui ne fonctionnent qu'en HTTPS.

Exécuter cette commande :

```console
make cert-install
```

Pour lancer le serveur Symfony :

```console
make start
```

Pour couper le serveur Symfony :

```console
make stop
```

L'accès au panneau d'administration Sylius se fait à l'adresse [/admin](https://127.0.0.1:3000/admin) avec les informations d'identification
que vous avez saisies lors de la procédure d'installation.

_Par défaut, l'application est accessible à l'adresse https://127.0.0.1:3000.
Si vous souhaitez associer un nom de domaine à la place, voici la marche à suivre :_
[Local Domain Name](https://symfony.com/doc/current/setup/symfony_server.html#local-domain-names)

### Récupération des données

Si vous souhaitez importer les données de la base de données et les fichiers (images et pdf) présents sur un serveur, il faut exécuter la commande suivante :

```console
dep db:to_local ALIAS_DU_SERVEUR
dep uploads:to_local ALIAS_DU_SERVEUR
```

## — RÉSOLUTION DE PROBLÈMES

En cas de problème, les erreurs et les exceptions sont enregistrées dans les logs de l'application :

```console
tail -f var/log/prod.log
tail -f var/log/dev.log
```

## — RÉCUPÉRATION DES ÉVÉNEMENTS

Le système fonctionne à l'aide de l'API Open Agenda ([documentation](https://developers.openagenda.com/)) dont les clés ont été anonymisées ici. Vous pouvez par ailleurs exploiter [la version antérieure des flux JSON](https://openagenda.com/agendas/253926/events.json) en modifiant le code source ([documentation](https://developers.openagenda.com/export-json-dun-agenda/)).

### En local

Ajouter au .env.local les informations de connexion à l'API d'OpenAgenda :

```
###> app/open-agenda-api-client ###
OPEN_AGENDA_API_AGENDA_ID=''
OPEN_AGENDA_API_KEY=''
###< app/open-agenda-api-client ###
```

Commande d'import des dernières mises à jours des événements :

```console
bin/console app:import-events
```

Commande d'import de tous les événements :

```console
bin/console app:import-events --all
```

### Sur les serveurs distants

Ajouter au fichier `.env.local` les informations de connexion à l'API d'OpenAgenda :

```
###> app/open-agenda-api-client ###
OPEN_AGENDA_API_AGENDA_ID=''
OPEN_AGENDA_API_KEY=''
###< app/open-agenda-api-client ###
```

Commande d'import des dernières mises à jours des événements :

```console
dep console:run --command="app:import-events" ALIAS_DU_SERVEUR
```


## — NETTOYAGE DES ÉVÉNEMENTS

### En local

```console
bin/console app:clear-events
```

### Sur les serveurs distants

```console
dep console:run --command="app:clear-events" ALIAS_DU_SERVEUR
```


## — NETTOYAGE DES IMAGES

### En local

```console
bin/console app:clear-medias
```

### Sur les serveurs distants

```console
dep console:run --command="app:clear-medias" ALIAS_DU_SERVEUR
```

## — GÉNÉRATION DU SITEMAP.XML

### En local

```console
bin/console sylius:sitemap:generate
```

---

## — LICENCES
Sauf mentions contraires, les documents et contenus sont diffusés sous licence [Creative Commons Attribution - Partage dans les Mêmes Conditions (CC BY-SA 4.0)](https://www.creativecommons.org/licenses/by-sa/4.0/deed.fr) et les codes sources sous licence [European Union Public Licence (EUPL)](https://eupl.eu/1.2/fr/) (Union Europénne)

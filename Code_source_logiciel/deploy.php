<?php

declare(strict_types=1);

namespace Deployer;

require __DIR__ . '/vendor/autoload.php';
use Deployer\Task\Context;
use Luna\CoreBundle\Deployer\Deployer;

require 'vendor/deployer/deployer/recipe/symfony.php';
Deployer::init();

// Configuration
set('repository', 'TODO CONFIGURE ME');

set('local_url', 'https://localhost:3000');

// if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

$sharedDirectories = ['public/build', 'public/media', 'var/log', 'var/maintenance', 'var/sitemap'];
set('shared_dirs', $sharedDirectories);

$writableDirectories = array_merge($sharedDirectories, ['var/build', 'var/cache']);
set('writable_dirs', $writableDirectories);

set('application', 'TODO CONFIGURE ME');
set('local_pwd', __DIR__);

// Hosts

task('make-reset', function (): void {
    $host = Context::get()->getHost();

    // APP_ENV=dev
    // if ($host->getAlias() == 'dev') {
    if ('staging' == $host->getAlias()) {
        // This action is only allowed in dev env.
        run('cd {{release_or_current_path}} && make reset');
    }
});

host('preprod')
    ->setHostname('TODO CONFIGURE ME')
    ->setRemoteUser('TODO CONFIGURE ME')
    //->setPort()
    ->set('keep_releases', 5)
    ->set('labels', ['stage' => 'prod'])
    ->set('branch', 'master')
    ->set('env', ['APP_ENV' => 'prod', 'COMPOSER_MEMORY_LIMIT' => '-1'])
    ->set('deploy_path', 'TODO CONFIGURE ME')
    ->set('writable_dirs', [])
    ->set('remote_url', 'TODO CONFIGURE ME')
;

host('prod')
    ->setHostname('TODO CONFIGURE ME')
    ->setRemoteUser('TODO CONFIGURE ME')
    //->setPort()
    ->set('keep_releases', 5)
    ->set('labels', ['stage' => 'prod'])
    ->set('branch', 'stable')
    ->set('env', ['APP_ENV' => 'prod', 'COMPOSER_MEMORY_LIMIT' => '-1'])
    ->set('deploy_path', 'TODO CONFIGURE ME')
    ->set('writable_dirs', [])
    ->set('remote_url', 'TODO CONFIGURE ME')
;

// before('deploy:publish', 'make-reset');

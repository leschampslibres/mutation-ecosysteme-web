const path = require('path');
const Encore = require('@symfony/webpack-encore');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const syliusBundles = path.resolve(__dirname, 'vendor/sylius/sylius/src/Sylius/Bundle/');
const uiBundleScripts = path.resolve(syliusBundles, 'UiBundle/Resources/private/js/');
const uiBundleResources = path.resolve(syliusBundles, 'UiBundle/Resources/private/');

// Shop config
Encore
  .setOutputPath('public/build/shop/')
  .setPublicPath('/build/shop')
  .addEntry('shop-entry', './vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Resources/private/entry.js')
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())
  .enableSassLoader();

  if (!Encore.isProduction()) {
    Encore.addPlugin(new BrowserSyncPlugin({
      host: "127.0.0.1",
      port: 3001,
      proxy: process.env.PROXY,
      files: [{
        match: ['public/build/**/*.(js|css|svg)', 'themes/**/*.twig'],
        fn: function (event, file) {
          if (event === 'change') {
            const bs = require('browser-sync').get('bs-webpack-plugin');
            bs.reload();
          }
        }
      }]
    }, {
      reload: false, // this allow webpack server to take care of instead browser sync
      name: 'bs-webpack-plugin',
    }));
  }

const shopConfig = Encore.getWebpackConfig();

shopConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
shopConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
shopConfig.resolve.alias['sylius/bundle'] = syliusBundles;
shopConfig.name = 'shop';

Encore.reset();

// Admin config
Encore
  .setOutputPath('public/build/admin/')
  .setPublicPath('/build/admin')
  .addEntry('admin-entry', './vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/private/entry.js')
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())
  .enableSassLoader();

const adminConfig = Encore.getWebpackConfig();

adminConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
adminConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
adminConfig.resolve.alias['sylius/bundle'] = syliusBundles;
adminConfig.externals = Object.assign({}, adminConfig.externals, { window: 'window', document: 'document' });
adminConfig.name = 'admin';

Encore.reset();

// App shop config
Encore
    .setOutputPath('public/build/app/shop')
    .setPublicPath('/build/app/shop')
    .addEntry('app-shop-entry', './assets/shop/entry.js')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader();

const appShopConfig = Encore.getWebpackConfig();

appShopConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
appShopConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
appShopConfig.resolve.alias['sylius/bundle'] = syliusBundles;
appShopConfig.externals = Object.assign({}, appShopConfig.externals, { window: 'window', document: 'document' });
appShopConfig.name = 'app.shop';

Encore.reset();

// App admin config
Encore
    .setOutputPath('public/build/app/admin')
    .setPublicPath('/build/app/admin')
    .addEntry('app-admin-entry', './assets/admin/entry.js')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader();

const appAdminConfig = Encore.getWebpackConfig();

appAdminConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
appAdminConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
appAdminConfig.resolve.alias['sylius/bundle'] = syliusBundles;
appAdminConfig.externals = Object.assign({}, appAdminConfig.externals, { window: 'window', document: 'document' });
appAdminConfig.name = 'app.admin';

Encore.reset();

// App editor config
Encore
    .setOutputPath('public/build/app/editor')
    .setPublicPath('/build/app/editor')
    .addEntry('app-editor-entry', './assets/editor/entry.js')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(false)
    .enableSassLoader();

const appEditorConfig = Encore.getWebpackConfig();

appEditorConfig.resolve.alias['sylius/ui'] = uiBundleScripts;
appEditorConfig.resolve.alias['sylius/ui-resources'] = uiBundleResources;
appEditorConfig.resolve.alias['sylius/bundle'] = syliusBundles;
appEditorConfig.externals = Object.assign({}, appEditorConfig.externals, { window: 'window', document: 'document' });
appEditorConfig.name = 'app.editor';

module.exports = [shopConfig, adminConfig, appShopConfig, appAdminConfig, appEditorConfig];

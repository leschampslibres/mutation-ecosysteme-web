const { series, parallel, watch } = require('gulp');

// Import task definitions
const Indexer                      = require('./Indexer');
const { clean }                    = require('./tasks/clean');
const { packFolders }              = require('./tasks/sprites');

// Import config
const config = require('./config');
// Init task's indexer
const ndxr = new Indexer();

// Various tasks
ndxr.setTask('clean', () => clean(config.clean.src));

ndxr.setTask('sprites', (cb) => packFolders(config.sprites, cb));

// Build task
const buildSeries = [
  ndxr.getTask('clean'),
  parallel(
    ndxr.getTask('sprites'),
  ),
];

const buildTask = series(buildSeries);

ndxr.setTask('build', buildTask);

// Export tasks
module.exports = Object.assign({
  default: buildTask,
}, ndxr.getTasks());

const notifier = require('node-notifier');

const notifyError = function(err) {
  notifier.notify({
    title: `Gulp error (${err.plugin})`,
    message: err.message,
    sound: true,
    type: 'error',
  });
};

module.exports = {
  notifyError: notifyError,
};

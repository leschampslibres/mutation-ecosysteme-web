const { readFileSync }      = require('fs');
const { src, dest, series } = require('gulp');
const plumber               = require('gulp-plumber');
const rev                   = require('gulp-rev');
const revRewrite            = require('gulp-rev-rewrite');

const { notifyError } = require('../notifier');

// Rev all assets
const all = (options, cb) => {
  return series(
    Object.assign(() => init({ src: options.init.src, dest: options.dest, manifest: options.manifestPath, revOptions: options.revOptions, manifestDest: options.manifestDest }), { displayName: "luna-gulp:rev:init" }),
    Object.assign((cb) => rewrite({ src: options.rewrite.src, dest: options.dest, manifest: options.manifestPath }, cb), { displayName: "luna-gulp:rev:rewrite" }),
    Object.assign(() => complete({ src: options.complete.src, dest: options.dest, manifest: options.manifestPath, revOptions: options.revOptions, manifestDest: options.manifestDest }), { displayName: "luna-gulp:rev:complete" }),
  )(cb);
};

// Init revision
const init = (options) => {
  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(rev())
    .pipe(dest(options.dest))
    .pipe(rev.manifest(options.manifest, options.revOptions))
    .pipe(dest(options.manifestDest));
};

// Rewrite assets path in dependant assets
const rewrite = (options, cb) => {
  let manifest;

  try {
    manifest = readFileSync(options.manifest);
  } catch {
    console.log(
      '[luna-gulp:rev-rewrite]: Skipping operation because Manifest file doesn\'t exist.\r\n' +
      '  Possibly because there is no potential (sub-)assets which can be referenced by others.'
    );
    return cb();
  }

  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(revRewrite({ manifest }))
    .pipe(dest(options.dest));

};

// Complete assets revision
const complete = (options) => {
  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(rev())
    .pipe(dest(options.dest))
    .pipe(rev.manifest(options.manifest, Object.assign(options.revOptions, { merge: true })))
    .pipe(dest(options.manifestDest));
};

module.exports = {
  all: all,
  init: init,
  rewrite: rewrite,
  complete: complete,
};

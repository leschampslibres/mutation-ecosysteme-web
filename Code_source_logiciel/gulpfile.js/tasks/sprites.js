const glob          = require('glob');
const { src, dest } = require('gulp');
const plumber       = require('gulp-plumber');
const rename        = require('gulp-rename');
const svgmin        = require('gulp-svgmin');
const svgstore      = require('gulp-svgstore');
const merge         = require('merge-stream');
const path          = require('path');

const { notifyError } = require('../notifier');

// Concatenate svg files in a single sprite
const pack = (options) => {
  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(svgmin(function (file) {
      const prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
          // Remove unused and minify + prefix used IDs (in case of clip-paths for example)
          name: 'cleanupIDs',
          params: {
            prefix: prefix + '-',
            minify: true
          },
        },
        // Keep comments as we mostly use them for specific reasons (like ACF SVG Icon Field on WordPress)
        {
          name: 'removeComments',
          active: false,
        }]
      };
    }))
    .pipe(rename({ prefix: options.symbolIDPrefix || null }))
    .pipe(svgstore({ inline: true }))
    .pipe(dest(options.dest));
};

// Concatenate multiple svg folders into corresponding sprites
const packFolders = (options, cb) => {
  const stream = merge(
    glob.sync(options.src).map((dirpath) => {
      const srcBase = options.src.split('**')[0];
      const relativePath = dirpath.replace(srcBase, '');
      const dirs = relativePath.split('/');

      // Last entry is empty cause source ending with "/" and so has no value
      dirs.pop();

      // Don't need sprite folder anymore
      dirs.pop();

      const wildcardPath = dirs.join('/');

      oneOptions = {
        src: dirpath + '/*.svg',
        symbolIDPrefix: options.symbolIDPrefix || null,
        dest: options.dest + '/' + wildcardPath,
      }

      return pack(oneOptions);
    })
  );

  return stream.isEmpty() ? cb() : stream;
};

module.exports = {
  pack: pack,
  packFolders: packFolders,
};

const { pack: packWithRollup } = require('./javascripts-rollup');
const { pack: packWithEsbuild } = require('./javascripts-esbuild');

module.exports = {
  pack: packWithRollup,
  packWithRollup,
  packWithEsbuild,
};

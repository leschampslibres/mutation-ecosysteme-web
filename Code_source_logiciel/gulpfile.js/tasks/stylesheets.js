/**
 * REQUIREMENTS
 */
const autoprefixer  = require('autoprefixer');
const cssnano       = require('cssnano');
const Fiber         = require('fibers'); // Speed up Dart Sass JS distribution
const { src, dest } = require('gulp');
const plumber       = require('gulp-plumber');
const postcss       = require('gulp-postcss');
const sass          = require('gulp-sass')(require('sass'));
const sourcemaps    = require('gulp-sourcemaps');

const browsersync     = require('./browsersync');
const { notifyError } = require('../notifier');

// Compile, improve and minify sass files
const pack = (options) => {
  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(sourcemaps.init())
    .pipe(sass(Object.assign(options.sass, { fiber: Fiber })).on('error', sass.logError))
    .pipe(postcss([
      autoprefixer(),
      cssnano()
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(options.dest))
    .pipe(browsersync.server.stream());
};

module.exports = {
  pack: pack,
};

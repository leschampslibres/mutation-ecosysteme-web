const del = require('del');

// Delete files
const clean = (files) => del(files);

module.exports = {
  clean: clean,
};

const browserSync = require('browser-sync');
const { series }  = require('gulp');

const serverName = "Gulp Server";

// Create server
const bs = browserSync.create(serverName);

// Get server
const server = browserSync.get(serverName);
let serverStatus = 'off'; // off => pending => running
let pendingCallbacks = [];

// Initialize server
const serve = (options, cb) => {
  switch (serverStatus) {
    case 'off':
      serverStatus = 'pending';
      server.init(options, () => {
        serverStatus = 'running';
        cb();
        pendingCallbacks.forEach((pcb, index) => {
          pcb();
          pendingCallbacks.splice(index, 1);
        });
      });
      break;

    case 'pending':
      pendingCallbacks.push(cb);
      break;

    default:
      cb();
      break;
  }
};

// Reload task
const reload = (cb) => {
  bs.reload();
  cb();
};

// Watch task
const watch = (options, watchTask, cb) => {
  return series(
    Object.assign((cb) => serve(options, cb), { displayName: 'luna-gulp:watch:serveFiles' }),
    Object.assign(watchTask, { displayName: 'luna-gulp:watch:watchFiles' }),
  )(cb);
}

module.exports = {
  server: server,
  serve: serve,
  reload: reload,
  watch: watch,
};

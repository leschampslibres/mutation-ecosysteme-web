const esbuild = require('esbuild');
const babel = require('@babel/core');
const fs = require('fs');

const { notifyError } = require('../notifier');

const babelPlugin = {
  name: 'babel',
  setup(build) {
    build.onLoad({ filter: /\.js$/ }, (args) => {
      let code = babel.transformFileSync(args.path, { sourceMaps: 'inline' });

      return {
        contents: code.code,
        loader: 'js',
      }
    })
  },
};

async function pack(options) {
  const plugins = [];
  const target = options.target || 'es2016';

  if (options.target === 'es5') {
    plugins.push(babelPlugin);
  }

  await Promise.all(options.files.map(async (file) => {
    await esbuild.build({
      entryPoints: [file.input],
      outfile: file.output,
      bundle: true,
      mainFields: ['module', 'main', 'browser'],
      format: file.outputFormat || 'iife',
      minify: true,
      sourcemap: true,
      target,
      color: true,
      plugins,
    }).catch((e) => {
      notifyError({
        plugin: 'esbuild',
        message: e.toString(),
      });
      throw e;
    });
  }));
};

module.exports = {
  pack: pack,
};

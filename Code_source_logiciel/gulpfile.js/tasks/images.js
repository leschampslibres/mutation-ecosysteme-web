const { src, dest, lastRun } = require('gulp');
const imagemin               = require('gulp-imagemin');
const plumber                = require('gulp-plumber');

const browsersync     = require('./browsersync');
const { notifyError } = require('../notifier');

// Optimize images
const optimize = (options) => {
  return src(options.src, { since: options.taskName ? lastRun(options.taskName) : null })
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(imagemin())
    .pipe(dest(options.dest))
    .pipe(browsersync.server.stream());
};

module.exports = {
  optimize: optimize,
};

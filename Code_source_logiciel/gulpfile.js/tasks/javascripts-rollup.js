const rollup = require('rollup');
const { babel, getBabelOutputPlugin } = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { terser } = require('rollup-plugin-terser');
const path = require('path');

const { notifyError } = require('../notifier');

// Error Handler plugin
function errorHandler() {
  return {
    name: 'errorHandler',
    buildEnd(err) {
      if (err) {
        notifyError(err);
      }
    }
  };
}

// Concatenate and minify javascripts
async function pack(options) {
  await Promise.all(options.files.map(async (file) => {
    const bundle = await rollup.rollup({
      input: file.input,
      plugins: [
        errorHandler(),
        nodeResolve({
          /**
           * Import preference order.
           * Need to match the param of esbuild version to avoid bundle issues.
           * We should not use this order but it's the one used from the first version. It's corresponding to the
           * plugin default. Changing the order would mean a breaking change and a major version update.
           */
          mainFields: ['module', 'main', 'browser'],
        }),
        commonjs(),
        babel({ babelHelpers: 'bundled' }),
        terser(),
      ],
    });

    await bundle.write({
      file: file.output,
      format: file.outputFormat || 'iife',
      sourcemap: true,
    });
  }));
};

module.exports = {
  pack: pack,
};

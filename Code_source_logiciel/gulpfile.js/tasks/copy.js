const { src, dest } = require('gulp');
const plumber = require('gulp-plumber');

const browsersync = require('./browsersync');
const { notifyError } = require('../notifier');

// Copy files
const copy = (options) => {
  return src(options.src)
    .pipe(plumber({ errorHandler: notifyError }))
    .pipe(dest(options.dest))
    .pipe(browsersync.server.stream());
};

module.exports = {
  copy: copy,
};

let config = {};

// Global paths
config.paths = {
  root: ".",
  sourceAssets: './sources/shop',
  publicAssets: './assets/shop',
  nodeModules: './node_modules',
};

// BrowserSync server
config.browsersync = {
  port: 3001,
  logSnippet: false,
  open: false,
  socket: {
    domain: 'localhost:3001',
  }
};

// Task settings : clean
config.clean = {
  src: config.paths.publicAssets + '/images/**/sprite*.svg',
};

// Task settings : fonts
config.fonts = {
  src: config.paths.sourceAssets + '/fonts/**/*.{woff,woff2}',
  dest: config.paths.publicAssets + '/fonts',
};

// Task settings : images
config.images = {
  src: [
    config.paths.sourceAssets + '/images/**/*.{jpg,jpeg,png,gif,svg,webp}',
    '!' + config.paths.sourceAssets + '/images/**/sprite*/*.svg'
  ],
  dest: config.paths.publicAssets + '/images',
};

// Task settings : javascripts
config.javascripts = {
  files: [
    {
      input: config.paths.sourceAssets + '/javascripts/front.js',
      output: config.paths.publicAssets + '/javascripts/front.js',
      // outputFormat: 'iife',
    }, {
      input: config.paths.sourceAssets + '/javascripts/vendors.js',
      output: config.paths.publicAssets + '/javascripts/vendors.js',
      // outputFormat: 'iife',
    },, {
      input: config.paths.sourceAssets + '/javascripts/prismjs.js',
      output: config.paths.publicAssets + '/javascripts/prismjs.js',
      // outputFormat: 'iife',
    },
  ],
  srcWatch: config.paths.sourceAssets + '/javascripts/**/*.js',
};

// Task settings : rev
config.rev = {
  init: {
    src: [
      config.paths.publicAssets + '/**/*',
      '!' + config.paths.publicAssets + '/**/*.{css,js,map}',
    ]
  },
  rewrite: {
    src: config.paths.publicAssets + '/**/*.{css,js}',
  },
  complete: {
    src: config.paths.publicAssets + '/**/*.{css,js}'
  },
  dest: config.paths.publicAssets,
  manifestDest: config.paths.publicAssets,
  manifestPath: config.paths.publicAssets + '/.manifest.json',
  revOptions: { base: config.paths.publicAssets },
};

// Task settings : SVG sprites
config.sprites = {
  src: config.paths.sourceAssets + '/images/**/sprite*/',
  srcWatch: config.paths.sourceAssets + '/images/**/sprite*/**/*.svg',
  dest: config.paths.publicAssets + '/images',
  symbolIDPrefix: 'sprite-',
};

// Task settings : stylesheets
config.stylesheets = {
  src: config.paths.sourceAssets + '/stylesheets/**/*.scss',
  dest: config.paths.publicAssets + '/stylesheets',
  sass: {
    includePaths: [config.paths.nodeModules],
    outputStyle: 'compressed',
  },
};

// Task settings : watch
config.watch = {
  src: [
    config.paths.root + '/views/**/*.twig',
  ],
};

// Export config
module.exports = config;

class Indexer {
  constructor(scope = '') {
    this.index = {};
    this.prefix = scope ? `${scope}:` : '';

    return this;
  }

  setTask(name, task) {
    const clonedTask = task.bind({});
    clonedTask.displayName = this.getCoumpoundName(name);

    this.index[this.getCoumpoundName(name)] = clonedTask;
  }

  getTask(name) {
    return this.index[this.getCoumpoundName(name)];
  }

  getCoumpoundName(name) {
    return `${this.prefix}${name}`;
  }

  getTasks() {
    return this.index;
  }
}

module.exports = Indexer;

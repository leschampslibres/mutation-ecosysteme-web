import './components/_burger';
import './components/_toggle';
import './components/_nav';
import './components/_alert';
import './components/_page';
import './components/_details';
import './components/_customselect';
import './components/_form';
import './components/_search';
import './components/_summary';
import './components/_period';
import './components/_button-mask';
import './components/_podcast';
import './components/_video';
import './components/_form-validation';

import browserUpdate from 'browser-update/update.npm';

// See : https://github.com/browser-update/browser-update/wiki/Details-on-configuration
browserUpdate({
  required: {
    e: -2,
    i: 12,
    f: -2,
    s: -2,
    c: -2,
  },
  insecure: true,
});

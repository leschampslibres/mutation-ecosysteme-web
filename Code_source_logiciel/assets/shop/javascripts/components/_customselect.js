import DropdownChoice from './Dropdown/DropdownChoice';

window.addEventListener('DOMContentLoaded', () => {
  const $dropdownsChoice = document.querySelectorAll('.js-customselect');
  const dropdownsChoice = [];

  if ($dropdownsChoice.length) {
    $dropdownsChoice.forEach(($dropdownChoice) => {
      let options = {
        control: '.js-customselect-control',
        target: '.js-customselect-popover',
        cancel: '.js-customselect-cancel',
        confirm: '.js-customselect-confirm',
        reset: '.js-customselect-reset',
        label: '.js-customselect-label',
        i18n: {
          label_empty: 'Aucun filtre',
          label_count: '%selected% sur %length% %descriptor%',
          singular: 'sélectionné',
          plural: 'sélectionnés',
        }
      };

      const dropdownChoice = new DropdownChoice($dropdownChoice, options);
      dropdownsChoice.push(dropdownChoice);
      dropdownChoice.mount();
    });
  }
});

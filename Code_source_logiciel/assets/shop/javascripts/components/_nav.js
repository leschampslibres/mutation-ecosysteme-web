/**
** JAVASCRIPTS
** Name: Nav
********************************************************************************/

if(document.getElementById('nav') && document.getElementById('nav-aside') && document.getElementById('nav-main')) {



  /**
  ** VARIABLES
  ********************************************************************************/

  var $nav = document.getElementById('nav');
  var $navAside = document.getElementById('nav-aside');
  var $navMain = document.getElementById('nav-main');
  var ww = window.innerWidth;



  /**
  ** FUNCTIONS
  ********************************************************************************/


  /**
  ** Name: setNavOrder
  ** Description: Set navigation order on mobile and desktop
  ****************************************/

  function setNavOrder(width) {
    var $navFirstChild = $nav.firstElementChild;

    if(width >= 960 && $nav.classList.contains('is-switched')) {
      $nav.appendChild($navFirstChild);
      $nav.classList.remove('is-switched');
    }

    if(width < 960 && !$nav.classList.contains('is-switched')) {
      $nav.appendChild($navFirstChild);
      $nav.classList.add('is-switched');
    }
  }


  /**
  ** Name: setCloseContent
  ** Description: Edit close button label & icon
  ****************************************/

  function setCloseContent(width) {
    const $closes = document.querySelectorAll('.js-toggle-close');

    if(width >= 960 && $nav.classList.contains('is-switched')) {
      $closes.forEach(function($close) {
        if($close.dataset.desktopLabel) {
          $close.querySelector('.js-toggle-label').innerHTML = $close.dataset.desktopLabel;
        }
        if($close.dataset.desktopIcon) {
          $close.querySelector('.js-toggle-icon use').setAttribute('href', $close.querySelector('.js-toggle-icon use').getAttribute('href').replace($close.dataset.mobileIcon, $close.dataset.desktopIcon));
        }
      });
    }

    if(width < 960 && !$nav.classList.contains('is-switched')) {
      $closes.forEach(function($close) {
        if($close.dataset.mobileLabel) {
          $close.querySelector('.js-toggle-label').innerHTML = $close.dataset.mobileLabel;
        }
        if($close.dataset.mobileIcon) {
          $close.querySelector('.js-toggle-icon use').setAttribute('href', $close.querySelector('.js-toggle-icon use').getAttribute('href').replace($close.dataset.desktopIcon, $close.dataset.mobileIcon));
        }
      });
    }
  }



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Resize
  ** Description: Manage nav on window resize
  ****************************************/

  window.addEventListener('resize', function(event) {
    ww = window.innerWidth;
    setCloseContent(ww);
    setNavOrder(ww);
  });


  /**
  ** Name: Load
  ** Description: Manage nav on window load
  ****************************************/

  window.addEventListener('load', function(event) {
    ww = window.innerWidth;
    setCloseContent(ww);
    setNavOrder(ww);
  });



}

/**
** JAVASCRIPTS
** Name: Toggle
********************************************************************************/

import { isVisible } from '../utilities/_functions';

if(document.querySelectorAll('.js-toggle').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  var $nav = document.getElementById('nav');



  /**
  ** FUNCTIONS
  ********************************************************************************/


  /**
  ** Name: Expand
  ** Description: Open a toggle menu
  ****************************************/

  function expand(id) {
    const $element = id ? document.getElementById(id) : false;

    if($element) {
      $element.removeAttribute('hidden');
      document.querySelectorAll('.js-toggle[aria-controls="' + id + '"]:not(.js-toggle-close)').forEach(function($_toggle) {
        $_toggle.setAttribute('aria-expanded', 'true');
      });

      const focusableElements = $element.querySelectorAll('button, a');
      if(focusableElements.length > 0) {
        let $firstFocusableElement = null;
        focusableElements.forEach(($focusableElement) => {
          if ($firstFocusableElement === null && isVisible($focusableElement)) {
            $firstFocusableElement = $focusableElement;
          }
        });

        if ($firstFocusableElement) {
          $firstFocusableElement.focus();
        }
      }
    }
  }


  /**
  ** Name: Shrink
  ** Description: Close a toggle menu
  ****************************************/

  function shrink(id, noreferrer = false) {
    const $element = id ? document.getElementById(id) : false;
    const $referrer = document.querySelector('.js-toggle[aria-controls="' + id + '"][data-toggle-referrer]');

    if($element) {
      $element.setAttribute('hidden', true);
      document.querySelectorAll('.js-toggle[aria-controls="' + id + '"]:not(.js-toggle-close)').forEach(function($_toggle) {
        $_toggle.setAttribute('aria-expanded', 'false');
      });
      if($referrer && !noreferrer) {
        $referrer.focus();
      }
    }
  }



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Click
  ** Description: Detect click on a toggle
  ****************************************/

  window.addEventListener('click', function(event) {
    var $toggle = event.target.closest('.js-toggle');


    /**
    ** Click on a toggle
    ****************************************/

    if($toggle) {
      const isExpanded = ($toggle.getAttribute('aria-expanded') == 'true' || $toggle.classList.contains('js-toggle-close'));
      const id = $toggle.getAttribute('aria-controls') ? $toggle.getAttribute('aria-controls') : false;
      const group = $toggle.dataset.toggleGroup;
      const $groupExpandedToggles = group ? document.querySelectorAll('.js-toggle[data-toggle-group="' + group + '"][aria-expanded="true"]:not([aria-controls="' + id + '"])') : false;

      if(id) {


        /**
        ** Close this toggle
        ****************************************/

        if(isExpanded) {

          // Set nav sublevel state
          if('toggleRich' in $toggle.dataset) {
            $nav.classList.remove('is-sublevel');
          }

          shrink(id);
        }


        /**
        ** Open this toggle
        ****************************************/

        else {

          // Close group's other toggles
          if(group && $groupExpandedToggles.length > 0) {
            var groupExpandedIds = [...new Set(Array.from($groupExpandedToggles, ($_element) => $_element.getAttribute('aria-controls')))];
            groupExpandedIds.forEach(function(_id) {
              shrink(_id, true);
            });
          }

          // Set nav sublevel state
          if('toggleRich' in $toggle.dataset) {
            $nav.classList.add('is-sublevel');
          }

          expand(id);

        }
      }
    }


    /**
    ** Click somewhere else
    ****************************************/

    else if(!event.target.closest('.js-toggle-target') && document.querySelectorAll('.js-toggle[aria-expanded="true"]').length > 0) {
      const $expandedToggles = document.querySelectorAll('.js-toggle[aria-expanded="true"]');
      var expandedIds = [...new Set(Array.from($expandedToggles, ($_element) => $_element.getAttribute('aria-controls')))];
      expandedIds.forEach(function(_id) {
        const $togglers = document.querySelectorAll(`.js-toggle[aria-controls="${ _id }"]`);
        $togglers.forEach(($_toggler) => {
          if ('toggleRich' in $_toggler.dataset) {
            $nav.classList.remove('is-sublevel');
          }
        });
        shrink(_id);
      });
    }


  });


  /**
  ** Name: Keydown
  ** Description: Detect escape press
  ****************************************/

  window.addEventListener('keydown', function(event) {

    // ESC KEY
    if(event.keyCode === 27) {
      if(document.querySelectorAll('.js-toggle[aria-expanded="true"]').length > 0) {
        document.querySelectorAll('.js-toggle[aria-expanded="true"]').forEach(function($toggle) {
          const id = $toggle.getAttribute('aria-controls') ? $toggle.getAttribute('aria-controls') : false;
          if(id) {
            if('toggleRich' in $toggle.dataset) {
              $nav.classList.remove('is-sublevel');
            }
            shrink(id);
          }
        });
      }
    }
  });

  /**
  ** Name: Focusout
  ** Description: Detect when focus leave the current toggler
  ****************************************/

  window.addEventListener('focusout', function(event) {
    if(document.querySelectorAll('.js-toggle[aria-expanded="true"]').length > 0) {
      document.querySelectorAll('.js-toggle[aria-expanded="true"]').forEach(function($toggle) {

        const id = $toggle.getAttribute('aria-controls') ? $toggle.getAttribute('aria-controls') : false;
        if(id) {
          const $target = document.getElementById(id);
          if (event.relatedTarget && !$target.contains(event.relatedTarget) && !$toggle.contains(event.relatedTarget)) {
            if('toggleRich' in $toggle.dataset) {
              $nav.classList.remove('is-sublevel');
            }
            shrink(id);
          }
        }
      });
    }
  });



}

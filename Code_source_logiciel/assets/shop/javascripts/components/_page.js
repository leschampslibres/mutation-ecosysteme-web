/**
** JAVASCRIPTS
** Name: Page
********************************************************************************/

import { throttle } from "../utilities/_functions";

document.addEventListener('DOMContentLoaded', () => {
  const $page = document.getElementById('page');

  if (!$page) {
    return;
  }

  const $head = document.getElementById('page-head');
  const $headerTop = document.getElementById('nav-aside');
  const $body = document.getElementById('page-body');
  const $alert = document.getElementById('page-alert');
  let scrollDelta = $headerTop.offsetHeight;
  let isSticky = false;
  let tmpWidth = window.innerWidth;

  const update = () => {
    scrollDelta = $headerTop.offsetHeight;

    if ($alert) {
      document.documentElement.style.setProperty('--alert-height', `${ $alert.offsetHeight }px`);
      scrollDelta += $alert.offsetHeight;
    }

    document.documentElement.style.setProperty('--header-height', `${ $head.offsetHeight }px`);
    document.documentElement.style.setProperty('--header-top-height', `${ $headerTop.offsetHeight }px`);
  };

  window.addEventListener('resize', (e) => {
    if (window.innerWidth !== tmpWidth) {
      update();
      tmpWidth = window.innerWidth;
    }
  });

  window.addEventListener('scroll', throttle((e) => {
    if (window.scrollY > scrollDelta + 20 && !isSticky) {
      $head.classList.add('is-sticky');
      isSticky = true;
    } else if (window.scrollY <= scrollDelta && isSticky) {
      $head.classList.remove('is-sticky');
      isSticky = false;
    }
  }, 100));

  if ($alert) {
    document.addEventListener('alert:removed', (e) => {
      if ($alert.children.length === 0) {
        document.documentElement.style.setProperty('--alert-height', `0px`);
        $alert = null;
        update();
      }
    });
  }

  update();

});

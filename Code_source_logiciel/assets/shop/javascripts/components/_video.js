/**
** JAVASCRIPTS
** Name: Video
********************************************************************************/

if(document.querySelectorAll('.js-video').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $videos = document.querySelectorAll('.js-video');



  /**
  ** FUNCTIONS
  ********************************************************************************/


  /**
  ** Name: Inject Video
  ** Description: Create video's iframe element
  ****************************************/

  function injectVideo($video, href, title = null) {
    var $container = $video.querySelector('.js-video-container');
    var $wrapper = $video.querySelector('.js-video-wrapper');

    if($container && $wrapper) {
      var $iframe = document.createElement("iframe");

      $iframe.setAttribute("src", href);
      $iframe.setAttribute("scrolling", "no");
      $iframe.setAttribute("allow", "autoplay");
      $iframe.setAttribute("tabindex", "-1");
      $iframe.classList.add("v-video__element");

      if (title) {
        $iframe.setAttribute("title", title);
      }

      $wrapper.append($iframe);
      $iframe.focus();
      $container.remove();
      $video.classList.add('is-active');
    }
  }



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Clicks
  ** Description: Manage terms acceptance & show element
  ****************************************/

  $videos.forEach(function($video) {
    const $checkbox = $video.querySelector('input[type="checkbox"]');
    const $button = $video.querySelector('.js-video-button');

    $checkbox.addEventListener("change", function(event) {
      if($checkbox.checked) {
        $checkbox.setAttribute('aria-checked', 'true');
        $button.removeAttribute('disabled');
        $button.focus();
      } else {
        $checkbox.removeAttribute('aria-checked');
        $button.setAttribute('disabled', '');
      }
    });

    $button.addEventListener("click", function(event) {
      if($button.dataset.href) {
        injectVideo($video, $button.dataset.href, $button.dataset.title);
      }
    });
  });



}

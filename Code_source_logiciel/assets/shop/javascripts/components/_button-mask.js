/**
** JAVASCRIPTS
** Name: Button mask
********************************************************************************/

document.addEventListener('DOMContentLoaded', function() {
  const btnContainer = document.querySelector('.js-event-container-btn');
  const enablerButton = document.querySelector('.js-enabler-btn');

  if(btnContainer && enablerButton) {

    const observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (!entry.isIntersecting) {
          enablerButton.classList.add('out');
        } else {
          enablerButton.classList.remove('none');
          enablerButton.classList.remove('out');
        }
      });
    });

    observer.observe(btnContainer);

    enablerButton.addEventListener('transitionend', (event) => {
      if (event.propertyName === 'opacity') {
        if (enablerButton.classList.contains('out')) {
          enablerButton.classList.add('none');
        }
      }
    });

  }
  
});

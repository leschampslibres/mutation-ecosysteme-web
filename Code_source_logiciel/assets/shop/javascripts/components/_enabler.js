/**
** JAVASCRIPTS
** Name: Enabler
********************************************************************************/

if(document.querySelector('.js-enabler')) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $enabler = document.querySelector('.js-enabler');
  let enablerPosY = 0;
  let lastKnownScrollPosition = 0;
  let ticking = false;



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name:
  ** Description:
  ****************************************/

  window.addEventListener("load", (event) => {
    enablerPosY = $enabler.getBoundingClientRect().top + window.scrollY;

    document.addEventListener("scroll", (event) => {
      lastKnownScrollPosition = window.scrollY;

      if (!ticking) {
        window.requestAnimationFrame(() => {
          if(!$enabler.classList.contains('is-fixed') && window.scrollY >= enablerPosY) {
            $enabler.classList.add('is-fixed');
          }
          else if($enabler.classList.contains('is-fixed') && window.scrollY < enablerPosY) {
            $enabler.classList.remove('is-fixed');
          }
          ticking = false;
        });

        ticking = true;
      }
    });
  });



}

/**
** JAVASCRIPTS
** Name: Details
********************************************************************************/

if(document.querySelectorAll('.js-details').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $details = document.querySelectorAll('.js-details');



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Click
  ** Description: Detect click on an alert button
  ****************************************/

  $details.forEach(function($detail) {
    const $summary = $detail.querySelector('.js-details-summary');
    const $label = $detail.querySelector('.js-details-label');

    $detail.addEventListener("toggle", function(event) {
      if($detail.open) {
        $summary.setAttribute('aria-expanded', 'true');
        $label.innerHTML = $label.dataset.expandedLabel;
      } else {
        $summary.setAttribute('aria-expanded', 'false');
        $label.innerHTML = $label.dataset.shrankLabel;
      }
    });
  });



}

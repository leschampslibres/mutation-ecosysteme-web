/**
** JAVASCRIPTS
** Name: Podcast
********************************************************************************/

if(document.querySelectorAll('.js-podcast').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $podcasts = document.querySelectorAll('.js-podcast');



  /**
  ** FUNCTIONS
  ********************************************************************************/


  /**
  ** Name: Inject Podcast
  ** Description: Create podcast's iframe element
  ****************************************/

  function injectPodcast($podcast, href, title = null) {
    var $container = $podcast.querySelector('.js-podcast-container');

    if($container) {
      var $iframe = document.createElement("iframe");

      $iframe.setAttribute("src", href);
      $iframe.setAttribute("scrolling", "yes");
      $iframe.setAttribute("allow", "autoplay");
      $iframe.setAttribute("tabindex", "-1");
      $iframe.classList.add("v-podcast__element");

      if (title) {
        $iframe.setAttribute("title", title);
      }

      $container.after($iframe);
      $iframe.focus();
      $container.remove();
    }
  }



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Clicks
  ** Description: Manage terms acceptance & show element
  ****************************************/

  $podcasts.forEach(function($podcast) {
    const $checkbox = $podcast.querySelector('input[type="checkbox"]');
    const $button = $podcast.querySelector('.js-podcast-button');

    $checkbox.addEventListener("change", function(event) {
      if($checkbox.checked) {
        $checkbox.setAttribute('aria-checked', 'true');
        $button.removeAttribute('disabled');
        $button.focus();
      } else {
        $checkbox.removeAttribute('aria-checked');
        $button.setAttribute('disabled', '');
      }
    });

    $button.addEventListener("click", function(event) {
      if($button.dataset.href) {
        injectPodcast($podcast, $button.dataset.href, $button.dataset.title);
      }
    });
  });



}

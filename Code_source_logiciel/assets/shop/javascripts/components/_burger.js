/**
** JAVASCRIPTS
** Name: Burger
********************************************************************************/

import { isVisible } from '../utilities/_functions';

if(document.querySelector('.js-burger')) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $burger = document.querySelector('.js-burger');
  const $icon = $burger.querySelector('.js-burger-icon');
  const $label = $burger.querySelector('.js-burger-label');
  const $pageHead = document.getElementById('page-head');
  const $pageBody = document.getElementById('page-body');
  const $pageFoot = document.getElementById('page-foot');
  const $body = document.getElementsByTagName('body')[0];
  let $nav = document.getElementById('nav');
  let focusableEls = $pageHead.querySelectorAll('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])');
  let visibleFocusableEls = Array.from(focusableEls).filter(($element) => isVisible($element));
  let firstFocusableEl = focusableEls[0];
  let lastFocusableEl = focusableEls[focusableEls.length - 1];
  const KEYCODE_TAB = 9;

  const keydownListener = (e) => {
    focusableEls = $pageHead.querySelectorAll('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])');
    visibleFocusableEls = Array.from(focusableEls).filter(($element) => isVisible($element));
    firstFocusableEl = visibleFocusableEls[0];
    lastFocusableEl = visibleFocusableEls[visibleFocusableEls.length - 1];

    const isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);

    if (!isTabPressed) {
      return;
    }

    if ( e.shiftKey ) /* shift + tab */ {
      if (document.activeElement === firstFocusableEl) {
        lastFocusableEl.focus();
        e.preventDefault();
      }
    } else /* tab */ {
      if (document.activeElement === lastFocusableEl) {
        firstFocusableEl.focus();
        e.preventDefault();
      }
    }
  };



  /**
  ** FUNCTIONS
  ********************************************************************************/


  /**
  ** Name: OpenBurger
  ****************************************/

  function openBurger($target) {
    $target.classList.add('is-visible');
    $burger.setAttribute('aria-expanded', 'true');
    $pageBody.setAttribute('hidden', true);
    $pageFoot.setAttribute('hidden', true);
    $body.classList.add('burger-active');

    document.addEventListener('keydown', keydownListener);
  }


  /**
  ** Name: CloseBurger
  ****************************************/

  function closeBurger($target) {
    document.removeEventListener('keydown', keydownListener);

    $target.classList.remove('is-visible');
    $burger.setAttribute('aria-expanded', 'false');
    $pageBody.removeAttribute('hidden');
    $pageFoot.removeAttribute('hidden');
    $nav.classList.remove('is-sublevel');
    $body.classList.remove('burger-active');

    $burger.focus();
  }



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Click
  ** Description: Detect click on a toggle
  ****************************************/

  $burger.addEventListener('click', function(event) {
    const $target = document.getElementById($burger.getAttribute('aria-controls'));

    if($target) {

      if($burger.getAttribute('aria-expanded') == 'true') {
        closeBurger($target);
      }
      else {
        openBurger($target);
      }
    }
  });


  /**
  ** Name: Keydown
  ** Description: Detect escape press
  ****************************************/

  window.addEventListener('keydown', function(event) {

    // ESC KEY
    if(event.keyCode === 27) {
      if(document.querySelectorAll('.js-toggle[aria-expanded="true"]').length == 0 && $burger.getAttribute('aria-expanded') == 'true') {
        const $target = document.getElementById($burger.getAttribute('aria-controls'));
        if($target) {
          closeBurger($target);
        }
      }
    }
  });



}

/**
** JAVASCRIPTS
** Name: Search
********************************************************************************/

if(document.querySelector('.js-search')) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $search = document.querySelector('.js-search');
  const $toggle = $search.querySelector('.js-search-toggle');
  const $refocus = $search.querySelector('.js-search-refocus');
  const $additional = document.getElementById('search-additional');



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Click
  ** Description: Detect click on an toggle button
  ****************************************/

  $toggle.addEventListener("click", function(event) {
    if($toggle.getAttribute('aria-expanded') == 'true') {
      $additional.setAttribute('hidden', true);
      $toggle.setAttribute('aria-expanded', 'false');
      $toggle.querySelector('.c-button__label').innerHTML = $toggle.dataset.shrankLabel;
      $refocus.focus();
    }
    else {
      $additional.removeAttribute('hidden');
      $toggle.setAttribute('aria-expanded', 'true');
      $toggle.querySelector('.c-button__label').innerHTML = $toggle.dataset.expandedLabel;
      $additional.querySelector('.v-field__control').focus();
    }
  });



}

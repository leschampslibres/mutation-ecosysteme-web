/**
** JAVASCRIPTS
** Name: Period
********************************************************************************/

if(document.querySelectorAll('.js-period').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $periods = document.querySelectorAll('.js-period');



  /**
  ** LISTENERS
  ********************************************************************************/


  /**
  ** Name: Click
  ** Description: Detect click on an alert button
  ****************************************/

  $periods.forEach(function($period) {
    const $radios = $period.querySelectorAll('.js-period-radio');
    const $custom = $period.querySelector('.js-period-custom');
    const $panel = $period.querySelector('.js-period-panel');

    $radios.forEach(function($radio) {
      $radio.addEventListener("click", function(event) {


        // Manage custom dates panel

        if($radio.classList.contains('js-period-custom')) {
          if($radio.dataset.wasChecked == "true") {
            $custom.setAttribute('aria-expanded', 'false');
            $panel.setAttribute('hidden', true);
            $period.classList.remove('is-opened');
          }
          else {
            $custom.setAttribute('aria-expanded', 'true');
            $panel.removeAttribute('hidden');
            $period.classList.add('is-opened');
          }
        }
        else {
          $custom.setAttribute('aria-expanded', 'false');
          $panel.setAttribute('hidden', true);
          $period.classList.remove('is-opened');
        }


        // Manage uncheck radio

        if($radio.dataset.wasChecked == "true") {
          $radio.checked = false;
          $radio.dataset.wasChecked = "false";
          $panel.querySelectorAll('input[type="date"]').forEach(function($input) {
            $input.value = "";
          });
        }
        else {
          document.querySelectorAll('.js-period-radio[data-was-checked="true"]').forEach(function($checkedRadio) {
            $checkedRadio.dataset.wasChecked = "false";
          });
          $radio.dataset.wasChecked = "true";
        }


      });
    });
  });



}

/**
** JAVASCRIPTS
** Name: Alert
********************************************************************************/

const $alerts = document.querySelectorAll('.js-alert');

if ($alerts.length > 0) {
  $alerts.forEach(($alert) => {
    const id = $alert.getAttribute('id');
    const isAlreadyClosed = localStorage.getItem(id);

    if (isAlreadyClosed) {
      $alert.remove();
    } else {
      const $buttons = $alert.querySelectorAll('.js-alert-button');

      $buttons.forEach(function($button) {
        $button.addEventListener('click', function(event) {
          localStorage.setItem(id, true);
          $alert.remove();
          document.dispatchEvent(new Event("alert:removed"));
        });
      });

      $alert.classList.add('is-mounted');
    }
  });

}

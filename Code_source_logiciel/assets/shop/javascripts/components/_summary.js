/**
** JAVASCRIPTS
** Name: Summary
********************************************************************************/

import { throttle } from '../utilities/_functions';

if(document.querySelectorAll('.js-summary').length > 0) {



  /**
  ** VARIABLES
  ********************************************************************************/

  const $summaries = document.querySelectorAll('.js-summary');
  let intersectingTargets = [];



  /**
  ** FUNCTIONS
  ********************************************************************************/

  function buildThresholdList() {
    let thresholds = [];
    let numSteps = 20;

    for (let i = 1.0; i <= numSteps; i++) {
      let ratio = i / numSteps;
      thresholds.push(ratio);
    }

    thresholds.push(0);
    return thresholds;
  }

  function handleIntersect (entries, observer) {
    entries.forEach((entry) => {
      intersectingTargets = intersectingTargets.filter((value) => entry.target !== value);
      if (entry.isIntersecting) {
        intersectingTargets.push(entry.target);
      }
    });
  }

  /**
  ** INITIALISATION
  ********************************************************************************/

  $summaries.forEach(function($summary) {
    const $anchors = $summary.querySelectorAll('.js-summary-anchor');
    const targets = [];
    let $currentAnchor = null;
    const observer = new IntersectionObserver(handleIntersect, {
      root: null,
      rootMargin: "0px",
      threshold: buildThresholdList(),
    });

    const update = () => {
      intersectingTargets.forEach(($intersectingTarget) => {
        const targetOffset = $intersectingTarget.getBoundingClientRect();
        const limit = 21;

        if (targetOffset.top <= limit && targetOffset.bottom > limit) {
          const $anchor = $summary.querySelector(`.js-summary-anchor[href="#${ $intersectingTarget.id }"]`);

          if ($currentAnchor && $anchor != $currentAnchor) {
            $currentAnchor.removeAttribute('aria-current');
          }

          $anchor.setAttribute('aria-current', "true");

          $currentAnchor = $anchor;
        }
      });
    }

    $anchors.forEach(($anchor) => {
      const $target = document.querySelector($anchor.getAttribute('href'));

      if ($target) {
        targets.push($target);
        observer.observe($target);
      }
    });

    document.addEventListener('scroll', throttle((e) => {
      update($summary, $currentAnchor);
    }, 60));

    update($summary, $currentAnchor);
  });



}

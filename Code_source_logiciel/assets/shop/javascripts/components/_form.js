/*
 * TODO
 * Dynamic select
 * Socorec Gulpfile Font
*/

const $dynamicSelects = document.querySelectorAll('[data-dynamic-select]');

$dynamicSelects.forEach(($dynamicSelect) => {
  const data = JSON.parse($dynamicSelect.dataset.dynamicSelect);
  const $select = document.getElementById(data.select);
  const $targetSelect = document.getElementById(data.target);
  const $targetSelectField = document.getElementById(data.targetField);
  const $targetSelectEmptyOption = $targetSelect.querySelector('option[value=""]');
  const $isTargetSelectRequired = $targetSelect.required;
  const $targetFallback = document.getElementById(data.fallback);
  const $targetFallbackField = document.getElementById(data.fallbackField);
  const $isTargetFallbackRequired = $targetFallback.required;

  const disable = () => {
    $targetSelect.setAttribute('disabled', "");
    $targetSelect.value = "";
    $targetSelectEmptyOption.textContent = data.placeholder;
  };

  const enable = () => {
    $targetSelect.removeAttribute('disabled');
    $targetSelectEmptyOption.textContent = data.placeholder_select;
  };

  const update = () => {

    if ($select.value) {
      enable();

      if ($select.value in data.values && data.values[$select.value].length > 0) {
        const targetedOptions = data.values[$select.value];

        if (!targetedOptions.includes($targetSelect.value)) {
          $targetSelect.value = "";
        }

        Array.from($targetSelect.options).forEach((option) => {
          if (!option.value || targetedOptions.includes(option.value)) {
            option.removeAttribute('hidden');
          } else {
            option.setAttribute('hidden', "");
          }
        });

        $targetFallbackField.setAttribute('hidden', "");
        $targetFallback.name = "disabled";
        $targetSelect.name = data.name;
        $targetSelectField.removeAttribute('hidden');

        if($isTargetSelectRequired) {
          $targetSelect.setAttribute('required', "");
        }

        if($isTargetFallbackRequired) {
          $targetFallback.removeAttribute('required');
        }
      } else {
        $targetSelectField.setAttribute('hidden', "");
        $targetSelect.value = "";
        $targetSelect.name = "disabled";
        $targetFallback.name = data.name;
        $targetFallbackField.removeAttribute('hidden');

        if($isTargetSelectRequired) {
          $targetSelect.removeAttribute('required');
        }

        if($isTargetFallbackRequired) {
          $targetFallback.setAttribute('required', "");
        }
      }

    } else {
      disable();
    };
  };

  $select.addEventListener('change', update);
  update();
});

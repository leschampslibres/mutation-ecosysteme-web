/**
 * Create a DocumentFragment from a string.
 * @param {String} htmlString - The string to convert.
 * @returns {DocumentFragment}
 * @see {@link https://davidwalsh.name/convert-html-stings-dom-nodes}
 */
export default function getFragmentFromString(htmlString) {
  return document.createRange().createContextualFragment(htmlString);
}

export function setAriaDescribedBy($element, value) {
  if (value) {
    $element.setAttribute('aria-describedby', value);
  } else {
    $element.removeAttribute('aria-describedby');
  }
}

export function addAriaDescribedBy($element, ids) {
  if (!Array.isArray(ids) && typeof ids !== 'string') {
    return;
  }

  const currentList = $element.getAttribute('aria-describedby')?.split(' ') ?? [];
  const newIds = [];

  if (Array.isArray(ids)) {
    ids.forEach((id) => {
      if (!currentList.includes(id)) {
        newIds.push(id);
      }
    });
  } else if (typeof ids === 'string') {
    newIds.push(ids);
  }

  setAriaDescribedBy($element, currentList.concat(newIds).join(' '));
}

export function removeAriaDescribedBy($element, ids = []) {
  if (!Array.isArray(ids) && typeof ids !== 'string') {
    return;
  }

  const list = $element.getAttribute('aria-describedby').split(' ');
  const idsToDelete = typeof ids === 'string' ? [ids] : ids;

  idsToDelete.forEach((id) => {
    if (list.includes(id)) {
      list.splice(list.indexOf(id), 1);
    }
  });

  setAriaDescribedBy($element, list.join(' '));
}

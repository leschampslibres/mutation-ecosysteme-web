// In this file you can import assets like images or stylesheets
import './stylesheets/front.scss';
import './javascripts/front';

// Images
import lcl from './images/lcl.jpg';
import lclLogo from './images/lcl-logo.svg';
import share from './images/share.png';
import browse from './images/browse.webp';
import sprite from './images/sprite.svg';

// Fonts
import oldschoolGroteskRegularWoff from './fonts/OldschoolGrotesk-Regular-subset.woff';
import oldschoolGroteskRegularWoff2 from './fonts/OldschoolGrotesk-Regular-subset.woff2';
import oldschoolGroteskMediumWoff from './fonts/OldschoolGrotesk-Medium-subset.woff';
import oldschoolGroteskMediumWoff2 from './fonts/OldschoolGrotesk-Medium-subset.woff2';

![Les Champs Libres - Photo d'Adrien Duquesnel](https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/img/LCL_Adrien-Duquesnel_2019.webp)
> Photo : Adrien Duquesnel CC BY-NC-SA

# Projet de mutation de l'écosystème web des Champs Libres

Bienvenue dans la documentation du projet. Cet espace est constitué dans une démarche ouverte de service public culturel en biens communs. Vous pouvez y accéder sans condition, consulter et réutiliser librement les ressources partagées, dans le respect des [licences libres utilisées](https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/tree/master?ref_type=heads#-licences) pour leur diffusion.

## — REPÈRES
- **Calendrier**
  - Conception : 2019
  - Marché public AMO : 10 juillet 2020
  - Lancement AMO : à partir de janvier 2021
  - Marché public de construction (2 publications) : octobre 2021
  - Lancement construction : à partir de janvier 2022 (en un seul lot projet global)
  - Nouvelle méthodologie projet : été 2023 (en 4 lots projet : 1/ Contenus, 2/ Evénements & Programme, 3/ Ressources & Le Mag, 4/ Pages Professionnels)
  - Livraison : 02 septembre 2024
- **Budget** global alloué : 360K€ TTC
- **Équipes**
  - Comité de Direction des Champs Libres
  - Direction de Projet : Guillaume Rouan (LCL)
  - Groupe Projet Les Champs Libres x Rennes Métropole : Estelle Soleillant (DigiCom/RM), Matthieu Delsaut (DigiCom/RM), Anita Duval (DSN/RM), Rozenn Chambard (LCL)
  - Assistance à Maîtrise d'Ouvrage : [Agence LEKSI](https://leksi.fr)
  - Maître d'Œuvre : [Agence Lunaweb](https://www.lunaweb.fr)
  - Contributions usagers & usagères (interne & externe)

## — RACONTER LE PROJET
Nous avons tenté de raconter en détails l'organisation du projet, son déroulement et ses aboutissements. Tous les articles sont à retrouver sur [comnum.rennes.fr](https://comnum.rennes.fr).

## — PLAN ASSURANCE QUALITÉ

Le **Plan Assurance Qualité** (PAQ), diffusé ici, représente la colonne vertébrale de cette documentation. Il centralise (pour la deuxième partie du projet) l'ensemble des décisions prises collectivement, en coopération entre les différentes équipes : zones de consensus, retours en arrières, hypothèses, indicateurs techniques, &hellip; Il permet de suivre l'état d'avancement complet du projet, étape par étape, et de disposer des liens de renvoi vers les documents correspondants.

→ Les versions du PAQ se situent au sein de `AMO_Etape-2` > `Phase-1` / `Phases-2-3` / `Lot-1`&hellip; dans un dossier dédié `PAQ`.

## — ORGANISATION DES DOSSIERS

L'organisation des fichiers suit le déroulement du projet, classée en phases puis en lots. Leur diffusion au sein de cette plateforme est sécurisée : chaque fichier est authentifié par une clé de chiffrement [Gnu Privacy Guard](https://www.gnupg.org) `GPG N° 1472D7D6276C4774`.

**Sommaire** :<br />
→ `AMO_Etape-1` = la première partie du projet assurée par l'AMO pour la conception et le Lancement<br />
→ `AMO_Etape-2` = la deuxième partie du projet, suivie par l'AMO et construite par le MO, versionnage des PAQ<br />
→ `Anciens_sites_web_LCL` = captures d'écrans des anciennes versions du site web depuis sa création en 2006 (source : [WayBack Machine](https://web.archive.org/web/*/https://www.leschampslibres.fr))<br />
→ `img` = quelques images utiles à la documentation sur cette plateforme<br />

**Contribution**
Les Champs Libres contribuent à [Software Heritage](https://www.softwareheritage.org/?lang=fr), archive universelle des logiciels pilotée par l'_Institut national de recherche en sciences et technologies du numérique_ (INRIA) en partenariat avec l'UNESCO. Ce projet vise à "collecter, préserver & partager" notamment le patrimoine logiciel culturel public.

[![Les Champs Libres contribuent à Software Heritage](https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/img/software-heritage-icon_365x80px.png)](https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.com/leschampslibres/mutation-ecosysteme-web)

## — LICENCES

Sauf mentions contraires, les documents et contenus sont diffusés sous licence [Creative Commons Attribution - Partage dans les Mêmes Conditions (CC BY-SA 4.0)](https://www.creativecommons.org/licenses/by-sa/4.0/deed.fr) et les codes sources sous licence [European Union Public Licence (EUPL)](https://eupl.eu/1.2/fr/) (Union Europénne)

<a href="https://www.creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank" alt="Creative Commons BY-SA 4.0 Licence"><img src="https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/img/license-cc-by-sa.svg" height="50px" /></a> <a href="https://eupl.eu/1.2/fr/" target="_blank" alt="European Union Public Licence"><img src="https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/img/license-eupl.svg" height="50px" /></a>

## — CONTACT

![Logo_LesChampsLibres](https://gitlab.com/leschampslibres/mutation-ecosysteme-web/-/raw/master/img/logo_LCL_200x200.webp)

> Les Champs Libres<br />
> 10, Cours des Alliés<br />
> 35000 Rennes<br />
> [leschampslibres.fr](https://leschampslibres.fr)<br />
> [Complétez le formulaire pour nous contacter](https://www.leschampslibres.fr/nous-contacter) (en précisant le Service `Communication`)
